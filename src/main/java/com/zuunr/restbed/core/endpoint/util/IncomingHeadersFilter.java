/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.endpoint.util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

/**
 * <p>The IncomingHeadersFilter is responsible for
 * filtering incoming headers, removing any unwanted
 * headers.</p>
 * 
 * <ul>
 * <li>authorization</li>
 * <li>api-base-url</li>
 * </ul>
 *
 * @author Mikael Ahlberg
 * @author Niklas Eldberger
 */
@Component
public class IncomingHeadersFilter {
    
    /**
     * <p>Filter headers from the provided {@link ServerHttpRequest} and
     * return the resulting headers in a {@link JsonObject}.</p>
     * 
     * @param httpRequest contains the headers to filter
     * @return a json object containing the filtered headers
     */
    public JsonObject filter(ServerHttpRequest httpRequest) {
        JsonObject headers = JsonObject.EMPTY;
        HttpHeaders httpHeaders = httpRequest.getHeaders();

        String authorizationHeader = httpHeaders.getFirst("authorization");
        if (authorizationHeader != null) {
            headers = headers.put("authorization", JsonArray.EMPTY.add(authorizationHeader));
        }

        String apiBaseUrl = httpHeaders.getFirst("api-base-url");
        if (apiBaseUrl != null) {
            headers = headers.put("api-base-url", JsonArray.EMPTY.add(apiBaseUrl));
        }

        return headers;
    }
}
