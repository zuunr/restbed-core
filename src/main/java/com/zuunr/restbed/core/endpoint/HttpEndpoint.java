/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.endpoint;

import java.util.Optional;

import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.api.ApiUriInfoBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.zuunr.restbed.core.endpoint.util.ExchangeCreator;
import com.zuunr.restbed.core.endpoint.util.ResponseEntityCreator;
import com.zuunr.restbed.core.service.ProtectedRestService;

import reactor.core.publisher.Mono;

/**
 * <p>The HttpEndpoint is responsible for exposing the
 * rest service through http endpoints.</p>
 *
 * @author Niklas Eldberger
 * @author Mikael Ahlberg
 */
@RestController
public class HttpEndpoint {

    private final Logger logger = LoggerFactory.getLogger(HttpEndpoint.class);

    private ProtectedRestService protectedRestService;
    private ExchangeCreator exchangeCreator;
    private ResponseEntityCreator responseEntityCreator;

    @Autowired
    public HttpEndpoint(
            ProtectedRestService protectedRestService,
            ExchangeCreator exchangeCreator,
            ResponseEntityCreator responseEntityCreator) {
        this.protectedRestService = protectedRestService;
        this.exchangeCreator = exchangeCreator;
        this.responseEntityCreator = responseEntityCreator;
    }
    
    @GetMapping(value = "v1/{apiName}/api/**", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getResource(@PathVariable String apiName, ServerHttpRequest httpRequest) {
        long start = System.currentTimeMillis();

        return Mono.just(httpRequest)
                .doOnNext(o -> logger.info("GET {}", removeLineBreakChars(o.getURI().getPath())))
                .map(exchangeCreator::createGetExchange)
                .flatMap(protectedRestService::service)
                .map(responseEntityCreator::createResponseEntity)
                .doOnNext(o -> {
                    logger.info("GET {}, statusCode: {}, responseBody: {}", removeLineBreakChars(httpRequest.getURI().getPath()), o.getStatusCodeValue(), o.getBody());

                    if (logger.isDebugEnabled()) {
                        logger.debug("Execution time for API: {}, method GET = {} ms", apiName, System.currentTimeMillis() - start);
                    }
                });
    }

    @PostMapping(value = "v1/{apiName}/api/**", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> postResource(@PathVariable String apiName, @RequestBody String body, ServerHttpRequest httpRequest) {
        long start = System.currentTimeMillis();

        ApiUriInfo apiUriInfo = ApiUriInfo.ofUri(httpRequest.getURI().toString());

        if (!apiUriInfo.isCollection()) {
            return Mono.just(ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build());
        }

        return Mono.just(httpRequest)
                .doOnNext(o -> logger.info("POST {}, requestBody: {}", removeLineBreakChars(o.getURI().getPath()), body))
                .map(o -> exchangeCreator.createPostExchange(o, body))
                .flatMap(protectedRestService::service)
                .map(responseEntityCreator::createResponseEntity)
                .doOnNext(o -> {
                    logger.info("POST {}, statusCode: {}, responseBody: {}", removeLineBreakChars(httpRequest.getURI().getPath()), o.getStatusCodeValue(), o.getBody());
                    
                    if (logger.isDebugEnabled()) {
                        logger.debug("Execution time for API: {}, method POST = {} ms", apiName, System.currentTimeMillis() - start);
                    }
                });
    }
    
    @PatchMapping(value = "v1/{apiName}/api/**", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> patchResource(@PathVariable String apiName, @RequestBody String body, ServerHttpRequest httpRequest) {
        long start = System.currentTimeMillis();
        
        return Mono.just(httpRequest)
                .doOnNext(o -> logger.info("PATCH {}, requestBody: {}", removeLineBreakChars(o.getURI().getPath()), body))
                .map(o -> exchangeCreator.createPatchExchange(o, body))
                .flatMap(protectedRestService::service)
                .map(responseEntityCreator::createResponseEntity)
                .doOnNext(o -> {
                    logger.info("PATCH {}, statusCode: {}, responseBody: {}", removeLineBreakChars(httpRequest.getURI().getPath()), o.getStatusCodeValue(), o.getBody());
                    
                    if (logger.isDebugEnabled()) {
                        logger.debug("Execution time for API: {}, method PATCH = {} ms", apiName, System.currentTimeMillis() - start);
                    }
                });
    }

    @DeleteMapping(value = "v1/{apiName}/api/**", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> deleteResource(@PathVariable String apiName, ServerHttpRequest httpRequest) {
        long start = System.currentTimeMillis();
        
        return Mono.just(httpRequest)
                .doOnNext(o -> logger.info("DELETE {}", removeLineBreakChars(o.getURI().getPath())))
                .map(exchangeCreator::createDeleteExchange)
                .flatMap(protectedRestService::service)
                .map(responseEntityCreator::createResponseEntity)
                .doOnNext(o -> {
                    logger.info("DELETE {}, statusCode: {}, responseBody: {}", removeLineBreakChars(httpRequest.getURI().getPath()), o.getStatusCodeValue(), o.getBody());

                    if (logger.isDebugEnabled()) {
                        logger.debug("Execution time for API: {}, method DELETE = {} ms", apiName, System.currentTimeMillis() - start);
                    }
                });
    }
    
    private String removeLineBreakChars(String userControlledLogEntry) {
        return Optional.ofNullable(userControlledLogEntry)
                .map(o -> o.replaceAll("[\n|\r|\t]", "_"))
                .orElse(null);
    }
}
