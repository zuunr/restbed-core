/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.endpoint.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectFactory;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;

/**
 * <p>The ExchangeCreator is responsible for creating
 * an internal {@link Exchange} representation of the
 * sent in request.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class ExchangeCreator {
    
    private JsonObjectFactory jsonObjectFactory;
    private IncomingHeadersFilter incomingHeadersFilter;
    
    @Autowired
    public ExchangeCreator(
            JsonObjectFactory jsonObjectFactory,
            IncomingHeadersFilter incomingHeadersFilter) {
        this.jsonObjectFactory = jsonObjectFactory;
        this.incomingHeadersFilter = incomingHeadersFilter;
    }
    
    /**
     * <p>Create an {@link Exchange} with an included GET {@link Request}
     * based on the provided {@link ServerHttpRequest}.</p>
     * 
     * @param httpRequest is used to create the exchange
     * @return an exchange containing a GET request
     */
    public Exchange<JsonObjectWrapper> createGetExchange(ServerHttpRequest httpRequest) {
        String url = httpRequest.getURI().toString();
        
        return Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.GET)
                        .url(url)
                        .headers(incomingHeadersFilter.filter(httpRequest)));
    }
    
    /**
     * <p>Create an {@link Exchange} with an included POST {@link Request}
     * based on the provided {@link ServerHttpRequest} and json body.</p>
     * 
     * @param httpRequest is used to create the exchange
     * @param body is the json request body
     * @return an exchange containing a POST request
     */
    public Exchange<JsonObjectWrapper> createPostExchange(ServerHttpRequest httpRequest, String body) {
        String url = httpRequest.getURI().toString();
        JsonObject bodyJsonObject = jsonObjectFactory.createJsonObject(body);

        return Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.POST)
                        .url(url)
                        .headers(incomingHeadersFilter.filter(httpRequest))
                        .body(JsonObjectWrapper.of(bodyJsonObject)));
    }
    
    /**
     * <p>Create an {@link Exchange} with an included PATCH {@link Request}
     * based on the provided {@link ServerHttpRequest} and json body.</p>
     * 
     * @param httpRequest is used to create the exchange
     * @param body is the json request body
     * @return an exchange containing a PATCH request
     */
    public Exchange<JsonObjectWrapper> createPatchExchange(ServerHttpRequest httpRequest, String body) {
        String url = httpRequest.getURI().toString();
        JsonObject bodyJsonObject = jsonObjectFactory.createJsonObject(body);

        return Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.PATCH)
                        .url(url)
                        .headers(incomingHeadersFilter.filter(httpRequest))
                        .body(JsonObjectWrapper.of(bodyJsonObject)));
    }
    
    /**
     * <p>Create an {@link Exchange} with an included DELETE {@link Request}
     * based on the provided {@link ServerHttpRequest}.</p>
     * 
     * @param httpRequest is used to create the exchange
     * @return an exchange containing a DELETE request
     */
    public Exchange<JsonObjectWrapper> createDeleteExchange(ServerHttpRequest httpRequest) {
        String url = httpRequest.getURI().toString();
        
        return Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.DELETE)
                        .url(url)
                        .headers(incomingHeadersFilter.filter(httpRequest)));
    }
}
