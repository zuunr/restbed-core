/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.endpoint.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.util.HeaderTranslator;

/**
 * <p>The ResponseEntityCreator is responsible for creating
 * the {@link ResponseEntity} based on the resulting
 * {@link Exchange}.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class ResponseEntityCreator {
    
    private HeaderTranslator headerTranslator;
    
    @Autowired
    public ResponseEntityCreator(HeaderTranslator headerTranslator) {
        this.headerTranslator = headerTranslator;
    }
    
    /**
     * <p>Create a {@link ResponseEntity} with a json response based on the provided
     * {@link Response} inside the {@link Exchange}.
     * 
     * @param exchange is the exchange to translate, containing the response
     * @return a response entity containing the json response string
     */
    public ResponseEntity<String> createResponseEntity(Exchange<JsonObjectWrapper> exchange) {
        Response<JsonObjectWrapper> response = exchange.getResponse();
        
        return ResponseEntity.status(response.getStatusCode())
                .headers(headerTranslator.translate(response.getHeaders()))
                .body(response.getBody() != null ? response.getBodyAsJsonObject().asJson() : null);
    }
}
