/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.restservice;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;

public class Validations {

    private JsonArray jsonArray;
    private JsonObject asJsonObject;

    public Validations(JsonValue jsonValue) {
        this.jsonArray = jsonValue.getValue(JsonArray.class);
    }

    public JsonObject perCollection() {
        if (asJsonObject == null) {
            JsonObjectBuilder builder = JsonObject.EMPTY.builder();
            for (ConfigValidation configValidation : jsonArray.asList(ConfigValidation.class)) {
                builder.put(configValidation.collectionName(), configValidation.asJsonObject());
            }
            asJsonObject = builder.build();
        }
        return asJsonObject;
    }

    public Validation getValidation(String collectionName) {
        return perCollection().get(collectionName, JsonValue.NULL).as(Validation.class);
    }
}
