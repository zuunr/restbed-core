/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.restservice;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.href.HrefUtil;
import com.zuunr.restbed.core.restservice.scheme.config.ConfigConfig;
import com.zuunr.restbed.core.restservice.scheme.meta.MetaConfig;
import com.zuunr.restbed.core.util.JsonObjectPathTranslator;

public class ServiceConfig {

    private static final RolePermissionsCreator rolePermissionsCreator = new RolePermissionsCreator();
    private static final HrefUtil hrefUtil = new HrefUtil();

    public static final JsonArray permissionsPath = JsonArray.ofDotSeparated("config.security.authorization.permissions");
    public static final JsonArray rolePermissionsPath = JsonArray.ofDotSeparated("config.security.authorization.rolePermissions");
    public static final JsonArray resourceModelsPath = JsonArray.ofDotSeparated("meta.resourceModels");
    public static final JsonArray queryModelsPath = JsonArray.ofDotSeparated("meta.queryModels");
    public static final JsonArray pathToApi = JsonArray.ofDotSeparated("config.api");
    public static final JsonArray pathToApiName = JsonArray.ofDotSeparated("config.api.apiName");
    public static final JsonArray pathToForms = JsonArray.of("forms");
    public static final JsonArray pathToConfigValidations = JsonArray.ofDotSeparated("config.validations");
    public static final JsonArray pathToDatabase = JsonArray.ofDotSeparated("config.database");
    public static final JsonArray pathToDecoration = JsonArray.ofDotSeparated("config.processing.decoration");

    public static final JsonObjectPathTranslator jsonObjectPathTranslator = new JsonObjectPathTranslator();

    private JsonObject serviceConfigAsJsonObject;

    public ServiceConfig(JsonObject serviceConfigAsJsonObject) {
        this.serviceConfigAsJsonObject = serviceConfigAsJsonObject;
    }

    public ServiceConfig translate(String newApiNameUrl) {
        return new ServiceConfig(translate(ApiUriInfo.ofUri(newApiNameUrl)));
    }

    private JsonObject translate(ApiUriInfo newApiNameUrl) {
        ApiUriInfo formatedNewApiUrl = formatUri(newApiNameUrl);
        ApiUriInfo formatedCurrentApiUrl = formatUri(getApiNameUrl());

        JsonObject forms = getForms();
        forms = jsonObjectPathTranslator.translatePaths("pattern", forms, formatedCurrentApiUrl.apiNameUri().replace(".", "[.]"), formatedNewApiUrl.apiNameUri().replace(".", "[.]"));

        JsonObject decoration = getDecorationConfig().asJsonObject;
        decoration = jsonObjectPathTranslator.translatePaths("url", decoration, formatedCurrentApiUrl.apiNameUri(), formatedNewApiUrl.apiNameUri());

        JsonObject updatedServiceConfig = asJsonObject().put(pathToForms, forms);
        updatedServiceConfig = updatedServiceConfig.put(pathToDecoration, decoration);
        updatedServiceConfig = updatedServiceConfig.put(pathToApi, makeApiConfig(formatedNewApiUrl));

        return updatedServiceConfig;
    }

    public ApiUriInfo getApiNameUrl() {
        String url = getConfigConfig().getApiConfig().getScheme() +
                "://" +
                getConfigConfig().getApiConfig().getHost() +
                ":" +
                getConfigConfig().getApiConfig().getPort() +
                "/v1/" +
                getApiName();

        return ApiUriInfo.ofUri(url);
    }

    private ApiUriInfo formatUri(ApiUriInfo uri) {
        HrefUtil.Replacement replacement = HrefUtil.Replacement.builder()
                .scheme(uri.protocol())
                .host(uri.host())
                .port("" + uri.port())
                .build();

        return ApiUriInfo.ofUri(hrefUtil.replace(uri.uri(), replacement));
    }

    public DecorationConfig getDecorationConfig() {
        return new DecorationConfig(serviceConfigAsJsonObject.get(pathToDecoration, JsonObject.EMPTY).getValue(JsonObject.class));
    }

    public TransitionConfig getTransitionConfig() {
        return new TransitionConfig(serviceConfigAsJsonObject.get(JsonArray.ofDotSeparated("config.processing.transition"), JsonObject.EMPTY).getValue(JsonObject.class));
    }

    public JsonArray getRolePermissions() {

        JsonArray permissions = serviceConfigAsJsonObject.get(permissionsPath, JsonValue.NULL).getValue(JsonArray.class);
        if (permissions != null) {
            return rolePermissionsCreator.createRolepermissions(permissions);
        } else {
            return serviceConfigAsJsonObject.get(rolePermissionsPath, JsonArray.EMPTY).getValue(JsonArray.class);
        }
    }

    public ConfigConfig getConfigConfig() {
        JsonObject config = serviceConfigAsJsonObject.get("config").getValue(JsonObject.class);
        return new ConfigConfig(config);
    }

    public MetaConfig getMeta() {
        return new MetaConfig(serviceConfigAsJsonObject.get("meta").getValue(JsonObject.class));
    }

    public JsonObject asJsonObject() {
        return serviceConfigAsJsonObject;
    }

    public String getApiName() {
        return serviceConfigAsJsonObject.get(pathToApiName).getValue(String.class);
    }

    public JsonObject getForms() {
        return serviceConfigAsJsonObject.get(pathToForms).getValue(JsonObject.class);
    }

    public JsonObject makeApiConfig(ApiUriInfo apiUriInfo) {
        return JsonObject.EMPTY
                .put("apiName", apiUriInfo.apiName())
                .put("scheme", apiUriInfo.protocol())
                .put("host", apiUriInfo.host())
                .put("port", apiUriInfo.port());
    }

    private JsonObject transitionProcessing() {
        return serviceConfigAsJsonObject.get(JsonArray.EMPTY
                .add("config").add("processing").add("transition"), JsonObject.EMPTY).getValue(JsonObject.class);
    }

    public boolean processingEnabled() {
        return transitionProcessing().get("enabled", false).getValue(Boolean.class);
    }

    public boolean useQueue() {
        return transitionProcessing().get("useQueue", true).getValue(Boolean.class);
    }

    public Validations validations() {
        return serviceConfigAsJsonObject.get(pathToConfigValidations, JsonArray.EMPTY).as(Validations.class);
    }

    public JsonArray getSecurityContextDecorators(String collectionName) {
        return getAuthorization(collectionName)
                .get("context", JsonObject.EMPTY)
                .get("contextModelDecorators", JsonArray.EMPTY).getJsonArray();
    }

    public JsonObject getAuthorization(String collectionName) {
        return serviceConfigAsJsonObject
                .get("config", JsonObject.EMPTY)
                .get("collections", JsonObject.EMPTY)
                .get(collectionName, JsonObject.EMPTY)
                .get("security", JsonObject.EMPTY)
                .get("authorization", JsonObject.EMPTY).getJsonObject();
    }
}
