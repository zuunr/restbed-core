/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.restservice;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

public class TransitionConfig {

    public final JsonObject asJsonObject;
    public final JsonArray processors;
    
    public TransitionConfig(JsonObject asJsonObject) {
        this.asJsonObject = asJsonObject;
        this.processors = asJsonObject.get("processors", JsonArray.EMPTY).getValue(JsonArray.class);
    }
}
