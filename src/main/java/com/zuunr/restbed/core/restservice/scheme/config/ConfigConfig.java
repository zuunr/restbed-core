/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.restservice.scheme.config;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.restbed.core.restservice.ServiceConfig;
import com.zuunr.restbed.core.restservice.scheme.config.api.ApiConfig;
import com.zuunr.restbed.core.restservice.scheme.config.processors.ProcessorsConfig;
import com.zuunr.restbed.core.restservice.scheme.config.validationschemas.ValidationSchemasConfig;

/**
 * Created by niklas on 2017-04-06.
 */
public class ConfigConfig {
    
    private JsonObject config;
    private ServiceConfig serviceConfig;

    public ConfigConfig(JsonObject config) {
        this.config = config;
    }

    public ConfigConfig(ServiceConfig serviceScheme) {
        this(serviceScheme.asJsonObject().get("config").getValue(JsonObject.class));
    }

    public ApiConfig getApiConfig() {
        return new ApiConfig(config.get("api", JsonValue.NULL).getValue(JsonObject.class));
    }

    public JsonObject asJsonObject() {
        return config;
    }

    public ProcessorsConfig getProcessorsConfig() {
        return new ProcessorsConfig(asJsonObject().get("processors", JsonArray.EMPTY).getValue(JsonArray.class));
    }

    public ValidationSchemasConfig getValidationSchemasConfig() {
        return new ValidationSchemasConfig(config.get("validationSchemas").getValue(JsonObject.class));
    }

    public ServiceConfig setValidationSchemasConfig(JsonObject validationSchemas) {
        return new ServiceConfig(serviceConfig.asJsonObject().put("validationSchemas", validationSchemas));
    }
}
