/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.restservice;

import com.zuunr.forms.Form;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

public class ResourceValidation {

    private JsonObject jsonObject;

    public ResourceValidation(JsonValue jsonValue) {
        this.jsonObject = jsonValue.getValue(JsonObject.class);
    }

    public Form matcherForm(){
        return jsonObject.get("matcherForm", JsonValue.NULL).as(Form.class);
    }

    public Form validationForm(){
        return jsonObject.get("validationForm", JsonValue.NULL).as(Form.class);
    }
}
