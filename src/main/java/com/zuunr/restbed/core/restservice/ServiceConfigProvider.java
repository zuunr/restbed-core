/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.restservice;

/**
 * <p>The ServiceConfigProvider interface is responsible for providing
 * the service config to the caller.</p>
 * 
 * <p>The implementation must take special care of always returning
 * the service config in stand alone mode, regardless of the content
 * of the provided api name. The api name can even be set to null, and
 * the service config should still be returned. The reason for this is
 * that in stand alone mode, there will only be one service config.</p>
 *
 * @author Niklas Eldberger
 * @author Mikael Ahlberg
 */
public interface ServiceConfigProvider {
    
    /**
     * <p>Retrieves the service config given the provided api name.</p>
     * 
     * <p>The api name may be null, so special care must be taken for
     * this scenario.</p>
     * 
     * @param apiName is the api name to fetch the service config for, or null
     * @return the service config, or null if no config exists
     */
    ServiceConfig getServiceConfig(String apiName);
}
