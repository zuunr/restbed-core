package com.zuunr.restbed.core.restservice;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

import java.util.Comparator;

/**
 * @author Niklas Eldberger
 */
public class RolePermissionsCreator {
    
    private static final String PERMISSION_ID_KEY = "permissionId";

    private static Comparator<JsonValue> comparator = (jsonValue1, jsonValue2) -> {
        JsonObject o1 = jsonValue1.getValue(JsonObject.class);
        JsonObject o2 = jsonValue2.getValue(JsonObject.class);

        JsonArray permissionIdArray1 = JsonArray.of((Object[]) o1.get(PERMISSION_ID_KEY).getString().split(":"));
        JsonArray permissionIdArray2 = JsonArray.of((Object[]) o2.get(PERMISSION_ID_KEY).getString().split(":"));

        int comparedRoles = permissionIdArray1.get(0).getString().compareTo(permissionIdArray2.get(0).getString());
        if (comparedRoles != 0) {
            return comparedRoles;
        }

        int comparedContextDepths = Integer.valueOf(permissionIdArray1.get(3, JsonValue.of("0")).getString()).compareTo(Integer.valueOf(permissionIdArray2.get(3, JsonValue.of("0")).getString()));
        if (comparedContextDepths != 0) {
            return comparedContextDepths;
        }

        int comparedResources = permissionIdArray1.get(1).getString().compareTo(permissionIdArray2.get(1).getString());
        if (comparedResources != 0) {
            return comparedResources;
        }

        int comparedPermissionType = permissionIdArray1.get(2).getString().compareTo(permissionIdArray2.get(2).getString());
        if (comparedPermissionType == 0) {
            throw new RuntimeException("permissionId: " + o1.get(PERMISSION_ID_KEY).getString() + " must be unique.");
        }
        return comparedPermissionType;
    };


    public JsonArray createRolepermissions(JsonArray permissionsInput) {
        JsonArray rolePermissions = JsonArray.EMPTY;

        // Sort the permissions based on role and matching context depth
        permissionsInput = permissionsInput.sort(comparator);

        String roleOfLastPermission = null;
        int matchingContextDepthOfLastPermission = -1;

        int rolePermissionIndex = -1;
        for (JsonObject jsonObject : permissionsInput.asList(JsonObject.class)) {

            JsonArray permissionIdArray = JsonArray.of((Object[]) jsonObject.get(PERMISSION_ID_KEY).getString().split(":"));
            JsonObject form = jsonObject.get("form", JsonValue.NULL).getValue(JsonObject.class);
            JsonValue roleName = permissionIdArray.get(0);
            JsonValue matchingContextDepth = JsonValue.of(Integer.valueOf(permissionIdArray.get(3, JsonValue.of("0")).getString()));

            if (isNewRole(roleName, roleOfLastPermission)) {
                rolePermissionIndex++;
            } else if (isNewContextDepth(matchingContextDepth, matchingContextDepthOfLastPermission)) {
                rolePermissionIndex++;
            }

            // Translate permission to permissionRole format
            // Creating the permission as in the rolePermission format
            roleOfLastPermission = permissionIdArray.get(0).getString();
            matchingContextDepthOfLastPermission = matchingContextDepth.asInteger();

            JsonObject permission;

            if (form == null) {
                permission = JsonObject.EMPTY.builder()
                        .put("collectionName", permissionIdArray.get(1))
                        .put("action", permissionIdArray.get(2))
                        .build();
            } else {
                permission = JsonObject.EMPTY.builder()
                        .put("collectionName", permissionIdArray.get(1))
                        .put("action", permissionIdArray.get(2))
                        .put("form", form)
                        .build();
            }

            // Put the permission into the rolePermssin structure
            rolePermissions = rolePermissions.put(JsonArray.of(rolePermissionIndex, "roleName"), roleName);
            rolePermissions = rolePermissions.put(JsonArray.of(rolePermissionIndex, "matchingContextDepth"), matchingContextDepth);
            rolePermissions = rolePermissions.put(JsonArray.of(rolePermissionIndex, "permissions", -1), permission);
        }

        return rolePermissions;
    }

    private boolean isNewContextDepth(JsonValue matchingContextDepth, int matchingContextDepthOfLastPermission) {
        return matchingContextDepth.asInteger() != matchingContextDepthOfLastPermission;
    }

    private boolean isNewRole(JsonValue roleName, String roleOfLastPermission) {
        return !roleName.getString().equals(roleOfLastPermission);
    }
}
