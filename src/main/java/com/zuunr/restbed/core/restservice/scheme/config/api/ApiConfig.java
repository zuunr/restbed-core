/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.restservice.scheme.config.api;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.restbed.core.href.HrefUtil;

/**
 * Created by niklas on 2017-03-28.
 */
public class ApiConfig {
    private static final HrefUtil hrefUtil = new HrefUtil();
    private JsonObject jsonObject;
    private String apiNameUrl;

    public ApiConfig(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public JsonObject asJsonObject() {
        return jsonObject;
    }

    public String getApiName() {
        return asJsonObject().get("apiName", JsonValue.NULL).getValue(String.class);
    }

    public String getScheme() {

        return asJsonObject().get("scheme").getValue(String.class);
    }

    public String getHost() {
        return asJsonObject().get("host").getValue(String.class);

    }

    public String getApiNameUrl () {

        if (apiNameUrl == null) {
            // By using HrefUtil this one is canonical
            apiNameUrl = hrefUtil.replace(getScheme() + "://" + getHost() + ":" +getPort() + "/v1/" +getApiName(), HrefUtil.Replacement.builder().host(getHost()).scheme(getScheme()).port(""+getPort()).build());
        }
        return apiNameUrl ;
    }

    public Integer getPort() {
        return asJsonObject().get("port").asInteger();
    }
}
