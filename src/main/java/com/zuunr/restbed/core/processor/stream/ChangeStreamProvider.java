/*
 * Copyright 2021 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.processor.stream;

import com.zuunr.restbed.core.processor.Event;

import reactor.core.publisher.Flux;

/**
 * <p>The ChangeStreamProvider is responsible for opening up the
 * desired change stream.</p>
 *
 * @author Mikael Ahlberg
 */
public interface ChangeStreamProvider {
    
    /**
     * <p>Opens a change stream given the provided collection name, resume token
     * and default resume date.</p>
     * 
     * <p>If resumeToken.getResumeToken() is null, the resumeToken.getDefaultResumeDate()
     * will be used.</p>
     * 
     * @param collectionName is the collection to open the stream against
     * @param resumeToken is the resume token to use
     * @param fullDocument true if the full document should be fetched on update
     * @return a flux event stream
     */
    Flux<Event> openChangeStream(String collectionName, ResumeToken resumeToken, boolean fullDocument);    
}
