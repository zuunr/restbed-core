/*
 * Copyright 2021 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.processor;

import java.util.Collections;
import java.util.List;

import com.zuunr.json.JsonObject;

/**
 * <p>The Event contains data regarding the operation
 * and the resource.</p>
 *
 * @author Mikael Ahlberg
 */
public class Event {
    
    public enum OperationType { INSERT, UPDATE, REPLACE, DELETE, OTHER }
    
    private final String resumeToken;
    private final OperationType type;
    private final String resourceId;
    private final JsonObject fullDocument;
    private final JsonObject updatedFields;
    private final List<String> removedFields;

    private Event(Builder builder) {
        this.resumeToken = builder.resumeToken;
        this.type = builder.type;
        this.resourceId = builder.resourceId;
        this.fullDocument = builder.fullDocument;
        this.updatedFields = builder.updatedFields;
        this.removedFields = builder.removedFields;
    }

    public String getResumeToken() {
        return resumeToken;
    }

    public OperationType getType() {
        return type;
    }

    public String getResourceId() {
        return resourceId;
    }

    public JsonObject getFullDocument() {
        return fullDocument;
    }

    public JsonObject getUpdatedFields() {
        return updatedFields;
    }

    public List<String> getRemovedFields() {
        return removedFields;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String resumeToken;
        private OperationType type;
        private String resourceId;
        private JsonObject fullDocument;
        private JsonObject updatedFields;
        private List<String> removedFields = Collections.emptyList();

        private Builder() {
        }

        public Builder resumeToken(String resumeToken) {
            this.resumeToken = resumeToken;
            return this;
        }

        public Builder type(OperationType type) {
            this.type = type;
            return this;
        }

        public Builder resourceId(String resourceId) {
            this.resourceId = resourceId;
            return this;
        }

        public Builder fullDocument(JsonObject fullDocument) {
            this.fullDocument = fullDocument;
            return this;
        }

        public Builder updatedFields(JsonObject updatedFields) {
            this.updatedFields = updatedFields;
            return this;
        }

        public Builder removedFields(List<String> removedFields) {
            this.removedFields = removedFields;
            return this;
        }

        public Event build() {
            return new Event(this);
        }
    }
}
