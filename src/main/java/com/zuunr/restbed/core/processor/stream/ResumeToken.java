/*
 * Copyright 2021 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.processor.stream;

import java.time.OffsetDateTime;
import java.util.Optional;

/**
 * <p>The ResumeToken holds the resume token, or
 * the default resume date, to use when starting a
 * change stream.</p>
 *
 * @author Mikael Ahlberg
 */
public class ResumeToken {
    
    private final Optional<String> resumeToken;
    private final OffsetDateTime defaultResumeDate;

    public Optional<String> getResumeToken() {
        return resumeToken;
    }

    public OffsetDateTime getDefaultResumeDate() {
        return defaultResumeDate;
    }

    private ResumeToken(Builder builder) {
        this.resumeToken = builder.resumeToken;
        this.defaultResumeDate = builder.defaultResumeDate;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private Optional<String> resumeToken = Optional.empty();
        private OffsetDateTime defaultResumeDate;

        private Builder() {
        }

        public Builder resumeToken(Optional<String> resumeToken) {
            this.resumeToken = resumeToken;
            return this;
        }

        public Builder defaultResumeDate(OffsetDateTime defaultResumeDate) {
            this.defaultResumeDate = defaultResumeDate;
            return this;
        }

        public ResumeToken build() {
            return new ResumeToken(this);
        }
    }
    
    @Override
    public String toString() {
        return super.toString();
    }
}
