/*
 * Copyright 2021 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.processor.stream;

import java.time.OffsetDateTime;

import com.zuunr.restbed.core.processor.Event;

import reactor.core.publisher.Mono;

/**
 * <p>The SimpleResumeTokenHandler is an interface
 * responsible for handling basic save/retrieve logic
 * around a resume token.</p>
 *
 * @author Mikael Ahlberg
 */
public interface SimpleResumeTokenHandler {
    
    /**
     * <p>Retrieves a resume token given a resume token name,
     * which can be a collection or a specific function for a
     * collection.</p>
     * 
     * <p>The resume token must be unique or else different functions
     * may share the resume token, resulting in unexpected behavior.</p>
     * 
     * @param resumeTokenName is the name of the resume token
     * @param defaultResumeDate is used if no resume token could be retrieved from source
     * @return a resume token
     */
    Mono<ResumeToken> getResumeToken(String resumeTokenName, OffsetDateTime defaultResumeDate);
    
    /**
     * <p>Saves a resume token given the provided resume token name
     * and the event (containing the resume token for that event).</p>
     * 
     * <p>The resume token must be unique or else different functions
     * may share the resume token, resulting in unexpected behaviour.</p>
     * 
     * @param event is the processed event
     * @param resumeTokenName is the name of the resume token
     * @return void
     */
    Mono<Void> saveResumeToken(Event event, String resumeTokenName);

}
