/*
 * Copyright 2021 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.processor;

import java.time.Duration;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.zuunr.restbed.core.processor.stream.ChangeStreamProvider;
import com.zuunr.restbed.core.processor.stream.ResumeToken;

import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

/**
 * <p>The EventProcessor is an abstract class used to set up an asynchronous
 * processor which will perform a task based on receiving an event.</p>
 * 
 * <p>A task can be any kind of operation and is implemented by the user of
 * this class.</p>
 * 
 * <p>The class opens a change stream and listens on configured events. In case
 * of failure, the stream can be configured for restart if required.</p>
 *
 * @author Mikael Ahlberg
 */
public abstract class EventProcessor {
    
    private final Logger logger = LoggerFactory.getLogger(EventProcessor.class);
    
    private @Autowired ChangeStreamProvider changeStreamProvider;
    
    @PostConstruct
    public void init() {
        start(Duration.ZERO);
    }
    
    private void start(Duration initialDelay) {
        Mono.just("")
            .delayElement(initialDelay)
            .flatMap(o -> getResumeToken())
            .flatMapMany(resumeToken -> changeStreamProvider.openChangeStream(getCollectionName(), resumeToken, fetchFullDocument()))
            .flatMapSequential(this::process, 1)
            .subscribeOn(Schedulers.boundedElastic())
            .subscribe(o -> logger.info("Closing stream"), this::handleErrorShutdown);
    }
    
    private void handleErrorShutdown(Throwable error) {
        if (restartOnFailure()) {
            logger.error("Error encountered for {}, stream configured for restart...", this, error);
            start(Duration.ofMinutes(5));
        } else {
            logger.error("Error encountered for {}, closing stream...", this, error);
        }
    }
    
    /**
     * <p>Process the received event.</p>
     * 
     * <p>If the stream should stop when an error occur, let the error
     * bubble up and the stream will be closed.</p>
     * 
     * <p>Implementors of this method must be careful not to block any threads.
     * Any blocking I/O events must be executed on a different thread pool. For example
     * the elastic thread pool provided by Reactor.</p>
     * 
     * <pre>{@link Mono}.fromCallable(this::myBlocking).subscribeOn(Schedulers.boundedElastic())</pre>
     * 
     * @param event is the event received
     * @return void
     */
    public abstract Mono<Void> process(Event event);
    
    /**
     * <p>Retrieve the resume token to use when starting up the stream,
     * i.e. after a restart.</p>
     * 
     * @return a {@link ResumeToken} containing the desired resume token
     */
    protected abstract Mono<ResumeToken> getResumeToken();
    
    /**
     * <p>Get the collection name for the stream to read.</p>
     * 
     * @return a string containing the collection name
     */
    protected abstract String getCollectionName();
    
    /**
     * <p>Should return true if the processor is configured for restart
     * on failure. The stream will restart after 5 minutes.</p>
     * 
     * @return true or false
     */
    protected abstract boolean restartOnFailure();
    
    /**
     * <p>Should return true if the stream should return the full
     * document on updates.</p>
     * 
     * @return true or false
     */
    protected abstract boolean fetchFullDocument();
}
