/*
 * Copyright 2021 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.processor.provided;

import java.time.OffsetDateTime;

import org.springframework.beans.factory.annotation.Autowired;

import com.zuunr.restbed.core.processor.EventProcessor;
import com.zuunr.restbed.core.processor.Event;
import com.zuunr.restbed.core.processor.stream.ResumeToken;
import com.zuunr.restbed.core.processor.stream.SimpleResumeTokenHandler;

import reactor.core.publisher.Mono;

/**
 * <p>The AtLeastOnceProcessorV1 is an abstract implementation
 * which will set up the 'at least once' pattern for users.</p>
 * 
 * <p>The resume token is saved as the last step, after the event
 * has been processed. If an error occur, and the implementors
 * of the processor lets the error bubble up, the resume token will not
 * be saved.</p>
 *
 * @author Mikael Ahlberg
 */
public abstract class AtLeastOnceProcessorV1 extends EventProcessor {
    
    private @Autowired SimpleResumeTokenHandler simpleResumeTokenHandler;

    @Override
    public Mono<Void> process(Event event) {
        return Mono.just(event)
                .flatMap(this::processAtLeastOnce)
                .then(Mono.defer(() -> saveResumeToken(event)));
    }

    private Mono<Void> saveResumeToken(Event event) {
        return simpleResumeTokenHandler.saveResumeToken(event, this.getClass().getName());
    }
    
    @Override
    protected Mono<ResumeToken> getResumeToken() {
        return simpleResumeTokenHandler.getResumeToken(this.getClass().getName(), getDefaultResumeDate());
    }
    
    /**
     * <p>Process the provided event.</p>
     * 
     * <p>If stream should stop when an error occur, let the error
     * bubble up through the reactive chain in order to close the stream.
     * If the processor is configured for restart it will pickup
     * from the previously saved resume token after the restart.</p>
     * 
     * <p>I.e the resume token will not be saved if an error occur.</p>
     * 
     * @param event is the event received
     * @return void
     */
    public abstract Mono<Void> processAtLeastOnce(Event event);
    
    /**
     * <p>In case no previous resume token is present, use this date as the
     * start point of the stream.</p>
     * 
     * @return a date used for starting the stream
     */
    protected abstract OffsetDateTime getDefaultResumeDate();
    
}
