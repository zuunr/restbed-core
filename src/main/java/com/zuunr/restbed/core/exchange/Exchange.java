/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.exchange;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectSupport;
import com.zuunr.json.JsonValue;

/**
 * <p>An Exchange is the message container for {@link Request} and {@link Response} messages.</p>
 * 
 * <p>It is used to deliver requests and/or response objects throughout the platform.
 * Exchanges are immutable objects.</p>
 *
 * @param <T> is the request and response object's body type
 * 
 * @author Niklas Eldberger
 * @author Mikael Ahlberg
 */
public class Exchange<T extends JsonObjectSupport> {

    private Request<T> request;
    private Response<T> response;

    private Exchange() {}

    private Exchange(Request<T> request, Response<T> response) {
        this.request = request;
        this.response = response;
    }

    /**
     * <p>A static generator that creates a new empty Exchange object.</p>
     * 
     * @param <T> is the request and response object's body type
     * @return an empty exchange
     */
    public static <T extends JsonObjectSupport> Exchange<T> empty() {
        return new Exchange<>();
    }

    /**
     * <p>Creates a new exchange object with the given request.</p>
     * 
     * @param request is the request object to set on the exchange
     * @return an exchange containing the set request object
     */
    public Exchange<T> request(Request<T> request) {
        return new Exchange<>(request, null);
    }

    /**
     * <p>Creates a new exchange object with the given response.</p>
     * 
     * <p>The exchange must contain a request object before this method can be called,
     * otherwise an {@link IllegalStateException} will be thrown.</p>
     * 
     * @param response is the response object to set on the exchange
     * @return an exchange containing the set response object
     */
    public Exchange<T> response(Response<T> response) {
        if (request == null) {
            throw new IllegalStateException("Request must be set before response");
        }

        return new Exchange<>(request, response);
    }

    /**
     * <p>Returns the request object of this exchange.</p>
     * 
     * @return an request object
     */
    public Request<T> getRequest() {
        return request;
    }

    /**
     * <p>Returns the response object of this exchange.</p>
     * 
     * @return an response object
     */
    public Response<T> getResponse() {
        return response;
    }

    public JsonObject asJsonObject() {
        return JsonObject.EMPTY
                .put("request", request.asJsonObject())
                .put("response", response == null ? JsonValue.NULL : response.asJsonObject().jsonValue());
    }
}
