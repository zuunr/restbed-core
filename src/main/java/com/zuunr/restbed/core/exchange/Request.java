/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.exchange;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectSupport;
import com.zuunr.json.JsonValue;
import com.zuunr.restbed.core.api.ApiUriInfo;

/**
 * Created by niklas on 2017-05-18.
 */
public class Request<T extends JsonObjectSupport> implements JsonObjectSupport {

    private Method method;
    private ApiUriInfo apiUriInfo;
    private JsonObject headers = JsonObject.EMPTY;
    private T body;

    private Request(Method method) {
        this(method, null, null, null);
        // FIXME to comply with earlier behaviour, check if headers should be set to null
    }

    private Request(Method method, ApiUriInfo url, JsonObject headers, T body) {
        this.method = method;
        this.apiUriInfo = url;
        this.headers = headers;
        this.body = body;
    }

    /**
     * <p>A static generator that creates a new request object with the given {@link Method}.</p>
     * 
     * @param <T> is the request object's body type
     * @param method the selected {@link Method} for this request
     * @return a new request object
     */
    public static <T extends JsonObjectSupport> Request<T> create(Method method) {
        return new Request<>(method);
    }

    /**
     * <p>Creates a new request object with the given URL.</p>
     * 
     * <p>Headers and body on the current request object are removed
     * on the newly created request object.</p>
     * 
     * @param url is the URL to set on the request
     * @return a new request object
     */
    public Request<T> url(String url) {
        return new Request<>(getMethod(), ApiUriInfo.ofUri(url), JsonObject.EMPTY, null);
    }

    /**
     * <p>Creates a new request object with the given headers as
     * a {@link JsonObject}.</p>
     * 
     * <p>The body on the current request is removed on the newly
     * created request object.</p>
     * 
     * @param headers is the header object to set on the request
     * @return a new request object
     */
    public Request<T> headers(JsonObject headers) {
        return new Request<>(getMethod(), apiUriInfo, headers, null);
    }

    /**
     * <p>Creates a new request object with the given body.</p>
     * 
     * @param body is the body to set on the request
     * @return a new request object
     */
    public Request<T> body(T body) {
        return new Request<>(getMethod(), apiUriInfo, getHeaders(), body);
    }

    /**
     * <p>Returns the url of this request.</p>
     * 
     * @return the url
     */
    public String getUrl() {
        return apiUriInfo == null ? null : apiUriInfo.uri();
    }

    /**
     * <p>Returns the {@link JsonObject} headers of this request.</p>
     * 
     * @return the headers
     */
    public JsonObject getHeaders() {
        return headers;
    }

    public T getBody() {
        return body;
    }

    public JsonObject getBodyAsJsonObject() {
        return body == null ? null : body.asJsonObject();
    }

    public Method getMethod() {
        return method;
    }

    @Override
    public JsonObject asJsonObject() {
        return JsonObject.EMPTY
                .put("method", method.toString())
                .put("url", apiUriInfo == null ? JsonValue.NULL : JsonValue.of(apiUriInfo.uri()))
                .put("headers", headers == null ? JsonValue.NULL : headers.jsonValue())
                .put("body", body == null ? JsonValue.NULL : body.asJsonObject().jsonValue());
    }

    public ApiUriInfo getApiUriInfo() {
        return apiUriInfo;
    }
}
