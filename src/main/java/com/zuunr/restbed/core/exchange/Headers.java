/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.exchange;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;

public final class Headers {

    private JsonObject meAsJsonObject;

    public static final Headers EMPTY = JsonObject.EMPTY.jsonValue().as(Headers.class);

    public HeadersBuilder builder() {
        return new HeadersBuilder(meAsJsonObject);
    }

    private Headers(JsonValue jsonValue) { //NOSONAR
        this(jsonValue.getValue(JsonObject.class));
    }

    private Headers(JsonObject headersAsJsonObject) {
        this.meAsJsonObject = headersAsJsonObject;
    }

    public Header get(String headerName) {
        return meAsJsonObject.get(headerName, JsonValue.NULL).as(Header.class);
    }

    public static class HeadersBuilder {
        JsonObjectBuilder jsonObjectBuilder;

        private HeadersBuilder(JsonObject initialHeaders) {
            jsonObjectBuilder = initialHeaders.builder();
        }

        public HeadersBuilder put(Header header) {
            jsonObjectBuilder.put(header.headerName(), header);
            return this;
        }

        public Headers build() {
            return new Headers(jsonObjectBuilder.build());
        }
    }
}
