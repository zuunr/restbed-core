/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.exchange;

/**
 * <p>The StatusCode enum contains status codes that describes the 
 * status of a resource.</p>
 * 
 * @author Mikael Ahlberg
 */
public enum StatusCode {
    _200_OK(200), //NOSONAR
    _201_CREATED(201), //NOSONAR
    _204_NO_CONTENT(204), //NOSONAR

    _400_BAD_REQUEST(400), //NOSONAR
    _401_UNAUTHORIZED(401), //NOSONAR
    _403_FORBIDDEN(403), //NOSONAR
    _404_NOT_FOUND(404), //NOSONAR
    _405_METHOD_NOT_ALLOWED(405), //NOSONAR

    _409_CONFLICT(409), //NOSONAR

    _429_TOO_MANY_REQUESTS(429), //NOSONAR
    _500_INTERNAL_SERVER_ERROR(500); //NOSONAR

    private int code;

    private StatusCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
