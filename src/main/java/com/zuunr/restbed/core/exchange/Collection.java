/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.exchange;

import java.util.List;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectSupport;
import java.util.Collections;

/**
 * <p>A Collection is the wrapper around the items contained in a collection,
 * as well as containing meta data.</p>
 * 
 * <p>Collections are immutable objects and the list is of unmodifiable type.</p>
 * 
 * @param <T> is the item's body type
 *
 * @author Mikael Ahlberg
 */
public class Collection<T extends JsonObjectSupport> implements JsonObjectSupport {

    private List<T> value;
    private int offset;
    private int limit;
    private String href;
    private int size;

    private Collection(Builder<T> builder) {
        this.value = Collections.unmodifiableList(builder.value);
        this.offset = builder.offset;
        this.limit = builder.limit;
        this.href = builder.href;
        this.size = builder.size;
    }

    /**
     * <p>Returns this collections offset value, e.g. how many
     * items that was skipped when reading from the repository.</p>
     * 
     * @return the offset value
     */
    public int getOffset() {
        return offset;
    }

    /**
     * <p>Returns this collections limit value, e.g. how many
     * items that was read (at maximum) from the repository.</p>
     * 
     * @return the limit value
     */
    public int getLimit() {
        return limit;
    }

    /**
     * <p>Returns an unmodifiable list containing the items in
     * this collection.</p>
     * 
     * @return a list of items
     */
    public List<T> getValue() {
        return value;
    }

    /**
     * <p>Returns this collections href.</p>
     * 
     * @return the href
     */
    public String getHref() {
        return href;
    }

    /**
     * <p>The actual size of the list of items in this collection.</p>
     * 
     * @return the size of the collection
     */
    public int getSize() {
        return size;
    }

    @Override
    public JsonObject asJsonObject() {
        JsonArray items = JsonArray.EMPTY;

        for (T item : value) {
            items = items.add(item.asJsonObject());
        }

        return JsonObject.EMPTY.builder()
                .put("offset", offset)
                .put("limit", limit)
                .put("value", items)
                .put("href", href)
                .put("size", value.size())
                .build();
    }

    /**
     * <p>Creates a new builder in order to create a new collection.</p>
     * 
     * @param <T> is the item's body type
     * @return a new builder object
     */
    public static <T extends JsonObjectSupport> Builder<T> builder() {
        return new Builder<>();
    }

    public static final class Builder<T extends JsonObjectSupport> {
        private List<T> value = Collections.emptyList();
        private int offset;
        private int limit;
        private String href;
        private int size;

        private Builder() {
        }

        public Builder<T> value(List<T> value) {
            this.value = value;
            return this;
        }

        public Builder<T> offset(int offset) {
            this.offset = offset;
            return this;
        }

        public Builder<T> limit(int limit) {
            this.limit = limit;
            return this;
        }

        public Builder<T> href(String href) {
            this.href = href;
            return this;
        }

        public Builder<T> size(int size) {
            this.size = size;
            return this;
        }

        /**
         * <p>Creates a new Collection given the value that has been set on the builder object.</p>
         * 
         * @return a new collection
         */
        public Collection<T> build() {
            return new Collection<>(this);
        }
    }
}
