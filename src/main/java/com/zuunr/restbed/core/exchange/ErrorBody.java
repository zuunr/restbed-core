/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.exchange;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectSupport;
import com.zuunr.json.JsonValue;

/**
 * <p>An ErrorBody is the container of the different error messages
 * to be sent back to the caller.</p>
 * 
 * @author Niklas Eldberger
 */
public class ErrorBody implements JsonObjectSupport {

    public static final JsonValue REQUEST_BODY_ERROR = JsonValue.of("REQUEST_BODY_ERROR");
    public static final JsonValue RESOURCE_STATE_ERROR = JsonValue.of("REQUEST_BODY_ERROR");
    public static final JsonValue QUERY_PARAMS_ERROR = JsonValue.of("QUERY_PARAMS_ERROR");
    public static final JsonValue PERMISSIONS_ERROR = JsonValue.of("PERMISSIONS_ERROR");

    private JsonObject body;

    public static ErrorBody of(JsonObject body) {
        ErrorBody result = null;
        if (body != null) {
            result = new ErrorBody(body);
        }
        return result;
    }

    public ErrorBody(JsonObject body) {
        this.body = body;
    }

    @Override
    public JsonObject asJsonObject() {
        return body;
    }
}
