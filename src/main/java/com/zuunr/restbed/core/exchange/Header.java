/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.exchange;

import com.zuunr.json.*;

import java.util.List;

public final class Header implements JsonObjectSupport {

    private JsonObject jsonObject;

    public static HeaderBuilder builder(String headerName) {
        return new HeaderBuilder(headerName);
    }

    private Header(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    private Header(JsonValue jsonValue) {
        this.jsonObject = jsonValue.getValue(JsonObject.class);
    }

    public String headerName() {
        return jsonObject.get("headerName").getValue(String.class);
    }

    public <T> T firstValue(Class<T> clazz) {
        return values().head().as(clazz);
    }

    public JsonArray values() {
        return jsonObject.get("values").getValue(JsonArray.class);
    }

    public <T> List<T> values(Class<T> clazz) {
        return values().asList(clazz);
    }

    @Override
    public JsonObject asJsonObject() {
        return jsonObject;
    }

    @Override
    public JsonValue asJsonValue() {
        return jsonObject.jsonValue();
    }

    public static class HeaderBuilder {

        private String headerName;
        private JsonArrayBuilder values;

        public HeaderBuilder(String headerName) {
            this.headerName = headerName;
            this.values = JsonArray.EMPTY.builder();
        }

        public HeaderBuilder addValue(String value) {
            values.add(value);
            return this;
        }

        public HeaderBuilder addValue(JsonObject value) {
            return addValue(value.jsonValue());
        }

        public HeaderBuilder addValue(JsonValue value) {
            values.add(value);
            return this;
        }

        public HeaderBuilder addValue(JsonValueSupport value) {
            values.add(value);
            return this;
        }

        public Header build() {
            return new Header(JsonObject.EMPTY.builder()
                    .put("headerName", headerName)
                    .put("values", values.build())
                    .build());
        }
    }
}
