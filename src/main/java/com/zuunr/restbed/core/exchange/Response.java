/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.exchange;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectSupport;
import com.zuunr.json.JsonValue;

/**
 * <p>A Response is the container for the following types:</p>
 * <ul>
 * <li>status code</li>
 * <li>headers</li>
 * <li>body</li>
 * <li>error body</li>
 * </ul>
 * 
 * <p>Response objects are immutable and are used to send response objects
 * throughout the platform.</p>
 * 
 * @param <T> is the response object's body type
 *
 * @author Niklas Eldberger
 * @author Mikael Ahlberg
 */
public class Response<T extends JsonObjectSupport> implements JsonObjectSupport {

    private Integer statusCode;
    private JsonObject headers;
    private T body;
    private ErrorBody errorBody;

    private Response(Integer status) {
        this(status, null, null, null);
    }

    private Response(Integer statusCode, JsonObject headers, T body, ErrorBody errorBody) {
        this.headers = headers;

        // Making sure headers are never null when body is set
        if (headers == null && (body != null || errorBody != null)) {
            headers = JsonObject.EMPTY;
        }
        this.headers = headers;
        this.body = body;
        this.statusCode = statusCode;
        this.errorBody = errorBody;
    }

    /**
     * <p>A static generator that creates a new response with the
     * given {@link StatusCode}.</p>
     * 
     * @param <T> is the response object's body type
     * @param statusCode the selected {@link StatusCode} for this response
     * @return a new response object
     */
    public static <T extends JsonObjectSupport> Response<T> create(StatusCode statusCode) {
        return new Response<>(statusCode.getCode());
    }

    /**
     * <p>Creates a new response object with the given headers as
     * a {@link JsonObject}.</p>
     * 
     * @param headers is the header object to set on the request
     * @return a new response object
     */
    public Response<T> headers(JsonObject headers) {
        return new Response<>(getStatusCode(), headers, body, errorBody);
    }

    /**
     * <p>Creates a new response object with the given body.</p>
     * 
     * @param body is the body to set on the response
     * @return a new response object
     */
    public Response<T> body(T body) {
        return new Response<>(getStatusCode(), getHeaders(), body, errorBody);
    }

    /**
     * <p>Creates a new response object with the given error body.</p>
     * 
     * @param errorBody is the error body to set on the response
     * @return a new response object
     */
    public Response<T> errorBody(ErrorBody errorBody) {
        return new Response<>(getStatusCode(), getHeaders(), body, errorBody);
    }

    /**
     * <p>Returns the status code of this response.</p>
     * 
     * @return the status code
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * <p>Returns the {@link JsonObject} headers of this response.</p>
     * 
     * @return the headers
     */
    public JsonObject getHeaders() {
        return headers;
    }

    /**
     * <p>Returns the body of this response.</p>
     * 
     * @return the body
     */
    public T getBody() {
        return body;
    }

    /**
     * <p>Returns the error body of this response.</p>
     * 
     * @return the error body
     */
    public ErrorBody getErrorBody() {
        return errorBody;
    }

    public JsonObject getBodyAsJsonObject() {
        return body == null ? null : body.asJsonObject();
    }

    @Override
    public JsonObject asJsonObject() {
        return JsonObject.EMPTY
                .put("statusCode", statusCode)
                .put("headers", headers == null ? JsonValue.NULL : headers.jsonValue())
                .put("body", body == null ? JsonValue.NULL : body.asJsonObject().jsonValue())
                .put("errorBody", errorBody == null ? JsonValue.NULL : errorBody.asJsonObject().jsonValue());
    }
}
