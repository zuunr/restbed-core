/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.api;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.JsonValueSupport;
import com.zuunr.json.JsonValueUtil;

public class ApiUriInfo implements JsonValueSupport {

    private static final Logger logger = LoggerFactory.getLogger(ApiUriInfo.class);
    private static Base64.Encoder base64Encoder = Base64.getEncoder();
    private static Base64.Decoder base64Decoder = Base64.getDecoder();

    private JsonValue asJsonValue;
    private String uri;
    private String collectionNameUri;
    private String collectionName;
    private String context;

    private String itemUri;
    private String itemId;
    private String minimalUid;
    private String decodedItemId;
    private Boolean isItem;
    private String queryString;
    private String apiUri;
    private String apiBaseUri;
    private String apiName;
    private String apiNameUri;
    private Boolean isSample;
    private JsonObject queryParams;
    private String protocol;
    private String host;
    private Integer port;
    private String hostAndPort;
    private String hostAndPortUri;
    private String version;
    private String versionUri;
    private JsonObject extensions;
    private Integer limit;
    private JsonArray sortOrder;
    private String pathUri;
    private Integer offset;
    private int pathStartIndex = -1;
    private JsonArray filter;


    private ApiUriInfo(JsonValue jsonValue) {
        // Needed by json
        this.uri = jsonValue.getValue(String.class);
        asJsonValue = jsonValue;
    }

    private ApiUriInfo(String uri) {
        this.uri = uri;
    }

    public ApiUriInfoBuilder builder() {
        return new ApiUriInfoBuilder(this);
    }


    public String collectionNameUri() {
        if (collectionNameUri == null) {
            int end = uri().length();
            int start = apiUri().length() + 1;
            for (int index = start; index < uri().length(); index++) {
                if (uri().charAt(index) == '/' || uri().charAt(index) == '?') {
                    end = index;
                    break;
                }
            }
            if (start < end) {
                collectionNameUri = uri().substring(0, end);
            }

        }
        return collectionNameUri;
    }

    public String itemUri() {
        if (isItem == null) {
            String iu = ApiUriUtil.itemUriOf(uri);
            if (iu == null) {
                isItem = false;
            } else {
                itemUri = iu;
                isItem = true;
            }
        }
        return itemUri;
    }

    public String pathUri() {
        if (pathUri == null) {
            int start = pathStartIndex();
            int end = -1;
            int lastDelimiter = -1;
            for (int i = start; i < uri.length(); i++) {
                char c = uri().charAt(i);
                if (c == '/') {
                    lastDelimiter = i;
                } else if (c == '?') {
                    lastDelimiter = i - 1;
                    break;
                }
                end = i;

            }

            if (end > lastDelimiter) {
                pathUri = uri().substring(0, end + 1);
            } else if (lastDelimiter > -1) {
                pathUri = uri().substring(0, lastDelimiter + 1);
            }
        }
        return pathUri;
    }


    public boolean isCollection() {
        try {
            return !isItem() && collectionNameUri() != null;
        } catch (NullPointerException e) {
            // do nothing
        }
        return false;
    }

    public boolean isItem() {
        if (isItem == null) {
            itemUri();
        }
        return isItem;
    }

    public String itemId() {
        if (isItem() && itemId == null) {
            itemId = itemUri().substring(collectionNameUri().length() + 1);
        }
        return itemId;
    }

    public static ApiUriInfo ofUri(String uri) {
        if (uri == null) {
            throw new NullPointerException("uri must not be null");
        }
        return new ApiUriInfo(uri);
    }

    public String uri() {
        return uri;
    }

    public String path() {
        return pathUri().substring(pathStartIndex());
    }


    public String collectionName() {
        if (collectionName == null) {
            if (apiUri() != null) {
                int start = apiUri().length() + 1;
                int end = uri().length();
                for (int i = start; i < uri().length(); i++) {
                    char currentChar = uri().charAt(i);
                    if (currentChar == '/' || currentChar == '?') {
                        end = i;
                        break;
                    }
                }
                if (end > start) {
                    collectionName = uri().substring(start, end);
                }
            }
        }
        return collectionName;
    }

    public String queryString() {
        if (queryString == null) {
            int questionMarkIndex = uri.indexOf('?');
            if (questionMarkIndex == -1) {
                queryString = "";
            } else {
                queryString = uri.substring(questionMarkIndex + 1);
            }
        }
        return queryString;
    }

    public String apiBaseUri() {
        if (apiBaseUri == null) {
            apiBaseUri = apiUri() + "/";
        }
        return apiBaseUri;
    }

    public String apiUri() {
        if (apiUri == null) {

            try {
                apiUri = new StringBuilder(
                        protocol()).append("://")
                        .append(hostAndPort()).append("/")
                        .append(version()).append("/")
                        .append(apiName())
                        .append(isSample() ? "/samples" : "/api").toString();
            } catch (Exception e) {
                // If anything is null - apiUri should be null
            }
        }
        return apiUri;
    }


    public boolean isSample() {
        if (isSample == null) {
            isSample = uri.startsWith("/samples/", apiNameUri().length());
        }
        return isSample;
    }

    public JsonObject queryParams() {
        if (queryParams == null) {
            JsonObject qParams = JsonObject.EMPTY;
            String[] pairs = queryString().split("[&]");
            for (String pairString : pairs) {
                String[] pair = pairString.split("=");
                if (pair.length == 2) {
                    qParams = qParams.put(decode(pair[0]), decode(pair[1]));
                }
            }
            queryParams = qParams;
        }
        return queryParams;
    }


    private String decode(String toBeDecoded) {
        try {
            return URLDecoder.decode(toBeDecoded, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage());
            throw new RuntimeException("Decoding failed", e);
        }
    }

    private String urlDecode(String tobeDecoded) {
        try {
            return URLDecoder.decode(tobeDecoded, StandardCharsets.UTF_8.name());
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }


    public String protocol() {
        if (protocol == null) {
            int colonIndex = 4;
            if (uri.startsWith("http")) {
                if (uri.charAt(4) == 's') {
                    colonIndex = 5;
                }
                if (uri.charAt(colonIndex) == ':') {
                    protocol = uri.substring(0, colonIndex);
                } else {
                    throw new RuntimeException("Malformed API URI. Must start with either http: or https:");
                }
            }
        }
        return protocol;
    }


    public String hostAndPort() {
        if (hostAndPort == null) {
            if (protocol() != null) {
                int start = protocol().length() + "://".length();
                int end = start;
                for (; end < uri.length(); end++) {
                    char endChar = uri.charAt(end);
                    if (endChar == '/') {
                        break;
                    }
                }
                if (start != end) {
                    hostAndPort = uri.substring(start, end);
                }
            }
        }
        return hostAndPort;
    }

    private int pathStartIndex() {
        if (pathStartIndex == -1) {
            pathStartIndex = protocol().length() + "://".length() + hostAndPort().length() + "/".length();
        }
        return pathStartIndex;
    }

    public String version() {
        if (version == null) {
            version = uri.substring(pathStartIndex(), versionUri().length());
        }
        return version;
    }

    public String versionUri() {
        if (versionUri == null) {
            int start = protocol().length() + "://".length() + hostAndPort().length() + "/".length();
            int end = uri().length();
            for (int i = start; i < uri().length(); i++) {
                char c = uri.charAt(i);

                if (c == '/' || c == '?') {
                    break;
                }
                end = i;
            }
            versionUri = uri.substring(0, end + 1);
        }
        return versionUri;
    }

    public String apiNameUri() {
        if (apiNameUri == null) {
            int start = versionUri().length() + 1;
            int end = uri().length();
            for (int i = start; i < uri().length(); i++) {
                char c = uri.charAt(i);

                if (c == '/' || c == '?') {
                    break;
                }
                end = i;
            }
            apiNameUri = uri.substring(0, end + 1);
        }
        return apiNameUri;
    }

    public String apiName() {
        if (apiName == null) {
            apiName = apiNameUri() == null ? null : apiNameUri().substring(versionUri().length() + 1);
        }
        return apiName;
    }

    public String context() {
        if (context == null) {
            if (isItem()) {
                context = contextOfDecodedItemId(decodedItemId());
            }
        }
        return context;
    }

    private static String contextOfDecodedItemId(String decodedItemId){

        String context = "";

        int start = 0;
        int end = start;
        for (; end < decodedItemId.length(); end++) {
            char endChar = decodedItemId.charAt(end);
            if (endChar == '!') {
                context = decodedItemId.substring(start, end);
                break;
            }
        }
        return context;
    }

    public String decodedItemId() {
        if (decodedItemId == null) {
            decodedItemId = decodeItemId(itemId());
        }
        return decodedItemId;
    }

    public static String createItemId() {
        return createItemId(null);
    }

    public static String createItemId(String context) {
        String id = ObjectId.get().toHexString();

        return createItemId(context, id);
    }

    public static String createItemId(String context, String coreId) {

        StringBuilder stringBuilder = new StringBuilder();
        if (context != null && !context.isEmpty()) {
            stringBuilder.append(context).append('!');
        }

        stringBuilder.append(coreId);
        return encode(stringBuilder.toString());
    }

    protected static String encode(String formattedContentOfId) {
        String encodedItemId = new String(base64Encoder.encode((formattedContentOfId).getBytes(StandardCharsets.UTF_8)));
        while (encodedItemId.endsWith("=") && encodedItemId.length() > 0) {
            encodedItemId = encodedItemId.substring(0, encodedItemId.length() - 1);
        }
        return encodedItemId;
    }

    protected static String decodeItemId(String itemId) {
        try {
            return new String(base64Decoder.decode((itemId).getBytes(StandardCharsets.UTF_8)));
        } catch (Exception e) {
            return itemId;
        }
    }

    public JsonObject extensionPaths() {

        if (extensions == null) {
            JsonObject ext = JsonObject.EMPTY;
            String extensionsParams = queryParams().get("extend", JsonValue.NULL).getValue(String.class);

            if (extensionsParams != null) {
                String[] extensionsStringArray = extensionsParams.split(",");
                for (String extension : extensionsStringArray) {
                    JsonValueUtil jsonValueUtil = new JsonValueUtil();
                    JsonArray extensionAsJsonArray = jsonValueUtil.createJsonArrayPathFromStringPath(extension, "\\.");
                    ext = ext.put(extension, extensionAsJsonArray);
                }
            }
            extensions = ext;
        }
        return extensions;
    }

    public Integer limit() {
        if (limit == null) {
            String limitStr = queryParams().get("limit", JsonValue.NULL).getValue(String.class);
            if (limitStr != null) {
                limit = Integer.valueOf(limitStr);
            }
        }
        return limit;
    }

    public JsonArray sortOrder() {
        if (sortOrder == null) {
            JsonArray result = JsonArray.EMPTY;
            String orderByEncoded = queryParams().get("orderBy", JsonValue.NULL).getValue(String.class);
            if (orderByEncoded != null) {
                String orderBy = null;
                try {
                    orderBy = URLDecoder.decode(orderByEncoded, StandardCharsets.UTF_8.name());
                } catch (UnsupportedEncodingException e) {
                    logger.error("UTF-8 not supported!!!");
                    throw new RuntimeException(e);
                }
                String[] allPathsToOrderBy = orderBy.split(",");
                JsonObject oneOrderBy = JsonObject.EMPTY;
                for (int i = 0; i < allPathsToOrderBy.length; i++) {
                    String singlePathAndAscendingInfo = allPathsToOrderBy[i].trim();
                    boolean orderAscending = true;
                    if (singlePathAndAscendingInfo != null) {
                        String[] singlePathAndAscArray = singlePathAndAscendingInfo.split(" ");
                        String path = singlePathAndAscArray[0];

                        oneOrderBy = oneOrderBy.put("param", path);
                        if (singlePathAndAscArray.length > 1) {
                            String asc = singlePathAndAscArray[1];
                            if ("desc".equals(asc)) {
                                orderAscending = false;
                            }
                        }
                        oneOrderBy = oneOrderBy.put("asc", orderAscending);
                        result = result.add(oneOrderBy);
                    }
                }
            }
            sortOrder = result;
        }
        return sortOrder;
    }

    public Integer offset() {
        if (offset == null) {
            String offsetString = queryParams().get("offset", JsonValue.NULL).getValue(String.class);
            if (offsetString != null) {
                offset = Integer.valueOf(offsetString);
            }
        }
        return offset;
    }

    public JsonArray filter() {
        if (filter == null) {
            JsonArray result = JsonArray.EMPTY;
            JsonObject.Pairs pairs = queryParams().getPairs(); // TODO: Remove use of getPairs(), bad impl.
            for (JsonObject.Pair pair = pairs.head(); !pairs.isEmpty(); pair = pairs.head()) {
                String key = pair.getKey();
                if (key.startsWith("value._.")) {
                    String itemKey = key.substring("value._.".length());
                    String value = pair.getValue().getValue(String.class);
                    String urlDecoded = urlDecode(value);
                    result = result.add(JsonObject.EMPTY.put("path", itemKey).put("expr", urlDecoded));
                }
                pairs = pairs.tail();
            }
            filter = result;
        }
        return filter;
    }

    public boolean isUri() {
        return hostAndPort() != null;
    }

    public String hostAndPortUri() {
        if (hostAndPortUri == null) {
            hostAndPortUri = protocol() + "://" + hostAndPort();
        }
        return hostAndPortUri;
    }

    public String host() {
        if (host == null) {
            setHostAndPort();
        }
        return host;
    }

    public Integer port() {
        if (port == null) {
            setHostAndPort();
        }
        return port;
    }

    private void setHostAndPort() {
        String hostAndPort = hostAndPort();
        int index = hostAndPort.indexOf(':');
        if (index == -1) {
            host = hostAndPort;
            if ("https".equals(protocol())) {
                port = 443;
            } else if ("http".equals(protocol())) {
                port = 80;
            } else {
                throw new RuntimeException("Only http and https is supported");
            }
        } else {
            host = hostAndPort.substring(0, index);
            port = Integer.parseInt(hostAndPort.substring(index + 1));
        }
    }

    @Override
    public JsonValue asJsonValue() {
        if (asJsonValue == null) {
            asJsonValue = JsonValue.of(uri);
        }
        return asJsonValue;
    }

    @Override
    public String toString() {
        return uri();
    }

    public String coreId() {
        if (minimalUid == null && itemId() != null) {
            String decoded = decodedItemId();
            int start = 0;
            if (context() != null && !context().isEmpty()) {
                start = context().length() + 1;
            }
            minimalUid = decoded.substring(start);
        }
        return minimalUid;
    }

    public static String coreIdOf(String itemId) {
        String decodedItemId = decodeItemId(itemId);
        int start = 0;
        String context = contextOfDecodedItemId(decodedItemId);
        if (context != null && !context.isEmpty()) {
            start = context.length() + 1;
        }
        String minimalUid = decodedItemId.substring(start);
        return minimalUid;
    }
}
