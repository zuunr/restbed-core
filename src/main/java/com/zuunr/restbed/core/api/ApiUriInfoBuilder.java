/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.api;

/**
 * @author Niklas Eldberger
 */
public class ApiUriInfoBuilder {

    private ApiUriInfo apiUriInfo;

    private String protocol;
    private String hostAndPort;

    public ApiUriInfoBuilder(ApiUriInfo apiUriInfo) {
        this.apiUriInfo = apiUriInfo;
    }

    public ApiUriInfoBuilder protocol(String protocol) {
        this.protocol = protocol;
        return this;
    }

    public ApiUriInfoBuilder hostAndPort(String hostAndPort) {
        this.hostAndPort = hostAndPort;
        return this;
    }

    public ApiUriInfo build() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(protocol == null ? apiUriInfo.protocol() : protocol);
        stringBuilder.append("://");
        stringBuilder.append(hostAndPort == null ? apiUriInfo.hostAndPort() : hostAndPort);
        
        if (apiUriInfo.path() != null && !"".equals(apiUriInfo.path())) {
            stringBuilder.append("/");
            stringBuilder.append(apiUriInfo.path());
            
            if (!apiUriInfo.queryParams().isEmpty()) {
                stringBuilder.append("?").append(apiUriInfo.queryString());
            }
        }
        
        return ApiUriInfo.ofUri(stringBuilder.toString());
    }
}
