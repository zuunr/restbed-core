/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.api;

/**
 * Created by niklas on 2017-08-15.
 * 
 * @deprecated Don't add new users of this class
 */
@Deprecated(forRemoval = true)
public class ApiUriUtil {
    
    private ApiUriUtil() {
        // Disable creating instances of ApiUriUtil
    }

    public static String itemUriOf(String uri) {
        int end = uri.indexOf('?');
        
        if (end == -1) {
            end = uri.length();
        }
        
        String uriWithoutQuery = uri.substring(0, end);
        
        return subUriOf(uriWithoutQuery, 7);
    }
    
    private static String subUriOf(String uri, int slashesIncluded) {
        String apiUri = null;
        int fromIndex = 0;
        int apiUriEndIndex = 0;
        
        for (int slashes = 0; slashes <= slashesIncluded; slashes++) { // http://domain:port/v1/<api-name>/ 5 slashedIncluded
            apiUriEndIndex = uri.indexOf('/', fromIndex + 1);
            if (apiUriEndIndex == -1) {
                if (slashesIncluded == slashes) { // ending slash (ie nothing follows) is reached
                    apiUriEndIndex = uri.length();
                }
                break;
            }
            fromIndex = apiUriEndIndex;
        }

        if (apiUriEndIndex == -1 || uri.charAt(apiUriEndIndex - 1) == '/') {
            apiUri = null;
        } else {
            apiUri = uri.substring(0, apiUriEndIndex);
        }
        
        return apiUri;
    }
}
