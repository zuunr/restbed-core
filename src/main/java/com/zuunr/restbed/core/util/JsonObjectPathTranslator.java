/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.util;

import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValueUtil;

/**
 * <p>The JsonObjectPathTranslator is responsible for translating
 * all paths matching the given lookupPath, replacing any string
 * matching the provided one.</p>
 * 
 * <p>The translator will look up all paths through the entire
 * object, e.g. not only on the root level.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class JsonObjectPathTranslator {
    
    private final JsonValueUtil jsonValueUtil = new JsonValueUtil();
    
    /**
     * <p>Translates the provided {@link JsonObject} where any path matching the
     * provided lookupPath will be translated, given that the string is matching
     * the provided fromApiNameUrl.</p>
     * 
     * @param lookupPath the path to lookup
     * @param object is the object to translate
     * @param fromApiNameUrl is the string that will be replaced on the given path(s)
     * @param toApiNameUrl is the string that will be the replacement string on the given path(s)
     * @return a {@link JsonObject} with any matched value translated
     */
    public JsonObject translatePaths(String lookupPath, JsonObject object, String fromApiNameUrl, String toApiNameUrl) {
        JsonArray paths = jsonValueUtil.pathsEndingWith(object.jsonValue(), JsonArray.of(lookupPath), false);

        for (int i = 0; i < paths.size(); i++) {
            String valueToBeUpdated = object.get(paths.get(i).getValue(JsonArray.class), "").getValue(String.class);
            String updatedValue = valueToBeUpdated.replace(fromApiNameUrl, toApiNameUrl);
            object = object.put(paths.get(i).getValue(JsonArray.class), updatedValue);
        }
        
        return object;
    }
}
