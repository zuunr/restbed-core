/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.util;

import java.util.List;
import java.util.Map.Entry;
import java.util.function.Consumer;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

/**
 * <p>The {@link HeaderTranslator} translates headers between the internal 
 * header representations with {@link JsonObject} and any external header 
 * representation, e.g. springs {@link HttpHeaders}.</p>
 * 
 * @author Mikael Ahlberg
 */
@Component
public class HeaderTranslator {

    /**
     * <p>Translates {@link JsonObject} headers into a {@link Consumer} of
     * {@link HttpHeaders}.</p>
     * 
     * @param headers is the headers to translate (non null)
     * @return a consumer object of httpheaders
     */
    public Consumer<HttpHeaders> translate(JsonObject headers) {
        MultiValueMap<String, String> mvmHeaders = new HttpHeaders();

        if (headers != null) {
            for (JsonValue headerKey : headers.keys().asList()) {
                String key = headerKey.getValue(String.class);
                mvmHeaders.add(key, headers.get(key).getString());
            }
        }
        
        return httpHeaders -> httpHeaders.addAll(mvmHeaders);
    }

    /**
     * <p>Translates {@link HttpHeaders} into a {@link JsonObject} containing
     * the headers.</p>
     * 
     * @param httpHeaders is the headers to translate
     * @return a new json object containing the headers
     */
    public JsonObject translate(HttpHeaders httpHeaders) {
        JsonObject translatedHeaders = JsonObject.EMPTY;

        for (Entry<String, List<String>> entry : httpHeaders.entrySet()) {
            translatedHeaders = translatedHeaders.put(entry.getKey(), entry.getValue().get(0));
        }

        return translatedHeaders;
    }
}
