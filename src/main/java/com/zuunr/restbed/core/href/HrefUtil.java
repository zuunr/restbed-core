/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.href;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

/**
 * Created by niklas on 25/03/16.
 */
@Component
public class HrefUtil {

    private static final Logger logger = LoggerFactory.getLogger(HrefUtil.class);

    private static final Pattern collectionPattern = Pattern.compile("(.*)/([^/?]+)");
    private static final Pattern queryPattern = Pattern.compile(".*[?](.*)");

    private static final Pattern schemeHostAndPortPattern = Pattern.compile("(http(s)?)+://(([^/:]*)((:([0-9]*))?[/]([^?]*)))([?]([^/]+))*");
    private static final Pattern apiNameUrl = Pattern.compile("((http(s)?)+://(([^/:]*)((:([0-9]*))?/v1/[^?/]*)))[/].*");

    public String pathOf(String url) {
        String path = null;
        Matcher matcher = schemeHostAndPortPattern.matcher(url);
        
        if (matcher.find()) {
            path = matcher.group(8);
        }
        
        return path;
    }

    public String idOf(String href) {
        String result = null;
        
        try {
            if (href != null) {
                int questionmark = href.indexOf("?");
                if (questionmark == -1) {
                    questionmark = href.length();
                }
                result = href.substring(0, questionmark);
                String result2 = result.replaceFirst("http(s)?://[^/]+/v1/[^/]+/(api|samples)/[^/]+", "");
                if (result2.isEmpty()) {
                    result = null;
                } else if (result.length() != result2.length()){
                    result = result2.substring(1);
                } else {
                    result = result.substring(result.lastIndexOf("/") + 1, result.length());
                }
            }
        } catch (Exception e) {
            logger.info("Failed to get id of href = {}", href, e);
        }
        
        return result;
    }

    public String collectionUrlOf(String href) {
        String result = href;

        try {
            if (result != null) {
                int questionmark = result.indexOf('?');
                if (questionmark == -1) {
                    questionmark = result.length();
                }
                result = result.substring(0, questionmark);


                String id = idOf(result);
                if (id != null) {
                    result = result.substring(0, result.length() - id.length() - 1); // -1 to remove '/'

                }
            }
        } catch (Exception e) {
            logger.error("failed to get collection of href: {}", href);
            throw new RuntimeException(e);
        }

        return result;
    }

    public String collectionAndIdUrlOf(String href) {
        String result = null;

        try {
            int questionmark = href.indexOf('?');
            if (questionmark == -1) {
                questionmark = href.length();
            }
            result = href.substring(0, questionmark);
        } catch (Exception e) {
            logger.info("Failed to get id of href: {}", href, e);
        }
        
        return result;
    }

    public String queryOf(String href) {
        String result = null;

        try {
            Matcher queryMatcher = queryPattern.matcher(href);
            if (queryMatcher.find()) {
                result = queryMatcher.group(1);
            }
        } catch (Exception e) {
            logger.info("Failed to get query of href: {}", href, e);
        }
        
        return result;
    }

    public String urlOfApiName(String url) {
        String result = null;
        Matcher matcher = apiNameUrl.matcher(url);
        if (matcher.find()) {
            result = matcher.group(1);
        }
        
        return result;
    }

    public String replace(String href, Replacement replacement) {
        String result = null;

        Matcher matcher = schemeHostAndPortPattern.matcher(href);
        if (matcher.find()) {
            String scheme = matcher.group(1);
            String host = matcher.group(4);
            String port = matcher.group(7);

            if (replacement.getScheme() != null) {
                scheme = replacement.getScheme();
            }

            if ("http".equals(scheme)) {
                if (replacement.getPort() != null) {
                    if ("80".equals(replacement.getPort())) {
                        port = null;
                    } else {
                        port = replacement.getPort();
                    }
                } else if ("80".equals(port)) {
                    port = null;
                }
            } else if ("https".equals(scheme)) {
                if (replacement.getPort() != null) {
                    if ("443".equals(replacement.getPort())) {
                        port = null;
                    } else {
                        port = replacement.getPort();
                    }
                } else if ("443".equals(port)) {
                    port = null;
                }
            }

            if (replacement.getHost() != null) {
                host = replacement.getHost();
            }

            String path = matcher.group(8);

            if (port == null) {
                result = scheme + "://" + host + "/" + path;
            } else {
                result = scheme + "://" + host + ":" + port + "/" + path;
            }
        }
        
        return result;
    }
    
    public JsonArray queryParams(String href) {
        JsonArray params = JsonArray.EMPTY;
        String query = queryOf(href);
        
        if (query != null) {
            String[] queryParamPairs = query.split("&");
            for (int i = 0; i < queryParamPairs.length; i++) {
                String queryParamPair = queryParamPairs[i];
                String[] nameAndValue = queryParamPair.split("=");
                if (nameAndValue != null && nameAndValue.length == 2) {
                    params = params.add(JsonObject.EMPTY.put(nameAndValue[0], nameAndValue[1]));
                }
            }
        }

        return params;
    }

    public boolean isSample(String collectionUrl) {
        return collectionUrl.startsWith(urlOfApiName(collectionUrl) + "/samples/");
    }
    
    public static class Replacement {
        private String scheme;
        private String host;
        private String port;

        private Replacement(Builder builder) {
            host = builder.host;
            scheme = builder.scheme;
            port = builder.port;
        }

        public String getHost() {
            return host;
        }

        public String getPort() {
            return port;
        }

        public String getScheme() {
            return scheme;
        }
        
        public static Builder builder() {
            return new Builder();
        }

        public static final class Builder {
            private String host;
            private String scheme;
            private String port;

            private Builder() {
            }

            public Builder host(String host) {
                this.host = host;
                return this;
            }

            public Builder scheme(String scheme) {
                this.scheme = scheme;
                return this;
            }

            public Builder port(String port) {
                this.port = port;
                return this;
            }

            public Replacement build() {
                return new Replacement(this);
            }
        }
    }
}
