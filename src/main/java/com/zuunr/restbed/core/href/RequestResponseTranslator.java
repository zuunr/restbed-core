/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.href;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.util.DecorationTemplate;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.exchange.ErrorBody;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.forms.FormProvider;
import com.zuunr.restbed.core.forms.FormProviderKey;

import java.util.Iterator;
import java.util.Optional;

/**
 * <p>The RequestResponseTranslator is responsible for translating
 * requests and responses whenever an external url-configuration is set,
 * and does not match the app's internal configured base url.</p>
 *
 * <p>Supported external url-configurations:</p>
 * <ul>
 * <li>header: api-base-url</li>
 * </ul>
 *
 * <p>The purpose of the translation is to translate any incoming requests
 * to the app's internal configured base url, and any outgoing responses to
 * the external url-configuration. This makes sure that all hrefs are stored
 * in the internal configured base url in the database, but clients will only
 * see the external url-configuration hrefs.</p>
 *
 * <p>In a typical setup, the app's internal configured base url will map 1:1
 * with the clients request host/url (and no translation is needed).</p>
 *
 * <p>However in a non-typical setup, where the app might be accessible from
 * multiple client hosts, the internal configured base url might not map 1:1
 * with the clients request host/url (resulting in the needed translation).</p>
 *
 * <p>Note: internal configured base url always includes /v1/ and /api/, but
 * the external configured base url might not (requiring this translation to
 * function downstream). However, the client request url must include /v1/
 * and /api/ so in order for api-base-url to change the path to external clients,
 * a proxy must be used in the middle.</p>
 *
 * @author Niklas Eldberger
 * @author Mikael Ahlberg
 */
public class RequestResponseTranslator {

    private static final JsonArray DEFAULT_NON_VALUE_HEADER = JsonArray.of(JsonValue.NULL);

    private FormProvider formProvider;

    public RequestResponseTranslator(com.zuunr.restbed.core.forms.FormProvider formProvider) {
        this.formProvider = formProvider;
    }

    /**
     * <p>Translates the incoming request if an external url-configuration is set
     * on the request, not matching the internal configured base url.</p>
     *
     * @param exchange                  contains the request to translate
     * @param internalConfiguredBaseUrl is the app's internal configured base url
     * @return a translated request wrapped in an {@link Exchange} object
     */
    public Exchange<JsonObjectWrapper> translateRequest(Exchange<JsonObjectWrapper> exchange, ApiUriInfo internalConfiguredBaseUrl) {
        String externalConfiguredBaseUrl = getExternalApiBaseUrlFromRequestHeaders(exchange.getRequest().getHeaders());

        return exchange.request(translateRequest(exchange.getRequest(), internalConfiguredBaseUrl, externalConfiguredBaseUrl));
    }

    private Request<JsonObjectWrapper> translateRequest(Request<JsonObjectWrapper> request, ApiUriInfo internalConfiguredBaseUrl, String externalConfiguredBaseUrl) {
        if (request.getBody() == null) {
            return request;
        } else {
            return request.body(JsonObjectWrapper.of(translateRequestBody(request, internalConfiguredBaseUrl, externalConfiguredBaseUrl)));
        }
    }

    private JsonObject translateRequestBody(final Request<JsonObjectWrapper> request, final ApiUriInfo internalConfiguredBaseUrl, final String externalConfiguredBaseUrl) {
        if (externalConfiguredBaseUrl == null || externalConfiguredBaseUrl.equals(internalConfiguredBaseUrl.apiBaseUri())) {
            return request.getBody().asJsonObject();
        }

        Form resourceModelForm = getResourceModelForm(getClientRequestApiInfo(request.getMethod(), request.getUrl()));

        return resourceModelForm.asJsonObject().as(DecorationTemplate.class)
                .decorateLinkedResourcesOfSameApiUri(request.getBody().asJsonObject(), hrefObject ->
                        // if href is not found where it was supposed to be, let request body validation take care of it downstream and silently ignore it here
                        Optional.ofNullable(hrefObject.get("href"))
                                .map(o -> hrefObject.put("href", o.getString().replace(externalConfiguredBaseUrl, internalConfiguredBaseUrl.apiBaseUri())))
                                .orElse(hrefObject)
                );
    }

    /**
     * <p>Translates the outgoing response if an external url-configuration is set
     * on the request headers, not matching the internal configured base url.</p>
     *
     * @param exchange                  contains the response to translate
     * @param internalConfiguredBaseUrl is the app's internal configured base url
     * @return a translated response wrapped in an {@link Exchange} object
     */
    public Exchange<JsonObjectWrapper> translateResponse(Exchange<JsonObjectWrapper> exchange, ApiUriInfo internalConfiguredBaseUrl) {
        String externalConfiguredBaseUrl = getExternalApiBaseUrlFromRequestHeaders(exchange.getRequest().getHeaders());

        return exchange.response(translateResponse(exchange, internalConfiguredBaseUrl, externalConfiguredBaseUrl));
    }

    private Response<JsonObjectWrapper> translateResponse(Exchange<JsonObjectWrapper> exchange, ApiUriInfo internalConfiguredBaseUrl, String externalConfiguredBaseUrl) {
        Response<JsonObjectWrapper> translatedResponse = exchange.getResponse();

        JsonObject headersToTranslate = translatedResponse.getHeaders();
        JsonObject bodyToTranslate = translatedResponse.getBodyAsJsonObject();

        if (headersToTranslate != null) {
            JsonObject translatedHeaders = translateResponseHeaders(headersToTranslate, internalConfiguredBaseUrl, externalConfiguredBaseUrl);
            translatedResponse = translatedResponse.headers(translatedHeaders);
        }

        if (bodyToTranslate != null) {
            JsonObject translatedBody = translateResponseBody(exchange.getRequest(), exchange.getResponse().getStatusCode(), bodyToTranslate, internalConfiguredBaseUrl, externalConfiguredBaseUrl);
            translatedResponse = translatedResponse.body(JsonObjectWrapper.of(translatedBody));
        }

        return translatedResponse;
    }

    private JsonObject translateResponseHeaders(JsonObject headersToTranslate, final ApiUriInfo internalConfiguredBaseUrl, String externalConfiguredBaseUrl) {
        if (externalConfiguredBaseUrl == null || externalConfiguredBaseUrl.equals(internalConfiguredBaseUrl.apiBaseUri())) {
            return headersToTranslate;
        }

        ApiUriInfo location = headersToTranslate.get("Location", JsonValue.NULL).as(ApiUriInfo.class);

        if (location != null) {
            headersToTranslate = headersToTranslate.put("Location", translateResponseUrl(location, externalConfiguredBaseUrl));
        }

        return headersToTranslate;
    }

    private String translateResponseUrl(ApiUriInfo internalUrlToBeTranslated, String externalConfiguredBaseUrl) {
        return internalUrlToBeTranslated.uri().replace(internalUrlToBeTranslated.apiBaseUri(), externalConfiguredBaseUrl);
    }

    private JsonObject translateResponseBody(final Request<JsonObjectWrapper> request, final Integer statusCode, final JsonObject internalBodyToBeTranslated, final ApiUriInfo internalConfiguredBaseUrl, final String externalConfiguredBaseUrl) {
        if (externalConfiguredBaseUrl == null || externalConfiguredBaseUrl.equals(internalConfiguredBaseUrl.apiBaseUri())) {
            return internalBodyToBeTranslated;
        }

        JsonObject translatedBody = null;
        Form resourceModelForm = getResourceModelForm(getClientRequestApiInfo(request.getMethod(), request.getUrl()));

        if (statusCode > 199 && statusCode < 300) {
            translatedBody = resourceModelForm.asJsonObject().as(DecorationTemplate.class)
                    .decorateLinkedResourcesOfSameApiUri(internalBodyToBeTranslated, hrefObject ->
                            hrefObject.put("href", hrefObject
                                    .get("href").getString().replace(internalConfiguredBaseUrl.apiBaseUri(), externalConfiguredBaseUrl)));
        } else {
            translatedBody = translate4xxResponse(internalConfiguredBaseUrl, externalConfiguredBaseUrl, internalBodyToBeTranslated);
        }

        String internalSelf = translatedBody.get("href", JsonValue.NULL).getString();
        if (internalSelf != null) {
            translatedBody = translatedBody.put("href", internalSelf.replace(internalConfiguredBaseUrl.apiBaseUri(), externalConfiguredBaseUrl));
        }

        return translatedBody;
    }

    private JsonObject translate4xxResponse(final ApiUriInfo internalConfiguredBaseUrl, final String externalConfiguredBaseUrl, final JsonObject internalBodyToBeTranslated) {
        String internalApiBaseUrlPattern = internalConfiguredBaseUrl.apiBaseUri().replace(".", "[.]");
        String externalBaseUrlPattern = externalConfiguredBaseUrl.replace(".", "[.]");

        String errorType = getErrorType(internalBodyToBeTranslated);

        if (errorType == null) {
            return internalBodyToBeTranslated;
        }

        JsonArray pathsArray = JsonArray.of(errorType, "paths");
        JsonObject paths = internalBodyToBeTranslated.get(pathsArray, JsonObject.EMPTY).getValue(JsonObject.class);
        JsonObjectBuilder translatedViolationsPerPath = null;
        Iterator<JsonObject> pathErrors = paths.values().asList(JsonObject.class).iterator();
        for (JsonValue path : paths.keys()) {
            JsonObject pathError = pathErrors.next();

            if (path.getString().endsWith(".href")) {
                String pattern = pathError.get(JsonArray.of("violations", "pattern"), JsonValue.NULL).getString();
                if (pattern != null) {
                    if (translatedViolationsPerPath == null) {
                        translatedViolationsPerPath = internalBodyToBeTranslated
                                .get(pathsArray, JsonObject.EMPTY).getValue(JsonObject.class).builder();
                    }
                    translatedViolationsPerPath.put(path.getString(), pathError.put(JsonArray.of("violations", "pattern"), pattern.replace(internalApiBaseUrlPattern, externalBaseUrlPattern)));
                }
            }
        }

        return translatedViolationsPerPath == null ? internalBodyToBeTranslated : internalBodyToBeTranslated.put(pathsArray, translatedViolationsPerPath.build());
    }

    private String getErrorType(JsonObject internalBodyToBeTranslated) {
        if (ErrorBody.REQUEST_BODY_ERROR.equals(internalBodyToBeTranslated.get("code"))) {
            return "requestBodyError";
        } else if (ErrorBody.RESOURCE_STATE_ERROR.equals(internalBodyToBeTranslated.get("code"))) {
            return "resourceStateError";
        } else if (ErrorBody.QUERY_PARAMS_ERROR.equals(internalBodyToBeTranslated.get("code"))) {
            return "queryParamsError";
        } else if (ErrorBody.PERMISSIONS_ERROR.equals(internalBodyToBeTranslated.get("code"))) {
            return "permissionsError";
        } else {
            return null;
        }
    }

    private Form getResourceModelForm(ApiUriInfo apiUriInfo) {
        FormProviderKey formProviderKey = new FormProviderKey.Builder()
                .apiName(apiUriInfo.apiName())
                .collectionName(apiUriInfo.collectionName())
                .formType(FormProviderKey.FormType.RESOURCE_MODEL).build();
        JsonArray formFields = formProvider.getForm(formProviderKey);
        Form rootForm = formFields.as(FormFields.class).of("root").schema().form();

        return apiUriInfo.isItem() ? rootForm.formField("value").schema().eform() : rootForm;
    }

    private String getExternalApiBaseUrlFromRequestHeaders(JsonObject requestHeaders) {
        return Optional.ofNullable(requestHeaders.get("api-base-url", DEFAULT_NON_VALUE_HEADER).getValue(JsonArray.class).get(0).getString())
                .map(this::urlEndingWithSlash)
                .orElse(null);
    }

    private String urlEndingWithSlash(String url) {
        if (url.endsWith("/")) {
            return url;
        } else {
            return url + '/';
        }
    }

    private ApiUriInfo getClientRequestApiInfo(Method method, String clientRequestUrl) {
        if (method == Method.POST) {
            return ApiUriInfo.ofUri(clientRequestUrl + "/DUMMYITEM");
        } else {
            return ApiUriInfo.ofUri(clientRequestUrl);
        }
    }
}
