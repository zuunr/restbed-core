/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.authorization;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.exchange.Method;

public interface PermissionProvider {

    JsonArray defaultWriteItemPermissionFormValue = JsonArray.EMPTY.add(JsonObject.EMPTY
            .put("name", "body")
            .put("type", "object")
            .put("form", JsonObject.EMPTY
                    .put("value", JsonArray.EMPTY)));

    JsonArray defaultReadItemPermissionFormValue = JsonArray.EMPTY.add(JsonObject.EMPTY
            .put("name", "body")
            .put("type", "object")
            .put("form", JsonObject.EMPTY
                    .put("value", JsonArray.EMPTY)));


    JsonArray defaultReadCollectionPermissionFormValue = JsonArray.EMPTY.add(JsonObject.EMPTY
            .put("name", "body")
            .put("type", "object")
            .put("form", JsonObject.EMPTY
                    .put("value", JsonArray.EMPTY)));


    JsonArray defaultQueryPermissionFormValue = JsonArray.EMPTY;


    JsonArray
            superUserWriteItemPermissionFormValue =
            JsonArray.EMPTY
                    .add(JsonObject.EMPTY
                            .put("name", "body")
                            .put("type", "object"));

    JsonArray
            superUserReadPermissionFormValue =
            JsonArray.EMPTY
                    .add(JsonObject.EMPTY
                            .put("name", "body")
                            .put("type", "object"));

    JsonArray
            superUserQueryPermissionFormValue =
            JsonArray.EMPTY
                    .add(JsonObject.EMPTY
                            .put("name", "queryParams")
                            .put("type", "object"));

    JsonArray getRequestPermissionFormValue(Method method, ApiUriInfo apiUriInfo, JsonArray rolesSortedByName, Integer contextDepth);

    JsonArray getResponsePermissionFormValue(Method method, ApiUriInfo apiUriInfo, JsonArray rolesSortedByName, Integer contextDepth);

    boolean deleteIsOk(ApiUriInfo apiUriInfo, JsonArray rolesSortedByName, Integer contextDepth);

}
