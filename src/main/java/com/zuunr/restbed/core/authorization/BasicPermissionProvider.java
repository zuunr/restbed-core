/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.authorization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zuunr.forms.Form;
import com.zuunr.forms.generation.FormMerger;
import com.zuunr.forms.generation.MergeStrategy;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.cache.Cache;
import com.zuunr.restbed.core.cache.CacheProvider;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.restservice.ServiceConfigProvider;

@Component
public class BasicPermissionProvider implements PermissionProvider {

    private static final String PERMISSIONS = "PERMISSIONS";

    private CacheProvider cacheProvider;
    private ServiceConfigProvider serviceConfigProvider;

    public BasicPermissionProvider(@Autowired ServiceConfigProvider serviceConfigProvider, @Autowired CacheProvider cacheProvider) {
        this.serviceConfigProvider = serviceConfigProvider;
        this.cacheProvider = cacheProvider;
    }

    private static final FormMerger formMerger = new FormMerger();

    private static final JsonValue MAX_CONTEXT_DEPTH = JsonValue.of(Integer.MAX_VALUE);

    private JsonValue getFromCache(ApiUriInfo apiUriInfo, JsonArray path, JsonValue defaultIfNull) {
        Cache<String, JsonObject> cache = cacheProvider.getCache(apiUriInfo.apiName(), CacheProvider.PERMISSION_CACHE);
        JsonObject permissions =  cache.get(PERMISSIONS);
        if (permissions == null) {
            permissions = JsonObject.EMPTY;
            cache.put(PERMISSIONS, permissions);
        }
        return permissions.get(path, defaultIfNull);
    }

    private void updateCache(ApiUriInfo apiUriInfo, JsonArray path, JsonValue value) {
        Cache<String, JsonObject> cache = cacheProvider.getCache(apiUriInfo.apiName(), CacheProvider.PERMISSION_CACHE);
        cache.put(PERMISSIONS, cache.get(PERMISSIONS).put(path, value));
    }

    @Override
    public boolean deleteIsOk(ApiUriInfo apiUriInfo, JsonArray rolesSortedByName, Integer contextDepth) {
        Boolean deleteIsOk = null;

        Action action = getAction(Method.DELETE, apiUriInfo);
        JsonArray cachePath = JsonArray.of(apiUriInfo.apiName(), apiUriInfo.collectionName(), action.name(), contextDepth.toString()).addAll(rolesSortedByName).add("value");
        deleteIsOk = getFromCache(apiUriInfo, cachePath, JsonValue.NULL).getValue(Boolean.class);

        if (deleteIsOk == null) {

            deleteIsOk = !getPermissionsAsJsonObject(apiUriInfo.apiName(), rolesSortedByName, contextDepth)
                    .get(apiUriInfo.collectionName(), JsonObject.EMPTY)
                    .get(action.name(), JsonObject.EMPTY)
                    .getValue(JsonObject.class)
                    .isEmpty();


            JsonObject nesting = getFromCache(apiUriInfo, cachePath.allButLast(), JsonObject.EMPTY.jsonValue()).getValue(JsonObject.class);
            updateCache(apiUriInfo, cachePath.allButLast(), nesting.put("value", deleteIsOk).jsonValue());
        }
        return deleteIsOk;

    }

    @Override
    public JsonArray getRequestPermissionFormValue(Method method, ApiUriInfo apiUriInfo, JsonArray rolesSortedByName, Integer contextDepth) {
        JsonArray result = null;

        if (isSuperUser(rolesSortedByName)) {
            result = method == Method.GET ? superUserQueryPermissionFormValue : superUserWriteItemPermissionFormValue;
        } else {

            Action action = getAction(method, apiUriInfo);

            JsonArray cachePath = JsonArray.of(apiUriInfo.apiName(), apiUriInfo.collectionName(), action.name(), contextDepth.toString()).addAll(rolesSortedByName).add("value");
            result = getFromCache(apiUriInfo, cachePath, JsonValue.NULL).getValue(JsonArray.class);

            if (result == null) {

                JsonObject permissions = getPermissionsAsJsonObject(apiUriInfo.apiName(), rolesSortedByName, contextDepth);

                if (action == Action.WRITE_ITEM) {
                    result = permissions
                            .get(apiUriInfo.collectionName(), JsonObject.EMPTY)
                            .get(Action.WRITE_ITEM.name(), JsonObject.EMPTY)
                            .get("form", JsonObject.EMPTY)
                            .get("value", defaultWriteItemPermissionFormValue)
                            .getValue(JsonArray.class);

                } else if (action == Action.QUERY_ITEM) {
                    result = permissions
                            .get(apiUriInfo.collectionName(), JsonObject.EMPTY)
                            .get(Action.QUERY_ITEM.name(), JsonObject.EMPTY)
                            .get("form", JsonObject.EMPTY)
                            .get("value", defaultQueryPermissionFormValue)
                            .getValue(JsonArray.class);

                } else if (action == Action.QUERY_COLLECTION) {
                    result = permissions
                            .get(apiUriInfo.collectionName(), JsonObject.EMPTY)
                            .get(Action.QUERY_COLLECTION.name(), JsonObject.EMPTY)
                            .get("form", JsonObject.EMPTY)
                            .get("value", defaultQueryPermissionFormValue)
                            .getValue(JsonArray.class);

                } else {
                    throw new RuntimeException("action not supported");
                }
                JsonObject nesting = getFromCache(apiUriInfo, cachePath.allButLast(), JsonObject.EMPTY.jsonValue()).getValue(JsonObject.class);
                updateCache(apiUriInfo, cachePath.allButLast(), nesting.put("value", result).jsonValue());

            }
        }

        return result;
    }

    @Override
    public JsonArray getResponsePermissionFormValue(Method method, ApiUriInfo apiUriInfo, JsonArray rolesSortedByName, Integer contextDepth) {

        JsonArray result;
        JsonObject permissions = getPermissionsAsJsonObject(apiUriInfo.apiName(), rolesSortedByName, contextDepth);

        if (isSuperUser(rolesSortedByName)) {
            result = superUserReadPermissionFormValue;
        } else {

            Action action = method == Method.POST || apiUriInfo.isItem() ? Action.READ_ITEM : Action.READ_COLLECTION;

            JsonArray cachePath = JsonArray.of(apiUriInfo.apiName(), apiUriInfo.collectionName(), action.name(), contextDepth.toString()).addAll(rolesSortedByName).add("value");
            result = getFromCache(apiUriInfo, cachePath, JsonValue.NULL).getValue(JsonArray.class);

            if (result == null) {

                if (action == Action.READ_ITEM) {
                    String collectionName = apiUriInfo.collectionName();
                    JsonArray formFieldsOfBaseObject = permissions
                            .get(collectionName, JsonObject.EMPTY)
                            .get(Action.READ_ITEM.name(), JsonObject.EMPTY)
                            .get("form", JsonObject.EMPTY)
                            .get("value", defaultReadItemPermissionFormValue)
                            .getValue(JsonArray.class);

                    result = formFieldsOfBaseObject;
                } else {
                    JsonArray collectionFormValue = permissions
                            .get(apiUriInfo.collectionName(), JsonObject.EMPTY)
                            .get(Action.READ_COLLECTION.name(), JsonObject.EMPTY)
                            .get("form", JsonObject.EMPTY)
                            .get("value", defaultReadCollectionPermissionFormValue)
                            .getValue(JsonArray.class);

                    int collectionFormValueBodyIndex = collectionFormValue.getKeyValueIndex("name", JsonValue.of("body"));

                    JsonArray collectionBodyFormValue = collectionFormValue.get(JsonArray.of(collectionFormValueBodyIndex, "form", "value"), JsonValue.NULL).getValue(JsonArray.class);

                    if (collectionBodyFormValue == null) {
                        result = collectionFormValue;
                    } else {

                        JsonArray formValueOfPermissionOfItemResponse =
                                getResponsePermissionFormValue(Method.GET, ApiUriInfo.ofUri(apiUriInfo.collectionNameUri() + "/DUMMYITEM"), rolesSortedByName, contextDepth);

                        int collectionFormValueOfItemsIndex = collectionFormValue.get(JsonArray.EMPTY.add(collectionFormValueBodyIndex).add("form").add("value"), JsonArray.EMPTY).getValue(JsonArray.class).getKeyValueIndex("name", JsonValue.of("value"));

                        int itemFormValueBodyIndex = formValueOfPermissionOfItemResponse.getKeyValueIndex("name", JsonValue.of("body"));

                        JsonArray itemFormValue = formValueOfPermissionOfItemResponse.get(itemFormValueBodyIndex, JsonObject.EMPTY).get(JsonArray.EMPTY.add("form").add("value"), JsonArray.EMPTY.jsonValue()).getValue(JsonArray.class);

                        if (itemFormValue.isEmpty()) {
                            result = collectionFormValue.put(JsonArray.EMPTY
                                            .add(collectionFormValueBodyIndex)
                                            .add("form")
                                            .add("value")
                                            .add(collectionFormValueOfItemsIndex),
                                    JsonObject.EMPTY
                                            .put("name", "value")
                                            .put("type", "array")
                                            .put("etype", "object"));
                        } else {
                            result = collectionFormValue.put(
                                    JsonArray.EMPTY
                                            .add(collectionFormValueBodyIndex)
                                            .add("form")
                                            .add("value")
                                            .add(collectionFormValueOfItemsIndex),
                                    JsonObject.EMPTY
                                            .put("name", "value")
                                            .put("type", "array")
                                            .put("eform", JsonObject.EMPTY
                                                    .put("value", itemFormValue)));
                        }
                    }
                }
            }
            JsonObject nesting = getFromCache(apiUriInfo, cachePath.allButLast(), JsonObject.EMPTY.jsonValue()).getValue(JsonObject.class);
            updateCache(apiUriInfo, cachePath.allButLast(), nesting.put("value", result).jsonValue());
        }
        return result;
    }

    private boolean isSuperUser(JsonArray rolesSortedByName) {
        return JsonArray.of("SU").equals(rolesSortedByName);
    }

    protected JsonObject mergePermissionsForRole(final JsonObject permissionsForRoleToBePatched, JsonObject permissionsForRole) {
        JsonObject patchedPermissionsForRole = permissionsForRoleToBePatched;
        JsonArray collectionKeys = permissionsForRole.keys();
        JsonArray collectionValues = permissionsForRole.values();

        for (int collectionIndex = 0; collectionIndex < permissionsForRole.size(); collectionIndex++) {
            String collectionName = collectionKeys.get(collectionIndex).getValue(String.class);
            JsonObject formPerPermissionType = collectionValues.get(collectionIndex).getValue(JsonObject.class);

            JsonArray permissionType = formPerPermissionType.keys();
            JsonArray forms = formPerPermissionType.values();
            for (int permissionTypeIndex = 0; permissionTypeIndex < formPerPermissionType.size(); permissionTypeIndex++) {

                JsonArray pathInMergedRolePermissions = JsonArray.of(collectionName, permissionType.get(permissionTypeIndex).getValue(), "form");
                JsonArray mergedFormFields = formMerger.merge(
                        patchedPermissionsForRole.get(pathInMergedRolePermissions, JsonObject.EMPTY).getValue(JsonObject.class).as(Form.class),
                        forms.get(JsonArray.of(permissionTypeIndex, "form"), JsonObject.EMPTY.jsonValue()).getValue(JsonObject.class).as(Form.class),
                        MergeStrategy.SOFTEN
                ).asJsonArray();

                patchedPermissionsForRole = patchedPermissionsForRole.put(pathInMergedRolePermissions.add("value"), mergedFormFields);

            }
        }
        return patchedPermissionsForRole;
    }

    private JsonObject getPermissionsPerRoleName(JsonArray rolePermissions, int contextDepth) {
        JsonObject asJsonObject = JsonObject.EMPTY;
        for (JsonValue rolePermission : rolePermissions.asList()) {

            if (rolePermission.get("matchingContextDepth", MAX_CONTEXT_DEPTH).asInteger() == contextDepth) {
                asJsonObject = asJsonObject.put(rolePermission.get("roleName").getValue(String.class), rolePermission);
            }
        }
        return asJsonObject;
    }

    public JsonObject getPermissionsAsJsonObject(String apiName, JsonArray roleNames, int contextDepth) {
        JsonObject result = null;
        
        JsonArray rolePermissions = getRolePermissions(apiName);
        JsonObject permissionsPerRoleName = getPermissionsPerRoleName(rolePermissions, contextDepth);
        JsonArray permissions;
        
        if (!roleNames.isEmpty()) {
            JsonObject mergedRolePermissions = JsonObject.EMPTY;

            for (int i = 0; i < roleNames.size(); i++) {
                JsonObject rolePermission = permissionsPerRoleName.get(roleNames.get(i).getValue(String.class), JsonObject.EMPTY).getValue(JsonObject.class);
                permissions = rolePermission.get("permissions", JsonArray.EMPTY).getValue(JsonArray.class);

                JsonObject permissionsForRole = permissionsAsJsonObject(permissions);
                mergedRolePermissions = mergePermissionsForRole(mergedRolePermissions, permissionsForRole);
                result = mergedRolePermissions;
            }
        }
        
        return result == null ? JsonObject.EMPTY : result;
    }

    private JsonObject permissionsAsJsonObject(JsonArray permissions) {
        JsonObject asJsonObject = JsonObject.EMPTY;
        for (JsonValue permission : permissions.asList()) {
            asJsonObject = asJsonObject.put(JsonArray.EMPTY.add(permission.get("collectionName")).add(permission.get("action")), permission);
        }
        return asJsonObject;
    }

    private Action getAction(Method method, ApiUriInfo apiUriInfo) {
        Action action;
        if (Method.GET == method) {
            if (apiUriInfo.isItem()) {

                action = Action.QUERY_ITEM;
            } else {
                action = Action.QUERY_COLLECTION;
            }
        } else if (Method.POST == method || Method.PATCH == method) {
            action = Action.WRITE_ITEM;
        } else if (Method.DELETE == method) {
            action = Action.DELETE_ITEM;
        } else {
            throw new RuntimeException("Method not supported");
        }

        return action;
    }

    private JsonArray getRolePermissions(String apiName) {
        return serviceConfigProvider.getServiceConfig(apiName).getRolePermissions();
    }

    private enum Action {
        QUERY_ITEM, QUERY_COLLECTION, DELETE_ITEM, WRITE_ITEM, READ_ITEM, READ_COLLECTION;
    }
}
