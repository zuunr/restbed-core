/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.authorization.claims;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

/**
 * <p>The RolesProvider is responsible for extracting
 * the roles from the provided claims.</p>
 * 
 * <p>Any translation of the claim must also be done
 * in order to get the roles on the correct format,
 * e.g. an array containing roles as strings:</p>
 * 
 * <p>["MEMBER", "ADMIN"]</p>
 *
 * @author Mikael Ahlberg
 */
public interface RolesProvider {
    
    /**
     * <p>Extracts the roles from the provided {@link JsonObject}.</p>
     * 
     * <p>If no roles are found, an {@link JsonArray#EMPTY} is returned.</p>
     * 
     * @param claims is the claims object to extract the roles from
     * @return an {@link JsonArray} containing the roles
     */
    JsonArray getRoles(JsonObject claims);

}
