/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.authorization.claims;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonObject;

/**
 * <p>The DefaultContextProvider provides a default 
 * context extractor given a claim.</p>
 * 
 * <p>The context must be a String placed at the configured
 * context claim namespace on the claim object.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class DefaultContextProvider implements ContextProvider {
    
    private ClaimsNamespaceConfig claimsNamespaceConfig;
    
    @Autowired
    public DefaultContextProvider(ClaimsNamespaceConfig claimsNamespaceConfig) {
        this.claimsNamespaceConfig = claimsNamespaceConfig;
    }
    
    @Override
    public String getContext(JsonObject claims) {
        return claims.get(claimsNamespaceConfig.getContextClaimNamespace(), "").getValue(String.class);
    }
}
