/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.authorization;

import com.zuunr.forms.FormFields;
import com.zuunr.forms.ValueFormat;
import com.zuunr.forms.filter.FormFilter;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.authorization.claims.ContextProvider;
import com.zuunr.restbed.core.authorization.claims.RolesProvider;
import com.zuunr.restbed.core.decorator.AuthorizationContextDecoratorDelegator;
import com.zuunr.restbed.core.decorator.AuthorizationContextDecoratorProvider;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.core.restservice.ServiceConfigProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Iterator;

/**
 * <p>The AccessController is responsible for authorizing requests
 * as well as filtering response.</p>
 *
 * @author Niklas Eldberger
 * @author Mikael Ahlberg
 */
@Component
public class AccessController {

    public static final JsonValue SUPER_USER = JsonValue.of("SU");
    private static final JsonValue STAR = JsonValue.of("*");
    private static final String PATHS_KEY = "paths";

    private final FormFilter formFilter = new FormFilter();

    private ContextAggregatorProvider contextAggregatorProvider;
    private PermissionProvider permissionProvider;
    private RolesProvider rolesProvider;
    private ContextProvider contextProvider;
    private ServiceConfigProvider serviceConfigProvider;
    private AuthorizationContextDecoratorDelegator decoratorDelegator;

    @Autowired
    public AccessController(
            ContextAggregatorProvider contextAggregatorProvider,
            PermissionProvider permissionProvider,
            ServiceConfigProvider serviceConfigProvider,
            RolesProvider rolesProvider,
            ContextProvider contextProvider,
            AuthorizationContextDecoratorProvider authorizationContextDecoratorProvider) {
        this.contextAggregatorProvider = contextAggregatorProvider;
        this.permissionProvider = permissionProvider;
        this.serviceConfigProvider = serviceConfigProvider;
        this.rolesProvider = rolesProvider;
        this.contextProvider = contextProvider;
        this.decoratorDelegator = new AuthorizationContextDecoratorDelegator(authorizationContextDecoratorProvider);
    }

    /**
     * <p>Authorize the provided request given the users claims (context and roles)
     * and provided effective context.</p>
     *
     * <p>If authorized the method will return null, otherwise a {@link Response}
     * containing an appropriate error will be returned.</p>
     *
     * @param request          is the users request to authorize
     * @param effectiveContext is the users effective context
     * @param claims           is the users retrieved context an roles
     * @return null if authorized, otherwise a response containing an error
     */
    public Response<JsonObjectWrapper> authorize(Request<JsonObjectWrapper> request, JsonArray effectiveContext, JsonObject claims) {
        if (claims.isEmpty()) {
            return Response.create(StatusCode._401_UNAUTHORIZED);
        }

        int contextDepth = effectiveContext.size();
        JsonArray roles = rolesProvider.getRoles(claims);

        if (request.getMethod() == Method.DELETE) {
            return authorizeDeleteRequest(request, claims, roles, contextDepth);
        } else {
            return authorizePostPatchGetRequest(request, claims, roles, contextDepth);
        }
    }

    private Response<JsonObjectWrapper> authorizeDeleteRequest(Request<JsonObjectWrapper> request, JsonObject claims, JsonArray roles, int contextDepth) {
        if (!isSuperUser(claims) && !permissionProvider.deleteIsOk(request.getApiUriInfo(), roles, contextDepth)) {
            return Response.create(StatusCode._403_FORBIDDEN);
        }

        return null;
    }

    private Response<JsonObjectWrapper> authorizePostPatchGetRequest(Request<JsonObjectWrapper> request, JsonObject claims, JsonArray roles, int contextDepth) {
        // Determine action of incoming request
        boolean isQuery = request.getMethod() == Method.GET;

        ValueFormat valueFormat = getValueFormatOfPermissionOfRequest(request, claims, contextDepth, roles, isQuery);

        // Authorize request
        JsonObject data = createRequestData(request, isQuery);
        JsonObject waste = formFilter.filter(valueFormat, data, false, true).wasteAsJsonValue().getValue(JsonObject.class);

        JsonArray wastePaths = waste.get(PATHS_KEY, JsonArray.EMPTY).getValue(JsonArray.class);

        if (wastePaths.isEmpty()) {
            return null;
        } else {
            return createForbiddenResponse(wastePaths, isQuery);
        }
    }

    private Response<JsonObjectWrapper> createForbiddenResponse(JsonArray wastePaths, boolean isQuery) {
        JsonObject errorsPerPath = errorsPerPath(wastePaths);

        if (queryNotAllowedWithOrWithoutQueryParams(isQuery, errorsPerPath)) {
            // This is when default permission for query is applied. No relevant path to provide to the client
            return Response.<JsonObjectWrapper>create(StatusCode._403_FORBIDDEN).body(JsonObjectWrapper.of(JsonObject.EMPTY.put("status", 403)));
        }

        return Response.<JsonObjectWrapper>create(StatusCode._403_FORBIDDEN)
                .body(JsonObjectWrapper.of(JsonObject.EMPTY
                        .put("code", isQuery ? "QUERY_PARAMS_ERROR" : "REQUEST_BODY_ERROR")
                        .put(isQuery ? "queryParamsError" : "requestBodyError", JsonObject.EMPTY.put(PATHS_KEY, errorsPerPath))
                        .put("status", 403)));
    }

    private ValueFormat getValueFormatOfPermissionOfRequest(Request<JsonObjectWrapper> request, JsonObject claims, int contextDepth, JsonArray roles, boolean isQuery) {
        JsonArray permissionFormValue = permissionProvider.getRequestPermissionFormValue(request.getMethod(), request.getApiUriInfo(), roles, contextDepth);

        if (isSuperUser(claims)) {
            if (isQuery) {
                permissionFormValue = PermissionProvider.superUserQueryPermissionFormValue;
            } else {
                permissionFormValue = PermissionProvider.superUserWriteItemPermissionFormValue;
            }
        }
        return permissionFormValue.as(FormFields.class).asExclusiveForm().asObjectValueFormat();
    }

    private JsonObject createRequestData(Request<JsonObjectWrapper> request, boolean isQuery) {
        return isQuery
                ? JsonObject.EMPTY.put("queryParams", request.getApiUriInfo().queryParams())
                : JsonObject.EMPTY.put("body", request.getBodyAsJsonObject());
    }

    private boolean queryNotAllowedWithOrWithoutQueryParams(boolean isQuery, JsonObject errorsPerPath) {
        return isQuery && errorsPerPath.size() == 1 && JsonArray.EMPTY.jsonValue().equals(
                errorsPerPath.get(JsonArray.of("", "violations", "supportedParams")));
    }

    /**
     * <p>Filters the response according to the users privileges, e.g. context and roles.</p>
     *
     * <p>Any error in the response body is also filtered according to the users
     * privileges (context and role).</p>
     *
     * @param exchange         contains the response to filter as well as the corresponding request
     * @param effectiveContext is the users effective context
     * @param claims           is the users retrieved context an roles
     * @return a filtered response
     */
    public Response<JsonObjectWrapper> filterResponseBody(Exchange<JsonObjectWrapper> exchange, JsonArray effectiveContext, JsonObject claims) {
        int contextDepth = effectiveContext.size();
        JsonArray roles = rolesProvider.getRoles(claims);

        JsonArray formValueOfPermissionOfResponse = permissionProvider.getResponsePermissionFormValue(exchange.getRequest().getMethod(), exchange.getRequest().getApiUriInfo(), roles, contextDepth);

        if (isErrorResponse(exchange.getResponse())) {
            return filterErrorResponseBody(exchange.getResponse(), formValueOfPermissionOfResponse);
        } else {
            return exchange.getResponse().body(JsonObjectWrapper.of(
                    filterSuccessfulResponseBody(exchange.getResponse(), formValueOfPermissionOfResponse)));
        }
    }

    private JsonObject filterSuccessfulResponseBody(Response<JsonObjectWrapper> response, JsonArray formValueOfPermissionOfResponse) {
        return filterSuccessfulResponseBody(response.getBodyAsJsonObject(), formValueOfPermissionOfResponse);
    }

    private JsonObject filterSuccessfulResponseBody(JsonObject responseBody, JsonArray formValueOfPermissionOfResponse) {
        JsonObject rootData = JsonObject.EMPTY.put("body", responseBody);
        JsonValue filtrate = formFilter.filter(formValueOfPermissionOfResponse, rootData, true, false).filtrateAsJsonValue();
        return filtrate.get("body").getJsonObject();
    }

    private boolean isErrorResponse(Response<JsonObjectWrapper> response) {
        return response.getStatusCode() >= 400;
    }

    private boolean isResourceStateError(Response<JsonObjectWrapper> response) {
        return response.getStatusCode() >= 400 && response.getStatusCode() <= 499 && response.getBody() != null && response.getBodyAsJsonObject().get("resourceStateError", JsonValue.NULL).getJsonObject() != null;
    }

    private Response<JsonObjectWrapper> filterErrorResponseBody(Response response, JsonArray formValueOfPermissionOfResponse) { // NOSONAR

        if (isResourceStateError(response)) {
            return filterResourceStateError(response, formValueOfPermissionOfResponse);
        } else {
            return response;
        }
    }

    private Response<JsonObjectWrapper> filterResourceStateError(Response response, JsonArray formValueOfPermissionOfResponse) { // NOSONAR

        JsonObject responseBody = response.getBodyAsJsonObject();

        JsonObject accessibleResourceState = filterSuccessfulResponseBody(responseBody.get("resourceState", JsonValue.NULL).getJsonObject(), formValueOfPermissionOfResponse);


        JsonArray pathsArray = JsonArray.of("resourceStateError", PATHS_KEY);

        JsonObjectBuilder pathsBuilder = JsonObject.EMPTY.builder();

        JsonObject pathsObject = responseBody.get(pathsArray, JsonObject.EMPTY).getJsonObject();

        Iterator<JsonValue> pathsIter = pathsObject.keys().iterator();
        for (JsonValue jsonValue : pathsObject.values()) {
            String pathString = pathsIter.next().getString();
            JsonArray path = JsonArray.ofDotSeparated(pathString);
            JsonObject pathValue = jsonValue.getJsonObject();
            JsonValue badValue = pathValue.get("badValue");

            JsonValue accessibleBadValue = accessibleResourceState.get(path);
            if (accessibleBadValue != null && accessibleBadValue.equals(badValue)) {
                pathsBuilder.put(pathString, pathValue);
            }
        }

        responseBody = responseBody.put(pathsArray, pathsBuilder.build()).remove("resourceState");

        return response.body(JsonObjectWrapper.of(responseBody));
    }

    /**
     * <p>If context in request claims is bank1.branch4.org36.user7 and resource
     * context is bank1.branch4.org666 then effective context is bank1.branch4.</p>
     *
     * <p>If context in request claims is bank1.*.org666.user6 and resource context
     * is bank1.branch4.org666 then effective context is bank1.branch4.org666.</p>
     *
     * @param claims  is the decrypted/parsed claims object from the request token
     * @param request is the request to extract the resource context from
     * @return an {@link JsonArray} containing the effective context
     */
    public Mono<JsonArray> getEffectiveContext(JsonObject claims, Request<JsonObjectWrapper> request) {
        if (isSuperUser(claims)) {
            return Mono.just(JsonArray.EMPTY);
        }

        String apiName = request.getApiUriInfo().apiName();
        String collectionTypeName = request.getApiUriInfo().collectionName();

        JsonObject authorizationConfig = serviceConfigProvider.getServiceConfig(apiName).getAuthorization(collectionTypeName);

        JsonObject contextConfig = authorizationConfig.get("context", JsonValue.NULL).getJsonObject();
        if (contextConfig != null) {

            Mono<JsonObject> decorated = Mono.just(request.asJsonObject())
                    .flatMap(model -> decoratorDelegator.decorate(
                            JsonObject.EMPTY, JsonObject.EMPTY
                                    .put("system", JsonObject.EMPTY.put("apiUrl", request.getApiUriInfo().apiUri()))
                                    .put("request", model)
                                    .put("claims", claims),
                            apiName,
                            collectionTypeName));

            JsonArray resourceContextTemplate = contextConfig.get("resourceContextTemplate", JsonArray.EMPTY).getJsonArray();
            JsonArray userContextTemplate = contextConfig.get("userContextTemplate", JsonArray.EMPTY).getJsonArray();

            return decorated.map(decoratedModel -> createContexts(userContextTemplate, resourceContextTemplate, decoratedModel)).map(this::createEffectiveContext);
        } else {

            String contextOfResource = getContextOfResource(request);

            if ("".equals(contextOfResource) || contextOfResource == null) {
                return Mono.just(JsonArray.EMPTY);
            }

            String authorizedContext = contextProvider.getContext(claims);

            JsonArray authorizedContextArray = JsonArray.ofDotSeparated(authorizedContext);
            JsonArray contextOfResourceArray = JsonArray.ofDotSeparated(contextOfResource);

            return Mono.just(createEffectiveContext(new Contexts(authorizedContextArray, contextOfResourceArray)));
        }
    }

    private Contexts createContexts(JsonArray userContextTemplate, JsonArray resourceContextTemplate, JsonObject model) {

        Contexts contexts = new Contexts(
                renderContextTemplate(resourceContextTemplate, model),
                renderContextTemplate(userContextTemplate, model));

        return contexts;
    }

    private JsonArray renderContextTemplate(JsonArray template, JsonObject model) {

        JsonArrayBuilder context = JsonArray.EMPTY.builder();
        for (JsonValue element: template) {
            //JsonPointer jsonPointer = new JsonPointer(element); // TODO: JsonPointer should be cached in JsonValue
            JsonValue modelValue = model.get(element.getJsonArray(), JsonValue.NULL);
            if (modelValue.isNull()) {
                return context.build();
            }
            context.add(modelValue);
        }
        return context.build();
    }

    private JsonArray createEffectiveContext(Contexts contexts) {
        JsonArray effectiveContext = null;

        JsonArray authorizedContextArray = contexts.userContext;
        JsonArray contextOfResourceArray = contexts.resourceContext;

        int i = 0;
        for (; i < contextOfResourceArray.size() && i < authorizedContextArray.size(); i++) {
            if (!STAR.equals(authorizedContextArray.get(i)) && !authorizedContextArray.get(i).equals(contextOfResourceArray.get(i))) {
                break;
            }
        }
        effectiveContext = contextOfResourceArray.subArray(0, i);

        return effectiveContext;
    }

    private String getContextOfResource(Request<JsonObjectWrapper> request) {
        String contextOfResource = null;
        if (request.getApiUriInfo().context() == null) {
            if (request.getMethod() == Method.GET && request.getApiUriInfo().isCollection()) {
                contextOfResource = contextAggregatorProvider
                        .getContextAggregator(request.getApiUriInfo().apiName(), request.getApiUriInfo().collectionName()).createContextFromCollectionQueryParams(request.getApiUriInfo().queryParams());

            } else if (request.getMethod() == Method.POST) {
                contextOfResource = contextAggregatorProvider
                        .getContextAggregator(request.getApiUriInfo().apiName(), request.getApiUriInfo().collectionName()).createContextFromRequestBody(request.getBodyAsJsonObject());

            }
        } else {
            contextOfResource = request.getApiUriInfo().context();
        }
        return contextOfResource;
    }

    private boolean isSuperUser(JsonObject claims) {
        return rolesProvider.getRoles(claims).get(0, JsonObject.EMPTY).equals(SUPER_USER);
    }

    private JsonObject errorsPerPath(JsonArray paths) {
        JsonObject result = JsonObject.EMPTY;
        for (JsonValue elem : paths.asList()) {
            JsonArray path = elem.get("path").getValue(JsonArray.class);
            if (isQueryParamAndDotsArePartOfName(path)) {
                result = result.put(path.get(1, "").getValue(String.class), elem.getValue(JsonObject.class).remove("path"));
            } else if (!path.isEmpty()) {
                result = result.put(path.subArray(1, paths.size()).asDotSeparatedString(), elem.getValue(JsonObject.class).remove("path"));
            }
        }
        return result;
    }

    private boolean isQueryParamAndDotsArePartOfName(JsonArray path) {
        return path.size() <= 2;
    }
}
