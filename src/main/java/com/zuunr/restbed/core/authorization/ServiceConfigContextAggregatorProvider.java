/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.authorization;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.restbed.core.cache.Cache;
import com.zuunr.restbed.core.restservice.ServiceConfigProvider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceConfigContextAggregatorProvider implements ContextAggregatorProvider {

    private static final String COLLECTION_NAME_KEY = "collectionName";
    
    private static final ContextAggregator alwaysNullContextAggregator = new ContextAggregator() {};

    private static final JsonObject DEFAULT_AGGREGATOR_CONFIG = JsonObject.EMPTY
            .put("default", JsonValue.TRUE)
            .put("contextAggregator", "com.restbed.authorization.SimpleContextAggregator")
            .put("template", JsonArray.EMPTY.add("context"));

    private static final String CACHE_KEY_OF_DEFAULT = "/DEFAULT_AGGREGATOR";

    private static final JsonArray PATH_TO_CONTEXT_AGGREGATORS =
            JsonArray.of("config", "security", "authorization", "contextAggregators");

    private final Cache<String, Cache<String, ContextAggregator>> cache = new Cache<>();

    private ServiceConfigProvider serviceConfigProvider;

    @Autowired
    public ServiceConfigContextAggregatorProvider(ServiceConfigProvider serviceConfigProvider) {
        this.serviceConfigProvider = serviceConfigProvider;
    }

    @Override
    public ContextAggregator getContextAggregator(String apiName, String collectionName) {
        ContextAggregator contextAggregator = null;

        JsonObject serviceConfig = serviceConfigProvider.getServiceConfig(apiName).asJsonObject();

        Cache<String, ContextAggregator> collectionCache = cache.get(apiName);
        
        if (collectionCache == null) {
            collectionCache = new Cache<>();

            JsonArray contextAggregators = serviceConfig.get(PATH_TO_CONTEXT_AGGREGATORS, JsonArray.EMPTY).getValue(JsonArray.class);
            JsonObject aggregatorsByCollectionNames = contextAggregatorsByCollectionName(contextAggregators);

            JsonObject defaultContextAggregator = aggregatorsByCollectionNames.get(CACHE_KEY_OF_DEFAULT, JsonValue.NULL).getValue(JsonObject.class);
            if (defaultContextAggregator != null) {
                aggregatorsByCollectionNames = aggregatorsByCollectionNames.put(JsonArray.of(CACHE_KEY_OF_DEFAULT, COLLECTION_NAME_KEY), CACHE_KEY_OF_DEFAULT);
            }

            for (JsonObject aggregatorConfig: aggregatorsByCollectionNames.values().asList(JsonObject.class)) {
                ContextAggregator aggregator = new SimpleContextAggregator(aggregatorConfig.get("template"));
                collectionCache.put(aggregatorConfig.get(COLLECTION_NAME_KEY).getValue(String.class), aggregator);
            }
        }

        contextAggregator = collectionCache.get(collectionName);

        if (contextAggregator == null) {
            contextAggregator = collectionCache.get(CACHE_KEY_OF_DEFAULT);
            if (contextAggregator == null) {
                contextAggregator = alwaysNullContextAggregator;
            }
            collectionCache = collectionCache.clone();
            collectionCache.put(collectionName, contextAggregator);
            cache.put(apiName, collectionCache);
        }
        return contextAggregator;
    }

    JsonObject contextAggregatorsByCollectionName(JsonArray source) {

        JsonObject defaultAggregator = source.asJsonObject(JsonArray.of("default")).get("true", DEFAULT_AGGREGATOR_CONFIG).getValue(JsonObject.class);
        return source.asJsonObject(JsonArray.of(COLLECTION_NAME_KEY)).put(CACHE_KEY_OF_DEFAULT, defaultAggregator);
    }
}
