/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.authorization.claims;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

/**
 * <p>Configuration for claims namespace.</p>
 *
 * @author Mikael Ahlberg
 */
@ConfigurationProperties(prefix = "zuunr.security.authorization.claims")
@Validated
public class ClaimsNamespaceConfig {
    
    @NotEmpty(message = "You must set the context claim namespace, e.g. https://restbed.zuunr.com/context")
    private final String contextClaimNamespace;
    
    @NotEmpty(message = "You must set the roles claim namespace, e.g. https://restbed.zuunr.com/roles")
    private final String rolesClaimNamespace;
    
    @ConstructorBinding
    public ClaimsNamespaceConfig(
            String contextClaimNamespace,
            String rolesClaimNamespace) {
        this.contextClaimNamespace = contextClaimNamespace;
        this.rolesClaimNamespace = rolesClaimNamespace;
    }

    public String getContextClaimNamespace() {
        return contextClaimNamespace;
    }

    public String getRolesClaimNamespace() {
        return rolesClaimNamespace;
    }
}
