package com.zuunr.restbed.core.authorization;

import com.zuunr.json.JsonArray;

/**
 * @author Niklas Eldberger
 */
public class Contexts {

    public final JsonArray userContext;
    public final JsonArray resourceContext;

    public Contexts(JsonArray userContext, JsonArray resourceContext) {
        this.userContext = userContext;
        this.resourceContext = resourceContext;
    }
}
