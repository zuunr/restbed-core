/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.authorization.claims;

import com.zuunr.json.JsonObject;

/**
 * <p>The ContextProvider is responsible for extracting
 * the context from the provided claims {@link JsonObject}.</p>
 * 
 * <p>The context must be on the format where every "subcontext"
 * is dot separated.</p>
 *
 * @author Mikael Ahlberg
 */
public interface ContextProvider {
    
    /**
     * <p>Extracts the context from the provided {@link JsonObject}.</p>
     * 
     * <p>If no context is found, an empty string is returned.</p>
     * 
     * @param claims is the claims object to extract the context from
     * @return a String containing the context
     */
    String getContext(JsonObject claims);

}
