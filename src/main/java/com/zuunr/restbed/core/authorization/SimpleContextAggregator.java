/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.authorization;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import reactor.core.publisher.Mono;

public class SimpleContextAggregator implements ContextAggregator {

    private JsonArray template;

    public SimpleContextAggregator(JsonArray template) {
        this.template = template;
    }

    public SimpleContextAggregator(JsonValue template) {
        this.template = template.getValue(JsonArray.class);
    }

    @Override
    public String createContextFromCollectionQueryParams(JsonObject collectionQueryParams) {
        return createContext("value._.", collectionQueryParams);
    }

    @Override
    public String createContextFromRequestBody(JsonObject requestBody) {
        return createContext("", requestBody);
    }

    private String createContext(String prefix, JsonObject model) {
        StringBuilder context = new StringBuilder();
        for (int i = 0; i < template.size(); i++) {
            if (i > 0) {
                context.append('.');
            }
            String val = model.get(prefix + template.get(i).getValue(String.class), JsonValue.NULL).getValue(String.class);
            if (val != null) {
                context.append(val);
            }
        }
        return context.toString();
    }

    @Override
    public Mono<Contexts> createContexts(JsonObject requestModel) {
        return ContextAggregator.super.createContexts(requestModel);
    }
}
