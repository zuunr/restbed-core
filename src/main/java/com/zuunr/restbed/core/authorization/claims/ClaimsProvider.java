/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.authorization.claims;

import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Request;

import reactor.core.publisher.Mono;

/**
 * <p>The ClaimsProvider interface is responsible for extracting
 * the claims from the provided request.</p>
 * 
 * <p>The implementor of this interface must return a
 * {@link JsonObject#EMPTY} if claims can not be read,
 * or if any exception occur when reading the claims in order to
 * be unauthorized.</p>
 * 
 * <p>If spring security is not explicitly used to authenticate the user,
 * this method must also validate the tokens authenticity.</p>
 * 
 * <p>Implementors of an {@link ClaimsProvider} must be careful not to block
 * any threads. Any blocking I/O events must be executed on a different thread pool.
 * For example the elastic thread pool provided by Reactor.</p>
 * 
 * <pre>{@link Mono}.fromCallable(this::myBlocking).subscribeOn(Schedulers.boundedElastic())</pre>
 *
 * @author Mikael Ahlberg
 */
public interface ClaimsProvider {
    
    /**
     * <p>Extracts the claims from the provided {@link Request}.</p>
     * 
     * <p>If any exception occur, e.g. if the token on the request is
     * invalid, an {@link JsonObject} is still returned.</p>
     * 
     * @param request is the request to extract the claims from
     * @return the claims as a {@link JsonObject}, or {@link JsonObject#EMPTY}
     */
    Mono<JsonObject> getClaims(Request<JsonObjectWrapper> request);
}
