package com.zuunr.restbed.core.data;

import com.zuunr.json.*;

/**
 * @author Niklas Eldberger
 */
public class TracedValueObject<T> implements JsonObjectSupport {

    private JsonValue value;
    private JsonValueSupport jsonValueSupport;
    private final JsonObject trace;
    private JsonObject asJsonObject;

    public TracedValueObject(JsonValue value, JsonObject trace) {
        this.value = value;
        this.trace = trace;
    }

    public TracedValueObject(JsonValue value, JsonObjectBuilder traceBuilder) {
        this.value = value;
        this.trace = traceBuilder == null ? null : traceBuilder.build();
    }

    public TracedValueObject(JsonArray value, JsonObject trace) {
        this.value = value.jsonValue();
        this.trace = trace;
    }

    public TracedValueObject(JsonArray value, JsonObjectBuilder traceBuilder) {
        this(value.jsonValue(), traceBuilder);
    }

    public TracedValueObject(JsonObject value, JsonObject trace) {
        this.value = value.jsonValue();
        this.trace = trace;
    }

    public TracedValueObject(JsonValueSupport value, JsonObject trace) {
        this(value.asJsonValue(), trace);
        this.jsonValueSupport = value;
    }

    public TracedValueObject(JsonValueSupport value, JsonObjectBuilder traceBuilder) {
        this(value, traceBuilder.build());
    }

    public T getValue() {
        return jsonValueSupport == null ? (T) value.getValue() : (T) jsonValueSupport;
    }

    public JsonObject getTrace() {
        return trace;
    }

    @Override
    public JsonObject asJsonObject() {
        if (asJsonObject == null) {
            asJsonObject = trace == null ?
                    JsonObject.EMPTY.put("value", value) :
                    JsonObject.EMPTY.put("value", value).put("trace", trace);
        }
        return asJsonObject;
    }

    @Override
    public JsonValue asJsonValue() {
        return asJsonObject().jsonValue();
    }
}
