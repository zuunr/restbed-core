package com.zuunr.restbed.core.data;

import com.zuunr.json.JsonValue;
import com.zuunr.json.JsonValueSupport;

/**
 * @author Niklas Eldberger
 */
public class ValueObject {

    protected final JsonValue value;


    public ValueObject(JsonValue value) {
        this.value = value;
    }

    public JsonValue getValue(){
        return value;
    }
}
