/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.ion;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.filter.FilterResult;
import com.zuunr.forms.filter.FormFilter;
import com.zuunr.forms.formfield.Type;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.forms.FormProvider;
import com.zuunr.restbed.core.forms.FormProviderKey;
import com.zuunr.restbed.core.restservice.*;

public class ServiceConfigFormProvider implements FormProvider {

    private static final int INDEX_OF_FROM_STATUS = 3;

    private FormFilter formFilter = new FormFilter();
    private ServiceConfigProvider serviceConfigProvider;

    public ServiceConfigFormProvider(ServiceConfigProvider serviceConfigProvider) {
        this.serviceConfigProvider = serviceConfigProvider;
    }

    protected JsonObject getServiceConfig(String apiName) {
        ServiceConfig serviceConfig = serviceConfigProvider
                .getServiceConfig(apiName);
        
        return serviceConfig == null ? JsonObject.EMPTY : serviceConfig.asJsonObject();
    }

    protected JsonObject getForms(String apiName) {
        return getServiceConfig(apiName)
                .get("forms", JsonObject.EMPTY)
                .getValue(JsonObject.class);
    }

    public FormFields getFormFieldsForResourceBodyRestricted(FormProviderKey key) {
        FormFields result = FormFields.EMPTY;

        Validation validation = getServiceConfig(key.apiName)
                .get(ServiceConfig.pathToConfigValidations, JsonArray.EMPTY).as(Validations.class)
                .getValidation(key.collectionName);

        if (validation != null) {
            for (ResourceValidation resourceValidation : validation.resourceValidations()
                    .asList()) {
                Form form = resourceValidation.matcherForm();

                FormField rootFormField = FormField.builder("root").form(form).type(Type.OBJECT).build();
                FilterResult<JsonObjectWrapper> filterResult = formFilter.filter(Form.EMPTY.builder().value(JsonArray.of(rootFormField.asJsonObject()).as(FormFields.class)).build(), key.data, false, true);
                if (filterResult.waste.isEmpty()) {
                    result = resourceValidation.validationForm().formFields();
                    break;
                }
            }
            
            if (result == null) {
                result = validation.defaultValidationForm().formFields();
            }
        }
        
        return result;
    }

    @Override
    public JsonArray getForm(FormProviderKey key){
        JsonArray result;
        if (key.formType == FormProviderKey.FormType.RESOURCE_BODY_RESTRICTED) {
            result = getFormFieldsForResourceBodyRestricted(key).asJsonArray();
        } else {
            result = getForm(key, JsonValue.NULL);
        }
        return result;
    }

    private JsonArray getForm(FormProviderKey key, JsonValue defaultIfMissing) {
        return getForms(key.apiName).get(getPathToFormFor(key), defaultIfMissing).getValue(JsonArray.class);
    }


    @Override
    public JsonArray getValidStatusTransitions(String apiName, String collectionName) {
        JsonArray pathToFromStatuses = JsonArray.of(apiName, collectionName, FormProviderKey.FormType.REQUEST_BODY.name()).remove(0);
        JsonObject fromAndTo = getForms(apiName).get(pathToFromStatuses, JsonValue.NULL).getValue(JsonObject.class);

        return validStatusTransitions(fromAndTo);
    }

    public static JsonArray validStatusTransitions(JsonObject fromToForms) {
        //NOSONAR {"from": {"to": {...form...}}}

        JsonArray transitions = JsonArray.EMPTY;
        if (fromToForms != null) {

            JsonArray keys = fromToForms.keys();
            JsonArray values = fromToForms.values();

            for (int i = 0; i < keys.size(); i++) {
                JsonValue fromJsonValue = keys.get(i);
                JsonObject tosAndForms = values.get(i).getValue(JsonObject.class);
                
                for (JsonValue to : tosAndForms.keys().asList()) {
                    transitions = transitions.add(JsonObject.EMPTY.put("from", fromJsonValue).put("to", to));
                }
            }
        }
        return transitions;
    }

    protected JsonArray getPathToFormFor(FormProviderKey key) {
        JsonArray result;
        if (key.formType == FormProviderKey.FormType.REQUEST_BODY) {
            result = key.asJsonArray;
        } else if (key.formType == FormProviderKey.FormType.RESOURCE_BODY) {
            result = key.asJsonArray.remove(INDEX_OF_FROM_STATUS);
        } else if (key.formType == FormProviderKey.FormType.ITEM_QUERY_PARAMS ||
                key.formType == FormProviderKey.FormType.COLLECTION_QUERY_PARAMS ||
                key.formType == FormProviderKey.FormType.RESOURCE_MODEL) {

            result = key.asJsonArray
                    .remove(INDEX_OF_FROM_STATUS)
                    .remove(INDEX_OF_FROM_STATUS); // On purpose removing the same index twice makes both from and to removed!
        } else {
            throw new RuntimeException("Cannot get you a good key!");
        }
        return result.remove(0);
    }
}
