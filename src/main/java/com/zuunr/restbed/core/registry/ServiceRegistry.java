/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.registry;

/**
 * <p>The ServiceRegistry is responsible for determining which services
 * are deployed inside this JVM runtime.</p>
 * 
 * @author Mikael Ahlberg
 */
public interface ServiceRegistry {

    /**
     * <p>Checks to see if the provided url is matching a service running inside this
     * JVM.</p>
     * 
     * @param url is the url of the service endpoint to check
     * @return true or false depending on if the service is running inside the JVM
     */
    boolean isServiceDeployedInJVM(String url);
    
}
