/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.registry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.restservice.ServiceConfig;
import com.zuunr.restbed.core.restservice.ServiceConfigProvider;

/**
 * <p>Primary implementation of {@link ServiceRegistry}. The DefaultServiceRegistry
 * determines which services are running inside this JVM.</p>
 * 
 * @see ServiceRegistry
 * 
 * @author Mikael Ahlberg
 */
@Component
public class DefaultServiceRegistry implements ServiceRegistry {

	private ServiceConfigProvider serviceConfigProvider;

	@Autowired
	public DefaultServiceRegistry(ServiceConfigProvider serviceConfigProvider) {
		this.serviceConfigProvider = serviceConfigProvider;
	}

	@Override
    public boolean isServiceDeployedInJVM(String url) {
        // In standalone-mode (non tool) the service config is returned regardless of the api name
        ServiceConfig serviceConfig = serviceConfigProvider.getServiceConfig(null);
        
        String collectionName = ApiUriInfo.ofUri(url).collectionName();
            
        return serviceConfig.getForms().get(collectionName) != null;
    }
}
