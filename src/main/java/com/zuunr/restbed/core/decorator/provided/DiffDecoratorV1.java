package com.zuunr.restbed.core.decorator.provided;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.restbed.core.decorator.AbstractDecorator;
import com.zuunr.restbed.core.decorator.model.ResourceModel;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * @author Niklas Eldberger
 */

@Component
public class DiffDecoratorV1 extends AbstractDecorator {

    private final static String DIFF_FROM_PREVIOUS = "diffFromPrevious";
    public final static JsonArray DIFF_FROM_PREVIOUS_PATH = ResourceModel.INTERNAL.add(DIFF_FROM_PREVIOUS);


    @Override
    public Mono<JsonObject> decorate(JsonObject resource, JsonObject configuration) {

        JsonObject resourceBeforeUpdate = resource.get(ResourceModel.INTERNAL_RESOURCE_BEFORE_UPDATE, JsonObject.EMPTY).getJsonObject();

        JsonValue diffFromPrevious = resource
                .remove(ResourceModel.INTERNAL)
                .diff(resourceBeforeUpdate == null
                        ? JsonObject.EMPTY
                        : resourceBeforeUpdate)
                .get("expansion", JsonObject.EMPTY);

        resource = resource.put(DIFF_FROM_PREVIOUS_PATH, diffFromPrevious);

        return Mono.just(resource);
    }
}
