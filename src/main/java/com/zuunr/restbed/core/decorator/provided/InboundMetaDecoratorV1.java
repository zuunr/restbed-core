/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.provided;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.restbed.core.decorator.AbstractDecorator;

import reactor.core.publisher.Mono;

/**
 * <p>This decorator places the following data on the
 * resource under an object named 'meta'.</p>
 * 
 * <p>It is important that no other decorator is creating
 * a 'createdAt' object, or else this decorator will not work
 * as expected.</p>
 * 
 * <ul>
 * <li>createdAt</li>
 * <li>modifiedAt</li>
 * </ul>
 *
 * @author Mikael Ahlberg
 */
@Component
public class InboundMetaDecoratorV1 extends AbstractDecorator {

    private static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    @Override
    public Mono<JsonObject> decorate(JsonObject resource, JsonObject configuration) {
        String currentTime = OffsetDateTime.now(ZoneOffset.UTC)
                .truncatedTo(ChronoUnit.MILLIS)
                .format(DATETIME_FORMATTER);
        
        return Mono.just(JsonObject.EMPTY)
                .map(o -> isNewResource(resource)
                        ? o.put(JsonArray.of("meta", "createdAt"), JsonValue.of(currentTime)).put(JsonArray.of("meta", "modifiedAt"), JsonValue.of(currentTime))
                        : o.put(JsonArray.of("meta", "modifiedAt"), JsonValue.of(currentTime)));
    }
    
    private boolean isNewResource(JsonObject resource) {
        return resource.get(JsonArray.ofDotSeparated("meta.createdAt"), JsonValue.NULL).isNull();
    }
}
