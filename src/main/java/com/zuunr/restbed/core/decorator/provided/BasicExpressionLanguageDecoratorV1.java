/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.provided;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.restbed.core.decorator.AbstractDecorator;
import com.zuunr.restbed.core.decorator.provided.expression.DecorationExpressionException;
import com.zuunr.restbed.core.decorator.provided.expression.JsonObjectPropertyAccessor;
import com.zuunr.restbed.core.decorator.provided.expression.JsonValuePropertyAccessor;
import com.zuunr.restbed.core.decorator.provided.expression.ResourceRetriever;
import com.zuunr.restbed.core.decorator.provided.expression.ResourceRetrieverProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.SpelCompilerMode;
import org.springframework.expression.spel.SpelParserConfiguration;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.Optional;

/**
 * <p>This decorator enables expression language and service calls to other services.</p>
 *
 * @author Mikael Ahlberg
 * @author Niklas Eldberger
 */
@Component
public class BasicExpressionLanguageDecoratorV1 extends AbstractDecorator {

    private static final String EXPRESSION_TARGET_PATH = "target";
    private static final String EXPRESSION_RESOURCE_PATH = "resource";
    private static final String EXPRESSION_PATH = "expression";

    private final SpelParserConfiguration config =
            new SpelParserConfiguration(SpelCompilerMode.IMMEDIATE, this.getClass().getClassLoader());
    private final ExpressionParser expressionParser = new SpelExpressionParser(config);
    private final StandardEvaluationContext evaluationContext = new StandardEvaluationContext();

    private ResourceRetrieverProvider resourceRetrieverProvider;

    @Autowired
    public BasicExpressionLanguageDecoratorV1(
            ResourceRetrieverProvider resourceRetrieverProvider) {
        this.resourceRetrieverProvider = resourceRetrieverProvider;
    }
    
    @PostConstruct
    public void initialize() {
        // Thread safe if we don't provide any root object
        evaluationContext.addPropertyAccessor(new JsonObjectPropertyAccessor());
        evaluationContext.addPropertyAccessor(new JsonValuePropertyAccessor());
        //evaluationContext.setTypeLocator(new GuardedTypeLocator());
        //evaluationContext.setConstructorResolvers(new ArrayList<>());
        //evaluationContext.addMethodResolver(new GuardedMethodResolver());
    }
    
    @Override
    public Mono<JsonObject> decorate(JsonObject resource, JsonObject configuration) {

        JsonArray outputModelLocation = configuration.get("outputModelLocation").getJsonArray();
        return Flux.fromIterable(configuration.get("expressions", JsonArray.EMPTY).as(JsonArray.class))
                .map(o -> o.as(JsonObject.class))
                .flatMapSequential(this::decorate, 1)
                .last(JsonObject.EMPTY)
                .map(context -> context.get(outputModelLocation, JsonObject.EMPTY).as(JsonObject.class))
                .contextWrite(contextHolder -> contextHolder.put(StateContainer.class, new StateContainer(resource)));
    }
    
    /**
     * <p>The context contains resource data available for the expressions,
     * e.g. decorationBody, resourceBody, any other created targets.</p>
     * 
     * <p>The returned {@link JsonObject} will be the context object,
     * with any modified data according to the evaluated expression. It is
     * used in following expression decorations as the "new context" object.</p>
     * 
     * <p>The decorationBody part inside the context object will later be
     * the returned decoration for the resource.</p>
     * 
     * @param expression is the expression object to evaluate
     * @return an updated context object
     */
    private Mono<JsonObject> decorate(JsonObject expression) {
        return Mono.subscriberContext()
                .flatMap(contextHolder -> {
                    JsonObject context = contextHolder.get(StateContainer.class).getState();
                    String target = expression.get(EXPRESSION_TARGET_PATH).getString();
                    
                    return Mono.just(context)
                            .flatMap(o -> shouldEvaluateExpression(expression, o)
                                    ? Mono.just(o)
                                    : Mono.empty())
                            .flatMap(o -> isResourceExpression(expression)
                                    ? Mono.<JsonObject>from(retrieveResource(expression.get(EXPRESSION_RESOURCE_PATH).as(JsonObject.class), o))
                                            .map(result -> context.put(JsonArray.ofDotSeparated(target), result))
                                    : Mono.<JsonValue>from(evaluateExpression(expression, o))
                                            .map(result -> context.put(JsonArray.ofDotSeparated(target), result)))
                            .map(updatedContext -> {
                                contextHolder.get(StateContainer.class).setState(updatedContext);
                                return updatedContext;
                            })
                            .onErrorMap(e -> errorMapper(e, expression, context));
                });
    }
    
    private boolean shouldEvaluateExpression(JsonObject expression, JsonObject context) {
        String condition = expression.get("condition", "").getString();
        
        if (condition.isEmpty()) {
            return true;
        }
        
        return Optional.ofNullable(evaluate(condition, context))
                .map(JsonValue::getBoolean)
                .orElseThrow();
    }
    
    private boolean isResourceExpression(JsonObject expression) {
        return expression.containsKey(EXPRESSION_RESOURCE_PATH);
    }
    
    private Mono<JsonValue> evaluateExpression(JsonObject expression, JsonObject context) {
        String expressionString = expression.get(EXPRESSION_PATH).getString();
        
        return Mono.justOrEmpty(evaluate(expressionString, context));
    }
    
    private Mono<JsonObject> retrieveResource(JsonObject resourceExpression, JsonObject context) {
        String url = resourceExpression.get("url", JsonValue.NULL).getString();
        String type = resourceExpression.get("resourceRetriever").getString();
        String queryExpression = resourceExpression.get("queryExpression").getString();

        String renderedUrl;

        if (url != null && !url.startsWith("$")) {
            renderedUrl = Optional.ofNullable(evaluate(url, context))
                    .orElse(JsonValue.EMPTY_STRING).getString();
        } else {
            renderedUrl = url;
        }

        JsonValue queryParams = Optional.ofNullable(evaluate(queryExpression, context))
                .orElse(JsonValue.EMPTY_STRING);
        
        ResourceRetriever resourceRetriever = resourceRetrieverProvider.getResourceRetriever(type);


        if (renderedUrl == null || renderedUrl.isEmpty()){
            return Mono.just(JsonObject.EMPTY);
        } else {
            return resourceRetriever.retrieve(renderedUrl, queryParams.getString());
        }
    }
    
    private JsonValue evaluate(String expressionString, JsonObject context) {
        Expression parsedExpression = expressionParser.parseExpression(expressionString);
        return parsedExpression.getValue(evaluationContext, context, JsonValue.class);
    }
    
    private Throwable errorMapper(Throwable originalCause, JsonObject expression, JsonObject context) {
        return new DecorationExpressionException("Failed to evaluate expression in decoration", originalCause, expression, context);
    }

    private static class StateContainer {
        private JsonObject state;

        private StateContainer(JsonObject state) {
            setState(state);
        }

        private JsonObject getState() {
            return state;
        }

        private void setState(JsonObject state) {
            this.state = state;
        }
    }
}
