/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.provided;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.decorator.AbstractDecorator;
import com.zuunr.restbed.core.exchange.Collection;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.repository.ReactiveRepository;

import reactor.core.publisher.Mono;

/**
 * <p>This decorator is a simple validator for resource uniqueness.</p>
 * 
 * <p>It does so by fetching resources given configured search patterns
 * (query parameters). If a match occur, and its 'id' is not equal to the
 * new/updated resource's 'id', then the resource is not unique.</p>
 * 
 * <p>Whenever a resource is not unique, a parameter of your choice will
 * be set to true on the decorated resource.</p>
 * 
 * <p>Configuration example:</p>
 * 
 * <p>{ 'parameters': [ 'name.firstName', 'name.lastName' ], 'resourceExistsParameter': 'nameIsTaken' }</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class InboundUniquenessVerifierDecoratorV1 extends AbstractDecorator {
    
    private ReactiveRepository<JsonObjectWrapper> reactiveRepository;
    
    @Autowired
    public InboundUniquenessVerifierDecoratorV1(
            ReactiveRepository<JsonObjectWrapper> reactiveRepository) {
        this.reactiveRepository = reactiveRepository;
    }

    @Override
    public Mono<JsonObject> decorate(JsonObject resource, JsonObject configuration) {
        return Mono.just(resource)
                .flatMap(o -> checkUniqueness(o, configuration))
                .onErrorMap(e -> errorMapper(e, configuration, resource));
    }
    
    private Mono<JsonObject> checkUniqueness(JsonObject resource, JsonObject configuration) {
        String collectionNameUri = ApiUriInfo.ofUri(resource.get("href").getString())
                .collectionNameUri();
        String resourceExistsParameter = Optional.ofNullable(configuration.get("resourceExistsParameter"))
                .orElseThrow(() -> new IllegalArgumentException("The 'resourceExistsParameter' configuration is missing"))
                .getString();
        JsonArray parameters = Optional.ofNullable(configuration.get("parameters"))
                .orElseThrow(() -> new IllegalArgumentException("The 'parameters' array configuration is missing"))
                .as(JsonArray.class);
        
        return Mono.just(parameters)
                .map(o -> getQueryParams(o, resource))
                .flatMap(o -> getResources(collectionNameUri, o))
                .map(o -> isResourceUnique(o, resource)
                        ? JsonObject.EMPTY
                        : JsonObject.EMPTY.put(JsonArray.ofDotSeparated(resourceExistsParameter), true));
    }

    private String getQueryParams(JsonArray parameters, JsonObject resource) {
        StringBuilder queryParamsBuilder =  new StringBuilder("?");
        
        for (JsonValue parameterName : parameters) {
            String queryParam = createQueryParam(resource, parameterName.getString());
            
            queryParamsBuilder.append(queryParam);
            queryParamsBuilder.append("&");
        }
        
        return queryParamsBuilder.deleteCharAt(queryParamsBuilder.length() - 1)
                .toString();
    }
    
    private String createQueryParam(JsonObject resource, String parameterName) {
        return "value._." + parameterName + "=" + Optional.ofNullable(resource.get(JsonArray.ofDotSeparated(parameterName)))
            .orElseThrow(() -> new IllegalArgumentException("The specified parameter: " + parameterName + ", does not exist on the resource"))
            .getString();
    }

    private Mono<Exchange<Collection<JsonObjectWrapper>>> getResources(String url, String queryParams) {
        Exchange<Collection<JsonObjectWrapper>> exchange = Exchange.<Collection<JsonObjectWrapper>>empty()
                .request(Request.<Collection<JsonObjectWrapper>>create(Method.GET)
                        .url(url + queryParams));
        
        return Mono.from(reactiveRepository.getCollection(exchange, JsonObjectWrapper.class));
    }
    
    private boolean isResourceUnique(Exchange<Collection<JsonObjectWrapper>> exchange, JsonObject resource) {
        Collection<JsonObjectWrapper> collection = exchange.getResponse().getBody();
        int bodySize = collection.getSize();
        
        if (bodySize > 1) {
            return false;
        }
        
        if (bodySize == 1) {
            String responseId = collection.getValue().get(0).asJsonObject().get("id").getString();
            
            if (!responseId.equals(resource.get("id").getString())) {
                return false;
            }
        }
        
        return true;
    }
    
    private Throwable errorMapper(Throwable originalCause, JsonObject configuration, JsonObject resource) {
        return new DecorationException("Failed to read configuration in decoration", originalCause, configuration, resource);
    }
}
