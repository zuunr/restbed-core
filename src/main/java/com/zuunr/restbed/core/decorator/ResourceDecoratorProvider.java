/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator;

import com.zuunr.json.JsonValue;
import com.zuunr.restbed.core.cache.CacheProvider;
import com.zuunr.restbed.core.decorator.model.DecoratorContainer;
import com.zuunr.restbed.core.restservice.DecorationConfig;
import com.zuunr.restbed.core.restservice.ServiceConfigProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>The DecoratorProvider is responsible for retrieving
 * the list of decorators attached to a specific collection.</p>
 *
 * @author Mikael Ahlberg
 * @author Niklas Eldberger
 */
@Component
public final class ResourceDecoratorProvider extends DecoratorProvider {

    @Autowired
    public ResourceDecoratorProvider(
            ServiceConfigProvider serviceConfigProvider,
            CacheProvider cacheProvider,
            DecoratorFactory decoratorFactory) {
        super(serviceConfigProvider, cacheProvider, CacheProvider.DECORATOR_CACHE, decoratorFactory);
    }

    protected List<DecoratorContainer> findDecorators(String apiName, String collectionName) {

        DecorationConfig decorationConfig = serviceConfigProvider.getServiceConfig(apiName).getDecorationConfig();
        List<DecoratorContainer> decorators = new ArrayList<>();

        for (JsonValue decoratorCandidate : decorationConfig.decorators.asList()) {
            String collectionNameCandidate = decoratorCandidate.get("collectionName", JsonValue.NULL).getValue(String.class);

            if (collectionName.equals(collectionNameCandidate)) {
                DecoratorContainer decoratorContainer = createDecoratorContainer(decoratorCandidate);

                decorators.add(decoratorContainer);
            }
        }
        return decorators;
    }
}