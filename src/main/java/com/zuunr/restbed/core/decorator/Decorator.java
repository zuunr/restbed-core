/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator;

import com.zuunr.json.JsonObject;

import reactor.core.publisher.Mono;

/**
 * <p>A Decorator is used to decorate the resource
 * with data that was not provided from the start.</p>
 * 
 * <p>The decorator should not merge the decoration
 * with the provided resource, only return the decoration data.
 * The decoration is merged by the framework at a later stage.</p>
 * 
 * <p>Implementors of a {@link Decorator} must be careful not to block
 * any threads. Any blocking I/O events must be executed on a different thread pool.
 * For example the elastic thread pool provided by Reactor.</p>
 * 
 * <pre>{@link Mono}.fromCallable(this::myBlocking).subscribeOn(Schedulers.boundedElastic())</pre>
 *
 * @author Niklas Eldberger
 * @author Mikael Ahlberg
 */
public interface Decorator {

    /**
     * <p>Returns a {@link Mono} {@link JsonObject} containing the decorated data.</p>
     * 
     * <p>The decorated data must only contain the decoration, and must not
     * be merged with the resource. The decoration is merged with the
     * resource at a later stage.</p>
     * 
     * <p>The configuration object is used for input placed in the service
     * scheme configuration file.</p>
     * 
     * @param resource is the fully extended resource
     * @param configuration is an {@link JsonObject} containing decorator configurations
     * @return the decoration as a mono object
     */
    Mono<JsonObject> decorate(JsonObject resource, JsonObject configuration);
    
}
