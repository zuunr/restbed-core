/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonValue;
import com.zuunr.restbed.core.cache.CacheProvider;
import com.zuunr.restbed.core.decorator.model.DecoratorContainer;
import com.zuunr.restbed.core.restservice.ServiceConfigProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>The DecoratorProvider is responsible for retrieving
 * the list of decorators attached to a specific collection.</p>
 *
 * @author Mikael Ahlberg
 * @author Niklas Eldberger
 */
@Component
public final class AuthorizationContextDecoratorProvider extends DecoratorProvider {

    @Autowired
    public AuthorizationContextDecoratorProvider(
            ServiceConfigProvider serviceConfigProvider,
            CacheProvider cacheProvider,
            DecoratorFactory decoratorFactory) {

        super(serviceConfigProvider, cacheProvider, CacheProvider.SECURITY_CONTEXT_DECORATOR_CACHE, decoratorFactory);
    }

    protected List<DecoratorContainer> findDecorators(String apiName, String collectionName) {

        JsonArray decoratorsOfCollectionType = serviceConfigProvider.getServiceConfig(apiName).getSecurityContextDecorators(collectionName);

        List<DecoratorContainer> decorators = new ArrayList<>();

        for (JsonValue decoratorJsonValue : decoratorsOfCollectionType) {
            DecoratorContainer decoratorContainer = createDecoratorContainer(decoratorJsonValue);
            decorators.add(decoratorContainer);
        }
        return decorators;
    }
}
