package com.zuunr.restbed.core.decorator.provided;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.restbed.core.decorator.AbstractDecorator;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * @author Niklas Eldberger
 */

@Component
public class StaticJsonDecoratorV1 extends AbstractDecorator {

    @Override
    public Mono<JsonObject> decorate(JsonObject resource, JsonObject configuration) {

        JsonPointer pointer = configuration.get("targetLocation", JsonValue.NULL).as(JsonPointer.class);
        JsonValue value = configuration.get("value");
        if (pointer != null && value != null) {
            JsonArray pointerArray = pointer.asArray();
            resource = resource.put(pointerArray, value);
        }
        return Mono.just(resource);
    }
}
