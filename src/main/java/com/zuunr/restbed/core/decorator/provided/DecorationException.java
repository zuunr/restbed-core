/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.provided;

import com.zuunr.json.JsonObject;

public class DecorationException extends RuntimeException {
    
    private static final long serialVersionUID = 1L;
    
    private final transient JsonObject configuration;
    private final transient JsonObject context;

    public DecorationException(String message, Throwable cause, JsonObject configuration, JsonObject context) {
        super(message, cause);
        this.configuration = configuration;
        this.context = context;
    }

    public JsonObject getConfiguration() {
        return configuration;
    }
    
    public JsonObject getContext() {
        return context;
    }
}
