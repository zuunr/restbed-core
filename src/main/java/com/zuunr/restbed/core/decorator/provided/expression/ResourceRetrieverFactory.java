/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.provided.expression;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

/**
 * <p>The ResourceRetrieverFactory keeps track of instantiated {@link ResourceRetriever}
 * objects given their full class name.</p>
 * 
 * @author Mikael Ahlberg
 */
@Component
public class ResourceRetrieverFactory {

    private final Map<String, ResourceRetriever> resourceRetrievers = new ConcurrentHashMap<>();

    /**
     * <p>Register a {@link ResourceRetriever} inside this factory.</p>
     * 
     * <p>If a {@link ResourceRetriever} of the same class already has been registered,
     * an {@link IllegalStateException} is thrown.</p>
     * 
     * @param resourceRetriever is the resource retriever to register
     */
    public void registerResourceRetriever(ResourceRetriever resourceRetriever) {
        String classType = resourceRetriever.getClass().getName();

        if (resourceRetrievers.containsKey(classType)) {
            throw new IllegalStateException("A resource retriver of the same type has already been registered: " + classType);
        }

        resourceRetrievers.put(classType, resourceRetriever);
    }

    /**
     * <p>Retrieve an already instantiated {@link ResourceRetriever} of the provided class
     * type, e.g. getClass().getName()</p>
     * 
     * <p>If no {@link ResourceRetriever} of the given id can be found an
     * {@link IllegalStateException} is thrown.</p>
     * 
     * @param classType is the full class name of the event processor
     * @return a resource retriever
     */
    public ResourceRetriever getResourceRetriever(String classType) {
        return Optional.ofNullable(resourceRetrievers.get(classType))
                .orElseThrow(() -> new IllegalStateException("No resource retriever of given type has been registered: " + classType));
    }
}
