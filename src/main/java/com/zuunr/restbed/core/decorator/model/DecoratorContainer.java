/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.model;

import com.zuunr.json.JsonObject;
import com.zuunr.restbed.core.decorator.Decorator;

/**
 * <p>DecoratorContainer is a wrapper for the {@link Decorator}.
 * It contains the decorator as well as any configuration
 * attached to the decorator.</p>
 *
 * @author Mikael Ahlberg
 */
public class DecoratorContainer {
    
    private Decorator decorator;
    private JsonObject configuration;

    private DecoratorContainer(Builder builder) {
        this.decorator = builder.decorator;
        this.configuration = builder.configuration;
    }

    /**
     * <p>Get the {@link Decorator} implementation.</p>
     * 
     * @return a {@link Decorator} implementation
     */
    public Decorator getDecorator() {
        return decorator;
    }

    /**
     * <p>Get the attached configuration part for this decorator.</p>
     * 
     * @return a {@link JsonObject} containing the configuration
     */
    public JsonObject getConfiguration() {
        return configuration;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private Decorator decorator;
        private JsonObject configuration;

        private Builder() {
        }

        public Builder decorator(Decorator decorator) {
            this.decorator = decorator;
            return this;
        }

        public Builder configuration(JsonObject configuration) {
            this.configuration = configuration;
            return this;
        }

        public DecoratorContainer build() {
            return new DecoratorContainer(this);
        }
    }
}
