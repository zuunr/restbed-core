/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.restbed.core.cache.CacheProvider;
import com.zuunr.restbed.core.decorator.model.DecoratorContainer;
import com.zuunr.restbed.core.restservice.ServiceConfig;
import com.zuunr.restbed.core.restservice.ServiceConfigProvider;

import java.util.List;

/**
 * <p>The DecoratorProvider is responsible for retrieving
 * the list of decorators attached to a specific collection.</p>
 *
 * @author Mikael Ahlberg
 * @author Niklas Eldberger
 */

public abstract class DecoratorProvider {

    protected final ServiceConfigProvider serviceConfigProvider;
    protected final CacheProvider cacheProvider;
    private CacheProvider.CacheType<List> cacheType;
    protected final DecoratorFactory decoratorFactory;

    protected DecoratorProvider(
            ServiceConfigProvider serviceConfigProvider,
            CacheProvider cacheProvider,
            CacheProvider.CacheType<List> cacheType,
            DecoratorFactory decoratorFactory) {
        this.serviceConfigProvider = serviceConfigProvider;
        this.cacheProvider = cacheProvider;
        this.cacheType = cacheType;
        this.decoratorFactory = decoratorFactory;
    }

    /**
     * <p>Returns a list of {@link DecoratorContainer}s configured in the {@link ServiceConfig}
     * given the provided apiName and collectionName.</p>
     *
     * @param apiName        is the api name
     * @param collectionName is the collection name used to identify the configured decorators
     * @return a list of decorators wrapped inside a container
     */
    public List<DecoratorContainer> getDecorators(String apiName, String collectionName) {
        @SuppressWarnings("unchecked")
        List<DecoratorContainer> decorators = cacheProvider.getCache(apiName, cacheType).get(collectionName);

        if (decorators == null) {
            decorators = findDecorators(apiName, collectionName);
            cacheProvider.getCache(apiName, cacheType).put(collectionName, decorators);
        }

        return decorators;
    }

    protected abstract List<DecoratorContainer> findDecorators(String apiName, String collectionName);

    protected DecoratorContainer createDecoratorContainer(JsonValue decoratorCandidate) {
        String decoratorClass = decoratorCandidate.get("decorator", JsonValue.NULL).getValue(String.class);
        JsonObject configuration = decoratorCandidate.get("configuration", JsonValue.NULL).getValue(JsonObject.class);

        Decorator decorator = decoratorFactory.getDecorator(decoratorClass);

        return DecoratorContainer.builder()
                .decorator(decorator)
                .configuration(configuration)
                .build();
    }
}
