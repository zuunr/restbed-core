/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.provided.expression;

import org.springframework.expression.AccessException;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.PropertyAccessor;
import org.springframework.expression.TypedValue;

import com.zuunr.json.JsonValue;

/**
 * <p>Enables the use of {@link JsonValue} as a {@link PropertyAccessor}
 * so it can be used in Spring Expression Language.</p>
 * 
 * <p>Values can only be read from the JsonValue. Writing is
 * not supported.</p>
 *
 * @author Mikael Ahlberg
 * @author Niklas Eldberger
 */
public class JsonValuePropertyAccessor implements PropertyAccessor {
    
    @Override
    public Class<?>[] getSpecificTargetClasses() {
        return new Class<?>[] {JsonValue.class};
    }
    
    @Override
    public boolean canRead(EvaluationContext context, Object target, String name) throws AccessException {
        return true;
    }
    
    @Override
    public TypedValue read(EvaluationContext context, Object target, String name) throws AccessException {
        return new TypedValue(((JsonValue) target).get(name, JsonValue.NULL).getValue());
    }
    
    @Override
    public boolean canWrite(EvaluationContext context, Object target, String name) throws AccessException {
        return false;
    }
    
    @Override
    public void write(EvaluationContext context, Object target, String name, Object newValue) throws AccessException {
        // Do nothing
    }
}
