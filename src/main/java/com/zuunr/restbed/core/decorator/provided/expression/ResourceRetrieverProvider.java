/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.provided.expression;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zuunr.restbed.core.restservice.ServiceConfigProvider;

/**
 * <p>The ResourceRetrieverProvider fetches the corresponding {@link ResourceRetriever}
 * given a provided class type.</p>
 * 
 * <p>In tool mode, only the {@link DefaultResourceRetriever} is returned to call
 * simulated collections.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class ResourceRetrieverProvider {
    
    private ResourceRetrieverFactory resourceRetrieverFactory;
    private ServiceConfigProvider serviceConfigProvider;
    
    @Autowired
    public ResourceRetrieverProvider(
            ResourceRetrieverFactory resourceRetrieverFactory,
            ServiceConfigProvider serviceConfigProvider) {
        this.resourceRetrieverFactory = resourceRetrieverFactory;
        this.serviceConfigProvider = serviceConfigProvider;
    }
    
    /**
     * <p>Returns a {@link ResourceRetriever} given the provided class type.</p>
     * 
     * @param classType is the type to return the resource retriever for
     * @return a resource retriever
     */
    public ResourceRetriever getResourceRetriever(String classType) {
        // FIXME Fix a better way of determining if we are running in tool mode
        if (serviceConfigProvider.getServiceConfig(null) != null) {
            return resourceRetrieverFactory.getResourceRetriever(classType);
        }
        
        return resourceRetrieverFactory.getResourceRetriever(DefaultResourceRetriever.class.getName());
    }
}
