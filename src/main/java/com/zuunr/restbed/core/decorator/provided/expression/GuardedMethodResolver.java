package com.zuunr.restbed.core.decorator.provided.expression;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.AccessException;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.MethodExecutor;
import org.springframework.expression.MethodResolver;
import org.springframework.expression.spel.support.ReflectiveMethodResolver;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;

/**
 * @author Niklas Eldberger
 */
public class GuardedMethodResolver implements MethodResolver {

    private ReflectiveMethodResolver internal = new ReflectiveMethodResolver();

    private static final HashSet<String> validStaticTypeNames;
    private static final HashSet<String> validInstanceTypeNames;

    static {
        validStaticTypeNames = new HashSet<>();
        validStaticTypeNames.add(String.class.getName());
        validStaticTypeNames.add(JsonValue.class.getName());
        validStaticTypeNames.add(JsonObject.class.getName());
        validStaticTypeNames.add(JsonArray.class.getName());
        validStaticTypeNames.add(Integer.class.getName());
        validStaticTypeNames.add(BigDecimal.class.getName());

        validInstanceTypeNames = new HashSet<>();
        validInstanceTypeNames.add(String.class.getName());
        validInstanceTypeNames.add(JsonValue.class.getName());
        validInstanceTypeNames.add(JsonObject.class.getName());
        validInstanceTypeNames.add(JsonArray.class.getName());
        validInstanceTypeNames.add(Integer.class.getName());
        validInstanceTypeNames.add(BigDecimal.class.getName());

    }

    @Override
    public MethodExecutor resolve(EvaluationContext evaluationContext, Object object, String method, List<TypeDescriptor> list) throws AccessException {

        boolean isStaticMethod = object.getClass() == Class.class;
        Class<?> objectClass = object.getClass();

        if (isStaticMethod) {
            if (validStaticType(((Class<?>) object).getName())) {
                return internal.resolve(evaluationContext, object, method, list);
            }
        } else if (validInstanceType(objectClass.getName())) {
            return internal.resolve(evaluationContext, object, method, list);
        }

        throw new AccessException(object.toString() + "." + method + " is not allowed");
    }

    private boolean validStaticType(String name) {
        return validStaticTypeNames.contains(name);
    }

    private boolean validInstanceType(String name) {
        return validInstanceTypeNames.contains(name);
    }
}
