package com.zuunr.restbed.core.decorator.provided;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectMerger;
import com.zuunr.json.JsonValue;
import com.zuunr.restbed.core.decorator.AbstractDecorator;
import com.zuunr.restbed.core.decorator.DecoratorFactory;
import com.zuunr.restbed.core.decorator.model.ResourceModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Niklas Eldberger
 */

@Component
public class SequentialDecoratorV1 extends AbstractDecorator {

    private static final JsonObjectMerger jsonObjectMerger = new JsonObjectMerger();

    @Autowired
    public SequentialDecoratorV1(DecoratorFactory decoratorFactory) {
        super.decoratorFactory = decoratorFactory;
    }

    @Override
    public Mono<JsonObject> decorate(JsonObject resource, JsonObject jsonObjectConfiguration) {

            SequentialDecoratorConfigV1 configuration = jsonObjectConfiguration.as(SequentialDecoratorConfigV1.class);

        Mono<JsonObject> updatedModelMono = decorateSequence(jsonObjectConfiguration);

        if (configuration.isRepeatable()) {

            updatedModelMono = updatedModelMono //decorateSequence(configuration)
                    .expand(neverUsed -> decorateLoop(configuration, configuration.getRepeatPredicateLocation())) // runs decoratorLoop until it returns Mono.empty()
                    .last(JsonObject.EMPTY);
        }

        return updatedModelMono
                .contextWrite(ctxHolder -> ctxHolder.put(StateContainer.class, new StateContainer(resource)));
    }

    private Mono<JsonObject> decorateLoop(SequentialDecoratorConfigV1 configuration, JsonArray repeatPredicateLocation) {

        return Mono.deferContextual(Mono::just).flatMap(context -> {
            StateContainer stateContainer = context.get(StateContainer.class);
            JsonObject state = stateContainer.getState();
            if (state.get(repeatPredicateLocation, JsonValue.FALSE).getBoolean() && stateContainer.iterationsDone() < configuration.getRepeatMax()) {
                return decorateSequence(configuration.asJsonObject());
            } else {
                return Mono.empty();
            }
        });
    }

    private Mono<JsonObject> decorateSequence(JsonObject configuration) {

        return Flux.fromIterable(configuration.get("sequence", JsonArray.EMPTY).as(JsonArray.class))
                .map(decoratorItem -> decoratorItem.getJsonObject())
                .flatMapSequential(decoratorItem -> delegateDecoration(decoratorItem), 1)
                .last(JsonObject.EMPTY);
    }

    public Mono<JsonObject> delegateDecoration(JsonObject decoratorItem) {

        return Mono.deferContextual(Mono::just).flatMap(context -> {
            JsonObject state = context.get(StateContainer.class).getState();
            return Mono.just(state)
                    .flatMap(resource ->
                            Mono.from(decoratorFactory
                                    .getDecorator(decoratorItem.get("decorator").getString())
                                    .decorate(resource, decoratorItem.get("configuration", JsonObject.EMPTY).getJsonObject())
                                    .map(decoration -> {
                                        JsonObject updatedResource = jsonObjectMerger.merge(resource.remove(ResourceModel.INTERNAL_DIFF_FROM_PREVIOUS), decoration);
                                        context.get(StateContainer.class).setNewState(updatedResource);
                                        return updatedResource;
                                    })));
        });
    }

    private static class StateContainer {
        private JsonObject state;
        private int iterations = 0;

        private StateContainer(JsonObject state) {
            setState(state);
        }

        private int iterationsDone(){
            return iterations;
        }

        private JsonObject getState() {
            return state;
        }

        private void setNewState(JsonObject state){
            iterations++;
            setState(state);
        }

        private void setState(JsonObject state) {
            this.state = state;
        }
    }
}
