/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.provided.expression;

import com.zuunr.json.JsonObject;

import reactor.core.publisher.Mono;

/**
 * <p>A ResourceRetriever fetches a desired resource from an
 * internal or external system and transforms it before returning
 * the data. The data can then be used in following expressions
 * to decorate the original resource.</p>
 *
 * @author Mikael Ahlberg
 */
public interface ResourceRetriever {
    
    /**
     * <p>The method must return the resource data as a {@link Mono} {@link JsonObject} for
     * following expressions to perform logic on.</p>
     * 
     * <p>The data will be placed at the provided "target" path in the expressions
     * configuration.</p>
     * 
     * @param url is mainly used in generating the services and should be ignored in generated applications
     * @param queryParams contains the parameters needed to fulfill this request
     * @return a {@link Mono} containing a {@link JsonObject} with the required parts of the resource
     */
    Mono<JsonObject> retrieve(String url, String queryParams);
    
}
