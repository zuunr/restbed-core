/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectMerger;
import com.zuunr.restbed.core.decorator.model.DecoratorContainer;
import com.zuunr.restbed.core.decorator.provided.DiffDecoratorV1;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * <p>The DecoratorDelegator is responsible for executing
 * the attached decorators to the resource.</p>
 *
 * <p>The class also merges the decorations with the resource
 * before it is returned to the caller.</p>
 *
 * @author Mikael Ahlberg
 * @author Niklas Eldberger
 */

public class DecoratorDelegator {

    private final JsonObjectMerger merger = new JsonObjectMerger();

    private DecoratorProvider decoratorProvider;

    @Autowired
    public DecoratorDelegator(DecoratorProvider decoratorProvider) {
        this.decoratorProvider = decoratorProvider;
    }

    /**
     * <p>Decorates the provided resource ({@link JsonObject}) by merging
     * all decorations applied to the collection, and then merging the
     * complete set of decorations with the actual resource.</p>
     *
     * @param resource         is the merged resource (e.g. request + saved resource)
     * @param extendedResource is a fully extended resource and only used as input to decoration
     * @param apiName          is the api name
     * @return a mono {@link JsonObject} containing the decorated resource, ready to be saved
     */
    public Mono<JsonObject> decorate(JsonObject resource, JsonObject extendedResource, String apiName, String collectionName) {
        return Flux.fromIterable(decoratorProvider.getDecorators(apiName, collectionName))
                .flatMapSequential(o -> decorate(extendedResource, o), 1)
                .collectList()
                .map(o -> mergeDecorations(resource, o));
    }

    private Mono<JsonObject> decorate(JsonObject extendedResource, DecoratorContainer decoratorContainer) {
        Decorator decorator = decoratorContainer.getDecorator();
        JsonObject configuration = decoratorContainer.getConfiguration();

        return decorator.decorate(extendedResource, configuration);
    }

    private JsonObject mergeDecorations(JsonObject resource, List<JsonObject> decorations) {
        JsonObject mergedDecorations = JsonObject.EMPTY;

        for (JsonObject decoration : decorations) {
            mergedDecorations = merger.merge(mergedDecorations, decoration);
        }

        // Remove diff from previous before merging - otherwise we get the full state instead of diff
        JsonObject result = merger.merge(resource.remove(DiffDecoratorV1.DIFF_FROM_PREVIOUS_PATH), mergedDecorations);
        return result;
    }
}
