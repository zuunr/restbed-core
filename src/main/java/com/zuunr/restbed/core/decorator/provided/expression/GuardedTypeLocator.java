package com.zuunr.restbed.core.decorator.provided.expression;

import org.springframework.expression.TypeLocator;
import org.springframework.expression.spel.support.StandardTypeLocator;

/**
 * @author Niklas Eldberger
 */
public class GuardedTypeLocator implements TypeLocator {

    private static final StandardTypeLocator internal = new StandardTypeLocator();

    @Override
    public Class<?> findType(String className) {
        return internal.findType(className);
    }
}
