package com.zuunr.restbed.core.decorator.model;

import com.zuunr.json.JsonArray;

/**
 * Describes the full model of a business agnostic resource (i.e the parts of the model that does mot change because of functional requirements)
 * @author Niklas Eldberger
 */
public class ResourceModel {

    public static final JsonArray INTERNAL = JsonArray.of("_internal");
    public static final JsonArray INTERNAL_ETAG = INTERNAL.add("etag");
    public static final JsonArray INTERNAL_RESOURCE_BEFORE_UPDATE = INTERNAL.add("resourceBeforeUpdate");
    public static final JsonArray INTERNAL_DIFF_FROM_PREVIOUS = INTERNAL.add("diffFromPrevious");

}
