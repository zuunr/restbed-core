package com.zuunr.restbed.core.decorator.provided;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class SequentialDecoratorConfigV1 {

    private static final JsonValue MAX_DEFAULT = JsonValue.of(10);

    private static final JsonArray PREDICATE = JsonArray.of("repeat", "predicateLocation");
    private static final JsonArray MAX = JsonArray.of("repeat", "max");

    private JsonValue configuration; // self

    private Integer repeatMax;
    private JsonArray repeatPredicateLocation;

    private SequentialDecoratorConfigV1(JsonValue configuration) {
        this.configuration = configuration;
    }

    public JsonObject asJsonObject(){
        return configuration.getJsonObject();
    }

    public int getRepeatMax() {
        if (repeatMax == null) {
            repeatMax = configuration.get(MAX, MAX_DEFAULT).getJsonNumber().intValue();
        }
        return repeatMax;
    }

    public JsonArray getRepeatPredicateLocation() {

        if (repeatPredicateLocation == null) {
            repeatPredicateLocation = configuration.get(PREDICATE, JsonArray.EMPTY.jsonValue()).getJsonArray();
        }

        if (repeatPredicateLocation.isEmpty()){
            return null;
        } else {
            return repeatPredicateLocation;
        }
    }

    public boolean isRepeatable(){
        return getRepeatPredicateLocation() != null;
    }

}
