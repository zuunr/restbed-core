/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.provided.expression.util;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

/**
 * <p>The ContextInitializer is responsible for setting up
 * the context object that is passed between expressions.</p>
 * 
 * <p>There are some defaults set up, like the resource
 * as well as current time.</p>
 * 
 * <ul>
 * <li>resourceBody: the resource</li>
 * <li>env.currentTime: the current time in ISO-8601 format.</li>
 * </ul>
 *
 * @author Mikael Ahlberg
 */
@Component
public class ContextInitializer {
    
    private static final JsonArray RESOURCE_BODY_PATH = JsonArray.of("resourceBody");
    private static final JsonArray ENV_CURRENT_TIME = JsonArray.of("env", "currentTime");
    
    /**
     * <p>Creates the context used for expressions, containing
     * the resource and current time.</p>
     * 
     * @param resource is the resource to place on the context object
     * @return a context {@link JsonObject}
     */
    public JsonObject initializeResourceBodyDecorationContext(JsonObject resource) {
        String currentTime = OffsetDateTime.now(ZoneOffset.UTC)
                .truncatedTo(ChronoUnit.MILLIS)
                .format(DateTimeFormatter.ISO_DATE_TIME);
        
        return initializeContext(RESOURCE_BODY_PATH, resource)
                .put(ENV_CURRENT_TIME, currentTime);
    }

    public JsonObject initializeContext(JsonArray basepath, JsonObject resource) {
        return JsonObject.EMPTY.put(basepath, resource);
    }
}
