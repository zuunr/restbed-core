/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

/**
 * <p>The DecoratorFactory keeps track of instantiated {@link Decorator}
 * objects given their full class name.</p>
 * 
 * @author Mikael Ahlberg
 */
@Component
public class DecoratorFactory {

    private final Map<String, Decorator> decorators = new ConcurrentHashMap<>();

    /**
     * <p>Register a {@link Decorator} inside this factory.</p>
     * 
     * <p>If a {@link Decorator} of the same class already has been registered,
     * an {@link IllegalStateException} is thrown.</p>
     * 
     * @param decorator is the decorator to register
     */
    public void registerDecorator(Decorator decorator) {
        String classType = decorator.getClass().getName();

        if (decorators.containsKey(classType)) {
            throw new IllegalStateException("A decorator of the same type has already been registered: " + classType);
        }

        decorators.put(classType, decorator);
    }

    /**
     * <p>Retrieve an already instantiated {@link Decorator} of the provided class
     * type, e.g. getClass().getName()</p>
     * 
     * <p>If no {@link Decorator} of the given type can be found an
     * {@link IllegalStateException} is thrown.</p>
     * 
     * @param classType is the full class name of the event processor
     * @return a decorator
     */
    public Decorator getDecorator(String classType) {
        return Optional.ofNullable(decorators.get(classType))
                .orElseThrow(() -> new IllegalStateException("No decorator of given type has been registered: " + classType));
    }
}
