/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectMerger;
import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.decorator.model.DecoratorContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * <p>TODO</p>
 * 
 * @author Niklas Eldberger
 */
@Component
public class AuthorizationContextDecoratorDelegator extends DecoratorDelegator {

    @Autowired
    public AuthorizationContextDecoratorDelegator(AuthorizationContextDecoratorProvider decoratorProvider) {
        super(decoratorProvider);
    }
}
