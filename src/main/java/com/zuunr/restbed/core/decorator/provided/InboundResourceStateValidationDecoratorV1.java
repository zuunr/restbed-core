package com.zuunr.restbed.core.decorator.provided;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.restbed.core.decorator.AbstractDecorator;
import com.zuunr.restbed.core.validation.SchemaProvider;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * @author Niklas Eldberger
 */

@Component
public class InboundResourceStateValidationDecoratorV1 extends AbstractDecorator {


    @Override
    public Mono<JsonObject> decorate(JsonObject resource, JsonObject configuration) {

        JsonValue schemaLocation = configuration.get("schemaLocation");
        JsonPointer locationOfSchemaInResourceBody = schemaLocation.as(JsonPointer.class);
        JsonArray pathToSchemaInResourceBody = locationOfSchemaInResourceBody.asArray();
        JsonValue schema = resource.get(pathToSchemaInResourceBody, false);
        JsonValue schemaKeywordPropertyLocation = configuration.get("schemaKeywordPropertyLocation", "");

        JsonArray resourceValidation = resource.get(SchemaProvider.PATH_TO_VALIDATIONS_IN_MODEL, JsonArray.EMPTY).getJsonArray()
                .add(JsonObject.EMPTY
                        .put("schema", schema)
                        .put("schemaKeywordPropertyLocation", schemaKeywordPropertyLocation));

        JsonObject updatedResource = resource.put(SchemaProvider.PATH_TO_VALIDATIONS_IN_MODEL, resourceValidation);

        JsonValue cleanup = configuration.get("postDecorationSchemaLocationRemove", true);

        if (cleanup.isBoolean() && cleanup.getBoolean()) {
            updatedResource = updatedResource.remove(pathToSchemaInResourceBody);
        }
        return Mono.just(updatedResource);
    }
}
