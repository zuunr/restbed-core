package com.zuunr.restbed.core.validation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.pointer.JsonPointer;
import com.zuunr.restbed.core.forms.FormProvider;
import com.zuunr.restbed.core.forms.FormProviderKey;
import com.zuunr.restbed.core.ion.ServiceConfigFormProvider;
import com.zuunr.restbed.core.restservice.ServiceConfig;
import com.zuunr.restbed.core.restservice.ServiceConfigProvider;

/**
 * @author Niklas Eldberger
 */
public class SchemaProvider implements FormProvider {

    public static JsonValue SCHEMA_OF_EMPTY_OBJECT = JsonObject.EMPTY
            .put("type", "object")
            .put("additionalProperties", false)
            .jsonValue();
    public static JsonArray PATH_TO_VALIDATIONS_IN_MODEL = JsonArray.of("_internal", "resourceValidation");

    public static final JsonObject EXCLUDE_VALIDATIONS_IN_MODEL = schemaWhereValidationIsNotPersisted();

    private FormProvider formProvider;
    private ServiceConfigProvider serviceConfigProvider;

    public SchemaProvider(ServiceConfigProvider serviceConfigProvider, FormProvider formProvider) {
        this.serviceConfigProvider = serviceConfigProvider;
        this.formProvider = formProvider;
    }

    @Override
    public JsonArray getForm(FormProviderKey formProviderKey) {
        return formProvider.getForm(formProviderKey);
    }

    @Override
    public JsonArray getValidStatusTransitions(String apiName, String collectionName) {

        JsonArray schemaStyleTransitions = getValidStatusTransistionsSchemaStyle(apiName, collectionName);
        return schemaStyleTransitions == null ? formProvider.getValidStatusTransitions(apiName, collectionName) : schemaStyleTransitions;
    }

    public JsonArray getValidStatusTransistionsSchemaStyle(String apiName, String collectionType) {

        JsonObject validation = serviceConfigProvider.getServiceConfig(apiName)
                .getConfigConfig().asJsonObject()
                .get("collections", JsonObject.EMPTY)
                .get(collectionType, JsonObject.EMPTY)
                .get("validation", JsonObject.EMPTY).getJsonObject();
        if (validation.get("bySchema", false).getBoolean()) {
            return ServiceConfigFormProvider.validStatusTransitions(validation.get("resourceState", JsonObject.EMPTY).getJsonObject());
        } else {
            return null;
        }
    }

    public boolean isConfigurationBySchema(FormProviderKey formProviderKey) {

        JsonArray pathToSchema = formProviderKey.pathToSchema();

        if (pathToSchema == null) {
            return false;
        }

        JsonObject serviceConfig = serviceConfigProvider.getServiceConfig(formProviderKey.apiName).asJsonObject();

        JsonObject validation = serviceConfig.get(pathToSchema.subArray(0, 4), JsonObject.EMPTY).getJsonObject();
        return validation.get("bySchema", false).getBoolean();
    }

    public JsonValue getSchema(FormProviderKey formProviderKey, JsonObject extensiveDataModel) {

        ServiceConfig serviceConfig = serviceConfigProvider.getServiceConfig(formProviderKey.apiName);

        JsonArray pathToSchema = formProviderKey.pathToSchema();

        JsonValue schema = serviceConfig == null || pathToSchema == null
                ? JsonValue.NULL
                : serviceConfig.asJsonObject()
                .get(pathToSchema, JsonValue.NULL);

        if (schema.isNull()) {

            if (isConfigurationBySchema(formProviderKey)) {
                schema = SCHEMA_OF_EMPTY_OBJECT;
            } else {
                return null;
            }
        }
        return updateSchemaDynamically(schema, extensiveDataModel, PATH_TO_VALIDATIONS_IN_MODEL);
    }

    private JsonValue updateSchemaDynamically(JsonValue schema, JsonObject model, JsonArray pathInModelToValidationSchemaAndLocationArray) {
        if (schema != null && schema.isJsonObject()) {
            JsonArray dynamicValidationSchema = model.get(pathInModelToValidationSchemaAndLocationArray, JsonArray.EMPTY).getJsonArray();
            for (int i = 0; i < dynamicValidationSchema.size(); i++) {

                JsonObject schemaAndLocation = dynamicValidationSchema.get(i).getJsonObject();
                JsonValue dynamicSchema = schemaAndLocation.get("schema");
                if (schema == null) {
                    break;
                }
                JsonPointer schemaKeywordPropertyLocation = schemaAndLocation.get("schemaKeywordPropertyLocation", "/").as(JsonPointer.class);
                JsonArray pathToSchema = schemaKeywordPropertyLocation.asArray();
                schema = schema.put(pathToSchema, dynamicSchema);
            }
        }
        return schema;
    }

    public JsonObject translateSimpleModelToExtensiveModel(JsonObject formModel, FormProviderKey.FormType formType) {
        if (FormProviderKey.FormType.REQUEST_BODY == formType) {
            return JsonObject.EMPTY.put("request", JsonObject.EMPTY.put("body", formModel));
        } else if (FormProviderKey.FormType.RESOURCE_BODY == formType) {
            return JsonObject.EMPTY.put("resourceState", JsonObject.EMPTY.put("afterModification", formModel));
        } else {
            return formModel;
        }
    }

    public static JsonObject schemaWhereValidationIsNotPersisted() {
        JsonArrayBuilder builder = JsonArray.EMPTY.builder();
        for (JsonValue jsonValue : PATH_TO_VALIDATIONS_IN_MODEL) {
            builder.add("properties");
            builder.add(jsonValue);
        }
        return JsonObject.EMPTY.put(builder.build(), false);
    }
}
