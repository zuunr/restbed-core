/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.validation;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.filter.FilterResult;
import com.zuunr.forms.filter.FormFilter;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.validation.JsonSchemaValidator;
import com.zuunr.json.schema.validation.OutputStructure;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.decorator.model.ResourceModel;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.core.forms.FormProviderKey;

public class DataValidator {

    private static final JsonArray ROOT_INTERNAL = JsonArray.of("root").addAll(ResourceModel.INTERNAL);

    private static final JsonObject defaultError = JsonObject.EMPTY
            .put("queryParamsError", JsonValue.NULL)
            .put("permissionsError", JsonValue.NULL)
            .put("requestBodyError", JsonValue.NULL)
            .put("resourceStateError", JsonValue.NULL);

    private static final Form statusForm = Form.EMPTY.builder()
            .value(JsonArray.EMPTY
                    .add(JsonObject.EMPTY
                            .put("name", "root")
                            .put("type", "object")
                            .put("form", JsonObject.EMPTY
                                    .put("value", JsonArray.EMPTY
                                            .add(JsonObject.EMPTY
                                                    .put("name", "status")
                                                    .put("type", "string")
                                                    .put("required", true)
                                                    .put("nullable", false)
                                                    .put("pattern", ".*")))))
                    .as(FormFields.class))
            .build();

    private SchemaProvider schemaProvider;
    private JsonSchemaValidator jsonSchemaValidator = new JsonSchemaValidator();
    private ToFormWasteConverter toFormWasteConverter = new ToFormWasteConverter();

    private static FormFilter formFilter = new FormFilter();

    public DataValidator(SchemaProvider schemaProvider) {
        this.schemaProvider = schemaProvider;
    }

    public Response<JsonObjectWrapper> validate(FormProviderKey key, JsonObject simpleDataFormat) {
        JsonValue schema = schemaProvider.getSchema(key, simpleDataFormat);
        Form form = null;
        if (schema == null) {
            form = getForm(key);
        }
        return validate(schema, form, key, simpleDataFormat);
    }

    public Response<JsonObjectWrapper> validate(JsonValue jsonSchema, Form form, FormProviderKey key, JsonObject simpleDataModel) {
        Response<JsonObjectWrapper> response = null;

        simpleDataModel = JsonObject.EMPTY.put("root", simpleDataModel);

        JsonObject waste = null;
        if (jsonSchema == null && form == null) {
            if (key.formType == FormProviderKey.FormType.REQUEST_BODY) {

                waste = JsonObject.EMPTY;

                if (!key.toStatus.is(String.class)) {

                    FilterResult<JsonObjectWrapper> filterResult = formFilter.filter(
                            statusForm,
                            JsonObject.EMPTY
                                    .put("root",
                                            simpleDataModel.get(JsonArray.of("root", "status")) == null ?
                                                    JsonObject.EMPTY : JsonObject.EMPTY
                                                    .put("status", simpleDataModel.get(JsonArray.of("root", "status")))),
                            false,
                            true);
                    waste = filterResult.wasteAsJsonValue().getValue(JsonObject.class);
                } else {

                    JsonArray validStatusTransistions = schemaProvider.getValidStatusTransitions(key.apiName, key.collectionName);

                    if (validStatusTransistions.isEmpty()) {
                        response = Response.create(StatusCode._404_NOT_FOUND);
                    } else {

                        waste = waste.put("paths", JsonArray.EMPTY
                                .add(JsonObject.EMPTY
                                        .put("path", JsonArray.of("root", "status"))
                                        .put("badValue", simpleDataModel.get(JsonArray.of("root", "status"), JsonValue.NULL))
                                        .put("violations", JsonObject.EMPTY.put("validStatusTransitions", validStatusTransistions))));
                    }
                }
            } else if (key.formType == FormProviderKey.FormType.RESOURCE_BODY) {
                waste = JsonObject.EMPTY.put("paths", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("path", JsonArray.of("root", "status"))
                                .put("badValue", simpleDataModel.get(JsonArray.of("root", "status")))
                                .put("violations", JsonObject.EMPTY.put("validStatuses", "this should be a better message but should never show because it should be guarded by the request validation first..."))));
            } else {
                response = Response.create(StatusCode._404_NOT_FOUND);
            }
        } else {

            if (key.formType == FormProviderKey.FormType.RESOURCE_BODY) {
                simpleDataModel = simpleDataModel.remove(ROOT_INTERNAL);
            }
            if (jsonSchema != null) {
                JsonObject validationResult = jsonSchemaValidator.validate(simpleDataModel.get("root"), jsonSchema, OutputStructure.DETAILED);
                waste = toFormWasteConverter.convert(validationResult, simpleDataModel.get("root").getJsonObject(), JsonArray.of("root"));
            } else {
                waste = formFilter.filter(form, simpleDataModel, false, true).wasteAsJsonValue().getValue(JsonObject.class);
            }
        }

        if (response == null) {
            if (waste == null) {
                throw new IllegalStateException("Waste is null, but should not be null");
            }

            if (waste.get("paths", JsonArray.EMPTY).getValue(JsonArray.class).isEmpty()) {
                response = null;
            } else {

                String errorType;
                String errorTypeKey;
                if (key.formType == FormProviderKey.FormType.COLLECTION_QUERY_PARAMS) {
                    errorType = "QUERY_ERROR";
                    errorTypeKey = "queryError";

                    JsonObject errorsPerPath = errorsPerPath(waste.get("paths").getValue(JsonArray.class));
                    errorsPerPath = JsonObject.EMPTY.put("query", errorsPerPath.get(""));

                    JsonObject errorBody = defaultError
                            .put("code", errorType)
                            .put(errorTypeKey, JsonObject.EMPTY.put("paths", errorsPerPath));
                    response = Response.<JsonObjectWrapper>create(StatusCode._400_BAD_REQUEST)
                            .body(JsonObjectWrapper.of(errorBody.put("status", StatusCode._400_BAD_REQUEST.getCode())));

                } else if (key.formType == FormProviderKey.FormType.ITEM_QUERY_PARAMS) {
                    errorType = "QUERY_PARAMS_ERROR";
                    errorTypeKey = "queryParamsError";
                    JsonObject errorBody = defaultError
                            .put("code", errorType)
                            .put(errorTypeKey, JsonObject.EMPTY.put("paths", errorsPerPath(waste.get("paths").getValue(JsonArray.class))));
                    response = Response.<JsonObjectWrapper>create(StatusCode._400_BAD_REQUEST)
                            .body(JsonObjectWrapper.of(errorBody.put("status", StatusCode._400_BAD_REQUEST.getCode())));

                } else if (key.formType == FormProviderKey.FormType.REQUEST_BODY) {
                    errorType = "REQUEST_BODY_ERROR";
                    errorTypeKey = "requestBodyError";
                    JsonObject errorBody = defaultError
                            .put("code", errorType)
                            .put(errorTypeKey, JsonObject.EMPTY.put("paths", errorsPerPath(waste.get("paths").getValue(JsonArray.class))));
                    response = Response.<JsonObjectWrapper>create(StatusCode._409_CONFLICT)
                            .body(JsonObjectWrapper.of(errorBody.put("status", StatusCode._409_CONFLICT.getCode())));

                } else if (key.formType == FormProviderKey.FormType.RESOURCE_BODY) {
                    errorType = "RESOURCE_STATE_ERROR";
                    errorTypeKey = "resourceStateError";
                    JsonObject errorBody = defaultError
                            .put("code", errorType)
                            .put(errorTypeKey, JsonObject.EMPTY.put("paths", errorsPerPath(waste.get("paths").getValue(JsonArray.class))))
                            .put("resourceState", simpleDataModel.get("root"));
                    response = Response.<JsonObjectWrapper>create(StatusCode._409_CONFLICT)
                            .body(JsonObjectWrapper.of(errorBody.put("status", StatusCode._409_CONFLICT.getCode())));
                } else {
                    throw new DataValidator.UnsupportedFormTypeException("Form type is not supported! Implementation error!");
                }
            }
        }
        return response;
    }

    private JsonObject errorsPerPath(JsonArray paths) {
        JsonObject result = JsonObject.EMPTY;
        for (JsonValue elem : paths.asList()) {
            JsonArray path = elem.get("path").getValue(JsonArray.class);
            if (isQueryParamAndDotsArePartOfName(path)) {
                result = result.put(path.get(1).getValue(String.class), removeFirstElementOfEqualsPaths(elem.getValue(JsonObject.class).remove("path")));
            } else {
                result = result.put(path.subArray(1, path.size()).asDotSeparatedString(), removeFirstElementOfEqualsPaths(elem.getValue(JsonObject.class).remove("path")));
            }
        }
        return result;
    }

    private boolean isQueryParamAndDotsArePartOfName(JsonArray path) {
        return path.size() == 2;
    }

    private JsonObject removeFirstElementOfEqualsPaths(JsonObject elem) {
        JsonObject violations = elem.get("violations").getValue(JsonObject.class);
        JsonObject equals = violations.get("equals", JsonValue.NULL).getValue(JsonObject.class);
        if (equals != null) {
            JsonArray paths = equals.get("paths", JsonArray.EMPTY).getValue(JsonArray.class);
            JsonArray updatedPaths = JsonArray.EMPTY;
            for (JsonValue path : paths.asList()) {
                updatedPaths = updatedPaths.add(path.getValue(JsonArray.class).tail());
            }
            elem = elem.put(JsonArray.of("violations", "equals", "paths"), updatedPaths);
        }
        return elem;
    }

    private Form getForm(FormProviderKey key) {
        Form form = null;
        if (key.formType == FormProviderKey.FormType.REQUEST_BODY && !key.toStatus.is(String.class)) {
            // do nothing - form is already null
        } else {

            JsonArray formFieldsArray = schemaProvider.getForm(key);
            if (formFieldsArray != null) {
                form = Form.EMPTY.builder().value(formFieldsArray.as(FormFields.class)).build();
            }
        }
        return form;
    }

    public static class UnsupportedFormTypeException extends RuntimeException {
        public UnsupportedFormTypeException(String message) {
            super(message);
        }
    }
}
