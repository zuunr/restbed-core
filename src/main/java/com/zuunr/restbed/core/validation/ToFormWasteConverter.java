package com.zuunr.restbed.core.validation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

/**
 * @author Niklas Eldberger
 */
public class ToFormWasteConverter {

    private final static JsonObject EMPTY_WASTE = JsonObject.EMPTY.put("paths", JsonArray.EMPTY);

    public JsonObject convert(JsonObject jsonSchemaValidationOutput, JsonObject data, JsonArray basepath) {

        if (jsonSchemaValidationOutput.get("valid", false).getBoolean()){
            return EMPTY_WASTE;
        }

        return JsonObject.EMPTY.put("paths", JsonArray.EMPTY
                .add(JsonObject.EMPTY
                        .put("path", basepath)
                        .put("violations", JsonObject.EMPTY.put("jsonSchemaError", jsonSchemaValidationOutput))
                        .put("description", "")
                        .put("badValue", data)));
    }
}
