/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.forms;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

public class FormProviderKey {

    public final String apiName;
    public final String collectionName;
    public final FormType formType;
    public final String fromStatus;
    public final JsonValue toStatus;
    public final JsonObject data;

    private JsonArray collectionValidationProperty;

    public final JsonArray asJsonArray;

    public FormProviderKey(String apiName, String collectionName, FormType formType, String fromStatus, JsonValue toStatus, JsonObject data) {
        this.apiName = apiName;
        this.collectionName = collectionName;
        this.formType = formType;
        this.fromStatus = fromStatus;
        this.toStatus = toStatus;
        this.data = data;

        asJsonArray = JsonArray.of(
                apiName,
                collectionName,
                formType.name(),
                fromStatus == null ? JsonValue.NULL : fromStatus,
                toStatus == null ? JsonValue.NULL : toStatus);
    }

    public JsonArray getCollectionValidationProperty2() {
        if (collectionValidationProperty == null) {
            collectionValidationProperty = JsonArray.of("validation", formType.name(), formType.collectionValidationProperty, "schema");
        }
        return collectionValidationProperty;
    }

    public JsonArray pathToSchema() {
        JsonArray pathToSchema;
        if (formType == FormProviderKey.FormType.REQUEST_BODY) {
            if (toStatus.isString()) {
                pathToSchema = JsonArray.of("config", "collections", collectionName, "validation", "requestBody", "fromStatus:" + fromStatus, "toStatus:" + toStatus.getString(), "schema");
            } else {
                return null;
            }
        } else if (formType == FormType.RESOURCE_BODY) {
            pathToSchema = JsonArray.of("config", "collections", collectionName, "validation", "resourceState", "toStatus:" + toStatus.getString(), "schema");
        } else {
            pathToSchema = null;
        }
        return pathToSchema;
    }

    public Builder builder() {
        return new Builder()
                .apiName(apiName)
                .collectionName(collectionName)
                .data(data)
                .formType(formType)
                .fromStatus(fromStatus)
                .toStatus(toStatus);
    }

    public static class Builder {

        private String apiName;
        private String collectionName;
        private FormType formType;
        private String fromStatus;
        private JsonValue toStatus;
        private JsonObject data;

        public Builder() {
        }

        public Builder data(JsonObject data) {
            this.data = data;
            return this;
        }

        public Builder formType(FormType formType) {
            this.formType = formType;
            return this;
        }

        public Builder fromStatus(String fromStatus) {
            this.fromStatus = fromStatus;
            return this;
        }

        public Builder toStatus(JsonValue toStatus) {
            this.toStatus = toStatus;
            return this;
        }

        public Builder apiName(String apiName) {
            this.apiName = apiName;
            return this;
        }

        public Builder collectionName(String collectionName) {
            this.collectionName = collectionName;
            return this;
        }

        public FormProviderKey build() {
            return new FormProviderKey(apiName, collectionName, formType, fromStatus, toStatus, data);
        }
    }

    public enum FormType {
        REQUEST_BODY(false, JsonValue.of("requestBody")),
        RESOURCE_BODY(false, JsonValue.of("resourceState")),
        RESOURCE_BODY_RESTRICTED(false, null), // Configured extra validations which are done before RESOURCE_BODY is validated
        ITEM_QUERY_PARAMS(true, JsonValue.of("itemQuery")),
        COLLECTION_QUERY_PARAMS(true, JsonValue.of("collectionQuery")),
        RESOURCE_MODEL(false, null);

        private boolean isQueryParams;
        private JsonValue collectionValidationProperty;

        private FormType(boolean isQueryParams, JsonValue collectionValidationProperty) {
            this.isQueryParams = isQueryParams;
            this.collectionValidationProperty = collectionValidationProperty;
        }

        public boolean isQueryParams() {
            return isQueryParams;
        }
    }
}
