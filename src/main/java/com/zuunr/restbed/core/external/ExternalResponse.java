/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.external;

import org.springframework.http.HttpStatus;

import com.zuunr.json.JsonObject;

/**
 * <p>An ExternalResponse is the container for the following types:</p>
 * 
 * <ul>
 * <li>statusCode</li>
 * <li>headers</li>
 * <li>body</li>
 * </ul>
 * 
 * @param <T> is the body type of the response
 *
 * @author Mikael Ahlberg
 */
public class ExternalResponse<T> {

    private HttpStatus statusCode;
    private JsonObject headers = JsonObject.EMPTY;
    private T body;

    private ExternalResponse(Builder<T> builder) {
        this.statusCode = builder.statusCode;
        this.headers = builder.headers;
        this.body = builder.body;
    }

    /**
     * <p>Returns this response status code as a {@link HttpStatus}.</p>
     * 
     * @return the status code
     */
    public HttpStatus getStatusCode() {
        return statusCode;
    }

    /**
     * <p>Returns the response header object as an {@link JsonObject}.</p>
     * 
     * @return the headers
     */
    public JsonObject getHeaders() {
        return headers;
    }

    /**
     * <p>Returns the typed response body.</p>
     * 
     * @return the body
     */
    public T getBody() {
        return body;
    }

    /**
     * <p>A static builder to create a new external response.</p>
     * 
     * <p>The builder takes a {@link HttpStatus} since the response must know
     * its response code.</p>
     * 
     * @param <T> is the body type of the response
     * @param statusCode the selected status code
     * @return a new builder object
     */
    public static <T> Builder<T> builder(HttpStatus statusCode) {
        return new Builder<>(statusCode);
    }

    public static final class Builder<T> {
        private HttpStatus statusCode;
        private JsonObject headers;
        private T body;

        private Builder(HttpStatus statusCode) {
            this.statusCode = statusCode;
        }

        /**
         * <p>Sets the header as a {@link JsonObject} on this builder.</p>
         * 
         * @param headers is the headers to set on the response when building
         * @return the builder object
         */
        public Builder<T> headers(JsonObject headers) {
            this.headers = headers;
            return this;
        }

        /**
         * <p>Sets the typed body on this builder.</p>
         * 
         * @param body is the body to set on the response when building
         * @return the builder object
         */
        public Builder<T> body(T body) {
            this.body = body;
            return this;
        }

        /**
         * <p>Creates a new {@link ExternalResponse} with the parameters set
         * on the builder object.</p>
         * 
         * @return a new external response
         */
        public ExternalResponse<T> build() {
            return new ExternalResponse<>(this);
        }
    }
}
