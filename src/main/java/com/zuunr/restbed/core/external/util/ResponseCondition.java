/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.external.util;

import com.zuunr.restbed.core.external.ExternalExchange;

/**
 * <p>The ResponseCondition is responsible for checking
 * a response exchange, to see if it meets the desired
 * conditions.</p>
 *
 * @author Mikael Ahlberg
 */
public interface ResponseCondition {
    
    /**
     * <p>The method checks the response exchange and either
     * returns true or false based on the implemented condition.</p>
     * 
     * @param <S> is the request body
     * @param <T> is the response body
     * @param exchange contains the response to verify
     * @return true if it meets the condition, otherwise false
     */
    <S, T> boolean isConditionMet(ExternalExchange<S, T> exchange);

}
