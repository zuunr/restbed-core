/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.external;

/**
 * <p>An ExternalExchange is the message container for {@link ExternalRequest}
 * and {@link ExternalResponse} objects.</p>
 * 
 * <p>ExternalExchange objects are immutable.</p>
 * 
 * @param <S> is the body type of the request
 * @param <T> is the body type of the response
 *
 * @author Mikael Ahlberg
 */
public class ExternalExchange<S, T> {

    private ExternalRequest<S> request;
    private ExternalResponse<T> response;

    private ExternalExchange(Builder<S, T> builder) {
        this.request = builder.request;
        this.response = builder.response;
    }

    /**
     * <p>Returns the request object of this exchange.</p>
     * 
     * @return the request object
     */
    public ExternalRequest<S> getRequest() {
        return request;
    }

    /**
     * <p>Returns the response object of this exchange.</p>
     * 
     * @return the response object
     */
    public ExternalResponse<T> getResponse() {
        return response;
    }

    /**
     * <p>Creates a new external exchange object with the given response.</p>
     * 
     * <p>The exchange must contain a request object before this method can be called,
     * otherwise an {@link IllegalStateException} will be thrown.</p>
     * 
     * @param response is the response object to set on the exchange
     * @return an external exchange containing the set response object
     */
    public ExternalExchange<S, T> response(ExternalResponse<T> response) {
        if (request == null) {
            throw new IllegalStateException("Request must be set before response");
        }

        return ExternalExchange.<S, T>builder()
                .request(request)
                .response(response)
                .build();
    }

    /**
     * <p>A static builder for this exchange.</p>
     * 
     * @param <S> is the body type of the request
     * @param <T> is the body type of the response
     * @return a new builder object
     */
    public static <S, T> Builder<S, T> builder() {
        return new Builder<>();
    }

    public static final class Builder<S, T> {
        private ExternalRequest<S> request;
        private ExternalResponse<T> response;

        private Builder() {
        }

        /**
         * <p>Sets the request on this builder.</p>
         * 
         * @param request is the request to set on this exchange when building
         * @return the builder object
         */
        public Builder<S, T> request(ExternalRequest<S> request) {
            this.request = request;
            return this;
        }

        /**
         * <p>Sets the response object on this builder.</p>
         * 
         * <p>If a request has not been set before, this method will throw an
         * {@link IllegalStateException}.</p>
         * 
         * @param response is the response to set on this exchange when building
         * @return the builder object
         */
        public Builder<S, T> response(ExternalResponse<T> response) {
            if (request == null) {
                throw new IllegalStateException("Request must be set before response");
            }

            this.response = response;
            return this;
        }

        /**
         * <p>Creates a new {@link ExternalExchange} with the parameters set on the
         * builder object.</p>
         * 
         * @return a new external exchange object
         */
        public ExternalExchange<S, T> build() {
            return new ExternalExchange<>(this);
        }
    }
}
