/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.external.util;

import org.springframework.http.HttpStatus;

import com.zuunr.restbed.core.external.ExternalExchange;

/**
 * <p>The HttpStatusCondition is responsible for checking
 * if the response exchange matches the provided
 * {@link HttpStatus}.</p>
 *
 * @author Mikael Ahlberg
 */
public class HttpStatusCondition implements ResponseCondition {
    
    private HttpStatus validStatus;

    private HttpStatusCondition(Builder builder) {
        this.validStatus = builder.validStatus;
    }

    @Override
    public <S, T> boolean isConditionMet(ExternalExchange<S, T> exchange) {
        return exchange.getResponse().getStatusCode() == validStatus;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private HttpStatus validStatus;

        private Builder() {
        }

        public Builder validStatus(HttpStatus validStatus) {
            this.validStatus = validStatus;
            return this;
        }

        public HttpStatusCondition build() {
            return new HttpStatusCondition(this);
        }
    }
}
