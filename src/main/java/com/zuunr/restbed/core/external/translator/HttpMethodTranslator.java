/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.external.translator;

import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.zuunr.restbed.core.exchange.Method;

/**
 * <p>The {@link HttpMethodTranslator} is responsible for translating between
 * springs {@link HttpMethod}, and the internal {@link Method}.</p>
 * 
 * <p>There is no guarantee that the {@link Method} supports a given {@link HttpMethod}.</p>
 * 
 * @author Mikael Ahlberg
 */
@Component
public class HttpMethodTranslator {

    private static final Map<HttpMethod, Method> methodTranslation = new EnumMap<>(HttpMethod.class);

    static {
        methodTranslation.put(HttpMethod.GET, Method.GET);
        methodTranslation.put(HttpMethod.POST, Method.POST);
        methodTranslation.put(HttpMethod.PATCH, Method.PATCH);
        methodTranslation.put(HttpMethod.DELETE, Method.DELETE);
    }

    /**
     * <p>Translates a given {@link HttpMethod} to an internal {@link Method}
     * representation.</p>
     * 
     * <p>Throws an {@link IllegalArgumentException} if the given {@link HttpMethod}
     * is not a supported {@link Method} type. E.g. if HttpMethod.TRACE is not supported
     * by {@link Method}.</p>
     * 
     * @param httpMethod is the method to translate
     * @return a new translated method
     */
    public Method translate(HttpMethod httpMethod) {
        return Optional.ofNullable(methodTranslation.get(httpMethod))
                .orElseThrow(() -> new IllegalArgumentException("Generated services does not support the applied http method " + httpMethod));
    }
}
