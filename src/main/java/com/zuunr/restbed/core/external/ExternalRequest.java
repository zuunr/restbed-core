/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.external;

import org.springframework.http.HttpMethod;

import com.zuunr.json.JsonObject;

/**
 * <p>An ExternalRequest is the container for the following types:</p>
 * 
 * <ul>
 * <li>method</li>
 * <li>url</li>
 * <li>headers</li>
 * <li>body</li>
 * </ul>
 * 
 * @param <T> is the body type of the request
 *
 * @author Mikael Ahlberg
 */
public class ExternalRequest<T> {

    private HttpMethod method;
    private String url;
    private JsonObject headers = JsonObject.EMPTY;
    private T body;

    private ExternalRequest(Builder<T> builder) {
        this.method = builder.method;
        this.url = builder.url;
        this.headers = builder.headers;
        this.body = builder.body;
    }

    /**
     * <p>Returns this requests {@link HttpMethod}, e.g. if
     * it is POST, GET or any other valid method types.</p>
     * 
     * @return the selected method
     */
    public HttpMethod getMethod() {
        return method;
    }

    /**
     * <p>Returns the requests URL.</p>
     * 
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * <p>Returns the requests header object as an {@link JsonObject}.</p>
     * 
     * @return the headers
     */
    public JsonObject getHeaders() {
        return headers;
    }

    /**
     * <p>Returns the typed request body.</p>
     * 
     * @return the body
     */
    public T getBody() {
        return body;
    }

    /**
     * <p>A static builder to create a new external request.</p>
     * 
     * <p>The builder takes a {@link HttpMethod} since the request must know
     * its method type.</p>
     * 
     * @param <T> is the body type of the request
     * @param method the selected http method
     * @return a new builder object
     */
    public static <T> Builder<T> builder(HttpMethod method) {
        return new Builder<>(method);
    }

    public static final class Builder<T> {
        private HttpMethod method;
        private String url;
        private JsonObject headers;
        private T body;

        private Builder(HttpMethod method) {
            this.method = method;
        }

        /**
         * <p>Sets the URL on this builder.</p>
         * 
         * <p>For example http://...</p>
         * 
         * @param url is the url to set on this request when building
         * @return the builder object
         */
        public Builder<T> url(String url) {
            this.url = url;
            return this;
        }

        /**
         * <p>Sets the header as a {@link JsonObject} on this builder.</p>
         * 
         * @param headers is the headers to set on the request when building
         * @return the builder object
         */
        public Builder<T> headers(JsonObject headers) {
            this.headers = headers;
            return this;
        }

        /**
         * <p>Sets the typed body on this builder.</p>
         * 
         * @param body is the body to set on the request when building
         * @return the builder object
         */
        public Builder<T> body(T body) {
            this.body = body;
            return this;
        }

        /**
         * <p>Creates a new {@link ExternalRequest} with the parameters set
         * on the builder object.</p>
         * 
         * @return a new external request
         */
        public ExternalRequest<T> build() {
            return new ExternalRequest<>(this);
        }
    }
}
