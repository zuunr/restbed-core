/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.external;

import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectSupport;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.external.translator.HttpMethodTranslator;
import com.zuunr.restbed.core.registry.ServiceRegistry;
import com.zuunr.restbed.core.service.RestService;
import com.zuunr.restbed.core.util.HeaderTranslator;

import reactor.core.publisher.Mono;

/**
 * <p>Primary implementation of {@link ServiceCaller}. The DefaultServiceCaller handles
 * calls to external services on other networks, or generated services in the same JVM.</p>
 * 
 * <p>Services that should be accessible inside the jvm is determined by
 * the {@link ServiceRegistry} component.</p>
 * 
 * <p>Both the {@link ExternalRequest} and {@link ExternalResponse} inside
 * the {@link ExternalExchange} container must be of the type {@link JsonObjectSupport}
 * if the service is internally called.</p>
 * 
 * @see ServiceCaller
 * 
 * @author Mikael Ahlberg
 */
@Component
public class DefaultServiceCaller implements ServiceCaller {
    
    private final Logger logger = LoggerFactory.getLogger(DefaultServiceCaller.class);

    private WebClient webClient;
    private RestService restService;
    private ServiceRegistry serviceRegistry;
    private HeaderTranslator headerTranslator;
    private HttpMethodTranslator httpMethodTranslator;

    @Autowired
    public DefaultServiceCaller(
            WebClient webClient,
            @Qualifier("basicRestService") RestService restService,
            ServiceRegistry serviceRegistry,
            HeaderTranslator headerTranslator,
            HttpMethodTranslator httpMethodTranslator) {
        this.webClient = webClient;
        this.restService = restService;
        this.serviceRegistry = serviceRegistry;
        this.headerTranslator = headerTranslator;
        this.httpMethodTranslator = httpMethodTranslator;
    }

    @Override
    public <S, T> Mono<ExternalExchange<S, T>> send(ExternalExchange<S, T> exchange, Class<T> responseType) {
        return Mono.just(exchange)
                .doOnNext(o -> {
                    if (o.getRequest().getMethod() == HttpMethod.GET) {
                        logger.info("{} {}", o.getRequest().getMethod(), o.getRequest().getUrl());
                    } else {
                        logger.info("{} {}, requestBody: {}", o.getRequest().getMethod(), o.getRequest().getUrl(), o.getRequest().getBody());
                    }
                })
                .flatMap(o -> sendExchange(o, responseType))
                .doOnNext(o -> logger.info("{} {}, statusCode: {}, responseBody: {}", o.getRequest().getMethod(), o.getRequest().getUrl(), o.getResponse().getStatusCode(), o.getResponse().getBody()));
    }

    private <S, T> Mono<ExternalExchange<S, T>> sendExchange(ExternalExchange<S, T> exchange, Class<T> responseType) {
        Consumer<HttpHeaders> httpHeaders = headerTranslator.translate(exchange.getRequest().getHeaders());
        S body = exchange.getRequest().getBody();

        if (serviceRegistry.isServiceDeployedInJVM(exchange.getRequest().getUrl())) {
            return internalJvmCall(exchange);
        }

        return webClient.method(exchange.getRequest().getMethod())
                .uri(exchange.getRequest().getUrl())
                .headers(httpHeaders)
                .body(body == null ? BodyInserters.empty() : BodyInserters.fromValue(body))
                .exchange()
                .flatMap(r -> r.toEntity(responseType))
                .flatMap(r -> successExchange(r, exchange));
    }

    private <S, T> Mono<ExternalExchange<S, T>> successExchange(ResponseEntity<T> response, ExternalExchange<S, T> exchange) {
        JsonObject headers = headerTranslator.translate(response.getHeaders());

        return Mono.just(exchange.response(ExternalResponse.<T>builder(response.getStatusCode())
                .headers(headers)
                .body(response.getBody())
                .build()));
    }

    @SuppressWarnings("unchecked")
    private <S, T> Mono<ExternalExchange<S, T>> internalJvmCall(ExternalExchange<S, T> exchange) {
        return Mono.just(exchange)
                .map(this::createRequestFromExchange)
                .flatMap(restService::service)
                .map(Exchange::getResponse)
                .map(o -> exchange.response(ExternalResponse.<T>builder(HttpStatus.valueOf(o.getStatusCode()))
                        .headers(o.getHeaders())
                        .body((T) o.getBody())
                .build()));
    }

    private <S, T> Exchange<JsonObjectWrapper> createRequestFromExchange(ExternalExchange<S, T> exchange) {
        ExternalRequest<S> externalRequest = exchange.getRequest();

        return Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(httpMethodTranslator.translate(externalRequest.getMethod()))
                .url(externalRequest.getUrl())
                .headers(externalRequest.getHeaders())
                        .body((JsonObjectWrapper) externalRequest.getBody()));
    }
}
