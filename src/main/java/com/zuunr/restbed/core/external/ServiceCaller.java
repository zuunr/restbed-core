/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.external;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import com.zuunr.json.JsonObject;
import com.zuunr.restbed.core.exchange.StatusCode;

import reactor.core.publisher.Mono;

/**
 * <p>The ServiceCaller is responsible for integrating with other services, be it 
 * external services on other networks or services in the same JVM.</p>
 * 
 * <p>The {@link ExternalExchange} is the selected message container for transporting
 * the {@link ExternalRequest} and {@link ExternalResponse} objects.</p>
 * 
 * <p>Implementers of this interface must return the input request object on the 
 * returned exchange, and set the correct body and {@link StatusCode}.</p>
 * 
 * @author Mikael Ahlberg
 */
public interface ServiceCaller {

    /**
     * <p>The method sends the given request to the given endpoint. Any received data
     * is set on the output exchange.</p>
     * 
     * <p>The returned {@link Mono} can be subscribed to at a later stage.</p>
     * 
     * @param <S> is the request body type
     * @param <T> is the response body type
     * @param exchange is the exchange containing the request
     * @param responseType is the class type of the response body
     * @return a new mono exchange containing both the request and response
     */
    <S, T> Mono<ExternalExchange<S, T>> send(ExternalExchange<S, T> exchange, Class<T> responseType);

    /**
     * <p>A convenience method for sending a GET request without the need
     * to create an {@link ExternalExchange} container.</p>
     * 
     * <p>See {@link #send(ExternalExchange, Class)}.</p>
     * 
     * @param <T> is the response body type
     * @param url is the url to send the request to
     * @param responseType is the class type of the response body
     * @param headers contains the headers to send in the request
     * @return a new mono exchange containing both request and response
     */
    default <T> Mono<ExternalExchange<Void, T>> get(String url, Class<T> responseType, JsonObject headers) {
        ExternalExchange<Void, T> exchange = ExternalExchange.<Void, T>builder()
                .request(ExternalRequest.<Void>builder(HttpMethod.GET)
                        .url(url)
                        .headers(headers)
                        .build())
                .build();

        return send(exchange, responseType);
    }

    /**
     * <p>A convenience method for sending a POST request without the need
     * to create an {@link ExternalExchange} container.</p>
     * 
     * <p>See {@link #send(ExternalExchange, Class)}.</p>
     * 
     * @param <S> is the request body type
     * @param <T> is the response body type
     * @param url is the url to sent the request to
     * @param body is the request body
     * @param responseType is the class type of the response body
     * @param headers contains the headers to send in the request
     * @return a new mono exchange containing both request and response
     */
    default <S, T> Mono<ExternalExchange<S, T>> post(String url, S body, Class<T> responseType, JsonObject headers) {
        ExternalExchange<S, T> exchange = ExternalExchange.<S, T>builder()
                .request(ExternalRequest.<S>builder(HttpMethod.POST)
                        .url(url)
                        .headers(headers)
                        .body(body)
                        .build())
                .build();

        return send(exchange, responseType);
    }

    /**
     * <p>A convenience method for sending a PATCH request without the need
     * to create an {@link ExternalExchange} container.</p>
     * 
     * <p>See {@link #send(ExternalExchange, Class)}.</p>
     * 
     * @param <S> is the request body type
     * @param <T> is the response body type
     * @param url is the url to sent the request to
     * @param body is the request body
     * @param responseType is the class type of the response body
     * @param headers contains the headers to send in the request
     * @return a new mono exchange containing both request and response
     */
    private <S, T> Mono<ExternalExchange<S, T>> patch(String url, S body, Class<T> responseType, JsonObject headers) {
        ExternalExchange<S, T> exchange = ExternalExchange.<S, T>builder()
                .request(ExternalRequest.<S>builder(HttpMethod.PATCH)
                        .url(url)
                        .headers(headers)
                        .body(body)
                        .build())
                .build();

        return send(exchange, responseType);
    }

    /**
     * <p>A convenience method for sending a DELETE request without the need
     * to create an {@link ExternalExchange} container.</p>
     * 
     * <p>See {@link #send(ExternalExchange, Class)}.</p>
     * 
     * @param <T> is the response body type
     * @param url is the url to sent the request to
     * @param responseType is the class type of the response body
     * @param headers contains the headers to send in the request
     * @return a new mono exchange containing both request and response
     */
    private <T> Mono<ExternalExchange<Void, T>> delete(String url, Class<T> responseType, JsonObject headers) {
        ExternalExchange<Void, T> exchange = ExternalExchange.<Void, T>builder()
                .request(ExternalRequest.<Void>builder(HttpMethod.DELETE)
                        .url(url)
                        .headers(headers)
                        .build())
                .build();

        return send(exchange, responseType);
    }
    
    /**
     * <p>A convenience method for sending a GET request without the need
     * to create an {@link ExternalExchange} container.</p>
     * 
     * <p>A default Content-Type header of 'application/json' is attached to the
     * sent request.</p>
     * 
     * <p>See {@link #send(ExternalExchange, Class)}.</p>
     * 
     * @param <T> is the response body type
     * @param url is the url to send the request to
     * @param responseType is the class type of the response body
     * @param authorizationHeader is the value to send in the Authorization header field
     * @return a new mono exchange containing both request and response
     */
    default <T> Mono<ExternalExchange<Void, T>> get(String url, Class<T> responseType, String authorizationHeader) {
        JsonObject headers = JsonObject.EMPTY.builder()
                .put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .put(HttpHeaders.AUTHORIZATION, authorizationHeader)
                .build();

        return get(url, responseType, headers);
    }

    /**
     * <p>A convenience method for sending a POST request without the need
     * to create an {@link ExternalExchange} container.</p>
     * 
     * <p>A default Content-Type header of 'application/json' is attached to the
     * sent request.</p>
     * 
     * <p>See {@link #send(ExternalExchange, Class)}.</p>
     * 
     * @param <S> is the request body type
     * @param <T> is the response body type
     * @param url is the url to sent the request to
     * @param body is the request body
     * @param responseType is the class type of the response body
     * @param authorizationHeader is the value to send in the Authorization header field
     * @return a new mono exchange containing both request and response
     */
    default <S, T> Mono<ExternalExchange<S, T>> post(String url, S body, Class<T> responseType, String authorizationHeader) {
        JsonObject headers = JsonObject.EMPTY.builder()
                .put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .put(HttpHeaders.AUTHORIZATION, authorizationHeader)
                .build();

        return post(url, body, responseType, headers);
    }

    /**
     * <p>A convenience method for sending a PATCH request without the need
     * to create an {@link ExternalExchange} container.</p>
     * 
     * <p>A default Content-Type header of 'application/json' is attached to the
     * sent request.</p>
     * 
     * <p>See {@link #send(ExternalExchange, Class)}.</p>
     * 
     * @param <S> is the request body type
     * @param <T> is the response body type
     * @param url is the url to sent the request to
     * @param body is the request body
     * @param responseType is the class type of the response body
     * @param authorizationHeader is the value to send in the Authorization header field
     * @return a new mono exchange containing both request and response
     */
    default <S, T> Mono<ExternalExchange<S, T>> patch(String url, S body, Class<T> responseType, String authorizationHeader) {
        JsonObject headers = JsonObject.EMPTY.builder()
                .put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .put(HttpHeaders.AUTHORIZATION, authorizationHeader)
                .build();

        return patch(url, body, responseType, headers);
    }

    /**
     * <p>A convenience method for sending a DELETE request without the need
     * to create an {@link ExternalExchange} container.</p>
     * 
     * <p>A default Content-Type header of 'application/json' is attached to the
     * sent request.</p>
     * 
     * <p>See {@link #send(ExternalExchange, Class)}.</p>
     * 
     * @param <T> is the response body type
     * @param url is the url to sent the request to
     * @param responseType is the class type of the response body
     * @param authorizationHeader is the value to send in the Authorization header field
     * @return a new mono exchange containing both request and response
     */
    default <T> Mono<ExternalExchange<Void, T>> delete(String url, Class<T> responseType, String authorizationHeader) {
        JsonObject headers = JsonObject.EMPTY.builder()
                .put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .put(HttpHeaders.AUTHORIZATION, authorizationHeader)
                .build();

        return delete(url, responseType, headers);
    }
    
    /**
     * <p>A convenience method for sending a GET request without the need
     * to create an {@link ExternalExchange} container.</p>
     * 
     * <p>A default Content-Type header of 'application/json' is attached to the
     * sent request.</p>
     * 
     * <p>See {@link #send(ExternalExchange, Class)}.</p>
     * 
     * @param <T> is the response body type
     * @param url is the url to send the request to
     * @param responseType is the class type of the response body
     * @return a new mono exchange containing both request and response
     */
    default <T> Mono<ExternalExchange<Void, T>> get(String url, Class<T> responseType) {
        JsonObject headers = JsonObject.EMPTY.builder()
                .put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();

        return get(url, responseType, headers);
    }

    /**
     * <p>A convenience method for sending a POST request without the need
     * to create an {@link ExternalExchange} container.</p>
     * 
     * <p>A default Content-Type header of 'application/json' is attached to the
     * sent request.</p>
     * 
     * <p>See {@link #send(ExternalExchange, Class)}.</p>
     * 
     * @param <S> is the request body type
     * @param <T> is the response body type
     * @param url is the url to sent the request to
     * @param body is the request body
     * @param responseType is the class type of the response body
     * @return a new mono exchange containing both request and response
     */
    default <S, T> Mono<ExternalExchange<S, T>> post(String url, S body, Class<T> responseType) {
        JsonObject headers = JsonObject.EMPTY.builder()
                .put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();

        return post(url, body, responseType, headers);
    }

    /**
     * <p>A convenience method for sending a PATCH request without the need
     * to create an {@link ExternalExchange} container.</p>
     * 
     * <p>A default Content-Type header of 'application/json' is attached to the
     * sent request.</p>
     * 
     * <p>See {@link #send(ExternalExchange, Class)}.</p>
     * 
     * @param <S> is the request body type
     * @param <T> is the response body type
     * @param url is the url to sent the request to
     * @param body is the request body
     * @param responseType is the class type of the response body
     * @return a new mono exchange containing both request and response
     */
    default <S, T> Mono<ExternalExchange<S, T>> patch(String url, S body, Class<T> responseType) {
        JsonObject headers = JsonObject.EMPTY.builder()
                .put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();

        return patch(url, body, responseType, headers);
    }

    /**
     * <p>A convenience method for sending a DELETE request without the need
     * to create an {@link ExternalExchange} container.</p>
     * 
     * <p>A default Content-Type header of 'application/json' is attached to the
     * sent request.</p>
     * 
     * <p>See {@link #send(ExternalExchange, Class)}.</p>
     * 
     * @param <T> is the response body type
     * @param url is the url to sent the request to
     * @param responseType is the class type of the response body
     * @return a new mono exchange containing both request and response
     */
    default <T> Mono<ExternalExchange<Void, T>> delete(String url, Class<T> responseType) {
        JsonObject headers = JsonObject.EMPTY.builder()
                .put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();

        return delete(url, responseType, headers);
    }
}
