/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.external.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.zuunr.restbed.core.external.ExternalExchange;

/**
 * <p>The ResponseVerifier is responsible for verifying responses,
 * if it mets a provided condition ({@link ResponseCondition}).</p>
 * 
 * <p>If the condition is not met, an {@link InvalidResponseException}
 * is thrown.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class ResponseVerifier {
    
    private final Logger logger = LoggerFactory.getLogger(ResponseVerifier.class);
    
    private final HttpStatusCondition createdStatus = HttpStatusCondition.builder().validStatus(HttpStatus.CREATED).build();
    private final HttpStatusCondition okStatus = HttpStatusCondition.builder().validStatus(HttpStatus.OK).build();
    private final HttpStatusCondition noContentStatus = HttpStatusCondition.builder().validStatus(HttpStatus.NO_CONTENT).build();
    
    /**
     * <p>A convenience method that checks the status of the GET response,
     * to see if it matches an {@link HttpStatus#OK} condition.</p>
     * 
     * <p>See {@link #verifyResponse(HttpMethod, ExternalExchange, ResponseCondition)}.</p>
     * 
     * @param <S> is the request body
     * @param <T> is the response body
     * @param exchange contains the response to verify
     * @return the exchange, unaltered
     */
    public <S, T> ExternalExchange<S, T> verifyGetResponse(ExternalExchange<S, T> exchange) {
        return verifyResponse(HttpMethod.GET, exchange, okStatus);
    }
    
    /**
     * <p>A convenience method that checks the status of the POST response,
     * to see if it matches an {@link HttpStatus#CREATED} condition.</p>
     * 
     * <p>See {@link #verifyResponse(HttpMethod, ExternalExchange, ResponseCondition)}.</p>
     * 
     * @param <S> is the request body
     * @param <T> is the response body
     * @param exchange contains the response to verify
     * @return the exchange, unaltered
     */
    public <S, T> ExternalExchange<S, T> verifyPostResponse(ExternalExchange<S, T> exchange) {
        return verifyResponse(HttpMethod.POST, exchange, createdStatus);
    }
    
    /**
     * <p>A convenience method that checks the status of the PATCH response,
     * to see if it matches an {@link HttpStatus#OK} condition.</p>
     * 
     * <p>See {@link #verifyResponse(HttpMethod, ExternalExchange, ResponseCondition)}.</p>
     * 
     * @param <S> is the request body
     * @param <T> is the response body
     * @param exchange contains the response to verify
     * @return the exchange, unaltered
     */
    public <S, T> ExternalExchange<S, T> verifyPatchResponse(ExternalExchange<S, T> exchange) {
        return verifyResponse(HttpMethod.PATCH, exchange, okStatus);
    }
    
    /**
     * <p>A convenience method that checks the status of the DELETE response,
     * to see if it matches an {@link HttpStatus#NO_CONTENT} condition.</p>
     * 
     * <p>See {@link #verifyResponse(HttpMethod, ExternalExchange, ResponseCondition)}.</p>
     * 
     * @param <S> is the request body
     * @param <T> is the response body
     * @param exchange contains the response to verify
     * @return the exchange, unaltered
     */
    public <S, T> ExternalExchange<S, T> verifyDeleteResponse(ExternalExchange<S, T> exchange) {
        return verifyResponse(HttpMethod.DELETE, exchange, noContentStatus);
    }
    
    /**
     * <p>The method verifies the response exchange, that it meets the provided 
     * {@link ResponseCondition}.</p>
     * 
     * <p>Throws an {@link InvalidResponseException} if the condition is not met.</p>
     * 
     * @param <S> is the request body
     * @param <T> is the response body
     * @param httpMethod is the http method used (only used for logging purpose)
     * @param exchange contains the response to verify
     * @param responseCondition contains the condition to check against the response
     * @return the exchange, unaltered or an {@link InvalidResponseException}
     */
    public <S, T> ExternalExchange<S, T> verifyResponse(HttpMethod httpMethod, ExternalExchange<S, T> exchange, ResponseCondition responseCondition) {
        if (logger.isDebugEnabled()) {
            logger.debug("{} {}, status: {}, responseBody: {}",
                    httpMethod.name(), exchange.getRequest().getUrl(),
                    exchange.getResponse().getStatusCode(), exchange.getResponse().getBody());
        }
        
        if (!responseCondition.isConditionMet(exchange)) {
            throw new InvalidResponseException("Response not OK, status: " + exchange.getResponse().getStatusCode()
                    + " responseBody: " + exchange.getResponse().getBody()
                    + " for: " + exchange.getRequest().getUrl()
                    + " requestBody: " + exchange.getRequest().getBody());
        }

        return exchange;
    }
}
