/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service;

import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.exchange.Response;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/** 
 * <p>A RestService is the main handler for service requests. It supports reactive
 * components such as {@link Mono} and {@link Flux} and should not be blocking.</p>
 * 
 * <p>An {@link Exchange} is the selected message container for request and
 * response messages.</p>
 * 
 * <p>The implementers of this interface is bound to set the correct response code
 * on the returning {@link Exchange}, depending on outcome. Returned exchanges
 * contains both the incoming {@link Request} and returned {@link Response} objects.</p>
 *
 * @author Niklas Eldberger
 * @author Mikael Ahlberg
 */
public interface RestService {
    
    /**
     * <p>Processes the request in the incoming {@link Exchange}, and returns
     * a response in the outgoing {@link Exchange}.
     * 
     * @param exchange contains the request data
     * @return an exchange containing the request and response data
     */
    Mono<Exchange<JsonObjectWrapper>> service(Exchange<JsonObjectWrapper> exchange);
    
}
