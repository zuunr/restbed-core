/*
 * Copyright 2021 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service.translator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.restservice.ServiceConfig;
import com.zuunr.restbed.core.restservice.ServiceConfigProvider;
import com.zuunr.restbed.core.restservice.scheme.config.api.ApiConfig;

/**
 * <p>The ExchangeUrlTranslator is responsible for translating
 * the incoming request url, by extracting the path and query parameters
 * and appending it to the internal configured url.</p>
 * 
 * <p>The internal configured url is the database structure, and is the
 * only host/port etc that the service should know of internally.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class ExchangeUrlTranslator {
    
    private ServiceConfigProvider serviceConfigProvider;
    
    @Autowired
    public ExchangeUrlTranslator(
            ServiceConfigProvider serviceConfigProvider) {
        this.serviceConfigProvider = serviceConfigProvider;
    }
    
    /**
     * <p>Translates the incoming exchanges' request url to the internal configured url, e.g.
     * to the internal database structure of the url.</p>
     * 
     * <p>If the internal configured url is http and port is 80, or https and port is 443
     * the port will be removed.</p>
     * 
     * @param exchange contains the request url to translate
     * @return a translated exchange
     */
    public Exchange<JsonObjectWrapper> translate(Exchange<JsonObjectWrapper> exchange) {
        Request<JsonObjectWrapper> request = exchange.getRequest();
        
        return exchange.request(request
                .url(translateUrl(exchange))
                .headers(request.getHeaders())
                .body(request.getBody()));
    }
    
    private String translateUrl(Exchange<JsonObjectWrapper> exchange) {
        ApiConfig apiConfig = getServiceConfig(exchange).getConfigConfig().getApiConfig();
        
        return new StringBuilder()
                .append(apiConfig.getScheme())
                .append("://")
                .append(apiConfig.getHost())
                .append(getPort(apiConfig))
                .append("/")
                .append(getPath(exchange))
                .toString();
    }
    
    private String getPath(Exchange<JsonObjectWrapper> exchange) {
        String path = exchange.getRequest().getApiUriInfo().path();
        String queryString = exchange.getRequest().getApiUriInfo().queryString();

        if (queryString == null || queryString.isEmpty()) {
            return path;
        } else {
            return path + "?" + queryString;
        }
    }
    
    private String getPort(ApiConfig apiConfig) {
        if ((apiConfig.getScheme().equals("https") && apiConfig.getPort() == 443) || (apiConfig.getScheme().equals("http") && apiConfig.getPort() == 80)) {
            return "";
        } else {
            return ":" + apiConfig.getPort();
        }
    }

    private ServiceConfig getServiceConfig(Exchange<JsonObjectWrapper> exchange) {
        String apiName = null;

        try {
            apiName = exchange.getRequest().getApiUriInfo().apiName();
        } catch (Exception e) {
            // If an exception occurs, it means that we have probably changed path with an external configuration
            // Then use null as apiName key
        }

        return serviceConfigProvider.getServiceConfig(apiName);
    }
}
