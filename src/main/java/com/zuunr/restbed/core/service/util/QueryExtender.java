/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service.util;

import java.util.List;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;
import com.zuunr.json.JsonValueUtil;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.core.service.RestService;

import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

/**
 * <p>The QueryExtender is responsible for adding items or collections, 
 * e.g. extending them, on a given resource whenever the request contains
 * an 'extend' parameter and the resource contains a href at that location.</p>
 * 
 * <p>E.g. '/cars?extend=engine,owner' will extend 'engine' and 'owner' on the retrieved
 * 'car' resources, given that there is a href for 'engine' and 'owner' under their
 * respective json objects.</p>
 * 
 * <p>Be careful on what type of {@link RestService} that is injected into this class.
 * The extend resource functionality is not designed to go through any security layer,
 * or other type of extra layer.</p>
 *
 * @author Mikael Ahlberg
 * @author Niklas Eldberger
 */
public class QueryExtender {
    
    private final JsonValueUtil jsonValueUtil = new JsonValueUtil();
    
    private RestService restService;
    
    public QueryExtender(RestService restService) {
        this.restService = restService;
    }
    
    /**
     * <p>The method will, given that the URL contains extension parameters (extend),
     * extend the marked objects in the resource ({@link JsonObject}) with data retrieved
     * from other resource by its provided href.</p>
     *
     * @param request  is the original GET request
     * @param resource is the retrieved resource to extend
     * @return a response containing the resource with the extended resource
     */
    public Mono<JsonObject> extendResource(Request<JsonObjectWrapper> request, JsonObject resource) {
        return Mono.just(request)
                .flatMapIterable(this::getExtensionParameters)
                .flatMap(o -> retrieveResourcesForAllPaths(resource, o))
                .collectList()
                .map(o -> addRetrievedResources(resource, o));
    }
    
    private JsonArray getExtensionParameters(Request<JsonObjectWrapper> request) {
        return request.getApiUriInfo().extensionPaths().values();
    }
    
    private Mono<List<Tuple2<JsonArray, JsonObject>>> retrieveResourcesForAllPaths(JsonObject resource, JsonValue extensionValue) {
        return Mono.just(extensionValue)
                .flatMapIterable(o -> getExtensionHrefPaths(o, resource))
                .flatMap(path -> {
                    JsonArray distinctExtensionPath = path.getValue(JsonArray.class);
                    ApiUriInfo apiUriInfo = resource.get(distinctExtensionPath.add("href")).as(ApiUriInfo.class);
                    
                    return Mono.from(getResource(apiUriInfo))
                            .map(retrievedResource -> Tuples.of(distinctExtensionPath, retrievedResource));
                })
                .collectList();
    }
    
    private JsonArray getExtensionHrefPaths(JsonValue extensionValue, JsonObject resource) {
        JsonArray extensionPathRegexStyle = extensionValue.getValue(JsonArray.class).replace("_", -1);
        
        return jsonValueUtil.getPathsByRegex(resource.jsonValue(), extensionPathRegexStyle);
    }
    
    private JsonObject addRetrievedResources(JsonObject resource, List<List<Tuple2<JsonArray, JsonObject>>> extendedResources) {
        JsonObject updatedResource = resource;
        
        for (List<Tuple2<JsonArray, JsonObject>> extendedResource : extendedResources) {
            for (Tuple2<JsonArray, JsonObject> extended : extendedResource) {
                updatedResource = updatedResource.put(extended.getT1(), extended.getT2());
            }
        }
        
        return updatedResource;
    }
    
    private Mono<JsonObject> getResource(ApiUriInfo apiUriInfo) {
        if (!apiUriInfo.isItem() && !apiUriInfo.isCollection()) {
            throw new IllegalArgumentException("Must be either collection or item.");
        }
        
        return Mono.just(apiUriInfo)
                .map(o -> Exchange.<JsonObjectWrapper>empty().request(Request.<JsonObjectWrapper>create(Method.GET).url(o.uri())))
                .flatMap(restService::service)
                .map(Exchange::getResponse)
                .map(extendingResourceResponse -> {
                    if (extendingResourceResponse.getStatusCode() == StatusCode._404_NOT_FOUND.getCode()) {
                        JsonObjectBuilder jsonObjectBuilder = JsonObject.EMPTY.builder();
                        
                        if (apiUriInfo.isItem()) {
                            jsonObjectBuilder.put("id", apiUriInfo.itemId());
                            jsonObjectBuilder.put("status", "NO_STATUS");
                        }
                        
                        return jsonObjectBuilder.build();
                    } else {
                        return extendingResourceResponse.getBodyAsJsonObject();
                    }
                })
                .map(extendingResource -> extendingResource.put("href", apiUriInfo));
    }
}
