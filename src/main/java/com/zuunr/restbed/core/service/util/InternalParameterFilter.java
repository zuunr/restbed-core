/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service.util;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Response;

/**
 * <p>The InternalEtagFilter will remove the '_internal' parameter
 * from the resource body.</p>
 *
 * <p>The '_internal' object is a hidden internal parameter and
 * should not be exposed. It contains the internal etag used to
 * determine if a resource has changed.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class InternalParameterFilter {

    /**
     * <p>Removes the '_internal' object from the provided
     * resource body.</p>
     *
     * <p>If the resource body is null, null will be returned.</p>
     *
     * @param itemResponse contains a {@link JsonObject} which will be filtered
     * @return the resourceBody as a {@link Response} but without the '_internal' object
     */
    public Response<JsonObjectWrapper> filterItem(Response<JsonObjectWrapper> itemResponse) {
        return Optional.ofNullable(itemResponse.getBodyAsJsonObject())
                .map(resourceBody -> resourceBody.remove("_internal"))
                .map(JsonObjectWrapper::of)
                .map(itemResponse::body)
                .orElse(itemResponse);
    }

    /**
     * <p>Removes the '_internal' object on all items on the collection
     * in the provided resource body.</p>
     *
     * <p>The provided resourceBody may not be null.</p>
     *
     * @param collectionResponse contains a {@link JsonObject} which will be filtered
     * @return the resourceBody as a {@link Response} but without the '_internal' objects
     */
    public Response<JsonObjectWrapper> filterCollection(Response<JsonObjectWrapper> collectionResponse) {
        JsonObject resourceBody = collectionResponse.getBodyAsJsonObject();
        JsonArray items = resourceBody.get("value", JsonArray.EMPTY).as(JsonArray.class);
        JsonArrayBuilder filteredItemsBuilder = new JsonArrayBuilder(JsonArray.EMPTY);

        for (JsonValue item : items) {
            filteredItemsBuilder.add(item.getJsonObject().remove("_internal"));
        }

        return collectionResponse.body(JsonObjectWrapper.of(resourceBody.put("value", filteredItemsBuilder.build())));
    }
}
