/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectMerger;
import com.zuunr.json.JsonValue;
import com.zuunr.json.schema.validation.JsonSchemaValidator;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.decorator.ResourceDecoratorDelegator;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.core.forms.FormProvider;
import com.zuunr.restbed.core.forms.FormProviderKey;
import com.zuunr.restbed.core.ion.ServiceConfigFormProvider;
import com.zuunr.restbed.core.restservice.ServiceConfig;
import com.zuunr.restbed.core.restservice.ServiceConfigProvider;
import com.zuunr.restbed.core.service.translator.ExchangeUrlTranslator;
import com.zuunr.restbed.core.service.util.EtagProvider;
import com.zuunr.restbed.core.service.util.InternalParameterFilter;
import com.zuunr.restbed.core.service.util.ItemIdCreator;
import com.zuunr.restbed.core.service.util.QueryExtender;
import com.zuunr.restbed.core.validation.DataValidator;
import com.zuunr.restbed.core.validation.SchemaProvider;
import com.zuunr.restbed.repository.ReactiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Optional;

/**
 * <p>Unprotected implementation of {@link RestService}.</p>
 *
 * @author Niklas Eldberger
 * @author Mikael Ahlberg
 * @see RestService
 */
@Component
public class BasicRestService implements RestService {

    private JsonSchemaValidator jsonSchemaValidator = new JsonSchemaValidator();
    private JsonObjectMerger jsonObjectMerger = new JsonObjectMerger();

    private DataValidator dataValidator;
    private ReactiveRepository<JsonObjectWrapper> reactiveRepository;
    private RestServiceHelper restServiceHelper;
    private QueryExtender queryExtender;
    private ExchangeUrlTranslator exchangeUrlTranslator;
    private ItemIdCreator itemIdCreator;
    private ServiceConfigProvider serviceConfigProvider;

    @Autowired
    public BasicRestService(
            ReactiveRepository<JsonObjectWrapper> reactiveRepository,
            ServiceConfigProvider serviceConfigProvider,
            ResourceDecoratorDelegator decoratorDelegator,
            EtagProvider etagProvider,
            InternalParameterFilter internalParameterFilter,
            ExchangeUrlTranslator exchangeUrlTranslator,
            ItemIdCreator itemIdCreator) {
        FormProvider formProvider = new ServiceConfigFormProvider(serviceConfigProvider);
        this.dataValidator = new DataValidator(new SchemaProvider(serviceConfigProvider, formProvider));
        this.reactiveRepository = reactiveRepository;
        this.exchangeUrlTranslator = exchangeUrlTranslator;
        this.itemIdCreator = itemIdCreator;
        this.serviceConfigProvider = serviceConfigProvider;
        queryExtender = new QueryExtender(this);
        restServiceHelper = new RestServiceHelper(new ResourceExtender(this, formProvider), reactiveRepository, decoratorDelegator, etagProvider, internalParameterFilter);
    }

    @Override
    public Mono<Exchange<JsonObjectWrapper>> service(Exchange<JsonObjectWrapper> exchange) {
        return Mono.just(exchange)
                .map(exchangeUrlTranslator::translate)
                .flatMap(o -> exchange.getRequest().getMethod() == Method.GET
                        ? Mono.from(getResource(o))
                        : Mono.from(postOrPatchOrDeleteResource(o)));
    }

    private Mono<Exchange<JsonObjectWrapper>> postOrPatchOrDeleteResource(Exchange<JsonObjectWrapper> exchange) {
        Request<JsonObjectWrapper> request = exchange.getRequest();

        return Mono.just(request)
                .flatMap(this::getResourceBeforeUpdate)
                .flatMap(resourceBeforeUpdate -> isValidPostPatchDeleteState(request, resourceBeforeUpdate)
                        ? Mono.just(exchange)
                        .map(this::appendIdIfPost)
                        .flatMap(o -> validateAndSaveResource(o, resourceBeforeUpdate))
                        // save resource here instead
                        : Mono.just(exchange.response(Response.create(StatusCode._404_NOT_FOUND))));
    }

    private Mono<Exchange<JsonObjectWrapper>> validateAndSaveResource(Exchange<JsonObjectWrapper> exchange, Optional<JsonObject> resourceBeforeUpdate) {
        RequestBodyValidationParams requestBodyValidationParams = restServiceHelper.prepareForRequestBodyValidation(exchange.getRequest(), resourceBeforeUpdate.orElse(null));
        Response<JsonObjectWrapper> response = validateRequestBody(requestBodyValidationParams);

        if (response != null) {
            return Mono.just(exchange.response(response));
        }

        return Mono.just(requestBodyValidationParams)
                .map(o -> Method.DELETE == exchange.getRequest().getMethod()
                        // toStatus should be the status of the resource BEFORE deletion and therefore we cannot set it to NO_STATUS
                        ? requestBodyValidationParams.getRequest().getBodyAsJsonObject()
                        : requestBodyValidationParams.getRequest().getBodyAsJsonObject().put("status", requestBodyValidationParams.getToStatus()))
                .flatMap(o -> prepareForResourceStateValidation(exchange.getRequest().getApiUriInfo(), o, resourceBeforeUpdate.orElse(null)))
                .flatMap(resourceStateValidationParams -> Mono.just(resourceStateValidationParams)
                        .flatMap(i -> Mono.justOrEmpty(
                                validateResourceState(requestBodyValidationParams.getToStatus(), exchange.getRequest(), resourceStateValidationParams))
                        )
                        .map(exchange::response)
                        .switchIfEmpty(saveResource(exchange.getRequest(), resourceStateValidationParams)));
    }

    private Exchange<JsonObjectWrapper> appendIdIfPost(Exchange<JsonObjectWrapper> exchange) {
        if (exchange.getRequest().getMethod() == Method.POST) {
            exchange = itemIdCreator.appendId(exchange);
        }

        return exchange;
    }

    private Mono<Exchange<JsonObjectWrapper>> saveResource(Request<JsonObjectWrapper> request, ResourceStateValidationParams resourceStateValidationParams) {

        JsonObject decoratedObject = resourceStateValidationParams.getDecoratedResource();

        ServiceConfig serviceConfig = serviceConfigProvider.getServiceConfig(request.getApiUriInfo().apiName());

        // TODO: This merge could be done in as(SchemaPatchedToExcludeInternalResourceStateValidation.class)
        JsonValue persistentResourceModelSchema = serviceConfig.asJsonObject().get(JsonArray.of("config", "collections", request.getApiUriInfo().collectionName(), "persistentResourceBody", "schema"), true);
        if (persistentResourceModelSchema.isBoolean() && persistentResourceModelSchema.getBoolean()) {
            persistentResourceModelSchema = SchemaProvider.EXCLUDE_VALIDATIONS_IN_MODEL.jsonValue();
        } else {
            persistentResourceModelSchema = jsonObjectMerger.merge(persistentResourceModelSchema.getJsonObject(), SchemaProvider.EXCLUDE_VALIDATIONS_IN_MODEL).jsonValue();
        }
        // TODO end

        JsonObject decoratedItemWithOnlyFieldsToBePersisted = jsonSchemaValidator.filter(decoratedObject.jsonValue(), persistentResourceModelSchema).getJsonObject();

        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(request.body(JsonObjectWrapper.of(decoratedItemWithOnlyFieldsToBePersisted)));

        return Mono.just(exchange)
                .flatMap(restServiceHelper::saveResource);
    }

    private boolean isValidPostPatchDeleteState(Request<JsonObjectWrapper> request, Optional<JsonObject> resourceBeforeUpdate) {
        return Method.PATCH == request.getMethod() && resourceBeforeUpdate.isPresent() ||
                Method.POST == request.getMethod() ||
                Method.DELETE == request.getMethod() && resourceBeforeUpdate.isPresent();
    }

    private Mono<Optional<JsonObject>> getResourceBeforeUpdate(Request<JsonObjectWrapper> request) {
        if (request.getMethod() == Method.POST) {
            return Mono.just(Optional.empty());
        }

        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.GET)
                        .url(request.getUrl()));

        return Mono.just(exchange)
                .flatMap(o -> reactiveRepository.getItem(o, JsonObjectWrapper.class))
                .map(Exchange::getResponse)
                .map(o -> Optional.ofNullable(o.getBodyAsJsonObject()));
    }

    /**
     * <p>The method will decorate the resource (by first merging the resource with the
     * saved resource) and create an extended version of the resource, extending any
     * href for later state verification.</p>
     *
     * <p>The decoration is also extending the resource to be able to use it in the decoration
     * phase, i.e. the resource is extended twice (different instances).</p>
     *
     * <p>The decorated resource, as well as the extended resource is returned
     * in an {@link ResourceStateValidationParams}.</p>
     *
     * @param itemHref             is the resource original {@link ApiUriInfo}
     * @param requestBody          is the request, but with updated status parameter
     * @param resourceBeforeUpdate is the resource as it looks now in the database
     * @return a {@link ResourceStateValidationParams} including the decorated resource and the fully extended resource
     */
    protected Mono<ResourceStateValidationParams> prepareForResourceStateValidation(ApiUriInfo itemHref, JsonObject requestBody, JsonObject resourceBeforeUpdate) {
        return Mono.just(requestBody)
                .flatMap(o -> restServiceHelper.decorate(itemHref, o, resourceBeforeUpdate))
                .flatMap(restServiceHelper::extendResource)
                .map(extendedResource -> new ResourceStateValidationParamsImpl(extendedResource.getUnextended(), extendedResource.getFullyExtended()));
    }

    protected Response<JsonObjectWrapper> validateResourceState(JsonValue toStatus, Request<JsonObjectWrapper> request, ResourceStateValidationParams resourceStateValidationParams) {
        return dataValidator.validate(new FormProviderKey.Builder()
                        .toStatus(toStatus)
                        .formType(FormProviderKey.FormType.RESOURCE_BODY)
                        .apiName(request.getApiUriInfo().apiName())
                        .collectionName(request.getApiUriInfo().collectionName())
                        .build(),
                resourceStateValidationParams.getExtendedDecoratedResource());
    }

    protected Response<JsonObjectWrapper> validateRequestBody(RequestBodyValidationParams params) {
        return dataValidator.validate(
                new FormProviderKey.Builder()
                        .fromStatus(params.getFromStatus())
                        .toStatus(JsonValue.of(params.getToStatus()))
                        .formType(FormProviderKey.FormType.REQUEST_BODY)
                        .apiName(params.getRequest().getApiUriInfo().apiName())
                        .collectionName(params.getRequest().getApiUriInfo().collectionName())
                        .build(),
                params.getRequest().getBodyAsJsonObject());
    }

    protected Response<JsonObjectWrapper> validateGetRequest(Request<JsonObjectWrapper> request) {
        ApiUriInfo apiUriInfo = request.getApiUriInfo();

        FormProviderKey formProviderKey = new FormProviderKey.Builder()
                .apiName(apiUriInfo.apiName())
                .collectionName(apiUriInfo.collectionName())
                .formType(apiUriInfo.isItem() ?
                        FormProviderKey.FormType.ITEM_QUERY_PARAMS :
                        FormProviderKey.FormType.COLLECTION_QUERY_PARAMS)
                .build();
        return dataValidator.validate(formProviderKey, apiUriInfo.queryParams());
    }

    private Mono<Exchange<JsonObjectWrapper>> getResource(Exchange<JsonObjectWrapper> exchange) {
        Response<JsonObjectWrapper> response = validateGetRequest(exchange.getRequest());

        if (response != null) {
            return Mono.just(exchange.response(response));
        }

        return Mono.just(exchange)
                .flatMap(o -> o.getRequest().getApiUriInfo().isCollection()
                        ? restServiceHelper.getCollection(o)
                        : restServiceHelper.getItem(o))
                .flatMap(resultExchange -> resultExchange.getResponse().getStatusCode() != StatusCode._200_OK.getCode()
                        ? Mono.just(resultExchange)
                        : Mono.just(resultExchange)
                        .flatMap(i -> queryExtender.extendResource(i.getRequest(), i.getResponse().getBodyAsJsonObject()))
                        .map(JsonObjectWrapper::of)
                        .map(extendedResource -> resultExchange.response(resultExchange.getResponse().body(extendedResource))));
    }

}
