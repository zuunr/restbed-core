package com.zuunr.restbed.core.service.util;

import java.util.function.Function;

import com.zuunr.forms.util.JsonValuePathsFinder;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

/**
 * <p>TODO Add description.</p>
 *
 * @author Mikael Ahlberg
 * @author Niklas Eldberger
 */
public final class ReactiveLinkExtender {

    public Mono<JsonObject> decorateLinkedResources(JsonObject resource, JsonArray patternForHrefsToBeDecorated, Function<JsonObject, Mono<JsonObject>> decorator) {
        return decorateAllPathsButSelf(resource, patternForHrefsToBeDecorated, decorator);
    }

    public Mono<JsonObject> decorateAllPaths(JsonObject original, JsonArray pathsWithHrefs, Function<JsonObject, Mono<JsonObject>> hrefObjectDecorator) {
        return Flux.fromIterable(pathsWithHrefs)
                .map(o -> o.as(JsonArray.class))
                .flatMapSequential(path -> Mono.subscriberContext()
                        .flatMap(ctxHolder -> {
                            JsonObject result = ctxHolder.get(ReactiveLinkExtenderContext.class).getContext();
                            
                            return Mono.just(result)
                                    .flatMap(i -> decoratePath(i, path, hrefObjectDecorator))
                                    .map(updatedResult -> {
                                        ctxHolder.get(ReactiveLinkExtenderContext.class).setContext(updatedResult);
                                        return updatedResult;
                                    });
                        }))
                .last()
                .subscriberContext(ctxHolder -> ctxHolder.put(ReactiveLinkExtenderContext.class, ReactiveLinkExtenderContext.builder()
                        .context(original)
                        .build()));
    }

    public Mono<JsonObject> decorateAllPathsButSelf(JsonObject original, JsonArray pathsWithHrefs, Function<JsonObject, Mono<JsonObject>> hrefObjectDecorator) {
        return Flux.fromIterable(pathsWithHrefs)
                .map(o -> o.as(JsonArray.class))
                .flatMapSequential(path -> Mono.subscriberContext()
                        .flatMap(ctxHolder -> {
                            JsonObject result = ctxHolder.get(ReactiveLinkExtenderContext.class).getContext();
                            
                            if (path.size() != 1) {
                                return Mono.just(result)
                                        .flatMap(i -> decoratePath(i, path, hrefObjectDecorator))
                                        .map(updatedResult -> {
                                            ctxHolder.get(ReactiveLinkExtenderContext.class).setContext(updatedResult);
                                            return updatedResult;
                                        });
                            } else {
                                return Mono.just(result);
                            }
                        }), 1)
                .last()
                .subscriberContext(ctxHolder -> ctxHolder.put(ReactiveLinkExtenderContext.class, ReactiveLinkExtenderContext.builder()
                        .context(original)
                        .build()));
    }

    private Mono<JsonObject> decoratePath(JsonObject original, JsonArray pathWithHref, Function<JsonObject, Mono<JsonObject>> hrefObjectDecorator) {
        if (pathWithHref.size() == 1) {
            // pathWithHref is always ["href"] in this case
            return Mono.from(doDecorate(original, hrefObjectDecorator));
        }
        
        JsonValuePathsFinder pathsFinder = new JsonValuePathsFinder();
        JsonArray paths = pathsFinder.getPaths(original.jsonValue(), pathWithHref.allButLast(), true);
                
        return Flux.fromIterable(paths)
                .map(path -> path.as(JsonArray.class))
                .flatMap(jsonArrayPath -> JsonValue.NULL.equals(jsonArrayPath.last())
                        ? Mono.empty()
                        : Mono.from(doDecorate(jsonArrayPath.last().getValue(JsonObject.class), hrefObjectDecorator)
                                .map(decorated -> Tuples.of(jsonArrayPath.allButLast(), decorated))))
                .collectList()
                .map(o -> {
                    JsonObject result = original;
                    
                    for (Tuple2<JsonArray, JsonObject> resultTuple : o) {
                        result = result.put(resultTuple.getT1(), resultTuple.getT2());
                    }
                    
                    return result;
                });
    }

    private Mono<JsonObject> doDecorate(JsonObject original, Function<JsonObject, Mono<JsonObject>> hrefObjectDecorator) {
        return hrefObjectDecorator.apply(original);
    }
}
