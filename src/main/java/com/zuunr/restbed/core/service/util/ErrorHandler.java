/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service.util;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;

/**
 * <p>The ErrorHandler is responsible for handling any
 * exception in the service.</p>
 * 
 * <p>These errors should not be exposed to the client,
 * instead a HTTP 500 error should be sent along with
 * a generic error message.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class ErrorHandler {
    
    private final Logger logger = LoggerFactory.getLogger(ErrorHandler.class);
    
    /**
     * <p>Creates a generic HTTP 500 response containing a generated id,
     * which can be found in the logs together with the original
     * exception.</p>
     * 
     * @param e is the throwable to handle
     * @param exchange contains the actual user request
     * @return a HTTP 500 response containing a generic error message with a generated id
     */
    public Exchange<JsonObjectWrapper> handleException(Throwable e, Exchange<JsonObjectWrapper> exchange) {
        String id = UUID.randomUUID().toString();
        String errorMessage = "An unexpected error occurred, id: " + id;

        logger.error(errorMessage, e);
        
        return exchange.response(Response.<JsonObjectWrapper>create(StatusCode._500_INTERNAL_SERVER_ERROR)
                .body(JsonObjectWrapper.of(JsonObject.EMPTY.put("error", errorMessage))));
    }
}
