/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.authorization.AccessController;
import com.zuunr.restbed.core.authorization.claims.ClaimsProvider;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.core.href.RequestResponseTranslator;
import com.zuunr.restbed.core.ion.ServiceConfigFormProvider;
import com.zuunr.restbed.core.restservice.ServiceConfigProvider;
import com.zuunr.restbed.core.service.util.ErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

/**
 * <p>Protected implementation of {@link RestService}.</p>
 *
 * @author Niklas Eldberger
 * @author Mikael Ahlberg
 * @see RestService
 */
@Component
public class ProtectedRestService implements RestService {

    private RestService unprotectedRestService;
    private AccessController accessController;
    private ServiceConfigProvider serviceConfigProvider;
    private ErrorHandler errorHandler;
    private RequestResponseTranslator requestResponseTranslator;
    private ClaimsProvider claimsProvider;

    @Autowired
    public ProtectedRestService(
            AccessController accessController,
            @Qualifier("basicRestService") RestService unprotectedRestService,
            ServiceConfigProvider serviceConfigProvider,
            ErrorHandler errorHandler,
            ClaimsProvider claimsProvider) {
        this.accessController = accessController;
        this.unprotectedRestService = unprotectedRestService;
        this.serviceConfigProvider = serviceConfigProvider;
        this.errorHandler = errorHandler;
        this.requestResponseTranslator = new RequestResponseTranslator(new ServiceConfigFormProvider(serviceConfigProvider));
        this.claimsProvider = claimsProvider;
    }

    @Override
    public Mono<Exchange<JsonObjectWrapper>> service(Exchange<JsonObjectWrapper> exchange) {
        ApiUriInfo internalConfiguredBaseUrl = getInternalConfiguredBaseUrl(exchange);

        return Mono.just(exchange)
                .map(o -> requestResponseTranslator.translateRequest(o, internalConfiguredBaseUrl))
                .flatMap(o -> isAccessControlEnabled(o.getRequest().getApiUriInfo().apiName())
                        ? Mono.from(accessControlledService(o))
                        : Mono.from(processExchange(o)))
                .map(o -> requestResponseTranslator.translateResponse(o, internalConfiguredBaseUrl))
                .onErrorResume(error -> Mono.just(errorHandler.handleException(error, exchange)));
    }

    private Mono<Exchange<JsonObjectWrapper>> accessControlledService(Exchange<JsonObjectWrapper> exchange) {
        Request<JsonObjectWrapper> request = exchange.getRequest();


        Mono<JsonObject> claimsMono = Mono.just(request)
                .flatMap(claimsProvider::getClaims);

        Mono<JsonArray> effectiveContextMono = claimsMono.flatMap(claims -> accessController.getEffectiveContext(claims, request));

        Mono<Tuple2<JsonObject, JsonArray>> claimsAndEffectiveContextMono = Mono.zip(claimsMono, effectiveContextMono);

        return claimsAndEffectiveContextMono.flatMap(claimsAndEffectiveContext -> {

            JsonObject claims = claimsAndEffectiveContext.getT1();
            JsonArray effectiveContext = claimsAndEffectiveContext.getT2();

            Tuple2<JsonArray, Exchange<JsonObjectWrapper>> result = getShortestGrantedContextDepth(
                    exchange, claims, effectiveContext);

            JsonArray updatedEffectiveContext = result.getT1();
            Exchange<JsonObjectWrapper> updatedExchange = result.getT2();

            return Mono.just(updatedExchange)
                    .flatMap(o -> o.getResponse() == null
                            ? Mono.from(processExchange(exchange))
                            : Mono.just(o))
                    .map(responseExchange ->
                            responseExchange.getResponse().getBody() == null
                                    ? responseExchange
                                    : responseExchange.response(
                                    accessController.filterResponseBody(responseExchange, updatedEffectiveContext, claims)));
        });
    }

    private Tuple2<JsonArray, Exchange<JsonObjectWrapper>> getShortestGrantedContextDepth(Exchange<JsonObjectWrapper> exchange, JsonObject claims, JsonArray effectiveContext) {
        exchange = exchange.response(accessController.authorize(exchange.getRequest(), effectiveContext, claims));
        Exchange<JsonObjectWrapper> firstResultingExchange = exchange;

        // Check if a shorter common context depth will result in granted permission
        while (exchange.getResponse() != null && !effectiveContext.isEmpty()) {
            effectiveContext = effectiveContext.allButLast();
            exchange = exchange.response(accessController.authorize(exchange.getRequest(), effectiveContext, claims));
            if (exchange.getResponse() == null) {
                break;
            }
        }

        // If shorter common context depth does not result in granted permission then
        // return the error of deepest common context depth
        // TODO: Response should contain options where all possible context depths are possible to enable
        //       end user to understand all possible ways to get access.
        if (exchange.getResponse() != null) {
            exchange = firstResultingExchange;
        }

        return Tuples.of(effectiveContext, exchange);
    }

    private Mono<Exchange<JsonObjectWrapper>> processExchange(Exchange<JsonObjectWrapper> exchange) {
        Method method = exchange.getRequest().getMethod();

        return Mono.just(exchange)
                .flatMap(o -> isSupportedMethod(method)
                        ? unprotectedRestService.service(o)
                        : Mono.just(exchange.response(Response.<JsonObjectWrapper>create(StatusCode._405_METHOD_NOT_ALLOWED))));
    }

    private boolean isSupportedMethod(Method method) {
        return Method.GET == method || Method.POST == method || Method.PATCH == method || Method.DELETE == method;
    }

    private boolean isAccessControlEnabled(String apiName) {
        return serviceConfigProvider.getServiceConfig(apiName).asJsonObject().get(JsonArray.ofDotSeparated("config.security.enabled"), true).getValue(Boolean.class);
    }

    private ApiUriInfo getInternalConfiguredBaseUrl(Exchange<JsonObjectWrapper> exchange) {
        String apiName = null;

        try {
            apiName = exchange.getRequest().getApiUriInfo().apiName();
        } catch (Exception e) {
            // If an exception occurs, it means that we have probably changed path with an external configuration
            // Then use null as apiName key
        }

        return serviceConfigProvider.getServiceConfig(apiName).getApiNameUrl();
    }
}
