/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service.util;

import com.zuunr.json.JsonObject;

import reactor.util.context.Context;

/**
 * <p>The ReactiveLinkExtenderContext is a mutable value holder
 * to be used with immutable Reactor {@link Context} object, storing
 * the desired {@link JsonObject} context.</p>
 *
 * @author Mikael Ahlberg
 */
public class ReactiveLinkExtenderContext {
    
    private JsonObject context;

    private ReactiveLinkExtenderContext(Builder builder) {
        this.context = builder.context;
    }

    public JsonObject getContext() {
        return context;
    }
    
    public void setContext(JsonObject context) {
        this.context = context;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private JsonObject context;

        private Builder() {
        }

        public Builder context(JsonObject context) {
            this.context = context;
            return this;
        }

        public ReactiveLinkExtenderContext build() {
            return new ReactiveLinkExtenderContext(this);
        }
    }
}
