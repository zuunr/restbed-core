/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Request;

public class RequestBodyValidationParams {

    private String fromStatus;
    private JsonValue toStatus;
    private Request<JsonObjectWrapper> request;
    private JsonValue resourceItemState;

    private RequestBodyValidationParams(Builder builder) {
        this.fromStatus = builder.fromStatus;
        this.toStatus = builder.toStatus;
        this.request = builder.request;
        this.resourceItemState = builder.resourceItemState;
    }
    
    public String getFromStatus() {
        return fromStatus;
    }
    
    public JsonValue getToStatus() {
        return toStatus;
    }
    
    public Request<JsonObjectWrapper> getRequest() {
        return request;
    }

    public JsonValue getResourceItemState() {
        return resourceItemState;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String fromStatus;
        private JsonValue toStatus;
        private Request<JsonObjectWrapper> request;
        private JsonValue resourceItemState;

        private Builder() {
        }

        public Builder fromStatus(String fromStatus) {
            this.fromStatus = fromStatus;
            return this;
        }

        public Builder toStatus(JsonValue toStatus) {
            this.toStatus = toStatus;
            return this;
        }

        public Builder request(Request<JsonObjectWrapper> request) {
            this.request = request;
            return this;
        }

        public Builder resourceItemState(JsonValue resourceItemState) {
            this.resourceItemState = resourceItemState;
            return this;
        }

        public RequestBodyValidationParams build() {
            return new RequestBodyValidationParams(this);
        }
    }
}
