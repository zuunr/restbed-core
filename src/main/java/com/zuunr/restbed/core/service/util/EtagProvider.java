/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service.util;

import com.zuunr.json.JsonValue;
import com.zuunr.restbed.core.decorator.model.ResourceModel;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

/**
 * <p>The EtagProvider is responsible for setting and retrieving the _internal.etag
 * element of any provided {@link JsonObject}.</p>
 * 
 * @author Mikael Ahlberg
 */
@Component
public class EtagProvider {

    /**
     * <p>Sets a new etag string on the given body sent into the method, replacing any
     * previous etag.</p>
     * 
     * @param body used to set the new _internal.etag on
     * @return an updated {@link JsonObject} containing the newly created etag
     */
    public JsonObject appendEtag(JsonObject body) {
        return body.put(ResourceModel.INTERNAL_ETAG, new ObjectId().toString());
    }

    /**
     * <p>Retrieves the etag element of the given body.</p>
     * 
     * @param body used to retrieve the _internal.etag from
     * @return a string with the etag
     */
    public String getEtag(JsonObject body) {
        return body.get(ResourceModel.INTERNAL_ETAG, JsonValue.NULL).as(String.class);
    }
    
    /**
     * <p>Copies the etag string from the given body to another given body,
     * replacing any previous etag.</p>
     * 
     * @param body used to set the _internal.etag on
     * @param bodyToCopyFrom uset to copy the the _internal.etag from
     * @return an updated {@link JsonObject} containing the newly copied etag 
     */
    public JsonObject copyEtag(JsonObject body, JsonObject bodyToCopyFrom) {
        return body.put(ResourceModel.INTERNAL_ETAG, getEtag(bodyToCopyFrom));
    }
}
