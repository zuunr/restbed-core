/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectMerger;
import com.zuunr.json.JsonValue;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.decorator.DecoratorDelegator;
import com.zuunr.restbed.core.decorator.model.ResourceModel;
import com.zuunr.restbed.core.exchange.Collection;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.core.service.ResourceExtender.ExtendedResource;
import com.zuunr.restbed.core.service.util.EtagProvider;
import com.zuunr.restbed.core.service.util.InternalParameterFilter;
import com.zuunr.restbed.repository.ReactiveRepository;
import org.springframework.http.HttpHeaders;
import reactor.core.publisher.Mono;

/**
 * <p>The RestServiceHelper is planned for a split in upcoming releases TODO</p>
 *
 * @author Niklas Eldberger
 * @author Mikael Ahlberg
 */
public class RestServiceHelper {

    private static final String STATUS_KEY = "status";
    private static final String NO_STATUS_VALUE = "NO_STATUS";

    private static final JsonObjectMerger merger = new JsonObjectMerger();

    private ReactiveRepository<JsonObjectWrapper> reactiveRepository;
    private ResourceExtender resourceExtender;
    private DecoratorDelegator decoratorDelegator;
    private EtagProvider etagProvider;
    private InternalParameterFilter internalParameterFilter;

    public RestServiceHelper(
            ResourceExtender resourceExtender,
            ReactiveRepository<JsonObjectWrapper> reactiveRepository,
            DecoratorDelegator decoratorDelegator,
            EtagProvider etagProvider,
            InternalParameterFilter internalParameterFilter) {
        this.reactiveRepository = reactiveRepository;
        this.resourceExtender = resourceExtender;
        this.decoratorDelegator = decoratorDelegator;
        this.etagProvider = etagProvider;
        this.internalParameterFilter = internalParameterFilter;
    }

    /**
     * <p>Saves the {@link Request#body(com.zuunr.json.JsonObjectSupport)} from the provided exchange
     * object into the reactive repository.</p>
     *
     * <p>Internal attributes are filtered according to
     * {@link InternalParameterFilter#filterCollection(Response)} before response
     * is returned.</p>
     *
     * @param exchange contains the request body to save
     * @return an updated exchange containing the response
     */
    public Mono<Exchange<JsonObjectWrapper>> saveResource(Exchange<JsonObjectWrapper> exchange) {
        Mono<Exchange<JsonObjectWrapper>> exchangeMono = Mono.just(exchange);

        if (exchange.getRequest().getMethod() == Method.PATCH) {
            exchangeMono = exchangeMono.flatMap(o -> reactiveRepository.updateItem(o, JsonObjectWrapper.class))
                    .map(o -> o.response(internalParameterFilter.filterItem(o.getResponse())));
        } else if (Method.POST == exchange.getRequest().getMethod()) {
            exchangeMono = exchangeMono.flatMap(o -> reactiveRepository.saveItem(o, JsonObjectWrapper.class))
                    .map(o -> o.response(internalParameterFilter.filterItem(o.getResponse())))
                    .map(this::addLocationHeader);
        } else if (Method.DELETE == exchange.getRequest().getMethod()) {
            exchangeMono = exchangeMono.flatMap(o -> reactiveRepository.deleteItem(o));
        } else {
            exchangeMono = exchangeMono.map(o -> o.response(Response.<JsonObjectWrapper>create(StatusCode._500_INTERNAL_SERVER_ERROR)
                    .body(JsonObjectWrapper.of(null))));
        }

        return exchangeMono;
    }

    private Exchange<JsonObjectWrapper> addLocationHeader(Exchange<JsonObjectWrapper> exchange) {
        JsonObject locationHeader = JsonObject.EMPTY.put(HttpHeaders.LOCATION, getHref(exchange));

        return exchange.response(exchange.getResponse()
                .headers(locationHeader));
    }

    private String getHref(Exchange<JsonObjectWrapper> exchange) {
        JsonObject requestBody = exchange.getRequest().getBodyAsJsonObject();

        return requestBody.get("href").getValue(String.class);
    }

    /**
     * <p>Retrieves the item given the url in the provided request object.</p>
     *
     * <p>Internal attributes are filtered according to
     * {@link InternalParameterFilter#filterCollection(Response)} before response
     * is returned.</p>
     *
     * @param exchange contains the request with the url to fetch
     * @return an item response in the form of an {@link Exchange}
     */
    public Mono<Exchange<JsonObjectWrapper>> getItem(Exchange<JsonObjectWrapper> exchange) {
        return Mono.just(exchange)
                .flatMap(o -> reactiveRepository.getItem(o, JsonObjectWrapper.class))
                .map(o -> o.response(internalParameterFilter.filterItem(o.getResponse())));
    }

    /**
     * <p>Retrieves the collection given the url in the provided request object.</p>
     *
     * <p>Internal attributes are filtered according to
     * {@link InternalParameterFilter#filterCollection(Response)} before response
     * is returned.</p>
     *
     * @param exchange contains the request with the url to fetch
     * @return a collection response in the form of an {@link Exchange}
     */
    public Mono<Exchange<JsonObjectWrapper>> getCollection(Exchange<JsonObjectWrapper> exchange) {
        return Mono.just(exchange)
                .map(o -> Exchange.<Collection<JsonObjectWrapper>>empty()
                        .request(Request.<Collection<JsonObjectWrapper>>create(Method.GET).url(o.getRequest().getUrl())))
                .flatMap(o -> reactiveRepository.getCollection(o, JsonObjectWrapper.class))
                .map(o -> o.response(internalParameterFilter.filterCollection((Response) o.getResponse()))); // FIXME Collection<>
    }

    /**
     * <p>Merges the resourceBeforeUpdate with the new/updated resource, overriding previously
     * saved parameters and sets the resource to the state it will be when saved to the database
     * (before decorating).</p>
     *
     * <p>A new resource is also created, an extended resource where all hrefs pointing to internal
     * resources are extended. These two resources are used as input to the decorators.</p>
     *
     * <p>Returns a decorated resource with the etag of the previously saved resource (if any).</p>
     *
     * @param itemHref             is the resource original {@link ApiUriInfo}
     * @param requestBody          is the request, but with updated status parameter
     * @param resourceBeforeUpdate is the resource as it looks now in the database
     * @return a {@link JsonObject} containing the decorated resource with href, id and etag attached
     */
    public Mono<JsonObject> decorate(ApiUriInfo itemHref, JsonObject requestBody, JsonObject resourceBeforeUpdate) {
        JsonObject mergedResource = merger.merge(resourceBeforeUpdate == null ? JsonObject.EMPTY : resourceBeforeUpdate, requestBody)
                .put("href", itemHref.itemUri())
                .put("id", itemHref.itemId())
                .put(ResourceModel.INTERNAL_RESOURCE_BEFORE_UPDATE, resourceBeforeUpdate == null
                        ? JsonValue.NULL
                        : resourceBeforeUpdate.remove(ResourceModel.INTERNAL).jsonValue());

        return Mono.just(mergedResource)
                //.flatMap(this::extendResource)
                .map(this::decorateResourceWithHrefIds) // Should at some point be replaced with new ResourceExtender logic to use the knowledge of the model and not iterate every path to find hrefs
                .flatMap(mergedResourceDecoratedWithIds -> decoratorDelegator.decorate(mergedResourceDecoratedWithIds, mergedResourceDecoratedWithIds, itemHref.apiName(), itemHref.collectionName()))
                .map(decoratedResource -> restoreParametersAfterDecoration(itemHref, decoratedResource, resourceBeforeUpdate));
    }

    private JsonObject restoreParametersAfterDecoration(ApiUriInfo itemHref, JsonObject
            decoratedResource, JsonObject resourceBeforeUpdate) {
        if (resourceBeforeUpdate != null) {
            decoratedResource = etagProvider.copyEtag(decoratedResource, resourceBeforeUpdate);
        }

        decoratedResource = decoratedResource
                .put("href", itemHref.itemUri())
                .put("id", itemHref.itemId())
                .remove(ResourceModel.INTERNAL_RESOURCE_BEFORE_UPDATE);

        return decoratedResource;
    }

    /**
     * <p>The method will extend the resource ({@link JsonObject}) with data
     * for every found href in the resource.</p>
     *
     * @param resource is the resource to extend
     * @return a {@link ExtendedResource} containing the extended resource
     */
    public Mono<ResourceExtender.ExtendedResource> extendResource(JsonObject resource) {

        JsonValue internal = resource.get(ResourceModel.INTERNAL);
        Mono<ResourceExtender.ExtendedResource> extendedWithOutInternal = resourceExtender.extendResource(JsonObject.EMPTY, resource.remove(ResourceModel.INTERNAL));
        if (internal == null) {
            return extendedWithOutInternal;
        } else {
            return extendedWithOutInternal.map(noInternal -> new ExtendedResource(
                    noInternal.getUnextended().put(ResourceModel.INTERNAL, internal),
                    noInternal.getPartlyExtended().put(ResourceModel.INTERNAL, internal),
                    noInternal.getFullyExtended().put(ResourceModel.INTERNAL, internal)));
        }
    }

    public JsonObject decorateResourceWithHrefIds(JsonObject resource) {

        JsonObject updatedResource = resource;

        ApiUriInfo selfRef = resource.get("href").as(ApiUriInfo.class);

        JsonArray paths = resource.jsonValue().getPaths();
        for (JsonValue pathJsonValue : paths) {
            JsonArray path = pathJsonValue.getJsonArray();
            if (path.size() > 1 && JsonValue.of("href").equals(path.last())) {
                JsonValue href = resource.get(path);
                ApiUriInfo apiUriInfo = href.as(ApiUriInfo.class);
                if (selfRef.apiNameUri().equals(apiUriInfo.apiNameUri()) && apiUriInfo.isItem()) {
                    updatedResource = updatedResource.put(path.allButLast().add("id"), apiUriInfo.itemId());
                }
            }
        }
        return updatedResource;
    }

    /**
     * <p>The method extracts the "from" and "to" statuses given the two resource
     * inputs. The data is returned in the form of an {@link RequestBodyValidationParams}
     * containing the statuses as well as the request resource updated with the correct status parameter
     * set on the actual {@link JsonObject} object. I.e. the new status if it is to be updated,
     * or the current status if no new status is provided in the request.</p>
     *
     * @param request      is the request containing the resource, or resource updates
     * @param resourceView is the resource before update (can be null if POST)
     * @return a {@link RequestBodyValidationParams} containing the from and to status, as well as the request body
     */
    public RequestBodyValidationParams prepareForRequestBodyValidation(Request<JsonObjectWrapper> request, JsonObject resourceView) {
        JsonObject requestBody = request.getBodyAsJsonObject();

        if (Method.DELETE == request.getMethod()) {
            requestBody = JsonObject.EMPTY;
        }

        JsonValue requestStatusJsonValue = requestBody.get(STATUS_KEY);

        if (requestStatusJsonValue == null && resourceView != null) {
            String resourceStatus = resourceView.get(STATUS_KEY).getValue(String.class);
            requestBody = requestBody.put(STATUS_KEY, resourceStatus);
            request = request.body(JsonObjectWrapper.of(requestBody));
        }

        String fromStatus = null;
        if (Method.POST == request.getMethod()) {
            fromStatus = NO_STATUS_VALUE;
        } else if (resourceView != null) {
            fromStatus = resourceView.get(STATUS_KEY).getValue(String.class);
        } else {
            fromStatus = NO_STATUS_VALUE;
        }

        JsonValue toStatus = null;
        if (Method.DELETE == request.getMethod()) {
            toStatus = JsonValue.of(NO_STATUS_VALUE);
        } else if (requestStatusJsonValue != null) {
            toStatus = requestStatusJsonValue;
        } else if (Method.POST == request.getMethod()) {
            toStatus = defaultStatus();
        } else if (resourceView != null) {
            toStatus = resourceView.get(STATUS_KEY);
        } else {
            throw new IllegalStateException("Resource view is null for request: " + request.asJsonObject().asJson());
        }

        return RequestBodyValidationParams.builder()
                .fromStatus(fromStatus)
                .toStatus(toStatus)
                .request(request.body(JsonObjectWrapper.of(requestBody)))
                .resourceItemState(resourceView == null ? JsonValue.NULL : resourceView.jsonValue())
                .build();
    }

    private JsonValue defaultStatus() {
        return JsonValue.of("OK");
    }
}
