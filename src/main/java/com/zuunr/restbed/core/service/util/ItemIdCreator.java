/*
 * Copyright 2021 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.authorization.ContextAggregator;
import com.zuunr.restbed.core.authorization.ContextAggregatorProvider;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Request;

/**
 * <p>The ItemIdCreator is responsible for
 * creating item ids, containing eventual
 * contexts.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class ItemIdCreator {
    
    private ContextAggregatorProvider contextAggregatorProvider;
    
    @Autowired
    public ItemIdCreator(
            ContextAggregatorProvider contextAggregatorProvider) {
        this.contextAggregatorProvider = contextAggregatorProvider;
    }
    
    /**
     * <p>Creates and appends an item id to the URL of the provided
     * exchange's request, e.g. the URL example.com/persons will
     * become example.com/persons/12345.</p>
     * 
     * <p>The itemId will contain a context if the collection has
     * been configured to add a context to it.</p>
     * 
     * @param exchange contains the request with the url to append the item id to
     * @return an updated exchange
     */
    public Exchange<JsonObjectWrapper> appendId(Exchange<JsonObjectWrapper> exchange) {
        Request<JsonObjectWrapper> request = exchange.getRequest();
        
        return exchange.request(request
                .url(createUrlWithId(request))
                .headers(request.getHeaders())
                .body(request.getBody()));
    }

    private String createUrlWithId(Request<JsonObjectWrapper> request) {
        ContextAggregator contextAggregator = contextAggregatorProvider.getContextAggregator(request.getApiUriInfo().apiName(), request.getApiUriInfo().collectionName());
        String context = contextAggregator.createContextFromRequestBody(request.getBodyAsJsonObject());
        
        if (context != null && !context.equals("")) {
            return request.getUrl() + "/" + ApiUriInfo.createItemId(context);
        } else {
            return request.getUrl() + "/" + ApiUriInfo.createItemId();
        }
    }
}
