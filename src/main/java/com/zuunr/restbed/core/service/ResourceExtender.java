/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormFields;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.core.forms.FormProvider;
import com.zuunr.restbed.core.forms.FormProviderKey;
import com.zuunr.restbed.core.service.util.ReactiveDecorationTemplate;
import reactor.core.publisher.Mono;

import java.util.function.Function;

public class ResourceExtender {

    private static final String STATUS_KEY = "status";
    private static final String HREF_KEY = "href";
    private static final String ID_KEY = "id";
    private static final String NO_STATUS_VALUE = "NO_STATUS";

    private RestService restService;
    private FormProvider formProvider;

    public ResourceExtender(RestService restService, FormProvider formProvider) {
        this.restService = restService;
        this.formProvider = formProvider;
    }

    private final Function<JsonObject, Mono<JsonObject>> idAndStatusDecoratorFunction = jsonObject -> {
        JsonObject result = JsonObject.EMPTY;
        JsonValue href = jsonObject.get(HREF_KEY);
        ApiUriInfo apiUriInfo = href.as(ApiUriInfo.class);
        boolean isItem = apiUriInfo.isItem();

        if (isItem) {
            JsonValue status = jsonObject.get(STATUS_KEY, JsonValue.NULL);

            if (JsonValue.NULL.equals(status)) {
                result = result.put(STATUS_KEY, NO_STATUS_VALUE);
            } else {
                result = result.put(STATUS_KEY, status);
            }
            result = result.put(ID_KEY, apiUriInfo.itemId());
        }

        return Mono.just(result.put(HREF_KEY, href));
    };

    private final Function<JsonObject, Mono<JsonObject>> idDecoratorFunction = jsonObject -> {
        JsonObject result = JsonObject.EMPTY;
        JsonValue href = jsonObject.get(HREF_KEY);
        ApiUriInfo apiUriInfo = href.as(ApiUriInfo.class);
        boolean isItem = apiUriInfo.isItem();

        if (isItem) {
            result = result.put(ID_KEY, apiUriInfo.itemId());
        }

        return Mono.just(result.put(HREF_KEY, href));
    };

    /**
     * TODO
     *
     * @param exchange TODO
     * @return TODO
     */
    public Mono<Response<JsonObjectWrapper>> extendResponse(Exchange<JsonObjectWrapper> exchange) {
        return Mono.just(exchange)
                .map(Exchange::getResponse)
                .map(Response::getBodyAsJsonObject)
                .flatMap(extendedBaseResource -> extendedBaseResource != null
                        ? Mono.from(extendResource(exchange.getRequest().getHeaders(), extendedBaseResource, idDecoratorFunction)
                        .map(o -> o.partlyExtended))
                        : Mono.just(extendedBaseResource))
                .map(JsonObjectWrapper::of)
                .map(o -> exchange.getResponse().body(o));
    }

    /**
     * <p>Extends all hrefs in resourceToExtend of same api-name-uri with id and status.</p>
     *
     * @param requestHeaders   for use in calls to restService
     * @param resourceToExtend TODO
     * @return TODO
     */
    public Mono<ExtendedResource> extendResource(JsonObject requestHeaders, JsonObject resourceToExtend) {
        return Mono.from(extendResource(requestHeaders, resourceToExtend, idAndStatusDecoratorFunction));
    }

    /**
     * <p>Extends all hrefs in resourceToExtend of same api-name-uri according to partlyExtenderHandler.</p>
     *
     * @param requestHeaders   for use in calls to restService
     * @param resourceToExtend TODO
     * @param extensionHandler TODO
     * @return TODO
     */
    public Mono<ExtendedResource> extendResource(JsonObject requestHeaders, JsonObject resourceToExtend, Function<JsonObject, Mono<JsonObject>> extensionHandler) {
        ReactiveDecorationTemplate decorationTemplate = getResourceModelForm(resourceToExtend)
                .asJsonObject()
                .as(ReactiveDecorationTemplate.class);

        return Mono.defer(() -> decorationTemplate.decorateLinkedResourcesOfSameApiUri(resourceToExtend, jsonObject -> {
            JsonValue href = jsonObject.get(HREF_KEY);
            ApiUriInfo apiUriInfo = href.as(ApiUriInfo.class);

            if (apiUriInfo.isItem()) {
                return Mono.just(jsonObject)
                        .map(o -> o.put(ID_KEY, apiUriInfo.itemId()))
                        .flatMap(updatedJsonObject ->
                                Mono.just(Exchange.<JsonObjectWrapper>empty().request(Request.<JsonObjectWrapper>create(Method.GET).url(href.getValue(String.class)).headers(requestHeaders)))
                                        .flatMap(restService::service)
                                        .map(Exchange::getResponse)
                                        .map(o -> {
                                            if (StatusCode._200_OK.getCode() == o.getStatusCode()) {
                                                return o.getBodyAsJsonObject();
                                            } else if (StatusCode._404_NOT_FOUND.getCode() == o.getStatusCode()) {
                                                return updatedJsonObject.put(STATUS_KEY, NO_STATUS_VALUE);
                                            } else {
                                                return updatedJsonObject;
                                            }
                                        })
                        );
            }

            return Mono.just(jsonObject);
        }))
                .flatMap(fullyExtended -> {
                    return Mono.from(decorationTemplate.decorateLinkedResourcesOfSameApiUri(resourceToExtend, idDecoratorFunction))
                            .flatMap(updatedResource -> {
                                return Mono.from(decorationTemplate.decorateLinkedResourcesOfSameApiUri(fullyExtended, extensionHandler))
                                        .map(decoratedWithIdAndStatus -> new ExtendedResource(updatedResource, decoratedWithIdAndStatus, fullyExtended));
                            });
                });
    }

    /**
     * Returns the Form representing the model of the resourceToExtend
     *
     * @param resourceToExtend TODO
     * @return TODO
     */
    public Form getResourceModelForm(JsonObject resourceToExtend) {
        ApiUriInfo self;
        self = resourceToExtend.get(HREF_KEY).as(ApiUriInfo.class);

        FormProviderKey formProviderKey = new FormProviderKey.Builder()
                .apiName(self.apiName())
                .collectionName(self.collectionName())
                .formType(FormProviderKey.FormType.RESOURCE_MODEL).build();
        JsonArray formFields = formProvider.getForm(formProviderKey);
        Form rootForm = formFields.as(FormFields.class).of("root").schema().form().asExplicitForm();

        if (self.isItem()) {
            return rootForm.formField("value").schema().eform();
        } else {
            return rootForm;
        }
    }

    public static class ExtendedResource {
        private JsonObject unextended;
        private JsonObject fullyExtended;
        private JsonObject partlyExtended;

        public ExtendedResource(JsonObject unextended, JsonObject partlyExtended, JsonObject fullyExtended) {
            this.unextended = unextended;
            this.partlyExtended = partlyExtended;
            this.fullyExtended = fullyExtended;

        }

        public JsonObject getUnextended() {
            return unextended;
        }

        public JsonObject getFullyExtended() {
            return fullyExtended;
        }

        public JsonObject getPartlyExtended() {
            return partlyExtended;
        }
    }
}
