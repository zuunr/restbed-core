/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.cache;

import clojure.lang.IPersistentMap;
import clojure.lang.PersistentHashMap;

public class Cache<S, T> {
    
    private IPersistentMap persistentHashMap = PersistentHashMap.EMPTY;

    public Cache(){}

    private Cache(IPersistentMap persistentHashMap) {
        this.persistentHashMap = persistentHashMap;
    }

    public Cache<S, T> put(S key, T cachedObject) {
        persistentHashMap = persistentHashMap.assoc(key, cachedObject);
        return this;
    }

    @SuppressWarnings("unchecked")
    public T get(S key){
        return (T) persistentHashMap.valAt(key);
    }

    protected Cache<S, T> remove(S key) {
        persistentHashMap = persistentHashMap.without(key);
        return this;
    }

    @Override
    public Cache<S, T> clone() {
        return new Cache<>(persistentHashMap);
    }
}
