/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.cache;

import java.util.List;

import com.zuunr.json.JsonObject;

public class CacheProvider {
    private Cache<String, Cache<String, ? extends Object>> cacheOfCaches = new Cache<>();

    private Object monitor = "monitor";

    public static final CacheType<List> EVENT_PROCESSOR_CACHE = new CacheType<>("eventProcessors", List.class);
    public static final CacheType<JsonObject> PERMISSION_CACHE = new CacheType<>("permissions", JsonObject.class);
    public static final CacheType<List> DECORATOR_CACHE = new CacheType<>("decorators", List.class);
    public static final CacheType<List> SECURITY_CONTEXT_DECORATOR_CACHE = new CacheType<>("contextDecorators", List.class);

    public <T> Cache<String, T> getCache(String apiName, CacheType<T> cacheType) {
        return getCache(apiName, cacheType.cacheName, cacheType.typeOfCachedObject);
    }

    public <T> Cache<String, T> getCache(String apiName, String cacheName, Class<T> classOfCachedValues) {

        Cache<String, T> cache = null;
        Cache apiCache = cacheOfCaches.get(apiName);
        if (apiCache != null) {

            cache = (Cache<String, T>) apiCache.get(cacheName);

            if (cache == null) {
                cache = new Cache();
                apiCache.put(cacheName, cache);
            }
        } else {
            throw new NullPointerException("No cache for apiName: " + apiName);
        }

        return cache;
    }

    public CacheProvider dropCache(String apiName) {
        synchronized (monitor) {
            cacheOfCaches.remove(apiName);
        }
        return this;
    }

    public CacheProvider createCache(String apiName) {
        synchronized (monitor) {
            cacheOfCaches.put(apiName, new Cache<>());
        }
        return this;
    }


    public static class CacheType<T> {
        String cacheName;
        Class<T> typeOfCachedObject;

        public CacheType(String cacheName, Class<T> typeOfCachedObject) {
            this.cacheName = cacheName;
            this.typeOfCachedObject = typeOfCachedObject;
        }
    }

}
