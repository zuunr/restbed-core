/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository;

import com.zuunr.json.JsonObjectSupport;
import com.zuunr.restbed.core.exchange.Collection;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.exchange.Response;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * <p>A ReactiveRepository is the main handler for the data persistence logic. It
 * supports reactive components such as {@link Mono} and {@link Flux}.</p>
 * 
 * <p>An {@link Exchange} is the selected message container for request and
 * response messages.</p>
 * 
 * <p>The implementers of this interface is bound to set the correct response code
 * on the returning {@link Exchange}, depending on outcome. Returned exchanges
 * contains both the incoming {@link Request} and returned {@link Response} objects.</p>
 * 
 * @param <T> is the request and response object's body type
 * 
 * @author Mikael Ahlberg
 */
public interface ReactiveRepository<T extends JsonObjectSupport> {

    /**
     * <p>Saves the provided item contained in the exchange.</p>
     * 
     * @param exchange containing the request
     * @param resourceClass is the class of the resource to save
     * @return a mono exchange updated with the saved item
     */
    Mono<Exchange<T>> saveItem(Exchange<T> exchange, Class<T> resourceClass);

    /**
     * <p>Returns the content of the given item id (based on uri).</p>
     * 
     * @param exchange containing the request
     * @param resourceClass is the class of the resource to get
     * @return a mono exchange updated with the item
     */
    Mono<Exchange<T>> getItem(Exchange<T> exchange, Class<T> resourceClass);

    /**
     * <p>Updates the provided item contained in the exchange.</p>
     * 
     * @param exchange containing the request
     * @param resourceClass is the class of the resource to update
     * @return a mono exchange updated with the updated item
     */
    Mono<Exchange<T>> updateItem(Exchange<T> exchange, Class<T> resourceClass);

    /**
     * <p>Deletes the item based on the request url contained in the exchange.</p>
     * 
     * @param exchange containing the request
     * @return a mono exchange updated with response codes
     */
    Mono<Exchange<T>> deleteItem(Exchange<T> exchange);

    /**
     * <p>Returns the content of the selected collection (based on uri).</p>
     * 
     * @param exchange containing the request
     * @param resourceClass is the class of the collection to get
     * @return a mono exchange updated with the collection
     */
    Mono<Exchange<Collection<T>>> getCollection(Exchange<Collection<T>> exchange, Class<T> resourceClass);

}
