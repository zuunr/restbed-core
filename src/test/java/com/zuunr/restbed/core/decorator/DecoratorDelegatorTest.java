/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonObject;
import com.zuunr.restbed.core.decorator.model.DecoratorContainer;

import reactor.core.publisher.Mono;

class DecoratorDelegatorTest {
    
    private static final String MY_API_NAME = "myAPI";
    private static final String MY_COLLECTION_NAME = "myCollection";
    private static final JsonObject configration = JsonObject.EMPTY.put("parameters", "11223344");
    
    private DecoratorDelegator decoratorDelegator = new DecoratorDelegator(new MyDecoratorProvider());
    
    @Test
    void givenDecoratorForCollectionShouldDecorateResource() {
        // Creating resource
        JsonObject resource = JsonObject.EMPTY
                .put("href", "http://localhost/v1/" + MY_API_NAME + "/api/" + MY_COLLECTION_NAME + "/1")
                .put("someData", "myData");
        
        JsonObject extendedResource = resource.put("extended", "extended");
        
        // Decorating
        JsonObject decoratedResource = decoratorDelegator.decorate(resource, extendedResource, MY_API_NAME, MY_COLLECTION_NAME).block();
        
        assertEquals(resource.put("mydecoration", "12345"), decoratedResource);
    }
    
    @Test
    void givenNoDecoratorForCollectionShouldLeaveResourceAsIs() {
        // Creating resource
        JsonObject resource = JsonObject.EMPTY
                .put("href", "http://localhost/v1/" + MY_API_NAME + "/api/someOtherCollection/1")
                .put("someData", "myData");
        
        JsonObject extendedResource = resource.put("extended", "extended");
        
        // Decorating
        JsonObject decoratedResource = decoratorDelegator.decorate(resource, extendedResource, "someOtherApi", "someOtherCollection").block();
        
        assertEquals(resource, decoratedResource);
    }
    
    @Test
    void givenDecoratorWithSameDataAsResourceShouldOverwriteResourceData() {
        // Creating resource
        JsonObject resource = JsonObject.EMPTY
                .put("href", "http://localhost/v1/" + MY_API_NAME + "/api/" + MY_COLLECTION_NAME + "/1")
                .put("mydecoration", "shouldBeOverwritten");
        
        JsonObject extendedResource = resource.put("extended", "extended");
        
        // Decorating
        JsonObject decoratedResource = decoratorDelegator.decorate(resource, extendedResource, MY_API_NAME, MY_COLLECTION_NAME).block();
        
        assertEquals("12345", decoratedResource.get("mydecoration").as(String.class));
    }
    
    private class MyDecoratorProvider extends DecoratorProvider {
        
        public MyDecoratorProvider() {
            super(null, null, null, null);
        }
        
        @Override
        public List<DecoratorContainer> getDecorators(String apiName, String collectionName) {
            if (apiName.equals(MY_API_NAME) && collectionName.equals(MY_COLLECTION_NAME)) {
                return List.of(DecoratorContainer.builder()
                        .decorator(new MyDecorator())
                        .configuration(DecoratorDelegatorTest.configration)
                        .build());
            }
            
            return Collections.emptyList();
        }

        @Override
        protected List<DecoratorContainer> findDecorators(String apiName, String collectionName) {
            return null;
        }
    }
    
    private class MyDecorator implements Decorator {
        
        @Override
        public Mono<JsonObject> decorate(JsonObject resource, JsonObject configuration) {
            assertEquals(DecoratorDelegatorTest.configration, configuration, "Configration was not sent into the decorator");
            
            return Mono.just(JsonObject.EMPTY
                    .put("mydecoration", "12345"));
        }
    }
}
