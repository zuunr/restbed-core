/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.provided;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.restbed.core.decorator.DecoratorFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class SequentialDecoratorV1Test {

    private SequentialDecoratorV1 sequentialDecoratorV1;

    @BeforeEach
    public void init() {

        DecoratorFactory decoratorFactory = new DecoratorFactory();

        BasicExpressionLanguageDecoratorV1 basicExpressionLanguageDecoratorV1 = new BasicExpressionLanguageDecoratorV1(null);
        basicExpressionLanguageDecoratorV1.initialize();
        decoratorFactory.registerDecorator(basicExpressionLanguageDecoratorV1);

        sequentialDecoratorV1 = new SequentialDecoratorV1(decoratorFactory);
        sequentialDecoratorV1.init();
    }

    @Test
    void testMax3() {
        JsonObject resource = JsonObject.EMPTY
                        .put("count", 0);
        JsonObject result = sequentialDecoratorV1.decorate(resource, createConfiguration(3)).block();
        assertThat(result, is(JsonObject.EMPTY.put("count", 3).put("repeatMore", false)));
    }

    @Test
    void testMax2() {
        JsonObject resource = JsonObject.EMPTY
                .put("count", 0);
        JsonObject result = sequentialDecoratorV1.decorate(resource, createConfiguration(2)).block();
        assertThat(result, is(JsonObject.EMPTY.put("count", 2).put("repeatMore", true)));
    }

    private JsonObject createConfiguration(int max) {
        JsonObject configuration = JsonObject.EMPTY
                .put("sequence", JsonArray.of(
                        JsonObject.EMPTY
                                .put("decorator", "com.zuunr.restbed.core.decorator.provided.SequentialDecoratorV1")
                                .put("configuration", JsonObject.EMPTY
                                        .put("repeat", JsonObject.EMPTY
                                                .put("max", max)
                                                .put("predicateLocation", JsonArray.of("repeatMore")))
                                        .put("sequence", JsonArray.of(
                                                JsonObject.EMPTY
                                                        .put("decorator", "com.zuunr.restbed.core.decorator.provided.BasicExpressionLanguageDecoratorV1")
                                                        .put("configuration", JsonObject.EMPTY
                                                                .put("outputModelLocation", JsonArray.EMPTY)
                                                                .put("expressions", JsonArray.of(
                                                                        JsonObject.EMPTY
                                                                                .put("target", "count")
                                                                                .put("expression", "count + 1"),
                                                                        JsonObject.EMPTY
                                                                                .put("target", "repeatMore")
                                                                                .put("expression", "count < 3")
                                                                ))))))));
        return configuration;
    }
}
