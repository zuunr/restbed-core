/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.provided;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Collection;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.repository.ReactiveRepository;

import reactor.core.publisher.Mono;

class InboundUniquenessVerifierDecoratorV1Test { 
    
    @Test
    void givenParametersShouldCreateCorrectHref() {
        JsonObject resource = createResource();
        JsonObject configration = createConfiguration();
        
        JsonObject response = JsonObject.EMPTY
                .put("size", 0)
                .put("value", JsonArray.EMPTY);
        
        MyReactiveRepository myServiceCaller = new MyReactiveRepository(response);        
        new InboundUniquenessVerifierDecoratorV1(myServiceCaller)
                .decorate(resource, configration)
                .block();
        
        assertEquals("https://example.com/v1/myResource/api/myCollection?value._.name.firstName=Laura&value._.name.lastName=Andersson", myServiceCaller.sendingUrl);
    }
    
    @Test
    void givenNoMatchingResourcesShouldReturnEmptyJsonObject() {
        JsonObject resource = createResource();
        JsonObject configration = createConfiguration();
        
        JsonObject response = JsonObject.EMPTY
                .put("size", 0)
                .put("value", JsonArray.EMPTY);
        
        JsonObject decorationBody = new InboundUniquenessVerifierDecoratorV1(new MyReactiveRepository(response))
                .decorate(resource, configration)
                .block();
        
        assertEquals(JsonObject.EMPTY, decorationBody);
    }
    
    @Test
    void givenOneMatchingResourceWithSameIdShouldReturnEmptyJsonObject() {
        JsonObject resource = createResource();
        JsonObject configration = createConfiguration();
        
        JsonObject response = JsonObject.EMPTY
                .put("size", 1)
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("id", "123456")));
        
        JsonObject decorationBody = new InboundUniquenessVerifierDecoratorV1(new MyReactiveRepository(response))
                .decorate(resource, configration)
                .block();
        
        assertEquals(JsonObject.EMPTY, decorationBody);
    }
    
    @Test
    void givenOneMatchingResourceWithDifferentIdShouldReturnExistDecoration() {
        JsonObject resource = createResource();
        JsonObject configration = createConfiguration();
        
        JsonObject response = JsonObject.EMPTY
                .put("size", 1)
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("id", "99999999")));
        
        JsonObject decorationBody = new InboundUniquenessVerifierDecoratorV1(new MyReactiveRepository(response))
                .decorate(resource, configration)
                .block();
        
        assertEquals(JsonObject.EMPTY.put("userExists", true), decorationBody);
    }
    
    @Test
    void givenMatchingResourcesShouldReturnExistDecoration() {
        JsonObject resource = createResource();
        JsonObject configration = createConfiguration();
        
        JsonObject response = JsonObject.EMPTY
                .put("size", 2)
                .put("value", JsonArray.EMPTY);
        
        JsonObject decorationBody = new InboundUniquenessVerifierDecoratorV1(new MyReactiveRepository(response))
                .decorate(resource, configration)
                .block();
        
        assertEquals(JsonObject.EMPTY.put("userExists", true), decorationBody);
    }
    
    @Test
    void givenNoParametersConfigurationShouldThrowException() {
        JsonObject resource = createResource();
        JsonObject configration = JsonObject.EMPTY
                .put("resourceExistsParameter", "userExists");
        
        Mono<JsonObject> decorationBody = new InboundUniquenessVerifierDecoratorV1(new MyReactiveRepository(JsonObject.EMPTY))
                .decorate(resource, configration);
        
        assertThrows(DecorationException.class, () -> decorationBody.block());
    }
    
    @Test
    void givenNoResourceExistsParameterConfigurationShouldThrowException() {
        JsonObject resource = createResource();
        JsonObject configration = JsonObject.EMPTY
                .put("parameters12345", JsonArray.EMPTY
                        .add("name.firstName")
                        .add("name.lastName"));
        
        Mono<JsonObject> decorationBody = new InboundUniquenessVerifierDecoratorV1(new MyReactiveRepository(JsonObject.EMPTY))
                .decorate(resource, configration);
        
        assertThrows(DecorationException.class, () -> decorationBody.block());
    }
    
    @Test
    void givenNoMatchingParameterNameOnResourceShouldThrowException() {
        JsonObject resource = createResource();
        JsonObject configration = JsonObject.EMPTY
                .put("parameters12345", JsonArray.EMPTY
                        .add("name.someRandomParameter"))
                .put("resourceExistsParameter", "userExists");
        
        Mono<JsonObject> decorationBody = new InboundUniquenessVerifierDecoratorV1(new MyReactiveRepository(JsonObject.EMPTY))
                .decorate(resource, configration);
        
        assertThrows(DecorationException.class, () -> decorationBody.block());
    }
    
    private JsonObject createResource() {
        return JsonObject.EMPTY
                .put("id", "123456")
                .put("href", "https://example.com/v1/myResource/api/myCollection/123456")
                .put(JsonArray.ofDotSeparated("name.firstName"), "Laura")
                .put(JsonArray.ofDotSeparated("name.lastName"), "Andersson");
    }
    
    private JsonObject createConfiguration() {
        return JsonObject.EMPTY
                .put("parameters", JsonArray.EMPTY
                        .add("name.firstName")
                        .add("name.lastName"))
                .put("resourceExistsParameter", "userExists");
    }
    
    private class MyReactiveRepository implements ReactiveRepository<JsonObjectWrapper> {
        private JsonObject response;
        public String sendingUrl;
        
        public MyReactiveRepository(JsonObject response) {
            this.response = response;
        }

        @Override
        public Mono<Exchange<Collection<JsonObjectWrapper>>> getCollection(Exchange<Collection<JsonObjectWrapper>> exchange, Class<JsonObjectWrapper> resourceClass) {
            Collection<JsonObjectWrapper> collection = Collection.<JsonObjectWrapper>builder()
                    .value(Arrays.asList(JsonObjectWrapper.of(response.get("value").as(JsonArray.class).get(0, JsonObject.EMPTY).as(JsonObject.class))))
                    .size(response.get("size").asInteger())
                    .build();
            
            Response<Collection<JsonObjectWrapper>> response = Response.<Collection<JsonObjectWrapper>>create(StatusCode._200_OK)
                    .body(collection);
            
            return Mono.just(exchange)
                    .map(o -> { sendingUrl = o.getRequest().getUrl(); return o; })
                    .map(o -> o.response(response));
        }

        @Override
        public Mono<Exchange<JsonObjectWrapper>> saveItem(Exchange<JsonObjectWrapper> exchange, Class<JsonObjectWrapper> resourceClass) {
            return null;
        }
        @Override
        public Mono<Exchange<JsonObjectWrapper>> getItem(Exchange<JsonObjectWrapper> exchange, Class<JsonObjectWrapper> resourceClass) {
            return null;
        }
        @Override
        public Mono<Exchange<JsonObjectWrapper>> updateItem(Exchange<JsonObjectWrapper> exchange, Class<JsonObjectWrapper> resourceClass) {
            return null;
        }
        @Override
        public Mono<Exchange<JsonObjectWrapper>> deleteItem(Exchange<JsonObjectWrapper> exchange) {
            return null;
        }
    }
}
