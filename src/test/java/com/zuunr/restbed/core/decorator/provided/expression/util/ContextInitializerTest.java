/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.provided.expression.util;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

class ContextInitializerTest {
    
    private ContextInitializer contextInitializer = new ContextInitializer();
    
    @Test
    void givenResourceShouldCreateContextObject() {
        JsonObject resource = JsonObject.EMPTY
                .put("someKey", "someValue");
        
        OffsetDateTime beforeDecoration = OffsetDateTime.now(ZoneOffset.UTC).truncatedTo(ChronoUnit.MILLIS);
        JsonObject context = contextInitializer.initializeResourceBodyDecorationContext(resource);
        OffsetDateTime afterDecoration = OffsetDateTime.now(ZoneOffset.UTC).truncatedTo(ChronoUnit.MILLIS);
        
        assertSame(resource, context.get("resourceBody").as(JsonObject.class));
        checkTimeIsBetweenProvidedTime(context.get(JsonArray.ofDotSeparated("env.currentTime")).as(String.class), beforeDecoration, afterDecoration);
    }
    
    private void checkTimeIsBetweenProvidedTime(String offsetDateTime, OffsetDateTime before, OffsetDateTime after) {
        OffsetDateTime timeToCheck = OffsetDateTime.parse(offsetDateTime, DateTimeFormatter.ISO_DATE_TIME);
        
        assertTrue(timeToCheck.isEqual(before) || timeToCheck.isAfter(before));
        assertTrue(timeToCheck.isEqual(after) || timeToCheck.isBefore(after));
    }
}
