/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.provided.expression;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.external.ExternalExchange;
import com.zuunr.restbed.core.external.ExternalResponse;
import com.zuunr.restbed.core.external.ServiceCaller;

import reactor.core.publisher.Mono;

class DefaultResourceRetrieverTest {
    
    private final String url = "myUrl";
    private final String queryParams = "queryParams";
    private final JsonObject responseBody = JsonObject.EMPTY.put("someKey", "someValue");
    private final JsonObject headers = JsonObject.EMPTY.put("someHeaderKey", "someHeaderValue");
    
    @Test
    void givenUrlAndQueryShouldCallTheDesiredResource() {
        JsonObject response = new DefaultResourceRetriever(new MyServiceCaller()).retrieve(url, queryParams).block();
        
        assertEquals(200, response.get("statusCode").asInteger().intValue());
        assertEquals(headers, response.get("headers").as(JsonObject.class));
        assertEquals(responseBody, response.get("body").as(JsonObject.class));
    }
    
    @Test
    void given404ResourceShouldReturnCorrectStatusAndEmptyBody() {
        JsonObject response = new DefaultResourceRetriever(new My404ServiceCaller()).retrieve(url, queryParams).block();
        
        assertEquals(404, response.get("statusCode").asInteger().intValue());
        assertNull(response.get("headers").getValue());
        assertNull(response.get("body").getValue());
    }
    
    private class MyServiceCaller implements ServiceCaller {
        @SuppressWarnings("unchecked")
        @Override
        public <S, T> Mono<ExternalExchange<S, T>> send(ExternalExchange<S, T> exchange, Class<T> responseType) {
            assertEquals(url + queryParams, exchange.getRequest().getUrl());
            
            return Mono.just(exchange.response(ExternalResponse.<T>builder(HttpStatus.OK)
                    .headers(headers)
                    .body((T) JsonObjectWrapper.of(responseBody))
                    .build()));
        }   
    }
    
    private class My404ServiceCaller implements ServiceCaller {
        @Override
        public <S, T> Mono<ExternalExchange<S, T>> send(ExternalExchange<S, T> exchange, Class<T> responseType) {
            return Mono.just(exchange.response(ExternalResponse.<T>builder(HttpStatus.NOT_FOUND)
                    .build()));
        }   
    }
}
