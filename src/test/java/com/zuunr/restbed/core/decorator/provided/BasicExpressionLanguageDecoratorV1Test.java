/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.provided;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.restbed.core.decorator.provided.expression.DecorationExpressionException;
import com.zuunr.restbed.core.decorator.provided.expression.ResourceRetriever;
import com.zuunr.restbed.core.decorator.provided.expression.ResourceRetrieverProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class BasicExpressionLanguageDecoratorV1Test {

    private BasicExpressionLanguageDecoratorV1 basicExpressionLanguageDecoratorV1 = new BasicExpressionLanguageDecoratorV1(new MyResourceRetrieverProvider());

    @BeforeEach
    public void init() {
        basicExpressionLanguageDecoratorV1.initialize();
    }

    @Test
    void givenNoExpressionsShouldReturnEmptyJsonObject() {
        JsonObject resource = JsonObject.EMPTY
                .put("name", "Laura")
                .put("lastName", "Andersson");
        JsonObject configuration = JsonObject.EMPTY.put("outputModelLocation", JsonArray.of("decorationBody"))
                ;

        JsonObject decoration = basicExpressionLanguageDecoratorV1.decorate(resource, configuration).block();
        assertEquals(JsonObject.EMPTY, decoration);
    }

    @Test
    void givenExpressionsShouldEvaluateThem() {
        JsonObject resource = JsonObject.EMPTY.put("resourceBody", JsonObject.EMPTY
                .put("name", "Laura")
                .put("lastName", "Andersson")
                .put("phone", JsonValue.NULL));
        JsonObject configuration = JsonObject.EMPTY
                .put("outputModelLocation", JsonArray.of("other","decorationBody"))
                .put("expressions", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("target", "other.decorationBody.fullName")
                                .put("expression", "resourceBody.name + ' ' + resourceBody.lastName"))
                        .add(JsonObject.EMPTY
                                .put("target", "other.decorationBody.nameObject.isNameLaura")
                                .put("expression", "resourceBody.name eq 'Laura' ? true : false"))
                        .add(JsonObject.EMPTY
                                .put("target", "other.decorationBody.nameObject.firstLetterFullName")
                                .put("expression", "other.decorationBody.fullName.substring(0, 1)"))
                        .add(JsonObject.EMPTY
                                .put("target", "other.decorationBody.nameObject.firstLetterLastName")
                                .put("expression", "resourceBody.lastName.substring(0, 1)"))
                        .add(JsonObject.EMPTY
                                .put("target", "other.decorationBody.hasPhone")
                                .put("expression", "resourceBody.phone eq null ? false : true")));

        JsonObject decoration = basicExpressionLanguageDecoratorV1.decorate(resource, configuration).block();
        assertThat("Laura Andersson", is(decoration.get("fullName").getString()));
        assertThat(decoration.get(JsonArray.of("nameObject", "isNameLaura")).getBoolean(), is(true));
        assertThat("L", is(decoration.get(JsonArray.of("nameObject", "firstLetterFullName")).getString()));
        assertThat("A", is(decoration.get(JsonArray.of("nameObject", "firstLetterLastName")).getString()));
        assertThat(decoration.get("hasPhone").getBoolean(), is(false));
    }

    @Test
    void givenResourceParameterMissingShouldStillBeAbleToEvaluate() {
        JsonObject resource = JsonObject.EMPTY.put("resourceBody", JsonObject.EMPTY);
        JsonObject configuration = JsonObject.EMPTY
                .put("outputModelLocation", JsonArray.of("decorationBody"))
                .put("expressions", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("target", "decorationBody.hasPhone")
                                .put("expression", "resourceBody.phone eq null ? false : true")));

        JsonObject decoration = basicExpressionLanguageDecoratorV1.decorate(resource, configuration).block();
        assertFalse(decoration.get("hasPhone").as(Boolean.class));
    }

    @Test
    void givenExpressionsAndResourceWithArrayShouldEvaluateThem() {
        JsonObject resource = JsonObject.EMPTY.put("resourceBody", JsonObject.EMPTY
                .put("persons", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "Laura")
                                .put("lastName", "Andersson"))
                        .add(JsonObject.EMPTY
                                .put("name", "Phil")
                                .put("lastName", "Andersson"))));
        JsonObject configuration = JsonObject.EMPTY
                .put("outputModelLocation", JsonArray.of("decorationBody"))
                .put("expressions", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("target", "decorationBody.nameCombo")
                                .put("expression", "resourceBody.persons[0].name + '_' + resourceBody.persons[1].name")));

        JsonObject decoration = basicExpressionLanguageDecoratorV1.decorate(resource, configuration).block();
        assertEquals("Laura_Phil", decoration.get("nameCombo").as(String.class));
    }

    @Test
    void givenExpressionsAndResourceWithObjectShouldEvaluateThem() {
        JsonObject resource = JsonObject.EMPTY.put("resourceBody", JsonObject.EMPTY
                .put("person", JsonObject.EMPTY
                        .put("name", "Laura")
                        .put("lastName", "Andersson")));
        JsonObject configuration = JsonObject.EMPTY
                .put("outputModelLocation", JsonArray.of("decorationBody"))
                .put("expressions", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("target", "decorationBody.fullName")
                                .put("expression", "resourceBody.person.name + ' ' + resourceBody.person.lastName")));

        JsonObject decoration = basicExpressionLanguageDecoratorV1.decorate(resource, configuration).block();
        assertEquals("Laura Andersson", decoration.get("fullName").as(String.class));
    }

    @Test
    void givenExpressionsWithResourceRetrieverShouldEvaluateThem() {
        JsonObject resource = JsonObject.EMPTY.put("resourceBody", JsonObject.EMPTY
                .put("name", "Laura")
                .put("lastName", "Andersson"));
        JsonObject configuration = JsonObject.EMPTY
                .put("outputModelLocation", JsonArray.of("decorationBody"))
                .put("expressions", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("target", "personResponse")
                                .put("resource", JsonObject.EMPTY
                                        .put("url", "'someUrl'")
                                        .put("resourceRetriever", "someClass")
                                        .put("queryExpression", "'?name=' + resourceBody.name")))
                        .add(JsonObject.EMPTY
                                .put("target", "decorationBody.address")
                                .put("expression", "personResponse.street + ' ' + personResponse.streetNumber")));

        JsonObject decoration = basicExpressionLanguageDecoratorV1.decorate(resource, configuration).block();
        assertEquals("Sooner street 1", decoration.get("address").as(String.class));
    }

    @Test
    void givenExpressionsWithConditionFalseShouldNotCallRetriever() {
        JsonObject resource = JsonObject.EMPTY.put("resourceBody", JsonObject.EMPTY
                .put("name", "Laura")
                .put("lastName", "Andersson"));
        JsonObject configuration = JsonObject.EMPTY
                .put("outputModelLocation", JsonArray.of("decorationBody"))
                .put("expressions", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("target", "personResponse")
                                .put("condition", "resourceBody.name.equals('Peter')")
                                .put("resource", JsonObject.EMPTY
                                        .put("url", "someUrl")
                                        .put("resourceRetriever", "someClass")
                                        .put("queryExpression", "'?name=' + resourceBody.name")))
                        .add(JsonObject.EMPTY
                                .put("target", "decorationBody.address")
                                .put("condition", "personResponse != null")
                                .put("expression", "personResponse.street + ' ' + personResponse.streetNumber")));

        JsonObject decoration = basicExpressionLanguageDecoratorV1.decorate(resource, configuration).block();
        assertNull(decoration.get("address"));
    }

    @Test
    void givenExpressionsWithConditionFalseShouldNotEvaluateExpression() {
        JsonObject resource = JsonObject.EMPTY.put("resourceBody", JsonObject.EMPTY
                .put("name", "Laura")
                .put("lastName", "Andersson"));
        JsonObject configuration = JsonObject.EMPTY
                .put("outputModelLocation", JsonArray.of("decorationBody"))
                .put("expressions", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("target", "decorationBody.address")
                                .put("condition", "nonExistingParameter != null")
                                .put("expression", "'Sooner street 1'")));

        JsonObject decoration = basicExpressionLanguageDecoratorV1.decorate(resource, configuration).block();
        assertNull(decoration.get("address"));
    }

    @Test
    void countTest() {
        JsonObject resource = JsonObject.EMPTY
                .put("request", JsonObject.EMPTY.put("body", JsonObject.EMPTY.put("count", 0)));
        JsonObject configuration = JsonObject.EMPTY
                .put("outputModelLocation", JsonArray.of("output"))

                .put("expressions", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("target", "request.body.count")
                                .put("expression", "request.body.count + 1"))
                        .add(JsonObject.EMPTY
                                .put("target", "output.count")
                                .put("expression", "request.body.count")));

        JsonObject decoration = basicExpressionLanguageDecoratorV1.decorate(resource, configuration).block();
        assertThat(decoration, is(JsonObject.EMPTY.put("count", 1)));
    }

    @Test
    void givenExpressionsWithResourceRetrieverAndConditionTrueShouldCallRetriever() {
        JsonObject resource = JsonObject.EMPTY.put("resourceBody", JsonObject.EMPTY
                .put("name", "Laura")
                .put("lastName", "Andersson"));
        JsonObject configuration = JsonObject.EMPTY
                .put("outputModelLocation", JsonArray.of("decorationBody"))
                .put("expressions", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("target", "personResponse")
                                .put("condition", "resourceBody.name.equals('Laura')")
                                .put("resource", JsonObject.EMPTY
                                        .put("url", "'someUrl'")
                                        .put("resourceRetriever", "someClass")
                                        .put("queryExpression", "'?name=' + resourceBody.name")))
                        .add(JsonObject.EMPTY
                                .put("target", "decorationBody.address")
                                .put("condition", "personResponse != null")
                                .put("expression", "personResponse.street + ' ' + personResponse.streetNumber")));

        JsonObject decoration = basicExpressionLanguageDecoratorV1.decorate(resource, configuration).block();
        assertEquals("Sooner street 1", decoration.get("address").as(String.class));
    }

    @Test
    void givenExpressionResultIsNullShouldDecorateResource() {
        JsonObject resource = JsonObject.EMPTY.put("resourceBody", JsonObject.EMPTY
                .put("privatePhone", JsonValue.NULL));
        JsonObject configuration = JsonObject.EMPTY
                .put("outputModelLocation", JsonArray.of("decorationBody"))
                .put("expressions", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("target", "decorationBody.nullablePhone")
                                .put("expression", "resourceBody.privatePhone == null ? null : '+461234567'"))
                        .add(JsonObject.EMPTY
                                .put("target", "decorationBody.jsonNullPhone")
                                .put("expression", "resourceBody.privatePhone == null ? T(com.zuunr.json.JsonValue).NULL : '+461234567'")));

        JsonObject decoration = basicExpressionLanguageDecoratorV1.decorate(resource, configuration).block();

        // decoration will not contain nullablePhone key
        assertNull(decoration.get("nullablePhone"));
        // decoration WILL contain jsonNullPhone key
        assertEquals(JsonValue.NULL, decoration.get("jsonNullPhone"));
    }

    @Test
    void givenFaultyExpressionShouldThrowException() {
        JsonObject resource = JsonObject.EMPTY.put("resourceBody", JsonObject.EMPTY
                .put("name", "Laura"));
        JsonObject configuration = JsonObject.EMPTY
                .put("outputModelLocation", JsonArray.of("decorationBody"))
                .put("expressions", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("target", "decorationBody.fullName")));

        var decoratedMono = basicExpressionLanguageDecoratorV1.decorate(resource, configuration);
        assertThrows(DecorationExpressionException.class, () -> decoratedMono.block());
    }

    private class MyResourceRetrieverProvider extends ResourceRetrieverProvider {

        public MyResourceRetrieverProvider() {
            super(null, null);
        }

        @Override
        public ResourceRetriever getResourceRetriever(String classType) {
            assertEquals("someClass", classType);
            return (url, queryParams) -> {
                assertEquals("someUrl", url);
                assertEquals("?name=Laura", queryParams);
                return Mono.just(JsonObject.EMPTY
                        .put("street", "Sooner street")
                        .put("streetNumber", "1"));
            };
        }
    }
}
