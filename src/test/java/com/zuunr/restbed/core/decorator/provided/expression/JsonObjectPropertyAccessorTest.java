/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.provided.expression;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.expression.AccessException;
import org.springframework.expression.TypedValue;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

import clojure.lang.PersistentVector;

class JsonObjectPropertyAccessorTest {
    
    private JsonObjectPropertyAccessor jsonObjectPropertyAccessor = new JsonObjectPropertyAccessor();
    
    @Test
    void shouldReturnCorrectClasses() {
        Class<?>[] specificTargetClasses = jsonObjectPropertyAccessor.getSpecificTargetClasses();
        
        assertEquals(1, specificTargetClasses.length);
        assertEquals(JsonObject.class, specificTargetClasses[0]);
    }
    
    @Test
    void shouldAllowReadAccess() throws AccessException {
        boolean canRead = jsonObjectPropertyAccessor.canRead(null, null, null);
        
        assertTrue(canRead);
    }
    
    @Test
    void shouldNotAllowWriteAccess() throws AccessException {
        boolean canWrite = jsonObjectPropertyAccessor.canWrite(null, null, null);
        
        assertFalse(canWrite);
    }
    
    @Test
    void givenJsonObjectWithStringValueShouldBeAbleToReadIt() throws AccessException {
        TypedValue value = jsonObjectPropertyAccessor.read(null, JsonObject.EMPTY.put("myKey", "myValue"), "myKey");
        
        assertEquals(String.class, value.getTypeDescriptor().getObjectType());
        assertEquals("myValue", value.getValue());
    }
    
    @Test
    void givenJsonObjectWithAnotherJsonObjectShouldBeAbleToReadIt() throws AccessException {
        JsonObject nestedObject = JsonObject.EMPTY.put("someKey", "someValue");
        TypedValue value = jsonObjectPropertyAccessor.read(null, JsonObject.EMPTY.put("myKey", nestedObject), "myKey");
        
        assertEquals(JsonObject.class, value.getTypeDescriptor().getObjectType());
        assertEquals(nestedObject, value.getValue());
    }
    
    @Test
    void givenJsonObjectWithJsonArrayShouldBeAbleToReadIt() throws AccessException {
        JsonArray nestedArray = JsonArray.EMPTY.add(JsonObject.EMPTY.put("someKey", "someValue"));
        TypedValue value = jsonObjectPropertyAccessor.read(null, JsonObject.EMPTY.put("myKey", nestedArray), "myKey");
        
        assertEquals(PersistentVector.class, value.getTypeDescriptor().getObjectType());
        assertEquals(nestedArray.asList(), value.getValue());
    }
    
    @Test
    void givenJsonObjectWithoutValueShouldBeAbleToRetrieveNull() throws AccessException {
        TypedValue value = jsonObjectPropertyAccessor.read(null, JsonObject.EMPTY, "myKey");
        
        assertNull(value.getTypeDescriptor());
    }
}
