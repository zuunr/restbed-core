/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.restbed.core.cache.Cache;
import com.zuunr.restbed.core.cache.CacheProvider;
import com.zuunr.restbed.core.decorator.model.DecoratorContainer;
import com.zuunr.restbed.core.restservice.ServiceConfig;
import com.zuunr.restbed.core.restservice.ServiceConfigProvider;

import reactor.core.publisher.Mono;

class DecoratorProviderTest {
    
    private static final String MY_API_NAME = "myAPI";
    private static final String MY_COLLECTION_NAME = "myCollection";
    
    @Test
    void givenNoDecoratorShouldReturnEmptyList() {
        DecoratorFactory decoratorFactory = new DecoratorFactory();
        decoratorFactory.registerDecorator((resource, expressions) -> null);
        DecoratorProvider decoratorProvider = new ResourceDecoratorProvider(new MyServiceProvider(), new EmptyCacheProvider(), decoratorFactory);
        
        List<DecoratorContainer> decorators = decoratorProvider.getDecorators(MY_API_NAME, "someCollection");
        
        assertTrue(decorators.isEmpty());
    }
    
    @Test
    void givenDecoratorShouldReturnThemWithEventualExpressions() {
        DecoratorFactory decoratorFactory = new DecoratorFactory();
        MyDecorator myDecorator = new MyDecorator();
        decoratorFactory.registerDecorator(myDecorator);
        DecoratorProvider decoratorProvider = new ResourceDecoratorProvider(new MyServiceProvider(), new EmptyCacheProvider(), decoratorFactory);
        
        List<DecoratorContainer> decorators = decoratorProvider.getDecorators(MY_API_NAME, MY_COLLECTION_NAME);
        
        assertEquals(1, decorators.size());
        assertSame(myDecorator, decorators.iterator().next().getDecorator());
        assertEquals("myexpression", decorators.iterator().next().getConfiguration().get("expressions").as(JsonArray.class).get(0).get("target").as(String.class));
    }
    
    @Test
    void givenDecoratorAlreadyFetchedShouldRetrieveFromCache() {
        // FIXME Add test for cache
    }
    
    private class MyDecorator implements Decorator {
        
        @Override
        public Mono<JsonObject> decorate(JsonObject resource, JsonObject configuration) {
            return null;
        }
    }
    
    private class MyServiceProvider implements ServiceConfigProvider {
        
        @Override
        public ServiceConfig getServiceConfig(String apiName) {
            if (apiName.equals(MY_API_NAME)) {
                return new ServiceConfig(JsonObject.EMPTY
                        .put("config", JsonObject.EMPTY
                                .put("processing", JsonObject.EMPTY
                                        .put("decoration", JsonObject.EMPTY
                                                .put("decorators", JsonArray.EMPTY
                                                        .add(JsonObject.EMPTY
                                                                .put("collectionName", MY_COLLECTION_NAME)
                                                                .put("decorator", "com.zuunr.restbed.core.decorator.DecoratorProviderTest$MyDecorator")
                                                                .put("configuration", JsonObject.EMPTY
                                                                        .put("expressions", JsonArray.EMPTY
                                                                        .add(JsonObject.EMPTY
                                                                                .put("target", "myexpression"))))))))));
            }
            
            throw new RuntimeException("Api name does not match configured apiName");
        }
    }
    
    private class EmptyCacheProvider extends CacheProvider {
        
        @Override
        public <T> Cache<String, T> getCache(String apiName, CacheType<T> cacheType) {
            if (apiName.equals(MY_API_NAME)) {
                return new EmptyCache<String, T>();
            }
            
            throw new RuntimeException("Api name does not match provided apiName");
        }
    }
    
    private class EmptyCache<R, T> extends Cache<R, T> {
        
        @Override
        public T get(R key) {
            return null;
        }
        
        @Override
        public Cache<R, T> put(R key, T cachedObject) {
            return null;
        }
    }
}
