/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.decorator.provided.expression;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.zuunr.json.JsonNumber;
import org.junit.jupiter.api.Test;
import org.springframework.expression.AccessException;
import org.springframework.expression.TypedValue;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

class JsonValuePropertyAccessorTest {
    
    private JsonValuePropertyAccessor jsonValuePropertyAccessor = new JsonValuePropertyAccessor();
    
    @Test
    void shouldReturnCorrectClasses() {
        Class<?>[] specificTargetClasses = jsonValuePropertyAccessor.getSpecificTargetClasses();
        
        assertEquals(1, specificTargetClasses.length);
        assertEquals(JsonValue.class, specificTargetClasses[0]);
    }
    
    @Test
    void shouldAllowReadAccess() throws AccessException {
        boolean canRead = jsonValuePropertyAccessor.canRead(null, null, null);
        
        assertTrue(canRead);
    }
    
    @Test
    void shouldNotAllowWriteAccess() throws AccessException {
        boolean canWrite = jsonValuePropertyAccessor.canWrite(null, null, null);
        
        assertFalse(canWrite);
    }
    
    @Test
    void givenJsonValueWithStringValueShouldBeAbleToReadIt() throws AccessException {
        TypedValue value = jsonValuePropertyAccessor.read(null, JsonValue.of(JsonObject.EMPTY).put(JsonArray.of("myKey"), "myValue"), "myKey");
        
        assertEquals(String.class, value.getTypeDescriptor().getObjectType());
        assertEquals("myValue", value.getValue());
    }
    
    @Test
    void givenJsonValueWithLongValueShouldBeAbleToReadIt() throws AccessException {
        TypedValue value = jsonValuePropertyAccessor.read(null, JsonValue.of(JsonObject.EMPTY).put(JsonArray.of("myKey"), JsonValue.of(1234L)), "myKey");
        
        assertEquals(JsonNumber.class, value.getTypeDescriptor().getObjectType());
        assertEquals(1234L, ((JsonNumber)value.getValue()).longValue());
    }
    
    @Test
    void givenJsonObjectWithoutValueShouldBeAbleToRetrieveNull() throws AccessException {
        TypedValue value = jsonValuePropertyAccessor.read(null, JsonValue.of(JsonObject.EMPTY), "myKey");
        
        assertNull(value.getTypeDescriptor());
    }
}
