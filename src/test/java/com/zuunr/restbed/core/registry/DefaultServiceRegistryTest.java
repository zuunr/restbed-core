/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.registry;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonObject;
import com.zuunr.restbed.core.registry.ServiceRegistry;
import com.zuunr.restbed.core.restservice.ServiceConfig;

class DefaultServiceRegistryTest {
    
    @Test
    void givenWeAreRunningInSoloModeShouldReturnFalseIfServiceIsNotDeployedInJVM() {
        ServiceRegistry serviceRegistry = new DefaultServiceRegistry(apiName -> new ServiceConfig(createServiceConfig()));
        
        boolean serviceDeployedInJVM = serviceRegistry.isServiceDeployedInJVM("https://localhost/v1/myApi/api/collection3");
        assertFalse(serviceDeployedInJVM);
    }
    
    @Test
    void givenWeAreRunningInSoloModeShouldReturnTrueIfServiceIsDeployedInJVM() {
        ServiceRegistry serviceRegistry = new DefaultServiceRegistry(apiName -> new ServiceConfig(createServiceConfig()));
        
        boolean serviceDeployedInJVM = serviceRegistry.isServiceDeployedInJVM("https://localhost/v1/myApi/api/collection1");
        assertTrue(serviceDeployedInJVM);
    }
    
    private JsonObject createServiceConfig() {
        return JsonObject.EMPTY
                .put("forms", JsonObject.EMPTY
                        .put("collection1", JsonObject.EMPTY)
                        .put("collection2", JsonObject.EMPTY));
    }
}
