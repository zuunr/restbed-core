/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;

class InternalParameterFilterTest {
    
    private InternalParameterFilter internalParameterFilter = new InternalParameterFilter();
    
    @Test
    void givenJsonObjectWithInternalShouldFilterIt() {
        JsonObject resourceBody = JsonObject.EMPTY
                .put("someVar", "myValue")
                .put("_internal", JsonObject.EMPTY
                        .put("etag", "12345"));
        
        Response<JsonObjectWrapper> filteredItemResponse = internalParameterFilter.filterItem(createResponse(resourceBody));
        
        assertNull(filteredItemResponse.getBodyAsJsonObject().get("_internal"));
        assertEquals("myValue", filteredItemResponse.getBodyAsJsonObject().get("someVar").as(String.class));
    }
    
    @Test
    void givenJsonObjectWithoutInternalShouldFilterWithoutIssues() {
        JsonObject resourceBody = JsonObject.EMPTY
                .put("someVar", "myValue");
        
        Response<JsonObjectWrapper> filteredItemResponse = internalParameterFilter.filterItem(createResponse(resourceBody));
        
        assertNull(filteredItemResponse.getBodyAsJsonObject().get("_internal"));
        assertEquals("myValue", filteredItemResponse.getBodyAsJsonObject().get("someVar").as(String.class));
    }
    
    @Test
    void givenNoResourceBodyShouldReturnNull() {
        JsonObject resourceBody = null;
        
        Response<JsonObjectWrapper> filteredItemResponse = internalParameterFilter.filterItem(createResponse(resourceBody));
        
        assertNull(filteredItemResponse.getBodyAsJsonObject());
    }
    
    @Test
    void givenCollectionWithInternalsShouldFilterThem() {
        JsonObject resourceBody = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("someVar", "myValue1")
                                .put("_internal", JsonObject.EMPTY
                                        .put("etag", "12345")))
                        .add(JsonObject.EMPTY
                                .put("someVar", "myValue2")
                                .put("_internal", JsonObject.EMPTY
                                        .put("etag", "67890"))));
        
        Response<JsonObjectWrapper> filteredCollectionResponse = internalParameterFilter.filterCollection(createResponse(resourceBody));
        
        JsonArray filteredItems = filteredCollectionResponse.getBodyAsJsonObject().get("value").as(JsonArray.class);
        
        assertEquals(2, filteredItems.size());
        
        for (int i = 0; i < filteredItems.size(); i++) {
            JsonObject filteredItem = filteredItems.get(i).as(JsonObject.class);
            assertNull(filteredItem.get("_internal"));
            assertNotNull(filteredItem.get("someVar"));
        }
    }
    
    @Test
    void givenEmptyCollectionShouldFilterWithoutIssue() {
        JsonObject resourceBody = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY);
        
        Response<JsonObjectWrapper> filteredCollectionResponse = internalParameterFilter.filterCollection(createResponse(resourceBody));
        
        JsonArray filteredItems = filteredCollectionResponse.getBodyAsJsonObject().get("value").as(JsonArray.class);
        assertTrue(filteredItems.isEmpty());
    }
    
    private Response<JsonObjectWrapper> createResponse(JsonObject resourceBody) {
        return Response.<JsonObjectWrapper>create(StatusCode._200_OK)
                .body(JsonObjectWrapper.of(resourceBody));
    }
}
