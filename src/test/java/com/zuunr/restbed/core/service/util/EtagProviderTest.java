/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

class EtagProviderTest {

    private EtagProvider etagProvider = new EtagProvider();

    @Test
    void givenJsonWithNoEtagShouldAddOne() {
        JsonObject anObject = JsonObject.EMPTY;
        assertNull(anObject.get(JsonArray.of("_internal", "etag")));

        JsonObject objectWithEtag = etagProvider.appendEtag(anObject);
        assertNotNull(objectWithEtag.get(JsonArray.of("_internal", "etag")));
    }

    @Test
    void givenJsonWithEtagShouldReplaceIt() {
        JsonObject objectWithEtag = JsonObject.EMPTY.put(JsonArray.of("_internal", "etag"), "12345");
        assertEquals("12345", objectWithEtag.get(JsonArray.of("_internal", "etag")).as(String.class));

        objectWithEtag = etagProvider.appendEtag(objectWithEtag);
        assertNotNull(objectWithEtag.get(JsonArray.of("_internal", "etag")).asJson());
        assertNotEquals("12345", objectWithEtag.get(JsonArray.of("_internal", "etag")).as(String.class));
    }

    @Test
    void givenJsonWithEtagShouldReturnIt() {
        JsonObject objectWithEtag = JsonObject.EMPTY.put(JsonArray.of("_internal", "etag"), "12345");

        String etag = etagProvider.getEtag(objectWithEtag);
        assertEquals("12345", etag);
    }
    
    @Test
    void givenJsonWithEtagShouldCopyItToAnotherJsonObject() {
        JsonObject orgObject = JsonObject.EMPTY;
        JsonObject objectWithEtag = JsonObject.EMPTY.put(JsonArray.of("_internal", "etag"), "12345");

        JsonObject orgObjectWithEtag  = etagProvider.copyEtag(orgObject, objectWithEtag);
        assertEquals("12345", orgObjectWithEtag.get(JsonArray.of("_internal", "etag")).as(String.class));
    }
}
