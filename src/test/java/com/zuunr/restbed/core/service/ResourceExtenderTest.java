package com.zuunr.restbed.core.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import com.zuunr.restbed.core.forms.FormProvider;
import com.zuunr.restbed.core.forms.FormProviderKey;
import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.tool.JsonUtil;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;

import reactor.core.publisher.Mono;

class ResourceExtenderTest {

    private static JsonObject resources = JsonObject.EMPTY
            .put("https://example.com/v1/registersys/api/persons/peter", JsonObject.EMPTY
                    .put("href", "https://example.com/v1/registersys/api/persons/peter")
                    .put("id", "peter")
                    .put("name", "Peter Andersson")
                    .put("status", "ALIVE")
                    .put("mother", JsonObject.EMPTY
                            .put("href", "https://example.com/v1/registersys/api/persons/laura")))
            .put("https://example.com/v1/registersys/api/persons/laura", JsonObject.EMPTY
                    .put("href", "https://example.com/v1/registersys/api/persons/laura")
                    .put("id", "laura")
                    .put("name", "Laura Andersson")
                    .put("status", "ALIVE")
                    .put("children", JsonArray.EMPTY
                            .add(JsonObject.EMPTY
                                    .put("href", "https://example.com/v1/registersys/api/persons/noone"))
                            .add(JsonObject.EMPTY
                                    .put("href", "https://example.com/v1/registersys/api/persons/peter"))));


    private RestService restService = new RestService() {
        @Override
        public Mono<Exchange<JsonObjectWrapper>> service(Exchange<JsonObjectWrapper> exchange) {
            JsonValue resource = resources.get(exchange.getRequest().getUrl());
            if (resource == null) {
                return Mono.just(exchange.response(Response.create(StatusCode._404_NOT_FOUND)));
            } else {
                return Mono.just(exchange.response(Response.<JsonObjectWrapper>create(StatusCode._200_OK).body(JsonObjectWrapper.of(resource.as(JsonObject.class)))));
            }
        }
    };

    private FormProvider formProvider = new FormProvider() {

        JsonObject serviceConfig = JsonObject.EMPTY
                .put("forms", JsonObject.EMPTY
                        .put("persons", JsonObject.EMPTY
                                .put(FormProviderKey.FormType.RESOURCE_MODEL.name(), JsonArray.EMPTY
                                        .add(JsonObject.EMPTY
                                                .put("name", "root")
                                                .put("type", "object")
                                                .put("form", JsonObject.EMPTY
                                                        .put("value", JsonArray.EMPTY
                                                                .add(JsonObject.EMPTY
                                                                        .put("name", "value")
                                                                        .put("type", "array")
                                                                        .put("eform", JsonObject.EMPTY
                                                                                .put("value", JsonArray.EMPTY
                                                                                        .add(JsonObject.EMPTY
                                                                                                .put("name", "href")
                                                                                                .put("type", "string")
                                                                                                .put("pattern", "https://example[.]com/v1/registersys/api/persons/.*")
                                                                                        )
                                                                                        .add(JsonObject.EMPTY
                                                                                                .put("name", "name")
                                                                                                .put("type", "string"))
                                                                                        .add(JsonObject.EMPTY
                                                                                                .put("name", "status")
                                                                                                .put("type", "string"))
                                                                                        .add(JsonObject.EMPTY
                                                                                                .put("name", "mother")
                                                                                                .put("type", "object")
                                                                                                .put("form", JsonObject.EMPTY
                                                                                                        .put("value", JsonArray.EMPTY
                                                                                                                .add(JsonObject.EMPTY
                                                                                                                        .put("name", "href")
                                                                                                                        .put("pattern", "https://example[.]com/v1/registersys/api/persons/.*")
                                                                                                                ))))
                                                                                        .add(JsonObject.EMPTY
                                                                                                .put("name", "children")
                                                                                                .put("type", "array")
                                                                                                .put("eform", JsonObject.EMPTY
                                                                                                        .put("value", JsonArray.EMPTY
                                                                                                                .add(JsonObject.EMPTY
                                                                                                                        .put("name", "href")
                                                                                                                        .put("pattern", "https://example[.]com/v1/registersys/api/persons/.*")
                                                                                                                )))
                                                                                        )
                                                                                )
                                                                        )

                                                                )
                                                        )

                                                )

                                        )
                                )
                        )
                );

        @Override
        public JsonArray getForm(FormProviderKey key) {
            JsonArray jsonArrayKey = JsonArray.of("forms", key.asJsonArray.get(1), key.asJsonArray.get(2));
            return serviceConfig.get(jsonArrayKey, JsonValue.NULL).as(JsonArray.class);
        }

        @Override
        public JsonArray getValidStatusTransitions(String apiName, String collectionName) {
            return null;
        }
    };


    @Test
    void extendObject() {

        ResourceExtender resourceExtender = new ResourceExtender(restService, formProvider);
        ResourceExtender.ExtendedResource extendedResource = resourceExtender.extendResource(JsonObject.EMPTY, resources.get("https://example.com/v1/registersys/api/persons/peter").as(JsonObject.class)).block();
        JsonObject expected = JsonUtil.create("{" +
                "  'mother': {" +
                "    'children': [" +
                "      {" +
                "        'href': 'https://example.com/v1/registersys/api/persons/noone'" +
                "      }," +
                "      {" +
                "        'href': 'https://example.com/v1/registersys/api/persons/peter'" +
                "      }" +
                "    ]," +
                "    'id': 'laura'," +
                "    'href': 'https://example.com/v1/registersys/api/persons/laura'," +
                "    'name': 'Laura Andersson'," +
                "    'status': 'ALIVE'" +
                "  }," +
                "  'id': 'peter'," +
                "  'href': 'https://example.com/v1/registersys/api/persons/peter'," +
                "  'name': 'Peter Andersson'," +
                "  'status': 'ALIVE'" +
                "}");

        assertThat(extendedResource.getFullyExtended(), is(expected));
    }

    @Test
    void extendArray() {

        ResourceExtender resourceExtender = new ResourceExtender(restService, formProvider);
        ResourceExtender.ExtendedResource extendedResource = resourceExtender.extendResource(JsonObject.EMPTY, resources.get("https://example.com/v1/registersys/api/persons/laura").as(JsonObject.class)).block();

        JsonObject expectedFully = JsonUtil.create("{" +
                "  'children': [" +
                "    {" +
                "      'id': 'noone'," +
                "      'href': 'https://example.com/v1/registersys/api/persons/noone'," +
                "      'status': 'NO_STATUS'" +
                "    }," +
                "    {" +
                "      'mother': {" +
                "        'href': 'https://example.com/v1/registersys/api/persons/laura'" +
                "      }," +
                "      'id': 'peter'," +
                "      'href': 'https://example.com/v1/registersys/api/persons/peter'," +
                "      'name': 'Peter Andersson'," +
                "      'status': 'ALIVE'" +
                "    }" +
                "  ]," +
                "  'id': 'laura'," +
                "  'href': 'https://example.com/v1/registersys/api/persons/laura'," +
                "  'name': 'Laura Andersson'," +
                "  'status': 'ALIVE'" +
                "}");

        assertThat(extendedResource.getFullyExtended(), is(expectedFully));

        JsonObject expectedPartly = JsonUtil.create("{" +
                "  'children': [" +
                "    {" +
                "      'id': 'noone'," +
                "      'href': 'https://example.com/v1/registersys/api/persons/noone'," +
                "      'status': 'NO_STATUS'" +
                "    }," +
                "    {" +
                "      'id': 'peter'," +
                "      'href': 'https://example.com/v1/registersys/api/persons/peter'," +
                "      'status': 'ALIVE'" +
                "    }" +
                "  ]," +
                "  'id': 'laura'," +
                "  'href': 'https://example.com/v1/registersys/api/persons/laura'," +
                "  'name': 'Laura Andersson'," +
                "  'status': 'ALIVE'" +
                "}");

        assertThat(extendedResource.getPartlyExtended(), is(expectedPartly));
    }
}
