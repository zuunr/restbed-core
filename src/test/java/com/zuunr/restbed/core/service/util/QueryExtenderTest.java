/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;

import reactor.core.publisher.Mono;

class QueryExtenderTest {
    
    // TODO Add tests for collection extend, e.g extend=value._.name
    
    @Test
    void givenExtendItemShouldExtendResource() {
        QueryExtender queryExtender = new QueryExtender(exchange -> {
            assertEquals("https://example.com/v1/myApi/api/locations/12345", exchange.getRequest().getUrl());
            return Mono.just(exchange
                    .response(Response.<JsonObjectWrapper>create(StatusCode._200_OK)
                            .body(JsonObjectWrapper.of(JsonObject.EMPTY
                                    .put("name", "Stockholm")))));
        });
        
        JsonObject resource = JsonObject.EMPTY
                .put("name", "Laura")
                .put("location", JsonObject.EMPTY
                        .put("href", "https://example.com/v1/myApi/api/locations/12345"));
        
        JsonObject extendedResource = queryExtender.extendResource(Request.<JsonObjectWrapper>create(Method.GET).url("https://example.com/v1/myApi/api/users?extend=location"), resource).block();
        
        JsonObject expectedResponse = resource
                .put(JsonArray.ofDotSeparated("location.name"), "Stockholm");
        
        assertEquals(expectedResponse, extendedResource);
    }
    
    @Test
    void givenExtendCollectionShouldExtendResource() {
        QueryExtender queryExtender = new QueryExtender(exchange -> {
            assertEquals("https://example.com/v1/myApi/api/locations?value._.id=12345", exchange.getRequest().getUrl());
            return Mono.just(exchange
                    .response(Response.<JsonObjectWrapper>create(StatusCode._200_OK)
                            .body(JsonObjectWrapper.of(JsonObject.EMPTY
                                    .put("value", JsonArray.EMPTY
                                            .add(JsonObject.EMPTY.put("name", "Stockholm")))))));
        });
        
        JsonObject resource = JsonObject.EMPTY
                .put("name", "Laura")
                .put("location", JsonObject.EMPTY
                        .put("href", "https://example.com/v1/myApi/api/locations?value._.id=12345"));
        
        JsonObject extendedResource = queryExtender.extendResource(Request.<JsonObjectWrapper>create(Method.GET).url("https://example.com/v1/myApi/api/users?extend=location"), resource).block();
        
        JsonObject expectedResponse = resource
                .put(JsonArray.ofDotSeparated("location.value"), JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "Stockholm")));
        
        assertEquals(expectedResponse, extendedResource);
    }
    
    @Test
    void givenMultipleExtendParametersShouldExtendResource() {
        QueryExtender queryExtender = new QueryExtender(exchange -> {
            if (exchange.getRequest().getUrl().contains("car")) {
                assertEquals("https://example.com/v1/myApi/api/car?value._.id=56789", exchange.getRequest().getUrl());
                return Mono.just(exchange
                        .response(Response.<JsonObjectWrapper>create(StatusCode._200_OK)
                                .body(JsonObjectWrapper.of(JsonObject.EMPTY
                                        .put("value", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY.put("type", "Lamborghini")))))));
            } else {
                assertEquals("https://example.com/v1/myApi/api/locations/12345", exchange.getRequest().getUrl());
                return Mono.just(exchange
                        .response(Response.<JsonObjectWrapper>create(StatusCode._200_OK)
                                .body(JsonObjectWrapper.of(JsonObject.EMPTY
                                        .put("name", "Stockholm")))));
            }
        });
        
        JsonObject resource = JsonObject.EMPTY
                .put("name", "Laura")
                .put("location", JsonObject.EMPTY
                        .put("href", "https://example.com/v1/myApi/api/locations/12345"))
                .put("car", JsonObject.EMPTY
                        .put("href", "https://example.com/v1/myApi/api/car?value._.id=56789"));
        
        JsonObject extendedResource = queryExtender.extendResource(Request.<JsonObjectWrapper>create(Method.GET).url("https://example.com/v1/myApi/api/users?extend=location,car"), resource).block();
        
        JsonObject expectedResponse = resource
                .put(JsonArray.ofDotSeparated("location.name"), "Stockholm")
                .put(JsonArray.ofDotSeparated("car.value"), JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("type", "Lamborghini")));
        
        assertEquals(expectedResponse, extendedResource);
    }
    
    @Test
    void givenNoExtendParameterShouldNotExtendResource() {
        QueryExtender queryExtender = new QueryExtender(exchange -> {
            fail("Should not be calling any service if there is no extend on the path");
            return null;
        });
        
        JsonObject resource = JsonObject.EMPTY
                .put("name", "Laura")
                .put("location", JsonObject.EMPTY
                        .put("href", "https://example.com/v1/myApi/api/locations?value._.id=12345"));
        
        JsonObject extendedResource = queryExtender.extendResource(Request.<JsonObjectWrapper>create(Method.GET).url("https://example.com/v1/myApi/api/users"), resource).block();
        
        assertEquals(resource, extendedResource);
    }
    
    @Test
    void givenExtendItemWasNotFoundShouldSetNoStatusOnRootResource() {
        QueryExtender queryExtender = new QueryExtender(exchange -> {
            return Mono.just(exchange
                    .response(Response.<JsonObjectWrapper>create(StatusCode._404_NOT_FOUND)));
        });
        
        JsonObject resource = JsonObject.EMPTY
                .put("name", "Laura")
                .put("location", JsonObject.EMPTY
                        .put("href", "https://example.com/v1/myApi/api/locations/12345"));
        
        JsonObject extendedResource = queryExtender.extendResource(Request.<JsonObjectWrapper>create(Method.GET).url("https://example.com/v1/myApi/api/users?extend=location"), resource).block();
        
        JsonObject expectedResponse = resource
                .put(JsonArray.ofDotSeparated("location.id"), "12345")
                .put(JsonArray.ofDotSeparated("location.status"), "NO_STATUS");
        
        assertEquals(expectedResponse, extendedResource);
    }
    
    @Test
    void givenExtendCollectionWasNotFoundShouldDoNothingOnRootResource() {
        QueryExtender queryExtender = new QueryExtender(exchange -> {
            return Mono.just(exchange
                    .response(Response.<JsonObjectWrapper>create(StatusCode._404_NOT_FOUND)));
        });
        
        JsonObject resource = JsonObject.EMPTY
                .put("name", "Laura")
                .put("location", JsonObject.EMPTY
                        .put("href", "https://example.com/v1/myApi/api/locations?value._.id=12345"));
        
        JsonObject extendedResource = queryExtender.extendResource(Request.<JsonObjectWrapper>create(Method.GET).url("https://example.com/v1/myApi/api/users?extend=location"), resource).block();
        
        assertEquals(resource, extendedResource);
    }
    
    @Test
    void givenExtendHrefIsNotItemOrCollectionShouldThrowException() {
        QueryExtender queryExtender = new QueryExtender(exchange -> {
            fail("Should not be calling any service if there are errors in href to extend");
            return null;
        });
        
        JsonObject resource = JsonObject.EMPTY
                .put("name", "Laura")
                .put("location", JsonObject.EMPTY
                        .put("href", "https://example.com/v1/myApi/api"));
        
        assertThrows(IllegalArgumentException.class, () -> queryExtender.extendResource(Request.<JsonObjectWrapper>create(Method.GET).url("https://example.com/v1/myApi/api/users?extend=location"), resource).block());
    }
}
