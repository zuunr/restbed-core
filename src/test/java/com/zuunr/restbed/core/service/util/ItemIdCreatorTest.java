/*
 * Copyright 2021 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.authorization.ContextAggregator;
import com.zuunr.restbed.core.authorization.ContextAggregatorProvider;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;

/**
 * @author Mikael Ahlberg
 */
class ItemIdCreatorTest {
    
    private ItemIdCreator itemIdCreator = new ItemIdCreator(new ContextAggregatorProvider() { 
        @Override
        public ContextAggregator getContextAggregator(String apiName, String collectionName) {
            return new ContextAggregator() {
                @Override
                public String createContextFromRequestBody(JsonObject requestBody) {
                    if (apiName.equals("myContextApi")) {
                        return requestBody.get("name").getString();
                    }
                    
                    return null;
                }
            };
        }
    });
    
    @Test
    void givenRequestShouldAppendItemId() {
        JsonObject headers = JsonObject.EMPTY
                .put("myHeader", "myHeaderValue");
        JsonObject body = JsonObject.EMPTY
                .put("name", "Laura");
        
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.POST)
                        .url("https://example.com/v1/myApi/api/persons")
                        .headers(headers)
                        .body(JsonObjectWrapper.of(body)));
        
        Exchange<JsonObjectWrapper> updatedExchange = itemIdCreator.appendId(exchange);
        
        assertEquals(headers, updatedExchange.getRequest().getHeaders());
        assertEquals(body, updatedExchange.getRequest().getBody().asJsonObject());
        assertEquals("https://example.com/v1/myApi/api/persons", updatedExchange.getRequest().getApiUriInfo().collectionNameUri());
        assertNotNull(updatedExchange.getRequest().getApiUriInfo().itemId());
    }
    
    @Test
    void givenRequestAndContextConfiguredShouldAppendItemId() {
        JsonObject headers = JsonObject.EMPTY
                .put("myHeader", "myHeaderValue");
        JsonObject body = JsonObject.EMPTY
                .put("name", "Laura");
        
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.POST)
                        .url("https://example.com/v1/myContextApi/api/persons")
                        .headers(headers)
                        .body(JsonObjectWrapper.of(body)));
        
        Exchange<JsonObjectWrapper> updatedExchange = itemIdCreator.appendId(exchange);
        
        assertEquals(headers, updatedExchange.getRequest().getHeaders());
        assertEquals(body, updatedExchange.getRequest().getBody().asJsonObject());
        assertEquals("https://example.com/v1/myContextApi/api/persons", updatedExchange.getRequest().getApiUriInfo().collectionNameUri());
        assertNotNull(updatedExchange.getRequest().getApiUriInfo().itemId());
        
        assertEquals("Laura", updatedExchange.getRequest().getApiUriInfo().context());
    }
}
