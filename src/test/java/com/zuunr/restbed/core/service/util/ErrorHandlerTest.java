/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;

class ErrorHandlerTest {
    
    private ErrorHandler errorHandler = new ErrorHandler();
    
    @Test
    void givenExceptionShouldHandleIt() {
        Exchange<JsonObjectWrapper> exchange = errorHandler.handleException(new RuntimeException("my message"), Exchange.<JsonObjectWrapper>empty().request(Request.<JsonObjectWrapper>create(Method.GET)));
        
        assertEquals(500, exchange.getResponse().getStatusCode());
        assertTrue(exchange.getResponse().getBodyAsJsonObject().get("error").as(String.class).contains("An unexpected error occurred, id:"));
    }
}
