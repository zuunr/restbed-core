/*
 * Copyright 2021 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.service.translator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.restservice.ServiceConfig;
import com.zuunr.restbed.core.restservice.ServiceConfigProvider;

/**
 * @author Mikael Ahlberg
 */
class ExchangeUrlTranslatorTest {
    
    @Test
    void givenUrlShouldTranslateToInternal() {
        JsonObject headers = JsonObject.EMPTY
                .put("myHeader", "myHeaderValue");
        JsonObject body = JsonObject.EMPTY
                .put("name", "Laura");
        
        JsonObject serviceConfig = JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("scheme", "http")
                                .put("host", "zuunr.com")
                                .put("port", 8080)));
        
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.POST)
                        .url("http://example.com:12345/v1/myApi/api/persons")
                        .headers(headers)
                        .body(JsonObjectWrapper.of(body)));
        
        Exchange<JsonObjectWrapper> translatedExchange = createTranslator(serviceConfig).translate(exchange);
        
        assertEquals("http://zuunr.com:8080/v1/myApi/api/persons", translatedExchange.getRequest().getUrl());
        assertEquals(headers, translatedExchange.getRequest().getHeaders());
        assertEquals(body, translatedExchange.getRequest().getBody().asJsonObject());
    }
    
    @Test
    void givenHttpsAnd443ShouldRemovePort() {
        JsonObject serviceConfig = JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("scheme", "https")
                                .put("host", "zuunr.com")
                                .put("port", 443)));
        
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.POST)
                        .url("http://example.com:12345/v1/myApi/api/persons"));
        
        Exchange<JsonObjectWrapper> translatedExchange = createTranslator(serviceConfig).translate(exchange);
        
        assertEquals("https://zuunr.com/v1/myApi/api/persons", translatedExchange.getRequest().getUrl());
    }
    
    @Test
    void givenHttpAnd443ShouldNotRemovePort() {
        JsonObject serviceConfig = JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("scheme", "http")
                                .put("host", "zuunr.com")
                                .put("port", 443)));
        
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.POST)
                        .url("http://example.com:12345/v1/myApi/api/persons"));
        
        Exchange<JsonObjectWrapper> translatedExchange = createTranslator(serviceConfig).translate(exchange);
        
        assertEquals("http://zuunr.com:443/v1/myApi/api/persons", translatedExchange.getRequest().getUrl());
    }
    
    @Test
    void givenHttpAnd80ShouldRemovePort() {
        JsonObject serviceConfig = JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("scheme", "http")
                                .put("host", "zuunr.com")
                                .put("port", 80)));
        
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.POST)
                        .url("http://example.com:12345/v1/myApi/api/persons"));
        
        Exchange<JsonObjectWrapper> translatedExchange = createTranslator(serviceConfig).translate(exchange);
        
        assertEquals("http://zuunr.com/v1/myApi/api/persons", translatedExchange.getRequest().getUrl());
    }
    
    @Test
    void givenHttpsAnd80ShouldNotRemovePort() {
        JsonObject serviceConfig = JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("scheme", "https")
                                .put("host", "zuunr.com")
                                .put("port", 80)));
        
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.POST)
                        .url("http://example.com:12345/v1/myApi/api/persons"));
        
        Exchange<JsonObjectWrapper> translatedExchange = createTranslator(serviceConfig).translate(exchange);
        
        assertEquals("https://zuunr.com:80/v1/myApi/api/persons", translatedExchange.getRequest().getUrl());
    }
    
    private ExchangeUrlTranslator createTranslator(JsonObject serviceConfig) {
        return new ExchangeUrlTranslator(new ServiceConfigProvider() {
            @Override
            public ServiceConfig getServiceConfig(String apiName) {
                return new ServiceConfig(serviceConfig);
            }
        });
    }
}
