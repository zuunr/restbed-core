package com.zuunr.restbed.core.restservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.tool.JsonUtil;

/**
 * @author Niklas Eldberger
 */
class RolePermissionsCreatorTest {

    @Test
    void createPermissionsTest(){
        JsonArray permissionsInput = JsonArray.EMPTY
                .add(JsonObject.EMPTY
                        .put("permissionId", "BASIC_ROLE:RESOURCE:READ_ITEM:2")
                        .put("form", JsonObject.EMPTY.put("value", JsonArray.EMPTY)))
                .add(JsonObject.EMPTY
                        .put("permissionId", "BASIC_ROLE:RESOURCE:READ_ITEM:3")
                        .put("form", JsonObject.EMPTY.put("value", JsonArray.EMPTY)))
                .add(JsonObject.EMPTY
                        .put("permissionId", "ADMIN_ROLE:RESOURCE:READ_ITEM:2")
                        .put("form", JsonObject.EMPTY.put("value", JsonArray.EMPTY)))
                .add(JsonObject.EMPTY
                        .put("permissionId", "ADMIN_ROLE:RESOURCE:WRITE_ITEM:2")
                        .put("form", JsonObject.EMPTY.put("value", JsonArray.EMPTY)))
                .add(JsonObject.EMPTY
                        .put("permissionId", "ADMIN_ROLE:RESOURCE_B:READ_ITEM:2")
                        .put("form", JsonObject.EMPTY.put("value", JsonArray.EMPTY)))
                .add(JsonObject.EMPTY
                        .put("permissionId", "ADMIN_ROLE:RESOURCE:READ_ITEM")
                        .put("form", JsonObject.EMPTY.put("value", JsonArray.EMPTY)));

        JsonArray rolePermissions = new RolePermissionsCreator().createRolepermissions(permissionsInput);

        JsonObject expected = JsonUtil.create(
                "{'expected':[\n" +
                "  {\n" +
                "    'permissions': [\n" +
                "      {\n" +
                "        'form': {\n" +
                "          'value': []\n" +
                "        },\n" +
                "        'action': 'READ_ITEM',\n" +
                "        'collectionName': 'RESOURCE'\n" +
                "      }\n" +
                "    ],\n" +
                "    'matchingContextDepth': 0,\n" +
                "    'roleName': 'ADMIN_ROLE'\n" +
                "  },\n" +
                "  {\n" +
                "    'permissions': [\n" +
                "      {\n" +
                "        'form': {\n" +
                "          'value': []\n" +
                "        },\n" +
                "        'action': 'READ_ITEM',\n" +
                "        'collectionName': 'RESOURCE'\n" +
                "      },\n" +
                "      {\n" +
                "        'form': {\n" +
                "          'value': []\n" +
                "        },\n" +
                "        'action': 'WRITE_ITEM',\n" +
                "        'collectionName': 'RESOURCE'\n" +
                "      },\n" +
                "      {\n" +
                "        'form': {\n" +
                "          'value': []\n" +
                "        },\n" +
                "        'action': 'READ_ITEM',\n" +
                "        'collectionName': 'RESOURCE_B'\n" +
                "      }\n" +
                "    ],\n" +
                "    'matchingContextDepth': 2,\n" +
                "    'roleName': 'ADMIN_ROLE'\n" +
                "  },\n" +
                "  {\n" +
                "    'permissions': [\n" +
                "      {\n" +
                "        'form': {\n" +
                "          'value': []\n" +
                "        },\n" +
                "        'action': 'READ_ITEM',\n" +
                "        'collectionName': 'RESOURCE'\n" +
                "      }\n" +
                "    ],\n" +
                "    'matchingContextDepth': 2,\n" +
                "    'roleName': 'BASIC_ROLE'\n" +
                "  },\n" +
                "  {\n" +
                "    'permissions': [\n" +
                "      {\n" +
                "        'form': {\n" +
                "          'value': []\n" +
                "        },\n" +
                "        'action': 'READ_ITEM',\n" +
                "        'collectionName': 'RESOURCE'\n" +
                "      }\n" +
                "    ],\n" +
                "    'matchingContextDepth': 3,\n" +
                "    'roleName': 'BASIC_ROLE'\n" +
                "  }\n" +
                "]}");
        assertEquals(rolePermissions, expected.get("expected").getValue(JsonArray.class));
    }

    @Test
    void givenTwoEqualPermissionIdsShouldResultInException() {
        JsonArray permissionsInput = JsonArray.EMPTY
                .add(JsonObject.EMPTY
                        .put("permissionId", "ADMIN_ROLE:RESOURCE:READ_ITEM:2")
                        .put("form", JsonObject.EMPTY.put("value", JsonArray.EMPTY)))
                .add(JsonObject.EMPTY
                        .put("permissionId", "ADMIN_ROLE:RESOURCE:READ_ITEM:2")
                        .put("form", JsonObject.EMPTY.put("value", JsonArray.EMPTY)));

        boolean mustBeTrue = true;
        try {
            new RolePermissionsCreator().createRolepermissions(permissionsInput);
            mustBeTrue = false;
        } catch (Exception e) {

        }
        assertTrue(mustBeTrue);
    }

    @Test
    void givenDeleteItemPermissionWithNoFormShouldBeOk() {
        JsonArray permissionsInput = JsonArray.EMPTY
                .add(JsonObject.EMPTY
                        .put("permissionId", "ADMIN_ROLE:RESOURCE:DELETE_ITEM:2"));
        boolean mustBeTrue = false;
        try {
            new RolePermissionsCreator().createRolepermissions(permissionsInput);
            mustBeTrue = true;
        } catch (Exception e) {

        }
        assertTrue(mustBeTrue);
    }
}
