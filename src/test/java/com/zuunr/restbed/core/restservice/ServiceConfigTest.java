/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.restservice;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

class ServiceConfigTest {
    
    @Test
    void givenNewApiUrlShouldTranslate() {
        JsonObject serviceConfig = getServiceConfig();

        ServiceConfig config = new ServiceConfig(serviceConfig);
        config = config.translate("http://<localhost>:8080/v1/<GENERATED_SYSTEM>");
        
        JsonObject expectedServiceConfig = JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("host", "<localhost>")
                                .put("apiName", "<GENERATED_SYSTEM>")
                                .put("port", 8080)
                                .put("scheme", "http"))
                        .put("processing", JsonObject.EMPTY
                                .put("decoration", JsonObject.EMPTY
                                        .put("decorators", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("collectionName", "users")
                                                        .put("decorator", "com.zuunr.restbed.core.decorator.provided.ExpressionLanguageDecoratorV1")
                                                        .put("configuration", JsonObject.EMPTY
                                                                .put("expressions", JsonArray.EMPTY
                                                                        .add(JsonObject.EMPTY
                                                                                .put("target", "namesResponse")
                                                                                .put("resource", JsonObject.EMPTY
                                                                                        .put("resourceRetriever", "com.zuunr.restbed.core.decorator.provided.expression.DefaultResourceRetriever")
                                                                                        .put("url", "${apiUrl}/names")
                                                                                        .put("queryExpression", "'?value._.id=1234"))))))))))
                .put("forms", JsonObject.EMPTY
                        .put("users", JsonObject.EMPTY
                                .put("REQUEST_BODY", JsonObject.EMPTY
                                        .put("NO_STATUS", JsonObject.EMPTY
                                                .put("OK", JsonArray.EMPTY
                                                        .add(JsonObject.EMPTY
                                                                .put("form", JsonObject.EMPTY
                                                                        .put("value", JsonArray.EMPTY
                                                                                .add(JsonObject.EMPTY
                                                                                        .put("name", "car")
                                                                                        .put("form", JsonObject.EMPTY
                                                                                                .put("value", JsonArray.EMPTY
                                                                                                        .add(JsonObject.EMPTY
                                                                                                                .put("name", "href")
                                                                                                                .put("pattern", "http://<localhost>:8080/v1/<GENERATED_SYSTEM>/api/cars/\\w+")))))))))))));
        
        assertEquals(expectedServiceConfig, config.asJsonObject());
    }
    
    @Test
    void givenServiceConfigWithDotsInHostShouldTranslate() {
        JsonObject serviceConfig = getServiceConfigWithDotsInHost();

        ServiceConfig config = new ServiceConfig(serviceConfig);
        config = config.translate("http://<localhost>:8080/v1/<GENERATED_SYSTEM>");
        
        JsonObject expectedServiceConfig = JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("host", "<localhost>")
                                .put("apiName", "<GENERATED_SYSTEM>")
                                .put("port", 8080)
                                .put("scheme", "http"))
                        .put("processing", JsonObject.EMPTY
                                .put("decoration", JsonObject.EMPTY
                                        .put("decorators", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("collectionName", "users")
                                                        .put("decorator", "com.zuunr.restbed.core.decorator.provided.ExpressionLanguageDecoratorV1")
                                                        .put("configuration", JsonObject.EMPTY
                                                                .put("expressions", JsonArray.EMPTY
                                                                        .add(JsonObject.EMPTY
                                                                                .put("target", "namesResponse")
                                                                                .put("resource", JsonObject.EMPTY
                                                                                        .put("resourceRetriever", "com.zuunr.restbed.core.decorator.provided.expression.DefaultResourceRetriever")
                                                                                        .put("url", "${apiUrl}/names")
                                                                                        .put("queryExpression", "'?value._.id=1234"))))))))))
                .put("forms", JsonObject.EMPTY
                        .put("users", JsonObject.EMPTY
                                .put("REQUEST_BODY", JsonObject.EMPTY
                                        .put("NO_STATUS", JsonObject.EMPTY
                                                .put("OK", JsonArray.EMPTY
                                                        .add(JsonObject.EMPTY
                                                                .put("form", JsonObject.EMPTY
                                                                        .put("value", JsonArray.EMPTY
                                                                                .add(JsonObject.EMPTY
                                                                                        .put("name", "car")
                                                                                        .put("form", JsonObject.EMPTY
                                                                                                .put("value", JsonArray.EMPTY
                                                                                                        .add(JsonObject.EMPTY
                                                                                                                .put("name", "href")
                                                                                                                .put("pattern", "http://<localhost>:8080/v1/<GENERATED_SYSTEM>/api/cars/\\w+")))))))))))));
        
        assertEquals(expectedServiceConfig, config.asJsonObject());
    }
    
    @Test
    void givenNewApiUrlWithDotsInHostShouldTranslate() {
        JsonObject serviceConfig = getServiceConfig();
        
        ServiceConfig config = new ServiceConfig(serviceConfig);
        config = config.translate("http://rest.mydomain.com:8080/v1/newSystem");
        
        JsonObject expectedServiceConfig = JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("host", "rest.mydomain.com")
                                .put("apiName", "newSystem")
                                .put("port", 8080)
                                .put("scheme", "http"))
                        .put("processing", JsonObject.EMPTY
                                .put("decoration", JsonObject.EMPTY
                                        .put("decorators", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("collectionName", "users")
                                                        .put("decorator", "com.zuunr.restbed.core.decorator.provided.ExpressionLanguageDecoratorV1")
                                                        .put("configuration", JsonObject.EMPTY
                                                                .put("expressions", JsonArray.EMPTY
                                                                        .add(JsonObject.EMPTY
                                                                                .put("target", "namesResponse")
                                                                                .put("resource", JsonObject.EMPTY
                                                                                        .put("resourceRetriever", "com.zuunr.restbed.core.decorator.provided.expression.DefaultResourceRetriever")
                                                                                        .put("url", "${apiUrl}/names")
                                                                                        .put("queryExpression", "'?value._.id=1234"))))))))))
                .put("forms", JsonObject.EMPTY
                        .put("users", JsonObject.EMPTY
                                .put("REQUEST_BODY", JsonObject.EMPTY
                                        .put("NO_STATUS", JsonObject.EMPTY
                                                .put("OK", JsonArray.EMPTY
                                                        .add(JsonObject.EMPTY
                                                                .put("form", JsonObject.EMPTY
                                                                        .put("value", JsonArray.EMPTY
                                                                                .add(JsonObject.EMPTY
                                                                                        .put("name", "car")
                                                                                        .put("form", JsonObject.EMPTY
                                                                                                .put("value", JsonArray.EMPTY
                                                                                                        .add(JsonObject.EMPTY
                                                                                                                .put("name", "href")
                                                                                                                .put("pattern", "http://rest[.]mydomain[.]com:8080/v1/newSystem/api/cars/\\w+")))))))))))));
        
        assertEquals(expectedServiceConfig, config.asJsonObject());
    }
    
    @Test
    void givenCurrentAndNewApiUrlWithDotsInHostShouldTranslate() {
        JsonObject serviceConfig = getServiceConfigWithDotsInHost();
        
        ServiceConfig config = new ServiceConfig(serviceConfig);
        config = config.translate("http://rest.mydomain.com:8080/v1/newSystem");
        
        JsonObject expectedServiceConfig = JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("host", "rest.mydomain.com")
                                .put("apiName", "newSystem")
                                .put("port", 8080)
                                .put("scheme", "http"))
                        .put("processing", JsonObject.EMPTY
                                .put("decoration", JsonObject.EMPTY
                                        .put("decorators", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("collectionName", "users")
                                                        .put("decorator", "com.zuunr.restbed.core.decorator.provided.ExpressionLanguageDecoratorV1")
                                                        .put("configuration", JsonObject.EMPTY
                                                                .put("expressions", JsonArray.EMPTY
                                                                        .add(JsonObject.EMPTY
                                                                                .put("target", "namesResponse")
                                                                                .put("resource", JsonObject.EMPTY
                                                                                        .put("resourceRetriever", "com.zuunr.restbed.core.decorator.provided.expression.DefaultResourceRetriever")
                                                                                        .put("url", "${apiUrl}/names")
                                                                                        .put("queryExpression", "'?value._.id=1234"))))))))))
                .put("forms", JsonObject.EMPTY
                        .put("users", JsonObject.EMPTY
                                .put("REQUEST_BODY", JsonObject.EMPTY
                                        .put("NO_STATUS", JsonObject.EMPTY
                                                .put("OK", JsonArray.EMPTY
                                                        .add(JsonObject.EMPTY
                                                                .put("form", JsonObject.EMPTY
                                                                        .put("value", JsonArray.EMPTY
                                                                                .add(JsonObject.EMPTY
                                                                                        .put("name", "car")
                                                                                        .put("form", JsonObject.EMPTY
                                                                                                .put("value", JsonArray.EMPTY
                                                                                                        .add(JsonObject.EMPTY
                                                                                                                .put("name", "href")
                                                                                                                .put("pattern", "http://rest[.]mydomain[.]com:8080/v1/newSystem/api/cars/\\w+")))))))))))));
        
        assertEquals(expectedServiceConfig, config.asJsonObject());
    }
    
    @Test
    void givenSameApiUrlShouldTranslate() {
        JsonObject serviceConfig = getServiceConfig();
        ServiceConfig config = new ServiceConfig(serviceConfig);
        config = config.translate("https://localhost:9000/v1/myApi");
        
        assertEquals(serviceConfig, config.asJsonObject());
    }
    
    @Test
    void givenSameApiUrlAndHostWithDotsShouldTranslate() {
        JsonObject serviceConfig = getServiceConfigWithDotsInHost();
        ServiceConfig config = new ServiceConfig(serviceConfig);
        config = config.translate("https://api.example.com:9000/v1/myApi");
        
        assertEquals(serviceConfig, config.asJsonObject());
    }
    
    @Test
    void givenServiceConfigWithPort80ShouldTranslate() {
        JsonObject serviceConfig = getServiceConfigWithPort80();

        ServiceConfig config = new ServiceConfig(serviceConfig);
        config = config.translate("http://<localhost>:8080/v1/<GENERATED_SYSTEM>");
        
        JsonObject expectedServiceConfig = JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("host", "<localhost>")
                                .put("apiName", "<GENERATED_SYSTEM>")
                                .put("port", 8080)
                                .put("scheme", "http"))
                        .put("processing", JsonObject.EMPTY
                                .put("decoration", JsonObject.EMPTY
                                        .put("decorators", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("collectionName", "users")
                                                        .put("decorator", "com.zuunr.restbed.core.decorator.provided.ExpressionLanguageDecoratorV1")
                                                        .put("configuration", JsonObject.EMPTY
                                                                .put("expressions", JsonArray.EMPTY
                                                                        .add(JsonObject.EMPTY
                                                                                .put("target", "namesResponse")
                                                                                .put("resource", JsonObject.EMPTY
                                                                                        .put("resourceRetriever", "com.zuunr.restbed.core.decorator.provided.expression.DefaultResourceRetriever")
                                                                                        .put("url", "${apiUrl}/names")
                                                                                        .put("queryExpression", "'?value._.id=1234"))))))))))
                .put("forms", JsonObject.EMPTY
                        .put("users", JsonObject.EMPTY
                                .put("REQUEST_BODY", JsonObject.EMPTY
                                        .put("NO_STATUS", JsonObject.EMPTY
                                                .put("OK", JsonArray.EMPTY
                                                        .add(JsonObject.EMPTY
                                                                .put("form", JsonObject.EMPTY
                                                                        .put("value", JsonArray.EMPTY
                                                                                .add(JsonObject.EMPTY
                                                                                        .put("name", "car")
                                                                                        .put("form", JsonObject.EMPTY
                                                                                                .put("value", JsonArray.EMPTY
                                                                                                        .add(JsonObject.EMPTY
                                                                                                                .put("name", "href")
                                                                                                                .put("pattern", "http://<localhost>:8080/v1/<GENERATED_SYSTEM>/api/cars/\\w+")))))))))))));
        
        assertEquals(expectedServiceConfig, config.asJsonObject());
    }
    
    @Test
    void givenServiceConfigWithPort443ShouldTranslate() {
        JsonObject serviceConfig = getServiceConfigWithPort443();

        ServiceConfig config = new ServiceConfig(serviceConfig);
        config = config.translate("http://<localhost>:8080/v1/<GENERATED_SYSTEM>");
        
        JsonObject expectedServiceConfig = JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("host", "<localhost>")
                                .put("apiName", "<GENERATED_SYSTEM>")
                                .put("port", 8080)
                                .put("scheme", "http"))
                        .put("processing", JsonObject.EMPTY
                                .put("decoration", JsonObject.EMPTY
                                        .put("decorators", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("collectionName", "users")
                                                        .put("decorator", "com.zuunr.restbed.core.decorator.provided.ExpressionLanguageDecoratorV1")
                                                        .put("configuration", JsonObject.EMPTY
                                                                .put("expressions", JsonArray.EMPTY
                                                                        .add(JsonObject.EMPTY
                                                                                .put("target", "namesResponse")
                                                                                .put("resource", JsonObject.EMPTY
                                                                                        .put("resourceRetriever", "com.zuunr.restbed.core.decorator.provided.expression.DefaultResourceRetriever")
                                                                                        .put("url", "${apiUrl}/names")
                                                                                        .put("queryExpression", "'?value._.id=1234"))))))))))
                .put("forms", JsonObject.EMPTY
                        .put("users", JsonObject.EMPTY
                                .put("REQUEST_BODY", JsonObject.EMPTY
                                        .put("NO_STATUS", JsonObject.EMPTY
                                                .put("OK", JsonArray.EMPTY
                                                        .add(JsonObject.EMPTY
                                                                .put("form", JsonObject.EMPTY
                                                                        .put("value", JsonArray.EMPTY
                                                                                .add(JsonObject.EMPTY
                                                                                        .put("name", "car")
                                                                                        .put("form", JsonObject.EMPTY
                                                                                                .put("value", JsonArray.EMPTY
                                                                                                        .add(JsonObject.EMPTY
                                                                                                                .put("name", "href")
                                                                                                                .put("pattern", "http://<localhost>:8080/v1/<GENERATED_SYSTEM>/api/cars/\\w+")))))))))))));
        
        assertEquals(expectedServiceConfig, config.asJsonObject());
    }
    
    @Test
    void givenNewApiUrlWithPort80ShouldTranslate() {
        JsonObject serviceConfig = getServiceConfigWithPort80();

        ServiceConfig config = new ServiceConfig(serviceConfig);
        config = config.translate("http://<localhost>/v1/<GENERATED_SYSTEM>");
        
        JsonObject expectedServiceConfig = JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("host", "<localhost>")
                                .put("apiName", "<GENERATED_SYSTEM>")
                                .put("port", 80)
                                .put("scheme", "http"))
                        .put("processing", JsonObject.EMPTY
                                .put("decoration", JsonObject.EMPTY
                                        .put("decorators", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("collectionName", "users")
                                                        .put("decorator", "com.zuunr.restbed.core.decorator.provided.ExpressionLanguageDecoratorV1")
                                                        .put("configuration", JsonObject.EMPTY
                                                                .put("expressions", JsonArray.EMPTY
                                                                        .add(JsonObject.EMPTY
                                                                                .put("target", "namesResponse")
                                                                                .put("resource", JsonObject.EMPTY
                                                                                        .put("resourceRetriever", "com.zuunr.restbed.core.decorator.provided.expression.DefaultResourceRetriever")
                                                                                        .put("url", "${apiUrl}/names")
                                                                                        .put("queryExpression", "'?value._.id=1234"))))))))))
                .put("forms", JsonObject.EMPTY
                        .put("users", JsonObject.EMPTY
                                .put("REQUEST_BODY", JsonObject.EMPTY
                                        .put("NO_STATUS", JsonObject.EMPTY
                                                .put("OK", JsonArray.EMPTY
                                                        .add(JsonObject.EMPTY
                                                                .put("form", JsonObject.EMPTY
                                                                        .put("value", JsonArray.EMPTY
                                                                                .add(JsonObject.EMPTY
                                                                                        .put("name", "car")
                                                                                        .put("form", JsonObject.EMPTY
                                                                                                .put("value", JsonArray.EMPTY
                                                                                                        .add(JsonObject.EMPTY
                                                                                                                .put("name", "href")
                                                                                                                .put("pattern", "http://<localhost>/v1/<GENERATED_SYSTEM>/api/cars/\\w+")))))))))))));
        
        assertEquals(expectedServiceConfig, config.asJsonObject());
    }
    
    @Test
    void givenNewApiUrlWithPort443ShouldTranslate() {
        JsonObject serviceConfig = getServiceConfigWithPort443();

        ServiceConfig config = new ServiceConfig(serviceConfig);
        config = config.translate("https://<localhost>/v1/<GENERATED_SYSTEM>");
        
        JsonObject expectedServiceConfig = JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("host", "<localhost>")
                                .put("apiName", "<GENERATED_SYSTEM>")
                                .put("port", 443)
                                .put("scheme", "https"))
                        .put("processing", JsonObject.EMPTY
                                .put("decoration", JsonObject.EMPTY
                                        .put("decorators", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("collectionName", "users")
                                                        .put("decorator", "com.zuunr.restbed.core.decorator.provided.ExpressionLanguageDecoratorV1")
                                                        .put("configuration", JsonObject.EMPTY
                                                                .put("expressions", JsonArray.EMPTY
                                                                        .add(JsonObject.EMPTY
                                                                                .put("target", "namesResponse")
                                                                                .put("resource", JsonObject.EMPTY
                                                                                        .put("resourceRetriever", "com.zuunr.restbed.core.decorator.provided.expression.DefaultResourceRetriever")
                                                                                        .put("url", "${apiUrl}/names")
                                                                                        .put("queryExpression", "'?value._.id=1234"))))))))))
                .put("forms", JsonObject.EMPTY
                        .put("users", JsonObject.EMPTY
                                .put("REQUEST_BODY", JsonObject.EMPTY
                                        .put("NO_STATUS", JsonObject.EMPTY
                                                .put("OK", JsonArray.EMPTY
                                                        .add(JsonObject.EMPTY
                                                                .put("form", JsonObject.EMPTY
                                                                        .put("value", JsonArray.EMPTY
                                                                                .add(JsonObject.EMPTY
                                                                                        .put("name", "car")
                                                                                        .put("form", JsonObject.EMPTY
                                                                                                .put("value", JsonArray.EMPTY
                                                                                                        .add(JsonObject.EMPTY
                                                                                                                .put("name", "href")
                                                                                                                .put("pattern", "https://<localhost>/v1/<GENERATED_SYSTEM>/api/cars/\\w+")))))))))))));
        
        assertEquals(expectedServiceConfig, config.asJsonObject());
    }
    
    private JsonObject getServiceConfig() {
        return JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("host", "localhost")
                                .put("apiName", "myApi")
                                .put("port", 9000)
                                .put("scheme", "https"))
                        .put("processing", JsonObject.EMPTY
                                .put("decoration", JsonObject.EMPTY
                                        .put("decorators", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("collectionName", "users")
                                                        .put("decorator", "com.zuunr.restbed.core.decorator.provided.ExpressionLanguageDecoratorV1")
                                                        .put("configuration", JsonObject.EMPTY
                                                                .put("expressions", JsonArray.EMPTY
                                                                        .add(JsonObject.EMPTY
                                                                                .put("target", "namesResponse")
                                                                                .put("resource", JsonObject.EMPTY
                                                                                        .put("resourceRetriever", "com.zuunr.restbed.core.decorator.provided.expression.DefaultResourceRetriever")
                                                                                        .put("url", "${apiUrl}/names")
                                                                                        .put("queryExpression", "'?value._.id=1234"))))))))))
                .put("forms", JsonObject.EMPTY
                        .put("users", JsonObject.EMPTY
                                .put("REQUEST_BODY", JsonObject.EMPTY
                                        .put("NO_STATUS", JsonObject.EMPTY
                                                .put("OK", JsonArray.EMPTY
                                                        .add(JsonObject.EMPTY
                                                                .put("form", JsonObject.EMPTY
                                                                        .put("value", JsonArray.EMPTY
                                                                                .add(JsonObject.EMPTY
                                                                                        .put("name", "car")
                                                                                        .put("form", JsonObject.EMPTY
                                                                                                .put("value", JsonArray.EMPTY
                                                                                                        .add(JsonObject.EMPTY
                                                                                                                .put("name", "href")
                                                                                                                .put("pattern", "https://localhost:9000/v1/myApi/api/cars/\\w+")))))))))))));
    }
    
    private JsonObject getServiceConfigWithDotsInHost() {
        return JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("host", "api.example.com")
                                .put("apiName", "myApi")
                                .put("port", 9000)
                                .put("scheme", "https"))
                        .put("processing", JsonObject.EMPTY
                                .put("decoration", JsonObject.EMPTY
                                        .put("decorators", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("collectionName", "users")
                                                        .put("decorator", "com.zuunr.restbed.core.decorator.provided.ExpressionLanguageDecoratorV1")
                                                        .put("configuration", JsonObject.EMPTY
                                                                .put("expressions", JsonArray.EMPTY
                                                                        .add(JsonObject.EMPTY
                                                                                .put("target", "namesResponse")
                                                                                .put("resource", JsonObject.EMPTY
                                                                                        .put("resourceRetriever", "com.zuunr.restbed.core.decorator.provided.expression.DefaultResourceRetriever")
                                                                                        .put("url", "${apiUrl}/names")
                                                                                        .put("queryExpression", "'?value._.id=1234"))))))))))
                .put("forms", JsonObject.EMPTY
                        .put("users", JsonObject.EMPTY
                                .put("REQUEST_BODY", JsonObject.EMPTY
                                        .put("NO_STATUS", JsonObject.EMPTY
                                                .put("OK", JsonArray.EMPTY
                                                        .add(JsonObject.EMPTY
                                                                .put("form", JsonObject.EMPTY
                                                                        .put("value", JsonArray.EMPTY
                                                                                .add(JsonObject.EMPTY
                                                                                        .put("name", "car")
                                                                                        .put("form", JsonObject.EMPTY
                                                                                                .put("value", JsonArray.EMPTY
                                                                                                        .add(JsonObject.EMPTY
                                                                                                                .put("name", "href")
                                                                                                                .put("pattern", "https://api[.]example[.]com:9000/v1/myApi/api/cars/\\w+")))))))))))));
    }
    
    private JsonObject getServiceConfigWithPort80() {
        return JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("host", "api.example.com")
                                .put("apiName", "myApi")
                                .put("port", 80)
                                .put("scheme", "http"))
                        .put("processing", JsonObject.EMPTY
                                .put("decoration", JsonObject.EMPTY
                                        .put("decorators", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("collectionName", "users")
                                                        .put("decorator", "com.zuunr.restbed.core.decorator.provided.ExpressionLanguageDecoratorV1")
                                                        .put("configuration", JsonObject.EMPTY
                                                                .put("expressions", JsonArray.EMPTY
                                                                        .add(JsonObject.EMPTY
                                                                                .put("target", "namesResponse")
                                                                                .put("resource", JsonObject.EMPTY
                                                                                        .put("resourceRetriever", "com.zuunr.restbed.core.decorator.provided.expression.DefaultResourceRetriever")
                                                                                        .put("url", "${apiUrl}/names")
                                                                                        .put("queryExpression", "'?value._.id=1234"))))))))))
                .put("forms", JsonObject.EMPTY
                        .put("users", JsonObject.EMPTY
                                .put("REQUEST_BODY", JsonObject.EMPTY
                                        .put("NO_STATUS", JsonObject.EMPTY
                                                .put("OK", JsonArray.EMPTY
                                                        .add(JsonObject.EMPTY
                                                                .put("form", JsonObject.EMPTY
                                                                        .put("value", JsonArray.EMPTY
                                                                                .add(JsonObject.EMPTY
                                                                                        .put("name", "car")
                                                                                        .put("form", JsonObject.EMPTY
                                                                                                .put("value", JsonArray.EMPTY
                                                                                                        .add(JsonObject.EMPTY
                                                                                                                .put("name", "href")
                                                                                                                .put("pattern", "http://api[.]example[.]com/v1/myApi/api/cars/\\w+")))))))))))));
    }
    
    private JsonObject getServiceConfigWithPort443() {
        return JsonObject.EMPTY
                .put("config", JsonObject.EMPTY
                        .put("api", JsonObject.EMPTY
                                .put("host", "api.example.com")
                                .put("apiName", "myApi")
                                .put("port", 443)
                                .put("scheme", "https"))
                        .put("processing", JsonObject.EMPTY
                                .put("decoration", JsonObject.EMPTY
                                        .put("decorators", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("collectionName", "users")
                                                        .put("decorator", "com.zuunr.restbed.core.decorator.provided.ExpressionLanguageDecoratorV1")
                                                        .put("configuration", JsonObject.EMPTY
                                                                .put("expressions", JsonArray.EMPTY
                                                                        .add(JsonObject.EMPTY
                                                                                .put("target", "namesResponse")
                                                                                .put("resource", JsonObject.EMPTY
                                                                                        .put("resourceRetriever", "com.zuunr.restbed.core.decorator.provided.expression.DefaultResourceRetriever")
                                                                                        .put("url", "${apiUrl}/names")
                                                                                        .put("queryExpression", "'?value._.id=1234"))))))))))
                .put("forms", JsonObject.EMPTY
                        .put("users", JsonObject.EMPTY
                                .put("REQUEST_BODY", JsonObject.EMPTY
                                        .put("NO_STATUS", JsonObject.EMPTY
                                                .put("OK", JsonArray.EMPTY
                                                        .add(JsonObject.EMPTY
                                                                .put("form", JsonObject.EMPTY
                                                                        .put("value", JsonArray.EMPTY
                                                                                .add(JsonObject.EMPTY
                                                                                        .put("name", "car")
                                                                                        .put("form", JsonObject.EMPTY
                                                                                                .put("value", JsonArray.EMPTY
                                                                                                        .add(JsonObject.EMPTY
                                                                                                                .put("name", "href")
                                                                                                                .put("pattern", "https://api[.]example[.]com/v1/myApi/api/cars/\\w+")))))))))))));
    }
}
