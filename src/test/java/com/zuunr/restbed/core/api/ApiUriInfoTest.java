/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.api;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonObject;

class ApiUriInfoTest {

    @Test
    void test1() {
        ApiUriInfo apiUriInfo = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection/someItemId?extend=someExtensionParam");
        assertThat(apiUriInfo.collectionName(), is("someCollection"));
        assertThat(apiUriInfo.itemId(), is("someItemId"));
        assertThat(apiUriInfo.collectionName(), is("someCollection"));
        assertThat(apiUriInfo.queryString(), is("extend=someExtensionParam"));
    }


    @Test
    void queryString() {
        ApiUriInfo apiUriInfo1 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection/someItemId?extend=someExtensionParam");
        assertThat(apiUriInfo1.queryString(), is("extend=someExtensionParam"));
        ApiUriInfo apiUriInfo2 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection/someItemId");
        assertThat(apiUriInfo2.queryString(), is(""));
        ApiUriInfo apiUriInfo3 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection/someItemId?");
        assertThat(apiUriInfo3.queryString(), is(""));
        ApiUriInfo apiUriInfo4 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection/?");
        assertThat(apiUriInfo4.queryString(), is(""));
        ApiUriInfo apiUriInfo5 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection?");
        assertThat(apiUriInfo5.queryString(), is(""));
        ApiUriInfo apiUriInfo6 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection/");
        assertThat(apiUriInfo6.queryString(), is(""));
        ApiUriInfo apiUriInfo7 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection");
        assertThat(apiUriInfo7.queryString(), is(""));
        ApiUriInfo apiUriInfo8 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection?value._.status=SOMESTATUS");
        assertThat(apiUriInfo8.queryString(), is("value._.status=SOMESTATUS"));
    }

    @Test
    void apiUri() {
        ApiUriInfo apiUriInfo1 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection/someItemId?extend=someExtensionParam");
        assertThat(apiUriInfo1.apiUri(), is("http://some.domain.io/v1/system/api"));

    }

    @Test
    void isSample() {
        ApiUriInfo apiUriInfo1 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection/someItemId?extend=someExtensionParam");
        assertThat(apiUriInfo1.isSample(), is(false));
        ApiUriInfo apiUriInfo2 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/samples/someCollection/someItemId?extend=someExtensionParam");
        assertThat(apiUriInfo2.isSample(), is(true));
    }

    @Test
    void queryParams() {
        ApiUriInfo apiUriInfo0 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection?extend=someExtensionParam");
        assertThat(apiUriInfo0.queryString(), is("extend=someExtensionParam"));
        assertThat(apiUriInfo0.queryParams(), is(JsonObject.EMPTY.put("extend", "someExtensionParam")));
        ApiUriInfo apiUriInfo1 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection/someItemId?extend=someExtensionParam");
        assertThat(apiUriInfo1.queryParams(), is(JsonObject.EMPTY.put("extend", "someExtensionParam")));
        ApiUriInfo apiUriInfo2 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection/someItemId");
        assertThat(apiUriInfo2.queryParams(), is(JsonObject.EMPTY));
        ApiUriInfo apiUriInfo3 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection?value._.status=SOMESTATUS");
        assertThat(apiUriInfo3.queryParams(), is(JsonObject.EMPTY.put("value._.status", "SOMESTATUS")));
    }

    @Test
    void apiName() {
        ApiUriInfo apiUriInfo1 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection/some.context!someItemId?extend=someExtensionParam");
        assertThat(apiUriInfo1.protocol(), is("http"));
        assertThat(apiUriInfo1.hostAndPort(), is("some.domain.io"));
        assertThat(apiUriInfo1.version(), is("v1"));
        assertThat(apiUriInfo1.apiName(), is("system"));
        assertThat(apiUriInfo1.itemId(), is("some.context!someItemId"));
        assertThat(apiUriInfo1.context(), is("some.context"));
        String itemId = ApiUriInfo.createItemId();
        ApiUriInfo apiUriInfo2 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection/" + itemId + "?extend=someExtensionParam");
        assertThat(apiUriInfo2.context(), is(""));
        assertThat(apiUriInfo2.itemId(), is(itemId));
        ApiUriInfo apiUriInfo3 = ApiUriInfo.ofUri("http://some.domain.io/v1/system/api/someCollection");
        assertThat(apiUriInfo3.itemId(), nullValue());
        assertThat(apiUriInfo3.context(), nullValue());
        assertThat(apiUriInfo3.apiName(), is("system"));
        ApiUriInfo apiUriInfo4 = ApiUriInfo.ofUri("http://some.domain.io/v1/system");
        assertThat(apiUriInfo4.apiName(), is("system"));

    }

    @Test
    void createItemId() {
        String itemId1 = ApiUriInfo.createItemId();
        String decoded1 = ApiUriInfo.decodeItemId(itemId1);
        assertThat(decoded1.contains("-"), is(false));

        String itemId2 = ApiUriInfo.createItemId("zuunr.niklas", "abc123");
        String decoded2 = ApiUriInfo.decodeItemId(itemId2);
        assertThat(decoded2, is("zuunr.niklas!abc123"));

        String itemId3 = ApiUriInfo.createItemId("", "abc123");
        String decoded3 = ApiUriInfo.decodeItemId(itemId3);
        assertThat(decoded3, is("abc123"));

        String itemId4 = ApiUriInfo.createItemId(null, "abc123");
        String decoded4 = ApiUriInfo.decodeItemId(itemId4);
        assertThat(decoded4, is("abc123"));
    }

    @Test
    void encodeitemIdEncodingAndDecodingTest() {
        String encoded1 = ApiUriInfo.createItemId("se.sthlm");
        String encoded2 = ApiUriInfo.createItemId("se.sthlm1");
        String encoded3 = ApiUriInfo.createItemId("se.sthlm11");

        assertThat(encoded1.endsWith("="), is(false));
        assertThat(encoded2.endsWith("="), is(false));
        assertThat(encoded3.endsWith("="), is(false));

        String decoded1 = ApiUriInfo.decodeItemId(encoded1);
        String decoded2 = ApiUriInfo.decodeItemId(encoded2);
        String decoded3 = ApiUriInfo.decodeItemId(encoded3);

        assertThat(decoded1.startsWith("se.sthlm!"), is(true));
        assertThat(decoded2.startsWith("se.sthlm1!"), is(true));
        assertThat(decoded3.startsWith("se.sthlm11!"), is(true));
    }

    @Test
    void collectionName() {
        ApiUriInfo apiUriInfo0 = ApiUriInfo.ofUri("https://123.234.345.345/v1/SomeAPI/api/c?a");
        assertThat(apiUriInfo0.collectionName(), is("c"));
        ApiUriInfo apiUriInfo1 = ApiUriInfo.ofUri("https://123.234.345.345/v1/SomeAPI/api/collectionName");
        assertThat(apiUriInfo1.collectionName(), is("collectionName"));
        ApiUriInfo apiUriInfo2 = ApiUriInfo.ofUri("https://123.234.345.345/v1/SomeAPI/api/collectionName?someParam=someValue&someOtherParam=http://zuunr.com/v1/somesys/api/someCol?extend=value._.friend");
        assertThat(apiUriInfo2.collectionName(), is("collectionName"));
        ApiUriInfo apiUriInfo3 = ApiUriInfo.ofUri("https://123.234.345.345/v1/SomeAPI/api/collectionName/28763?someParam=someValue&someOtherParam=http://zuunr.com/v1/somesys/api/someCol?extend=value._.friend");
        assertThat(apiUriInfo3.collectionName(), is("collectionName"));
        ApiUriInfo apiUriInfo4 = ApiUriInfo.ofUri("https://123.234.345.345/v1/SomeAPI/api");
        assertThat(apiUriInfo4.collectionName(), nullValue());
    }



    @Test
    void collectionNameUri() {
        ApiUriInfo apiUriInfo0 = ApiUriInfo.ofUri("https://123.234.345.345/v1/SomeAPI/api/c?a");
        assertThat(apiUriInfo0.collectionNameUri(), is("https://123.234.345.345/v1/SomeAPI/api/c"));
        ApiUriInfo apiUriInfo1 = ApiUriInfo.ofUri("https://123.234.345.345/v1/SomeAPI/api/collectionName");
        assertThat(apiUriInfo1.collectionNameUri(), is("https://123.234.345.345/v1/SomeAPI/api/collectionName"));
        ApiUriInfo apiUriInfo2 = ApiUriInfo.ofUri("https://123.234.345.345/v1/SomeAPI/api/collectionName?someParam=someValue&someOtherParam=http://zuunr.com/v1/somesys/api/someCol?extend=value._.friend");
        assertThat(apiUriInfo2.collectionNameUri(), is("https://123.234.345.345/v1/SomeAPI/api/collectionName"));
        ApiUriInfo apiUriInfo3 = ApiUriInfo.ofUri("https://123.234.345.345/v1/SomeAPI/api/collectionName/28763?someParam=someValue&someOtherParam=http://zuunr.com/v1/somesys/api/someCol?extend=value._.friend");
        assertThat(apiUriInfo3.collectionNameUri(), is("https://123.234.345.345/v1/SomeAPI/api/collectionName"));
        ApiUriInfo apiUriInfo4 = ApiUriInfo.ofUri("https://123.234.345.345/v1/SomeAPI/api");
        assertThat(apiUriInfo4.collectionNameUri(), nullValue());
    }

    @Test
    void empty() {
        boolean exceptionThrown = false;
        try {
            ApiUriInfo apiUriInfo0 = ApiUriInfo.ofUri(null);
            assertThat(apiUriInfo0, nullValue());
        } catch (Exception e) {
            exceptionThrown = true;
        }
        assertThat(exceptionThrown, is(true));
    }

    @Test
    void path() {
        ApiUriInfo apiUriInfo0 = ApiUriInfo.ofUri("https://123.234.345.345/v1/SomeAPI/api/c?a");
        assertThat(apiUriInfo0.path(), is("v1/SomeAPI/api/c"));

    }

    @Test
    void pathUri(){
        ApiUriInfo apiUriInfo0 = ApiUriInfo.ofUri("https://123.234.345.345/abc?a");
        assertThat(apiUriInfo0.pathUri(), is("https://123.234.345.345/abc"));
        ApiUriInfo apiUriInfo1 = ApiUriInfo.ofUri("https://123.234.345.345/v1/SomeAPI/api/collectionName");
        assertThat(apiUriInfo1.pathUri(), is("https://123.234.345.345/v1/SomeAPI/api/collectionName"));
        ApiUriInfo apiUriInfo2 = ApiUriInfo.ofUri("https://123.234.345.345/v1/SomeAPI/api/collectionName?someParam=someValue&someOtherParam=http://zuunr.com/v1/somesys/api/someCol?extend=value._.friend");
        assertThat(apiUriInfo2.pathUri(), is("https://123.234.345.345/v1/SomeAPI/api/collectionName"));
        ApiUriInfo apiUriInfo3 = ApiUriInfo.ofUri("https://123.234.345.345/v1/SomeAPI/api/c?a");
        assertThat(apiUriInfo3.pathUri(), is("https://123.234.345.345/v1/SomeAPI/api/c"));
        ApiUriInfo apiUriInfo4 = ApiUriInfo.ofUri("https://123.234.345.345/SomeAPI/api/c?");
        assertThat(apiUriInfo4.pathUri(), is("https://123.234.345.345/SomeAPI/api/c"));

    }

    @Test
    void minimalUid(){
        ApiUriInfo apiUriInfo1 = ApiUriInfo.ofUri("https://example.com/v1/sys/api/collections/"+ApiUriInfo.createItemId("zuunr.niklas"));
        assertThat(apiUriInfo1.context()+"!"+apiUriInfo1.coreId(), is(apiUriInfo1.decodedItemId()));
        ApiUriInfo apiUriInfo2 = ApiUriInfo.ofUri("https://example.com/v1/sys/api/collections/"+ApiUriInfo.createItemId(null));
        assertThat(apiUriInfo2.coreId(), is(apiUriInfo2.decodedItemId()));
    }


}
