package com.zuunr.restbed.core.api;

import com.zuunr.restbed.core.api.ApiUriInfo;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


/**
 * @author Niklas Eldberger
 */
class ApiUriInfoBuilderTest {

    @Test
    void test() {

        assertThat(
                ApiUriInfo.ofUri("https://example.com:9090/v1/sys/api/collection?a=b").builder().protocol("http").hostAndPort("zuunr.io").build().uri(),
                is("http://zuunr.io/v1/sys/api/collection?a=b"));

    }
}
