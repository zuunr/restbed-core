/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.api;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

import org.junit.jupiter.api.Test;

/**
 * Created by niklas on 2017-08-15.
 */
class ApiUriUtilTest {

    @Test
    void itemUriOf() {
        String httpUri = "http://localhost:8080/v1/someApi/samples/collection/123455?extend=something";
        assertThat(ApiUriUtil.itemUriOf(httpUri), is("http://localhost:8080/v1/someApi/samples/collection/123455"));
        String httpsUri = "https://localhost:8080/v1/someApi/samples/collection/123455";
        assertThat(ApiUriUtil.itemUriOf(httpsUri), is("https://localhost:8080/v1/someApi/samples/collection/123455"));
        String collectionUri = "https://localhost:8080/v1/someApi/samples/collection";
        assertThat(ApiUriUtil.itemUriOf(collectionUri), nullValue());
        String collectionWithEndingSlashUri = "https://localhost:8080/v1/someApi/samples/collection/";
        assertThat(ApiUriUtil.itemUriOf(collectionWithEndingSlashUri), nullValue());
    }
}
