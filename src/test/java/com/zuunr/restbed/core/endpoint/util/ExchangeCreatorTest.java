/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.endpoint.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.MultiValueMap;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectFactory;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;

import reactor.core.publisher.Flux;

class ExchangeCreatorTest {
    
    private ExchangeCreator exchangeCreator = new ExchangeCreator(new JsonObjectFactory(), new IncomingHeadersFilter());
    
    @Test
    void givenPostRequestShouldCreatePostExchange() {
        final JsonObject bodyAsJson = JsonObject.EMPTY
                .put("name", "Laura Andersson");
        final String url = "http://example.com/v1/myApi/api/users";
        
        Exchange<JsonObjectWrapper> exchange = exchangeCreator.createPostExchange(new MyServerHttpRequest(url), bodyAsJson.asJson());
        
        assertEquals(Method.POST, exchange.getRequest().getMethod());
        assertEquals(url, exchange.getRequest().getUrl());
        assertEquals(bodyAsJson, exchange.getRequest().getBodyAsJsonObject());
    }
    
    @Test
    void givenPatchRequestShouldCreatePatchExchange() {
        final JsonObject bodyAsJson = JsonObject.EMPTY
                .put("name", "Laura Andersson");
        final String url = "http://example.com/v1/myApi/api/users/1234";
        
        Exchange<JsonObjectWrapper> exchange = exchangeCreator.createPatchExchange(new MyServerHttpRequest(url), bodyAsJson.asJson());
        
        assertEquals(Method.PATCH, exchange.getRequest().getMethod());
        assertEquals(url, exchange.getRequest().getUrl());
        assertEquals(bodyAsJson, exchange.getRequest().getBodyAsJsonObject());
    }
    
    @Test
    void givenGetRequestShouldCreateGetExchange() {
        final String url = "http://example.com/v1/myApi/api/users";
        
        Exchange<JsonObjectWrapper> exchange = exchangeCreator.createGetExchange(new MyServerHttpRequest(url));
        
        assertEquals(Method.GET, exchange.getRequest().getMethod());
        assertEquals(url, exchange.getRequest().getUrl());
    }
    
    @Test
    void givenDeleteRequestShouldCreateDeleteExchange() {
        final String url = "http://example.com/v1/myApi/api/users/1234";
        
        Exchange<JsonObjectWrapper> exchange = exchangeCreator.createDeleteExchange(new MyServerHttpRequest(url));
        
        assertEquals(Method.DELETE, exchange.getRequest().getMethod());
        assertEquals(url, exchange.getRequest().getUrl());
    }
    
    private class MyServerHttpRequest implements ServerHttpRequest {
        private final String url;
        
        public MyServerHttpRequest(String url) {
            this.url = url;
        }

        @Override
        public String getMethodValue() {
            return null;
        }

        @Override
        public URI getURI() {
            return URI.create(url);
        }

        @Override
        public HttpHeaders getHeaders() {
            return new HttpHeaders();
        }

        @Override
        public Flux<DataBuffer> getBody() {
            return null;
        }

        @Override
        public String getId() {
            return null;
        }

        @Override
        public RequestPath getPath() {
            return null;
        }

        @Override
        public MultiValueMap<String, String> getQueryParams() {
            return null;
        }

        @Override
        public MultiValueMap<String, HttpCookie> getCookies() {
            return null;
        }
        
    }

}
