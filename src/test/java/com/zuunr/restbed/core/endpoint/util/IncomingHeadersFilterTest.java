/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.endpoint.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.MultiValueMap;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

import reactor.core.publisher.Flux;

class IncomingHeadersFilterTest {
    
    @Test
    void givenNoHeadersShouldReturnAsIs() {
        ServerHttpRequest httpRequest = new MyServerHttpRequest(JsonObject.EMPTY);
        JsonObject filteredHeaders = new IncomingHeadersFilter().filter(httpRequest);
        
        assertEquals(JsonObject.EMPTY, filteredHeaders);
    }
    
    @Test
    void givenAuthorizationHeaderShouldAddIt() {
        JsonObject headers = JsonObject.EMPTY
                .put("authorization", "token-1234");
        ServerHttpRequest httpRequest = new MyServerHttpRequest(headers);
        JsonObject filteredHeaders = new IncomingHeadersFilter().filter(httpRequest);
        
        JsonObject expectedHeaders = JsonObject.EMPTY
                .put("authorization", JsonArray.of("token-1234"));
        
        assertEquals(expectedHeaders, filteredHeaders);
    }
    
    @Test
    void givenApiBaseUrlHeaderShouldAddIt() {
        JsonObject headers = JsonObject.EMPTY
                .put("api-base-url", "https://example.com/myApi");
        ServerHttpRequest httpRequest = new MyServerHttpRequest(headers);
        JsonObject filteredHeaders = new IncomingHeadersFilter().filter(httpRequest);
        
        JsonObject expectedHeaders = JsonObject.EMPTY
                .put("api-base-url", JsonArray.of("https://example.com/myApi"));
        
        assertEquals(expectedHeaders, filteredHeaders);
    }
    
    @Test
    void givenOtherHeadersShouldFilterThem() {
        JsonObject headers = JsonObject.EMPTY
                .put("authorization", "token-1234")
                .put("api-base-url", "https://example.com/myApi")
                .put("cookie", "JSESSIONID=12345")
                .put("my-header", "my-value");
        ServerHttpRequest httpRequest = new MyServerHttpRequest(headers);
        JsonObject filteredHeaders = new IncomingHeadersFilter().filter(httpRequest);
        
        JsonObject expectedHeaders = JsonObject.EMPTY
                .put("authorization", JsonArray.of("token-1234"))
                .put("api-base-url", JsonArray.of("https://example.com/myApi"));
        
        assertEquals(expectedHeaders, filteredHeaders);
    }
    
    private class MyServerHttpRequest implements ServerHttpRequest {
        private final JsonObject headers;
        
        public MyServerHttpRequest(JsonObject headers) {
            this.headers = headers;
        }

        @Override
        public String getMethodValue() {
            return null;
        }

        @Override
        public URI getURI() {
            return null;
        }

        @Override
        public HttpHeaders getHeaders() {
            HttpHeaders httpHeaders = new HttpHeaders();

            if (headers != null) {
                for (JsonValue headerKey : headers.keys().asList()) {
                    String key = headerKey.getValue(String.class);
                    httpHeaders.add(key, headers.get(key).getString());
                }
            }
            
            return httpHeaders;
        }

        @Override
        public Flux<DataBuffer> getBody() {
            return null;
        }

        @Override
        public String getId() {
            return null;
        }

        @Override
        public RequestPath getPath() {
            return null;
        }

        @Override
        public MultiValueMap<String, String> getQueryParams() {
            return null;
        }

        @Override
        public MultiValueMap<String, HttpCookie> getCookies() {
            return null;
        }
    }
}
