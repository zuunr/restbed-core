/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.endpoint.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.core.util.HeaderTranslator;

class ResponseEntityCreatorTest {
    
    @Test
    void givenExchangeWithResponseShouldCreateResponseEntity() {
        JsonObject responseBody = JsonObject.EMPTY
                .put("name", "Phil Andersson");
        Exchange<JsonObjectWrapper> exchange = createExchange()
                .response(Response.<JsonObjectWrapper>create(StatusCode._200_OK)
                        .headers(JsonObject.EMPTY
                                .put("Location", "https://example.com/v1/myApi/users/123"))
                        .body(JsonObjectWrapper.of(responseBody)));
        
        ResponseEntity<String> responseEntity = new ResponseEntityCreator(new HeaderTranslator()).createResponseEntity(exchange);
        
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("https://example.com/v1/myApi/users/123", responseEntity.getHeaders().get("Location").get(0));
        assertEquals(responseBody.asJson(), responseEntity.getBody());
    }
    
    @Test
    void givenExchangeWithNoBodyShouldCreateResponseEntity() {
        Exchange<JsonObjectWrapper> exchange = createExchange()
                .response(Response.<JsonObjectWrapper>create(StatusCode._204_NO_CONTENT)
                        .headers(JsonObject.EMPTY
                                .put("Location", "https://example.com/v1/myApi/users/123")));
        
        ResponseEntity<String> responseEntity = new ResponseEntityCreator(new HeaderTranslator()).createResponseEntity(exchange);
        
        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
        assertEquals("https://example.com/v1/myApi/users/123", responseEntity.getHeaders().get("Location").get(0));
        assertNull(responseEntity.getBody());
    }
    
    private Exchange<JsonObjectWrapper> createExchange() {
        return Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.GET));
    }

}
