/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.authorization;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.restbed.core.cache.CacheProvider;

class BasicPermissionProviderTest {

    @Test
    void mergePermissionsTest() {
        JsonObject permissionsOfRole1 = JsonObject.EMPTY
                .put("protectedCollection", JsonObject.EMPTY
                        .put("QUERY_ITEM", JsonObject.EMPTY
                                .put("form", JsonObject.EMPTY
                                        .put("value", JsonArray.EMPTY.add(JsonObject.EMPTY
                                                .put("form", JsonObject.EMPTY
                                                        .put("value", JsonArray.EMPTY.add(JsonObject.EMPTY
                                                                .put("name", "extend")
                                                                .put("pattern", "protectedLink")
                                                                .put("type", "string"))))
                                                .put("name", "queryParams")
                                                .put("type", "object"))))
                                .put("action", "QUERY_ITEM")
                                .put("collectionName", "protectedCollection"))
                        .put("WRITE_ITEM", JsonObject.EMPTY
                                .put("form", JsonObject.EMPTY
                                        .put("value", JsonArray.EMPTY.add(JsonObject.EMPTY
                                                .put("form", JsonObject.EMPTY
                                                        .put("value", JsonArray.EMPTY.add(JsonObject.EMPTY
                                                                .put("name", "status")
                                                                .put("pattern", "STATUS_TO_BE_SET_BY_ROLE1")
                                                                .put("type", "string")).add(JsonObject.EMPTY
                                                                .put("name", "context")).add(JsonObject.EMPTY
                                                                .put("form", JsonObject.EMPTY
                                                                        .put("value", JsonArray.EMPTY.add(JsonObject.EMPTY
                                                                                .put("name", "href"))))
                                                                .put("name", "protectedLink")
                                                                .put("type", "object")).add(JsonObject.EMPTY
                                                                .put("name", "linkToHidden")
                                                                .put("type", "object"))))
                                                .put("name", "body")
                                                .put("type", "object"))))
                                .put("action", "WRITE_ITEM")
                                .put("collectionName", "protectedCollection")));


        JsonObject permissionsOfRole2 = JsonObject.EMPTY
                .put("protectedCollection", JsonObject.EMPTY
                        .put("QUERY_ITEM", JsonObject.EMPTY
                                .put("form", JsonObject.EMPTY
                                        .put("value", JsonArray.EMPTY.add(JsonObject.EMPTY
                                                .put("name", "queryParams")
                                                .put("type", "object"))))
                                .put("action", "QUERY_ITEM")
                                .put("collectionName", "protectedCollection")));


        BasicPermissionProvider basicPermissionProvider = new BasicPermissionProvider(null, new CacheProvider());

        JsonObject mergedPermissionRules = basicPermissionProvider.mergePermissionsForRole(JsonObject.EMPTY, permissionsOfRole1);

        JsonObject expected = JsonObject.EMPTY
                .put("protectedCollection", JsonObject.EMPTY
                        .put("QUERY_ITEM", JsonObject.EMPTY
                                .put("form", JsonObject.EMPTY
                                        .put("value", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("nullable", false)
                                                        .put("name", "queryParams")
                                                        .put("mustBeNull", false)
                                                        .put("type", "object")
                                                        .put("required", false))))));

        mergedPermissionRules = basicPermissionProvider.mergePermissionsForRole(mergedPermissionRules, permissionsOfRole2);
        assertThat(mergedPermissionRules.get(JsonArray.of("protectedCollection", "QUERY_ITEM")).getValue(), is(expected.get(JsonArray.of("protectedCollection", "QUERY_ITEM")).getValue()));

    }
}
