/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.authorization;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;

class AccessControllerTest {
    
    @Test
    void givenNoClaimsWhenAuthorizingShouldReturnUnauthorized() {
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.GET);
        JsonArray effectiveContext = JsonArray.EMPTY;
        JsonObject claims = JsonObject.EMPTY;
        
        Response<JsonObjectWrapper> result = new AccessController(null, null, null, null, null, null).authorize(request, effectiveContext, claims);
        
        assertEquals(StatusCode._401_UNAUTHORIZED.getCode(), result.getStatusCode());
    }

}
