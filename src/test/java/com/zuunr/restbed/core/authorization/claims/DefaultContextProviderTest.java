/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.authorization.claims;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonObject;

class DefaultContextProviderTest {
    
    @Test
    void givenClaimsWithContextShouldReturnIt() {
        DefaultContextProvider defaultContextProvider = new DefaultContextProvider(new ClaimsNamespaceConfig("https://restbed.zuunr.com/context", null));
        
        JsonObject claims = JsonObject.EMPTY
                .put("https://restbed.zuunr.com/context", "laura.andersson@zuunr.com");
        
        String context = defaultContextProvider.getContext(claims);
        
        assertEquals("laura.andersson@zuunr.com", context);
    }
    
    @Test
    void givenClaimsWithNoContextShouldReturnEmptyArray() {
        DefaultContextProvider defaultContextProvider = new DefaultContextProvider(new ClaimsNamespaceConfig("https://restbed.zuunr.com/context", null));
        
        JsonObject claims = JsonObject.EMPTY;
        
        String context = defaultContextProvider.getContext(claims);
        
        assertEquals("", context);
    }
    
    @Test
    void givenClaimsWithWrongNamespaceShouldReturnEmptyArray() {
        DefaultContextProvider defaultContextProvider = new DefaultContextProvider(new ClaimsNamespaceConfig("https://restbed.zuunr.com/context", null));
        
        JsonObject claims = JsonObject.EMPTY
                .put("https://wrongNameSpace/context", "laura.andersson@zuunr.com");
        
        String context = defaultContextProvider.getContext(claims);
        
        assertEquals("", context);
    }
}
