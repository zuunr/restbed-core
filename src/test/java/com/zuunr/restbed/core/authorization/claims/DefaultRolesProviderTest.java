/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.authorization.claims;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

class DefaultRolesProviderTest {
    
    @Test
    void givenClaimsWithRolesShouldReturnThem() {
        DefaultRolesProvider defaultRolesProvider = new DefaultRolesProvider(new ClaimsNamespaceConfig(null, "https://restbed.zuunr.com/roles"));
        
        JsonObject claims = JsonObject.EMPTY
                .put("https://restbed.zuunr.com/roles", JsonArray.EMPTY
                        .add("MEMBER"));
        
        JsonArray roles = defaultRolesProvider.getRoles(claims);
        
        assertEquals(JsonArray.EMPTY.add("MEMBER"), roles);
    }
    
    @Test
    void givenClaimsWithNoRolesShouldReturnEmptyArray() {
        DefaultRolesProvider defaultRolesProvider = new DefaultRolesProvider(new ClaimsNamespaceConfig(null, "https://restbed.zuunr.com/roles"));
        
        JsonObject claims = JsonObject.EMPTY;
        
        JsonArray roles = defaultRolesProvider.getRoles(claims);
        
        assertEquals(JsonArray.EMPTY, roles);
    }
    
    @Test
    void givenClaimsWithWrongNamespaceShouldReturnEmptyArray() {
        DefaultRolesProvider defaultRolesProvider = new DefaultRolesProvider(new ClaimsNamespaceConfig(null, "https://restbed.zuunr.com/roles"));
        
        JsonObject claims = JsonObject.EMPTY
                .put("https://wrongNameSpace/roles", JsonArray.EMPTY
                        .add("MEMBER"));
        
        JsonArray roles = defaultRolesProvider.getRoles(claims);
        
        assertEquals(JsonArray.EMPTY, roles);
    }
}
