package com.zuunr.restbed.core.authorization;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class ServiceConfigContextAggregatorProviderTest {

    @Test
    void asJsonObjectTest(){
        JsonArray array = JsonArray.EMPTY
                .add(JsonObject.EMPTY.put("default", true).put("age",18))
                .add(JsonObject.EMPTY.put("collectionName", "users").put("age",32))
                .add(JsonObject.EMPTY.put("collectionName", "customers").put("age",29))
                .add(JsonObject.EMPTY.put("collectionName", "points").put("age",12));

        ServiceConfigContextAggregatorProvider provider = new ServiceConfigContextAggregatorProvider(null);

        JsonObject jsonObjectByName = provider.contextAggregatorsByCollectionName(array);

        JsonObject expected = JsonObject.EMPTY
                .put("/DEFAULT_AGGREGATOR", JsonObject.EMPTY.put("default", true).put("age",18))
                .put("users", JsonObject.EMPTY.put("collectionName", "users").put("age",32))
                .put("customers", JsonObject.EMPTY.put("collectionName", "customers").put("age",29))
                .put("points", JsonObject.EMPTY.put("collectionName", "points").put("age",12));

        assertThat(jsonObjectByName, is(expected));

    }
}
