/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.authorization;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

class SimpleContextAggregatorTest {
    
    @Test
    void givenContextShouldAggregateRequestBody() {
        JsonArray template = JsonArray.of("name");
        
        JsonObject requestBody = JsonObject.EMPTY
                .put("description", "This is an account")
                .put("name", "Laura Andersson");
        
        String context = new SimpleContextAggregator(template).createContextFromRequestBody(requestBody);
        
        assertEquals("Laura Andersson", context);
    }
    
    @Test
    void givenContextShouldAggregateQueryParameter() {
        JsonArray template = JsonArray.of("name");
        
        JsonObject queryParams = JsonObject.EMPTY
                .put("value._.accountType", "BUSINESS")
                .put("value._.name", "Laura Andersson");
        
        String context = new SimpleContextAggregator(template).createContextFromCollectionQueryParams(queryParams);
        
        assertEquals("Laura Andersson", context);
    }
    
    @Test
    void givenMultipleContextShouldAggregate() {
        JsonArray template = JsonArray.of("company", "name");
        
        JsonObject queryParams = JsonObject.EMPTY
                .put("value._.accountType", "BUSINESS")
                .put("value._.company", "Zuunr")
                .put("value._.name", "Laura Andersson");
        
        String context = new SimpleContextAggregator(template).createContextFromCollectionQueryParams(queryParams);
        
        assertEquals("Zuunr.Laura Andersson", context);
    }
    
    @Test
    void givenMissingContextShouldFoundValues() {
        JsonArray template = JsonArray.of("company", "name");
        
        JsonObject requestBody = JsonObject.EMPTY
                .put("accountType", "BUSINESS")
                .put("name", "Laura Andersson");
        
        String context = new SimpleContextAggregator(template).createContextFromRequestBody(requestBody);
        
        assertEquals(".Laura Andersson", context);
    }
}
