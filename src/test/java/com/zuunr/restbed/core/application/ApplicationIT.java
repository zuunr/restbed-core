/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.application;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ConfigurationPropertiesScan(basePackages = { "com.zuunr.restbed.core" })
@SpringBootApplication(scanBasePackages = { "com.zuunr.restbed.core" })
class ApplicationIT {
    
    private @Autowired WebTestClient client;
    
    @Test
    void createPlayer() {
        JsonObject request = JsonObject.EMPTY
                .put("name", "Laura Andersson");
        
        client.post()
            .uri("v1/application/api/players")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(JsonObjectWrapper.of(request))
            .exchange()
            .expectStatus().is2xxSuccessful()
            .expectBody().jsonPath("name").exists();
    }
    
    @Test
    void createPlayerAndGetPlayers() {
        JsonObject request = JsonObject.EMPTY
                .put("name", "Phil Andersson");
        
        client.post()
            .uri("v1/application/api/players")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(JsonObjectWrapper.of(request))
            .exchange()
            .expectStatus().is2xxSuccessful()
            .expectBody().jsonPath("name").exists();
        
        client.get()
            .uri("v1/application/api/players")
            .exchange()
            .expectStatus().is2xxSuccessful()
            .expectBody()
                .jsonPath("value").exists()
                .jsonPath("size").exists();
    }
}
