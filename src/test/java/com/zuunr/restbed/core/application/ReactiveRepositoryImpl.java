/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.application;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Collection;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.repository.ReactiveRepository;

import reactor.core.publisher.Mono;

@Component
public class ReactiveRepositoryImpl implements ReactiveRepository<JsonObjectWrapper> {

    @Override
    public Mono<Exchange<JsonObjectWrapper>> saveItem(Exchange<JsonObjectWrapper> exchange, Class<JsonObjectWrapper> resourceClass) {
        return Mono.just(exchange)
                .map(o -> o.response(Response.<JsonObjectWrapper>create(StatusCode._201_CREATED)
                        .body(o.getRequest().getBody())));
    }

    @Override
    public Mono<Exchange<JsonObjectWrapper>> getItem(Exchange<JsonObjectWrapper> exchange, Class<JsonObjectWrapper> resourceClass) {
        return Mono.just(exchange)
                .map(o -> o.response(Response.<JsonObjectWrapper>create(StatusCode._404_NOT_FOUND)));
    }

    @Override
    public Mono<Exchange<JsonObjectWrapper>> updateItem(Exchange<JsonObjectWrapper> exchange, Class<JsonObjectWrapper> resourceClass) {
        return Mono.just(exchange)
                .map(o -> o.response(Response.<JsonObjectWrapper>create(StatusCode._404_NOT_FOUND)));
    }

    @Override
    public Mono<Exchange<JsonObjectWrapper>> deleteItem(Exchange<JsonObjectWrapper> exchange) {
        return Mono.just(exchange)
                .map(o -> o.response(Response.<JsonObjectWrapper>create(StatusCode._204_NO_CONTENT)));
    }

    @Override
    public Mono<Exchange<Collection<JsonObjectWrapper>>> getCollection(Exchange<Collection<JsonObjectWrapper>> exchange, Class<JsonObjectWrapper> resourceClass) {
        Collection<JsonObjectWrapper> collection = Collection.<JsonObjectWrapper>builder()
                .offset(0)
                .limit(25)
                .value(new ArrayList<>())
                .href(exchange.getRequest().getApiUriInfo().uri())
                .size(0)
                .build();
        
        return Mono.just(exchange)
                .map(o -> o.response(Response.<Collection<JsonObjectWrapper>>create(StatusCode._200_OK)
                        .body(collection)));
    }
}
