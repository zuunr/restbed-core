/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.application;

import java.io.InputStream;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.reactive.function.client.WebClient;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectFactory;
import com.zuunr.restbed.core.authorization.claims.ClaimsProvider;
import com.zuunr.restbed.core.cache.CacheProvider;
import com.zuunr.restbed.core.restservice.ServiceConfig;
import com.zuunr.restbed.core.restservice.ServiceConfigProvider;

@ConfigurationProperties(prefix = "zuunr.config.api")
@Validated
public class SpringBootTestConfig {
    
    @NotEmpty(message = "You must set the api name to use")
    private final String name;
    @NotEmpty(message = "You must set the protocol to use (default: http)")
    private final String protocol;
    @NotEmpty(message = "You must set the host to use (default: localhost)")
    private final String host;
    @NotNull(message = "You must set the port to use (default: 8080)")
    private final Integer port;

    @ConstructorBinding
    public SpringBootTestConfig(
            String name,
            String protocol,
            String host,
            Integer port) {
        this.name = name;
        this.protocol = protocol;
        this.host = host;
        this.port = port;
    }
    
    @Bean
    public ServiceConfigProvider serviceConfigProvider(@Autowired ServiceConfig serviceConfig) {
        return apiName -> serviceConfig;
    }

    @Bean
    public ServiceConfig serviceConfig() {
        ServiceConfig newServiceConfig;
        try (InputStream inputStream = new ClassPathResource("/com/zuunr/restbed/core/application/Application.json").getInputStream()) {
            JsonObject serviceConfig = jsonObjectFactory().createJsonObject(inputStream);
            newServiceConfig = new ServiceConfig(serviceConfig);
        } catch (Exception e) {
            throw new RuntimeException("Failed to read service config", e);
        }

        String apiUrl = protocol + "://" + host + ":" + port + "/v1/" + name;
        return newServiceConfig.translate(apiUrl);
    }

    @Bean
    public CacheProvider cacheProvider() {
        CacheProvider cacheProvider = new CacheProvider();
        cacheProvider.createCache(name);
        return cacheProvider;
    }

    @Bean
    protected WebClient webClient(WebClient.Builder builder) {
        return builder.build();
    }

    @Bean
    public JsonObjectFactory jsonObjectFactory() {
        return new JsonObjectFactory();
    }

    @Bean
    public ClaimsProvider claimsProvider() {
        return request -> null;
    }
}
