/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.function.Consumer;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;

import com.zuunr.json.JsonObject;

class HeaderTranslatorTest {

    private HeaderTranslator headerTranslator = new HeaderTranslator();

    @Test
    void givenHeaderShouldTranslateToConsumerHttpHeader() {
        final String contentType = "Content-Type";
        final String appJson = "application/json";

        Consumer<HttpHeaders> consumer = headerTranslator.translate(JsonObject.EMPTY.put(contentType, appJson));

        HttpHeaders httpHeaders = new HttpHeaders();
        consumer.accept(httpHeaders);

        assertEquals(1, httpHeaders.size());
        assertEquals(appJson, httpHeaders.get(contentType).get(0));
    }

    @Test
    void givenHttpHeaderShouldTranslateToJsonObject() {
        final String contentType = "Content-Type";
        final String appJson = "application/json";
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(contentType, appJson);

        JsonObject jsonObjectHeaders = headerTranslator.translate(httpHeaders);

        assertEquals(1, jsonObjectHeaders.size());
        assertEquals(appJson, jsonObjectHeaders.get(contentType).getValue());
    }
    
    // Test null headers
}
