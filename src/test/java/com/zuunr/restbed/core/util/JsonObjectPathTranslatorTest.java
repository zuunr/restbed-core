/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

class JsonObjectPathTranslatorTest {
    
    private JsonObjectPathTranslator jsonObjectPathTranslator = new JsonObjectPathTranslator();
    
    @Test
    void givenObjectToTranslateShouldTranslateAllMatchingPaths() {
        JsonObject toTranslate = JsonObject.EMPTY
                .put("someArray", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("someKey", "someValue")
                                .put("url", "oldApiNameUrl"))
                        .add(JsonObject.EMPTY
                                .put("someKey", "someValue")
                                .put("url", "oldApiNameUrl"))
                        .add(JsonObject.EMPTY
                                .put("someKey", "someValue")
                                .put("url", "someOtherUrl")));
        
        JsonObject translatedObject = jsonObjectPathTranslator.translatePaths("url", toTranslate, "oldApiNameUrl", "newApiNameUrl");
        
        JsonObject expectedObject = JsonObject.EMPTY
                .put("someArray", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("someKey", "someValue")
                                .put("url", "newApiNameUrl"))
                        .add(JsonObject.EMPTY
                                .put("someKey", "someValue")
                                .put("url", "newApiNameUrl"))
                        .add(JsonObject.EMPTY
                                .put("someKey", "someValue")
                                .put("url", "someOtherUrl")));
        
        assertEquals(expectedObject, translatedObject);
    }
    
    @Test
    void givenObjectToTranslateButNoMatchingPathsShouldReturnUnchangedObject() {
        JsonObject toTranslate = JsonObject.EMPTY
                .put("someArray", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("someKey", "someValue")
                                .put("url", "someOtherUrl"))
                        .add(JsonObject.EMPTY
                                .put("someKey", "someValue")
                                .put("url", "someOtherUrl")));
        
        JsonObject translatedObject = jsonObjectPathTranslator.translatePaths("url", toTranslate, "oldApiNameUrl", "newApiNameUrl");
        
        assertEquals(toTranslate, translatedObject);
    }
}
