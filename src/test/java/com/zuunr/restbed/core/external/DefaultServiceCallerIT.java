/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.external;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.web.reactive.function.client.WebClient;

import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.core.external.petstore.Pet;
import com.zuunr.restbed.core.external.translator.HttpMethodTranslator;
import com.zuunr.restbed.core.registry.DefaultServiceRegistry;
import com.zuunr.restbed.core.registry.ServiceRegistry;
import com.zuunr.restbed.core.util.HeaderTranslator;

import reactor.core.publisher.Mono;

class DefaultServiceCallerIT {

    private final Logger logger = LoggerFactory.getLogger(DefaultServiceCallerIT.class);
    private static final String RESPONSE = "responseObject";

    private ServiceCaller serviceCaller;

    @BeforeEach
    void init() {
        serviceCaller = new DefaultServiceCaller(WebClient.create(),
                this::mockRestService,
                mockServiceRegistry(),
                new HeaderTranslator(),
                new HttpMethodTranslator());
    }

    @Test
    @Disabled(value = "The test will call external system")
    void givenPetExchangeShouldCallPetstore() {
        ExternalExchange<Void, Pet> exchange = ExternalExchange.<Void, Pet>builder()
                .request(ExternalRequest.<Void>builder(HttpMethod.GET)
                        .url("http://petstore.swagger.io/v2/pet/1")
                        .headers(JsonObject.EMPTY.put("Content-Type", "application/json"))
                        .build())
                .build();

        serviceCaller.send(exchange, Pet.class)
            .flatMap(this::successPetstore)
            .block();
    }

    private Mono<Void> successPetstore(ExternalExchange<Void, Pet> exchange) {
        Pet pet = exchange.getResponse().getBody();
        logger.info(pet.toString());
        
        assertEquals("hello", pet.getName());

        return Mono.empty();
    }

    @Test
    void givenGeneratedServiceShouldCallInternal() {
        ExternalExchange<Void, JsonObjectWrapper> exchange = ExternalExchange.<Void, JsonObjectWrapper>builder()
                .request(ExternalRequest.<Void>builder(HttpMethod.GET)
                        .url("http://localhost/myapi/v1/api/collections")
                        .headers(JsonObject.EMPTY.put("Content-Type", "application/json"))
                        .build())
                .build();

        serviceCaller.send(exchange, JsonObjectWrapper.class)
            .flatMap(this::successInternal)
            .block();
    }

    private Mono<Void> successInternal(ExternalExchange<Void, JsonObjectWrapper> exchange) {
        JsonObject jsonObject = exchange.getResponse().getBody().asJsonObject();
        logger.info(jsonObject.asJson());

        assertEquals(RESPONSE, jsonObject.get("value").getValue());

        return Mono.empty();
    }

    private ServiceRegistry mockServiceRegistry() {
        return new DefaultServiceRegistry(null) {
            @Override
            public boolean isServiceDeployedInJVM(String url) {
                return true;
            }
        };
    }

    private Mono<Exchange<JsonObjectWrapper>> mockRestService(Exchange<JsonObjectWrapper> exchange) {
        return Mono.just(exchange.response(Response.<JsonObjectWrapper>create(StatusCode._200_OK)
                .body(JsonObjectWrapper.of(JsonObject.EMPTY.put("value", RESPONSE)))));
    }
}
