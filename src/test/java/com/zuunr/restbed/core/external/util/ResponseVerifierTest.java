/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.external.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import com.zuunr.restbed.core.external.ExternalExchange;
import com.zuunr.restbed.core.external.ExternalRequest;
import com.zuunr.restbed.core.external.ExternalResponse;

class ResponseVerifierTest {
    
    private ResponseVerifier responseVerifier = new ResponseVerifier();
    
    @Test
    void givenOKStatusWhenVerifyingGetResponseShouldReturnExchange() {
        final ExternalExchange<Object,Object> exchange = ExternalExchange.builder()
                .request(ExternalRequest.builder(HttpMethod.GET)
                        .build())
                .response(ExternalResponse.builder(HttpStatus.OK)
                        .build())
                .build();
        
        ExternalExchange<Object, Object> returnedExchange = responseVerifier.verifyGetResponse(exchange);
        
        assertEquals(exchange, returnedExchange);
    }
    
    @Test
    void givenNonCorrectStatusWhenVerifyingGetResponseShouldThrowException() {
        final ExternalExchange<Object,Object> exchange = ExternalExchange.builder()
                .request(ExternalRequest.builder(HttpMethod.GET)
                        .build())
                .response(ExternalResponse.builder(HttpStatus.NO_CONTENT)
                        .build())
                .build();
        
        assertThrows(InvalidResponseException.class, () -> responseVerifier.verifyGetResponse(exchange));
    }
    
    @Test
    void givenOKStatusWhenVerifyingPostResponseShouldReturnExchange() {
        final ExternalExchange<Object,Object> exchange = ExternalExchange.builder()
                .request(ExternalRequest.builder(HttpMethod.POST)
                        .build())
                .response(ExternalResponse.builder(HttpStatus.CREATED)
                        .build())
                .build();
        
        ExternalExchange<Object, Object> returnedExchange = responseVerifier.verifyPostResponse(exchange);
        
        assertEquals(exchange, returnedExchange);
    }
    
    @Test
    void givenNonCorrectStatusWhenVerifyingPostResponseShouldThrowException() {
        final ExternalExchange<Object,Object> exchange = ExternalExchange.builder()
                .request(ExternalRequest.builder(HttpMethod.POST)
                        .build())
                .response(ExternalResponse.builder(HttpStatus.OK)
                        .build())
                .build();
        
        assertThrows(InvalidResponseException.class, () -> responseVerifier.verifyPostResponse(exchange));
    }
    
    @Test
    void givenOKStatusWhenVerifyingPatchResponseShouldReturnExchange() {
        final ExternalExchange<Object,Object> exchange = ExternalExchange.builder()
                .request(ExternalRequest.builder(HttpMethod.PATCH)
                        .build())
                .response(ExternalResponse.builder(HttpStatus.OK)
                        .build())
                .build();
        
        ExternalExchange<Object, Object> returnedExchange = responseVerifier.verifyPatchResponse(exchange);
        
        assertEquals(exchange, returnedExchange);
    }
    
    @Test
    void givenNonCorrectStatusWhenVerifyingPatchResponseShouldThrowException() {
        final ExternalExchange<Object,Object> exchange = ExternalExchange.builder()
                .request(ExternalRequest.builder(HttpMethod.PATCH)
                        .build())
                .response(ExternalResponse.builder(HttpStatus.ACCEPTED)
                        .build())
                .build();
        
        assertThrows(InvalidResponseException.class, () -> responseVerifier.verifyPatchResponse(exchange));
    }
    
    @Test
    void givenOKStatusWhenVerifyingDeleteResponseShouldReturnExchange() {
        final ExternalExchange<Object,Object> exchange = ExternalExchange.builder()
                .request(ExternalRequest.builder(HttpMethod.DELETE)
                        .build())
                .response(ExternalResponse.builder(HttpStatus.NO_CONTENT)
                        .build())
                .build();
        
        ExternalExchange<Object, Object> returnedExchange = responseVerifier.verifyDeleteResponse(exchange);
        
        assertEquals(exchange, returnedExchange);
    }
    
    @Test
    void givenNonCorrectStatusWhenVerifyingDeleteResponseShouldThrowException() {
        final ExternalExchange<Object,Object> exchange = ExternalExchange.builder()
                .request(ExternalRequest.builder(HttpMethod.DELETE)
                        .build())
                .response(ExternalResponse.builder(HttpStatus.OK)
                        .build())
                .build();
        
        assertThrows(InvalidResponseException.class, () -> responseVerifier.verifyDeleteResponse(exchange));
    }
    
    @Test
    void givenConditionMetShouldReturnExchange() {
        final ExternalExchange<Object,Object> exchange = ExternalExchange.builder()
                .request(ExternalRequest.builder(HttpMethod.GET)
                        .build())
                .response(ExternalResponse.builder(HttpStatus.OK)
                        .build())
                .build();
        
        ExternalExchange<Object, Object> returnedExchange = responseVerifier.verifyResponse(HttpMethod.GET, exchange, new ResponseCondition() {
            @Override
            public <S, T> boolean isConditionMet(ExternalExchange<S, T> exchange) {
                return true;
            }
        });
        
        assertEquals(exchange, returnedExchange);
    }
    
    @Test
    void givenConditionDoesNotMeetShouldThrowException() {
        final ExternalExchange<Object,Object> exchange = ExternalExchange.builder()
                .request(ExternalRequest.builder(HttpMethod.GET)
                        .build())
                .response(ExternalResponse.builder(HttpStatus.OK)
                        .build())
                .build();
        
        assertThrows(InvalidResponseException.class, () -> responseVerifier.verifyResponse(HttpMethod.GET, exchange, new ResponseCondition() {
            @Override
            public <S, T> boolean isConditionMet(ExternalExchange<S, T> exchange) {
                return false;
            }
        }));
    }
}
