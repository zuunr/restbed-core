/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.external.util;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import com.zuunr.restbed.core.external.ExternalExchange;
import com.zuunr.restbed.core.external.ExternalRequest;
import com.zuunr.restbed.core.external.ExternalResponse;

class HttpStatusConditionTest {
    
    @Test
    void givenMatchingHttpStatusShouldReturnTrue() {
        final HttpStatus validStatus = HttpStatus.OK;
        
        HttpStatusCondition condition = HttpStatusCondition.builder()
                .validStatus(validStatus)
                .build();
        
        boolean conditionMet = condition.isConditionMet(ExternalExchange.builder()
                .request(ExternalRequest.builder(HttpMethod.POST)
                        .build())
                .response(ExternalResponse.builder(validStatus)
                        .build())
                .build());
        
        assertTrue(conditionMet);
    }
    
    @Test
    void givenNonMatchingHttpStatusShouldReturnFalse() {
        final HttpStatus validStatus = HttpStatus.OK;
        
        HttpStatusCondition condition = HttpStatusCondition.builder()
                .validStatus(validStatus)
                .build();
        
        boolean conditionMet = condition.isConditionMet(ExternalExchange.builder()
                .request(ExternalRequest.builder(HttpMethod.POST)
                        .build())
                .response(ExternalResponse.builder(HttpStatus.CREATED)
                        .build())
                .build());
        
        assertFalse(conditionMet);
    }
}
