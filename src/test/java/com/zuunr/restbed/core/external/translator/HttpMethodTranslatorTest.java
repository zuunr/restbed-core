/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.external.translator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;

import com.zuunr.restbed.core.exchange.Method;

class HttpMethodTranslatorTest {

    private HttpMethodTranslator httpMethodTranslator = new HttpMethodTranslator();

    @Test
    void givenNonSupportedHttpMethodTraceShouldThrowException() {
        assertThrows(IllegalArgumentException.class, () -> httpMethodTranslator.translate(HttpMethod.TRACE));
    }

    @Test
    void givenNonSupportedHttpMethodHeadShouldThrowException() {
        assertThrows(IllegalArgumentException.class, () -> httpMethodTranslator.translate(HttpMethod.HEAD));
    }

    @Test
    void givenNonSupportedHttpMethodOptionsShouldThrowException() {
        assertThrows(IllegalArgumentException.class, () -> httpMethodTranslator.translate(HttpMethod.OPTIONS));
    }

    @Test
    void givenSupportedHttpMethodShouldTranslateCorrectly() {
        assertEquals(Method.GET, httpMethodTranslator.translate(HttpMethod.GET));
        assertEquals(Method.POST, httpMethodTranslator.translate(HttpMethod.POST));
        assertEquals(Method.PATCH, httpMethodTranslator.translate(HttpMethod.PATCH));
        assertEquals(Method.DELETE, httpMethodTranslator.translate(HttpMethod.DELETE));
    }
}
