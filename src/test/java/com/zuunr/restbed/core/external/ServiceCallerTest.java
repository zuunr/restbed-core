/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.external;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import reactor.core.publisher.Mono;

class ServiceCallerTest {
    
    @Test
    void givenGetRequestWithAuthHeaderShouldAppendIt() {
        ServiceCaller caller = new ServiceCaller() {    
            @Override
            public <S, T> Mono<ExternalExchange<S, T>> send(ExternalExchange<S, T> exchange, Class<T> responseType) {
                ExternalRequest<S> request = exchange.getRequest();
                
                assertEquals("someUrl", request.getUrl());
                assertEquals(String.class, responseType);
                assertEquals("myAuthHeader", request.getHeaders().get(HttpHeaders.AUTHORIZATION).getString());
                assertEquals(MediaType.APPLICATION_JSON_VALUE, request.getHeaders().get(HttpHeaders.CONTENT_TYPE).getString());
                
                return null;
            }
        };
        
        caller.get("someUrl", String.class, "myAuthHeader");
    }
    
    @Test
    void givenPostRequestWithAuthHeaderShouldAppendIt() {
        ServiceCaller caller = new ServiceCaller() {    
            @Override
            public <S, T> Mono<ExternalExchange<S, T>> send(ExternalExchange<S, T> exchange, Class<T> responseType) {
                ExternalRequest<S> request = exchange.getRequest();
                
                assertEquals("someUrl", request.getUrl());
                assertEquals(String.class, responseType);
                assertEquals("myBody", request.getBody());
                assertEquals("myAuthHeader", request.getHeaders().get(HttpHeaders.AUTHORIZATION).getString());
                assertEquals(MediaType.APPLICATION_JSON_VALUE, request.getHeaders().get(HttpHeaders.CONTENT_TYPE).getString());
                
                return null;
            }
        };
        
        caller.post("someUrl", "myBody", String.class, "myAuthHeader");
    }
    
    @Test
    void givenPatchRequestWithAuthHeaderShouldAppendIt() {
        ServiceCaller caller = new ServiceCaller() {    
            @Override
            public <S, T> Mono<ExternalExchange<S, T>> send(ExternalExchange<S, T> exchange, Class<T> responseType) {
                ExternalRequest<S> request = exchange.getRequest();
                
                assertEquals("someUrl", request.getUrl());
                assertEquals(String.class, responseType);
                assertEquals("myBody", request.getBody());
                assertEquals("myAuthHeader", request.getHeaders().get(HttpHeaders.AUTHORIZATION).getString());
                assertEquals(MediaType.APPLICATION_JSON_VALUE, request.getHeaders().get(HttpHeaders.CONTENT_TYPE).getString());
                
                return null;
            }
        };
        
        caller.patch("someUrl", "myBody", String.class, "myAuthHeader");
    }
    
    @Test
    void givenDeleteRequestWithAuthHeaderShouldAppendIt() {
        ServiceCaller caller = new ServiceCaller() {    
            @Override
            public <S, T> Mono<ExternalExchange<S, T>> send(ExternalExchange<S, T> exchange, Class<T> responseType) {
                ExternalRequest<S> request = exchange.getRequest();
                
                assertEquals("someUrl", request.getUrl());
                assertEquals(String.class, responseType);
                assertEquals("myAuthHeader", request.getHeaders().get(HttpHeaders.AUTHORIZATION).getString());
                assertEquals(MediaType.APPLICATION_JSON_VALUE, request.getHeaders().get(HttpHeaders.CONTENT_TYPE).getString());
                
                return null;
            }
        };
        
        caller.delete("someUrl", String.class, "myAuthHeader");
    }
    
    @Test
    void givenGetRequestShouldAppendContentType() {
        ServiceCaller caller = new ServiceCaller() {    
            @Override
            public <S, T> Mono<ExternalExchange<S, T>> send(ExternalExchange<S, T> exchange, Class<T> responseType) {
                ExternalRequest<S> request = exchange.getRequest();
                
                assertEquals("someUrl", request.getUrl());
                assertEquals(String.class, responseType);
                assertEquals(MediaType.APPLICATION_JSON_VALUE, request.getHeaders().get(HttpHeaders.CONTENT_TYPE).getString());
                
                return null;
            }
        };
        
        caller.get("someUrl", String.class);
    }
    
    @Test
    void givenPostRequestShouldAppendContentType() {
        ServiceCaller caller = new ServiceCaller() {    
            @Override
            public <S, T> Mono<ExternalExchange<S, T>> send(ExternalExchange<S, T> exchange, Class<T> responseType) {
                ExternalRequest<S> request = exchange.getRequest();
                
                assertEquals("someUrl", request.getUrl());
                assertEquals(String.class, responseType);
                assertEquals("myBody", request.getBody());
                assertEquals(MediaType.APPLICATION_JSON_VALUE, request.getHeaders().get(HttpHeaders.CONTENT_TYPE).getString());
                
                return null;
            }
        };
        
        caller.post("someUrl", "myBody", String.class);
    }
    
    @Test
    void givenPatchRequestShouldAppendContentType() {
        ServiceCaller caller = new ServiceCaller() {    
            @Override
            public <S, T> Mono<ExternalExchange<S, T>> send(ExternalExchange<S, T> exchange, Class<T> responseType) {
                ExternalRequest<S> request = exchange.getRequest();
                
                assertEquals("someUrl", request.getUrl());
                assertEquals(String.class, responseType);
                assertEquals("myBody", request.getBody());
                assertEquals(MediaType.APPLICATION_JSON_VALUE, request.getHeaders().get(HttpHeaders.CONTENT_TYPE).getString());
                
                return null;
            }
        };
        
        caller.patch("someUrl", "myBody", String.class);
    }
    
    @Test
    void givenDeleteRequestShouldAppendContentType() {
        ServiceCaller caller = new ServiceCaller() {    
            @Override
            public <S, T> Mono<ExternalExchange<S, T>> send(ExternalExchange<S, T> exchange, Class<T> responseType) {
                ExternalRequest<S> request = exchange.getRequest();
                
                assertEquals("someUrl", request.getUrl());
                assertEquals(String.class, responseType);
                assertEquals(MediaType.APPLICATION_JSON_VALUE, request.getHeaders().get(HttpHeaders.CONTENT_TYPE).getString());
                
                return null;
            }
        };
        
        caller.delete("someUrl", String.class);
    }
}
