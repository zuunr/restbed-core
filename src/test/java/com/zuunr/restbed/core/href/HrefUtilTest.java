/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.href;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.href.HrefUtil;

/**
 * Created by niklas on 18/04/16.
 */
class HrefUtilTest {

    @Test
    void replaceTest() {
        HrefUtil.Replacement replacement = HrefUtil.Replacement.builder().host("zuunr.cfapps.local").port("80").scheme("http").build();
        HrefUtil hrefUtil = new HrefUtil();
        String result = hrefUtil.replace("https://somedomain.super.com:7665/some/path", replacement);
        String expected = "http://zuunr.cfapps.local/some/path";
        assertThat(result, is(expected));
        result = hrefUtil.replace("http://somedomain.super.com:7665/some/path", replacement);
        expected = "http://zuunr.cfapps.local/some/path";
        assertThat(result, is(expected));
        replacement = HrefUtil.Replacement.builder().host("zuunr.com").build();
        result = hrefUtil.replace("http://somedomain.super.com:7665/some/path", replacement);
        expected = "http://zuunr.com:7665/some/path";
        assertThat(result, is(expected));
    }

    @Test
    void queryParams() {
        HrefUtil hrefUtil = new HrefUtil();
        JsonArray params = hrefUtil.queryParams("https://somedomain.super.com:7665/some/path?name=Isac&orderBy=name");
        assertThat(params.get(0).get("name").getValue(String.class), is("Isac"));
        assertThat(params.get(1).get("orderBy").getValue(String.class), is("name"));
        params = hrefUtil.queryParams("https://somedomain.super.com:7665/some/path");
        assertThat(params.isEmpty(), is(true));
        params = hrefUtil.queryParams("https://somedomain.super.com:7665/some/path?");
        assertThat(params.isEmpty(), is(true));
    }

    @Test
    void getCollectionUrl() {
        HrefUtil hrefUtil = new HrefUtil();
        String expected = "http://restbed.cfapps.local:8080/v1/myapi/samples/persons";
        String result = hrefUtil.collectionUrlOf("http://restbed.cfapps.local:8080/v1/myapi/samples/persons/ea99492c-52a1-42bd-bca3-7b355626b40a");
        assertThat(result, is(expected));

        String url = "http://localhost:8080/v1/TasksExampleIT-185caab7-72b3-4d59-ae0d-a1da19ed7f4a/samples/tasks/6d0dc814-f2e2-49ec-91df-1bc5922560b3";
        String expectedCollection = "http://localhost:8080/v1/TasksExampleIT-185caab7-72b3-4d59-ae0d-a1da19ed7f4a/samples/tasks";
        String actualCollection = hrefUtil.collectionUrlOf(url);
        assertThat(expectedCollection, is(actualCollection));
    }

    @Test
    void getQueryOfUrl() {
        HrefUtil hrefUtil = new HrefUtil();
        String expected = "name=Olle&birthDate=20010101";
        String result = hrefUtil.queryOf("http://restbed.cfapps.local:8080/v1/myapi/api/persons?name=Olle&birthDate=20010101");
        assertThat(result, is(expected));
        JsonArray queryArray = hrefUtil.queryParams("http://restbed.cfapps.local:8080/v1/myapi/api/persons?name=Olle&birthDate=20010101");
        assertThat(queryArray, is(JsonArray.EMPTY.add(JsonObject.EMPTY.put("name", "Olle")).add(JsonObject.EMPTY.put("birthDate", "20010101"))));
    }

    @Test
    void getIdOfUrl() {
        HrefUtil hrefUtil = new HrefUtil();
        String expected = "123";
        String result = hrefUtil.idOf("http://restbed.cfapps.local:8080/v1/SOMEAPI/api/persons/123?name=Olle&birthDate=20010101");
        assertThat(result, is(expected));

        String expected2 = "0a15814d-0886-4024-805d-e63dd2d7af8b";
        String result2 = hrefUtil.idOf("http://localhost:8080/v1/TasksExampleIT-6906f067-9bd6-4b9e-ab9c-0ebfa35650f6/samples/tasks/0a15814d-0886-4024-805d-e63dd2d7af8b");
        assertThat(result2, is(expected2));

        String expected3 = null;
        String result3 = hrefUtil.idOf("http://localhost:8080/v1/TasksExampleIT-6906f067-9bd6-4b9e-ab9c-0ebfa35650f6/samples/tasks");
        assertThat(result3, is(expected3));

        String expected4 = "123";
        String result4 = hrefUtil.idOf("111/123");
        assertThat(result4, is(expected4));
    }

    @Test
    void getCollectionAndIdOfUrl() {
        HrefUtil hrefUtil = new HrefUtil();
        String expected = "http://restbed.cfapps.local:8080/v1/oneapi/api/persons/123";
        String result = hrefUtil.collectionAndIdUrlOf("http://restbed.cfapps.local:8080/v1/oneapi/api/persons/123?name=Olle&birthDate=20010101");
        assertThat(result, is(expected));
    }

    @Test
    void isSampleTest() {
        HrefUtil hrefUtil = new HrefUtil();

        String sampleUrl = "https://zuunr.com/v1/someApi/samples/tasks";
        String apiUrl = "https://zuunr.com/v1/someApi/api/tasks";
        assertThat(hrefUtil.isSample(sampleUrl), is(true));
        assertThat(hrefUtil.isSample(apiUrl), is(false));
    }

    @Test
    void pathOf(){
        String path = new HrefUtil().pathOf("https://subdomain.zuunr.com:80/v1/my/api/planets?extend=value._");
        assertThat(path, is("v1/my/api/planets"));
        ApiUriInfo apiUriInfo = ApiUriInfo.ofUri("https://subdomain.zuunr.com:80/v1/my/api/planets?extend=value._");
        assertThat(apiUriInfo.path(), is("v1/my/api/planets"));
    }
}
