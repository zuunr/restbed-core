/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.core.href;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.zuunr.restbed.core.forms.FormProvider;
import com.zuunr.restbed.core.forms.FormProviderKey;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.api.ApiUriInfo;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;

class RequestResponseTranslatorTest {

    private static FormProvider formProvider = new FormProvider() {
        @Override
        public JsonArray getForm(FormProviderKey key) {
            FormProviderKey expectedKey = new FormProviderKey.Builder()
                    .apiName("somesystem")
                    .collectionName("persons")
                    .formType(FormProviderKey.FormType.RESOURCE_MODEL).build();
            
            assertEquals(expectedKey.asJsonArray, key.asJsonArray);

            return JsonArray.EMPTY.add(JsonObject.EMPTY
                    .put("name", "root")
                    .put("type", "object")
                    .put("form", JsonObject.EMPTY.put("value", JsonArray.EMPTY
                            .add(JsonObject.EMPTY
                                    .put("name", "value")
                                    .put("type", "array")
                                    .put("eform", JsonObject.EMPTY
                                            .put("value", JsonArray.EMPTY
                                                    .add(JsonObject.EMPTY
                                                            .put("name", "name"))
                                                    .add(JsonObject.EMPTY
                                                            .put("name", "href")
                                                            .put("pattern", "http://internal[.]zuunr[.]com:8080/v1/somesystem/api/persons/.*"))
                                                    .add(JsonObject.EMPTY
                                                            .put("name", "friend")
                                                            .put("type", "object")
                                                            .put("form", JsonObject.EMPTY
                                                                    .put("value", JsonArray.EMPTY
                                                                            .add(JsonObject.EMPTY
                                                                                    .put("name", "href")
                                                                                    .put("pattern", "http://internal[.]zuunr[.]com:8080/v1/somesystem/api/persons/.*"))))))))
                            .add(JsonObject.EMPTY
                                    .put("name", "href")
                                    .put("pattern", "http://internal[.]zuunr[.]com:8080/v1/somesystem/api/persons([?].*)?")))));
        }

        @Override
        public JsonArray getValidStatusTransitions(String apiName, String collectionName) {
            return null;
        }
    };
    
    private RequestResponseTranslator requestResponseTranslator;
    
    @BeforeEach
    public void init() {
        requestResponseTranslator = new RequestResponseTranslator(formProvider);
    }

    @Test
    void givenNoExternalUrlConfigurationShouldNotTranslateIncomingRequest() {
        JsonObject externalRequestBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("friend", JsonObject.EMPTY
                        .put("href", "http://internal.zuunr.com:8080/v1/somesystem/api/persons/456"));

        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.POST)
                        .url("http://internal.zuunr.com:8080/v1/somesystem/api/persons") // The client request url is ignored when no translation is made, can be any value
                        .body(JsonObjectWrapper.of(externalRequestBody)));
        ApiUriInfo internalConfiguredBaseUrl = ApiUriInfo.ofUri("http://internal.zuunr.com:8080/v1/somesystem/api");
        Exchange<JsonObjectWrapper> result = requestResponseTranslator.translateRequest(exchange, internalConfiguredBaseUrl);
        
        assertEquals(externalRequestBody, result.getRequest().getBodyAsJsonObject());
    }
    
    @Test
    void givenExternalUrlConfigurationShouldTranslateIncomingRequest() {
        JsonObject externalRequestBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("friend", JsonObject.EMPTY
                        .put("href", "https://example.com/persons/456"));

        JsonObject expectedInternalRequestBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("friend", JsonObject.EMPTY
                        .put("href", "http://internal.zuunr.com:8080/v1/somesystem/api/persons/456"));

        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.POST)
                        .url("https://example.com/v1/somesystem/api/persons") // The client might send https://example.com/persons, but the proxy must rewrite the path to match HttpEndpoint
                        .headers(JsonObject.EMPTY.put("api-base-url", JsonArray.of("https://example.com")))
                        .body(JsonObjectWrapper.of(externalRequestBody)));
        ApiUriInfo internalConfiguredBaseUrl = ApiUriInfo.ofUri("http://internal.zuunr.com:8080/v1/somesystem/api");
        Exchange<JsonObjectWrapper> result = requestResponseTranslator.translateRequest(exchange, internalConfiguredBaseUrl);
        
        assertEquals(expectedInternalRequestBody, result.getRequest().getBodyAsJsonObject());
    }
    
    @Test
    void givenExternalUrlConfigurationWithEndingSlashShouldTranslateIncomingRequest() {
        JsonObject externalRequestBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("friend", JsonObject.EMPTY
                        .put("href", "https://example.com/persons/456"));

        JsonObject expectedInternalRequestBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("friend", JsonObject.EMPTY
                        .put("href", "http://internal.zuunr.com:8080/v1/somesystem/api/persons/456"));

        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.POST)
                        .url("https://example.com/v1/somesystem/api/persons")
                        .headers(JsonObject.EMPTY.put("api-base-url", JsonArray.of("https://example.com/"))) // Ending slash should not make a difference
                        .body(JsonObjectWrapper.of(externalRequestBody)));
        ApiUriInfo internalConfiguredBaseUrl = ApiUriInfo.ofUri("http://internal.zuunr.com:8080/v1/somesystem/api");
        Exchange<JsonObjectWrapper> result = requestResponseTranslator.translateRequest(exchange, internalConfiguredBaseUrl);
        
        assertEquals(expectedInternalRequestBody, result.getRequest().getBodyAsJsonObject());
    }
    
    @Test
    void givenExternalUrlConfigurationShouldTranslateIncomingRequestRegardlessOfClientUrl() {
        JsonObject externalRequestBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("friend", JsonObject.EMPTY
                        .put("href", "https://example.com/persons/456"));

        JsonObject expectedInternalRequestBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("friend", JsonObject.EMPTY
                        .put("href", "http://internal.zuunr.com:8080/v1/somesystem/api/persons/456"));

        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.POST)
                        .url("https://proxy-url.com:8443/v1/somesystem/api/persons")// The client request urls protocol, domain and port is ignored here, can be any value (but path must match)
                        .headers(JsonObject.EMPTY.put("api-base-url", JsonArray.of("https://example.com")))
                        .body(JsonObjectWrapper.of(externalRequestBody)));
        ApiUriInfo internalConfiguredBaseUrl = ApiUriInfo.ofUri("http://internal.zuunr.com:8080/v1/somesystem/api");
        Exchange<JsonObjectWrapper> result = requestResponseTranslator.translateRequest(exchange, internalConfiguredBaseUrl);
        
        assertEquals(expectedInternalRequestBody, result.getRequest().getBodyAsJsonObject());
    }
    
    @Test
    void givenExternalUrlConfigurationButSameAsInternalBaseUrlShouldNotTranslateIncomingRequest() {
        JsonObject externalRequestBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("friend", JsonObject.EMPTY
                        .put("href", "https://will-not-be-translated.com/v1/somesystem/api/persons/456"));

        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.POST)
                        .url("https://will-not-be-translated.com/v1/somesystem/api/persons") // The client request url is ignored when no translation is made, can be any value
                        .headers(JsonObject.EMPTY.put("api-base-url", JsonArray.of("https://example.com/v1/somesystem/api")))
                        .body(JsonObjectWrapper.of(externalRequestBody)));
        ApiUriInfo internalConfiguredBaseUrl = ApiUriInfo.ofUri("https://example.com/v1/somesystem/api");
        Exchange<JsonObjectWrapper> result = requestResponseTranslator.translateRequest(exchange, internalConfiguredBaseUrl);
        
        assertEquals(externalRequestBody, result.getRequest().getBodyAsJsonObject());
    }
    
    @Test
    void givenExternalUrlConfigurationButNoRequestBodyShouldReturnIncomingRequest() {
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.POST)
                        .url("https://example.com/v1/somesystem/api/persons")
                        .headers(JsonObject.EMPTY.put("api-base-url", JsonArray.of("https://example.com/v1/somesystem/api"))));
        ApiUriInfo internalConfiguredBaseUrl = ApiUriInfo.ofUri("http://internal.zuunr.com:8080/v1/somesystem/api");
        Exchange<JsonObjectWrapper> result = requestResponseTranslator.translateRequest(exchange, internalConfiguredBaseUrl);
        
        assertNull(result.getRequest().getBodyAsJsonObject());
    }
    
    @Test
    void givenExternalUrlConfigurationAndFaultyRequestShouldSilentlyIgnoreFaultyHrefInIncomingRequest() {
        JsonObject externalRequestBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("friend", JsonObject.EMPTY
                        .put("hrefff", "https://example.com/persons/456"));

        JsonObject expectedInternalRequestBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("friend", JsonObject.EMPTY
                        .put("hrefff", "https://example.com/persons/456"));

        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(Request.<JsonObjectWrapper>create(Method.POST)
                        .url("https://example.com/v1/somesystem/api/persons")
                        .headers(JsonObject.EMPTY.put("api-base-url", JsonArray.of("https://example.com")))
                        .body(JsonObjectWrapper.of(externalRequestBody)));
        ApiUriInfo internalConfiguredBaseUrl = ApiUriInfo.ofUri("http://internal.zuunr.com:8080/v1/somesystem/api");
        Exchange<JsonObjectWrapper> result = requestResponseTranslator.translateRequest(exchange, internalConfiguredBaseUrl);
        
        assertEquals(expectedInternalRequestBody, result.getRequest().getBodyAsJsonObject());
    }
    
    @Test
    void givenNoExternalUrlConfigurationShouldNotTranslateOutgoingPOSTResponse() {
        String location = "http://internal.zuunr.com:8080/v1/somesystem/api/persons/123";
        JsonObject internalResponseBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("href", location)
                .put("friend", JsonObject.EMPTY
                        .put("href", "http://internal.zuunr.com:8080/v1/somesystem/api/persons/456"));

        Response<JsonObjectWrapper> internalResponse = Response.<JsonObjectWrapper>create(StatusCode._201_CREATED)
                .headers(JsonObject.EMPTY.put("Location", location))
                .body(JsonObjectWrapper.of(internalResponseBody));
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.POST)
                .url("http://internal.zuunr.com:8080/v1/somesystem/api/persons")
                .headers(JsonObject.EMPTY);
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(request)
                .response(internalResponse);
        ApiUriInfo internalConfiguredBaseUrl = ApiUriInfo.ofUri("http://internal.zuunr.com:8080/v1/somesystem/api");
        Exchange<JsonObjectWrapper> result = requestResponseTranslator.translateResponse(exchange, internalConfiguredBaseUrl);
        
        assertEquals(internalResponseBody, result.getResponse().getBodyAsJsonObject());
        assertEquals(location, result.getResponse().getHeaders().get("Location").getString());
    }
    
    @Test
    void givenNoExternalUrlConfigurationShouldNotTranslateOutgoingPATCHResponse() {
        String location = "http://internal.zuunr.com:8080/v1/somesystem/api/persons/123";
        JsonObject internalResponseBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("href", location)
                .put("friend", JsonObject.EMPTY
                        .put("href", "http://internal.zuunr.com:8080/v1/somesystem/api/persons/456"));

        Response<JsonObjectWrapper> internalResponse = Response.<JsonObjectWrapper>create(StatusCode._200_OK)
                .body(JsonObjectWrapper.of(internalResponseBody));
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.PATCH)
                .url("http://internal.zuunr.com:8080/v1/somesystem/api/persons/123")
                .headers(JsonObject.EMPTY);
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(request)
                .response(internalResponse);
        ApiUriInfo internalConfiguredBaseUrl = ApiUriInfo.ofUri("http://internal.zuunr.com:8080/v1/somesystem/api");
        Exchange<JsonObjectWrapper> result = requestResponseTranslator.translateResponse(exchange, internalConfiguredBaseUrl);
        
        assertEquals(internalResponseBody, result.getResponse().getBodyAsJsonObject());
        assertNull(result.getResponse().getHeaders().get("Location"));
    }
    
    @Test
    void givenExternalUrlConfigurationShouldTranslateOutgoingResponse() {
        String location = "http://internal.zuunr.com:8080/v1/somesystem/api/persons/123";
        JsonObject internalResponseBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("href", location)
                .put("friend", JsonObject.EMPTY
                        .put("href", "http://internal.zuunr.com:8080/v1/somesystem/api/persons/456"));
        
        JsonObject expectedExternalResponseBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("href", "https://example.com/persons/123")
                .put("friend", JsonObject.EMPTY
                        .put("href", "https://example.com/persons/456"));

        Response<JsonObjectWrapper> internalResponse = Response.<JsonObjectWrapper>create(StatusCode._201_CREATED)
                .headers(JsonObject.EMPTY.put("Location", location))
                .body(JsonObjectWrapper.of(internalResponseBody));
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.POST)
                .url("https://example.com/v1/somesystem/api/persons") // The client might send https://example.com/persons, but the proxy must rewrite the path to match HttpEndpoint
                .headers(JsonObject.EMPTY.put("api-base-url", JsonArray.of("https://example.com")));
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(request)
                .response(internalResponse);
        ApiUriInfo internalConfiguredBaseUrl = ApiUriInfo.ofUri("http://internal.zuunr.com:8080/v1/somesystem/api");

        Exchange<JsonObjectWrapper> result = requestResponseTranslator.translateResponse(exchange, internalConfiguredBaseUrl);
        
        assertEquals(expectedExternalResponseBody, result.getResponse().getBodyAsJsonObject());
        assertEquals("https://example.com/persons/123", result.getResponse().getHeaders().get("Location").getString());
    }
    
    @Test
    void givenExternalUrlConfigurationShouldTranslateOutgoingResponseRegardlessOfClientUrl() {
        String location = "http://internal.zuunr.com:8080/v1/somesystem/api/persons/123";
        JsonObject internalResponseBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("href", location)
                .put("friend", JsonObject.EMPTY
                        .put("href", "http://internal.zuunr.com:8080/v1/somesystem/api/persons/456"));
        
        JsonObject expectedExternalResponseBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("href", "https://example.com/persons/123")
                .put("friend", JsonObject.EMPTY
                        .put("href", "https://example.com/persons/456"));

        Response<JsonObjectWrapper> internalResponse = Response.<JsonObjectWrapper>create(StatusCode._201_CREATED)
                .headers(JsonObject.EMPTY.put("Location", location))
                .body(JsonObjectWrapper.of(internalResponseBody));
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.POST)
                .url("https://proxy-url.com:8443/v1/somesystem/api/persons") // The client request urls protocol, domain and port is ignored here, can be any value (but path must match)
                .headers(JsonObject.EMPTY.put("api-base-url", JsonArray.of("https://example.com")));
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(request)
                .response(internalResponse);
        ApiUriInfo internalConfiguredBaseUrl = ApiUriInfo.ofUri("http://internal.zuunr.com:8080/v1/somesystem/api");

        Exchange<JsonObjectWrapper> result = requestResponseTranslator.translateResponse(exchange, internalConfiguredBaseUrl);
        
        assertEquals(expectedExternalResponseBody, result.getResponse().getBodyAsJsonObject());
        assertEquals("https://example.com/persons/123", result.getResponse().getHeaders().get("Location").getString());
    }
    
    @Test
    void givenExternalUrlConfigurationButSameAsInternalBaseUrlShouldNotTranslateOutgoingResponse() {
        String location = "http://internal.zuunr.com:8080/v1/somesystem/api/persons/123";
        JsonObject internalResponseBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("href", location)
                .put("friend", JsonObject.EMPTY
                        .put("href", "http://internal.zuunr.com:8080/v1/somesystem/api/persons/456"));

        Response<JsonObjectWrapper> internalResponse = Response.<JsonObjectWrapper>create(StatusCode._201_CREATED)
                .headers(JsonObject.EMPTY.put("Location", location))
                .body(JsonObjectWrapper.of(internalResponseBody));
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.POST)
                .url("https://proxy-url.com:8443/v1/somesystem/api/persons") // The client request url is ignored when no translation is made, can be any value
                .headers(JsonObject.EMPTY.put("api-base-url", JsonArray.of("https://example.com/v1/somesystem/api")));
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(request)
                .response(internalResponse);
        ApiUriInfo internalConfiguredBaseUrl = ApiUriInfo.ofUri("https://example.com/v1/somesystem/api");

        Exchange<JsonObjectWrapper> result = requestResponseTranslator.translateResponse(exchange, internalConfiguredBaseUrl);
        
        assertEquals(internalResponseBody, result.getResponse().getBodyAsJsonObject());
        assertEquals(location, result.getResponse().getHeaders().get("Location").getString());
    }
    
    @Test
    void givenExternalUrlConfigurationButNoResponseBodyShouldReturnOutgoingResponse() {
        Response<JsonObjectWrapper> internalResponse = Response.<JsonObjectWrapper>create(StatusCode._204_NO_CONTENT);
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.DELETE)
                .url("https://example.com/v1/somesystem/api/persons/123")
                .headers(JsonObject.EMPTY.put("api-base-url", JsonArray.of("https://example.com")));
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(request)
                .response(internalResponse);
        ApiUriInfo internalConfiguredBaseUrl = ApiUriInfo.ofUri("http://internal.zuunr.com:8080/v1/somesystem/api");

        Exchange<JsonObjectWrapper> result = requestResponseTranslator.translateResponse(exchange, internalConfiguredBaseUrl);
        
        assertNull(result.getResponse().getBodyAsJsonObject());
        assertNull(result.getResponse().getHeaders());
    }
    
    @Test
    void givenExternalUrlConfigurationButNoLocationHeaderShouldReturnOutgoingResponse() {
        String location = "http://internal.zuunr.com:8080/v1/somesystem/api/persons/123";
        JsonObject internalResponseBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("href", location)
                .put("friend", JsonObject.EMPTY
                        .put("href", "http://internal.zuunr.com:8080/v1/somesystem/api/persons/456"));
        
        JsonObject expectedExternalResponseBody = JsonObject.EMPTY
                .put("name", "Peter Andersson")
                .put("href", "https://example.com/persons/123")
                .put("friend", JsonObject.EMPTY
                        .put("href", "https://example.com/persons/456"));

        Response<JsonObjectWrapper> internalResponse = Response.<JsonObjectWrapper>create(StatusCode._200_OK)
                .body(JsonObjectWrapper.of(internalResponseBody));
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.PATCH)
                .url("https://example.com/v1/somesystem/api/persons/123")
                .headers(JsonObject.EMPTY.put("api-base-url", JsonArray.of("https://example.com")));
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(request)
                .response(internalResponse);
        ApiUriInfo internalConfiguredBaseUrl = ApiUriInfo.ofUri("http://internal.zuunr.com:8080/v1/somesystem/api");

        Exchange<JsonObjectWrapper> result = requestResponseTranslator.translateResponse(exchange, internalConfiguredBaseUrl);
        
        assertEquals(expectedExternalResponseBody, result.getResponse().getBodyAsJsonObject());
        assertNull(result.getResponse().getHeaders().get("Location"));
    }
    
    @Test
    void givenExternalUrlConfigurationShouldTranslateOutgoingGETCollectionResponse() {
        // This tests URLs without item id which is not a POST request (since special handling of POST to get correct Form)
        JsonObject internalResponseBody = JsonObject.EMPTY
                .put("href", "http://internal.zuunr.com:8080/v1/somesystem/api/persons?value._.name=Peter Andersson")
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "Peter Andersson")
                                .put("href", "http://internal.zuunr.com:8080/v1/somesystem/api/persons/123")
                                .put("friend", JsonObject.EMPTY
                                        .put("href", "http://internal.zuunr.com:8080/v1/somesystem/api/persons/456"))));
        
        JsonObject expectedExternalResponseBody = JsonObject.EMPTY
                .put("href", "https://example.com/persons?value._.name=Peter Andersson")
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "Peter Andersson")
                                .put("href", "https://example.com/persons/123")
                                .put("friend", JsonObject.EMPTY
                                        .put("href", "https://example.com/persons/456"))));

        Response<JsonObjectWrapper> internalResponse = Response.<JsonObjectWrapper>create(StatusCode._200_OK)
                .body(JsonObjectWrapper.of(internalResponseBody));
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.GET)
                .url("https://example.com/v1/somesystem/api/persons?value._.name=Peter Andersson")
                .headers(JsonObject.EMPTY.put("api-base-url", JsonArray.of("https://example.com")));
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(request)
                .response(internalResponse);
        ApiUriInfo internalConfiguredBaseUrl = ApiUriInfo.ofUri("http://internal.zuunr.com:8080/v1/somesystem/api");

        Exchange<JsonObjectWrapper> result = requestResponseTranslator.translateResponse(exchange, internalConfiguredBaseUrl);
        
        assertEquals(expectedExternalResponseBody, result.getResponse().getBodyAsJsonObject());
        assertNull(result.getResponse().getHeaders().get("Location"));
    }
    
    @Test
    void givenExternalUrlConfigurationShouldTranslateErrorResponse() {
        JsonObject internalErrorBody = JsonObject.EMPTY
                .put("queryParamsError", JsonValue.NULL)
                .put("requestBodyError", JsonObject.EMPTY
                        .put("paths", JsonObject.EMPTY
                                .put("friend.href", JsonObject.EMPTY
                                        .put("badValue", "https://example.com/personssss")
                                        .put("description", "")
                                        .put("violations", JsonObject.EMPTY
                                                .put("pattern", "http://internal[.]zuunr[.]com:8080/v1/somesystem/api/persons/\\w+")))))
                .put("resourceStateError", JsonValue.NULL)
                .put("status", 409)
                .put("permissionsError", JsonValue.NULL)
                .put("code", "REQUEST_BODY_ERROR");
        
        JsonObject expectedErrorBody = JsonObject.EMPTY
                .put("queryParamsError", JsonValue.NULL)
                .put("requestBodyError", JsonObject.EMPTY
                        .put("paths", JsonObject.EMPTY
                                .put("friend.href", JsonObject.EMPTY
                                        .put("badValue", "https://example.com/personssss")
                                        .put("description", "")
                                        .put("violations", JsonObject.EMPTY
                                                .put("pattern", "https://example[.]com/persons/\\w+")))))
                .put("resourceStateError", JsonValue.NULL)
                .put("status", 409)
                .put("permissionsError", JsonValue.NULL)
                .put("code", "REQUEST_BODY_ERROR");

        Response<JsonObjectWrapper> internalResponse = Response.<JsonObjectWrapper>create(StatusCode._409_CONFLICT)
                .body(JsonObjectWrapper.of(internalErrorBody));
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.POST)
                .url("https://example.com/v1/somesystem/api/persons")
                .headers(JsonObject.EMPTY.put("api-base-url", JsonArray.of("https://example.com")));
        Exchange<JsonObjectWrapper> exchange = Exchange.<JsonObjectWrapper>empty()
                .request(request)
                .response(internalResponse);
        ApiUriInfo internalConfiguredBaseUrl = ApiUriInfo.ofUri("http://internal.zuunr.com:8080/v1/somesystem/api");

        Exchange<JsonObjectWrapper> result = requestResponseTranslator.translateResponse(exchange, internalConfiguredBaseUrl);
        
        assertEquals(expectedErrorBody, result.getResponse().getBodyAsJsonObject());
    }
}
