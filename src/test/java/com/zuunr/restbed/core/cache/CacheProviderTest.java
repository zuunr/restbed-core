/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zuunr.restbed.core.cache;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.Test;

class CacheProviderTest {

    @Test
    void givenRegisteredItemInCacheShouldReturnIt() {
        CacheProvider cacheProvider = new CacheProvider();

        cacheProvider.createCache("myApi");
        
        cacheProvider.getCache("myApi", CacheProvider.EVENT_PROCESSOR_CACHE).put("someKey", List.of("myEventProcessor"));
        @SuppressWarnings("unchecked")
        List<String> myEventProcessors = cacheProvider.getCache("myApi", CacheProvider.EVENT_PROCESSOR_CACHE).get("someKey");
        
        assertEquals("myEventProcessor", myEventProcessors.get(0));
    }
    
    @Test
    void givenNoItemRegisteredInCacheShouldThrowException() {
        CacheProvider cacheProvider = new CacheProvider();
        
        assertThrows(NullPointerException.class, () -> cacheProvider.getCache("myApi", CacheProvider.EVENT_PROCESSOR_CACHE));
    }
}
