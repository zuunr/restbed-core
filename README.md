# restbed-core

![Apache License](https://img.shields.io/static/v1.svg?label=License&message=Apache-2.0&color=%230af)
[![Maven Central](https://img.shields.io/maven-central/v/com.zuunr.restbed/restbed-core.svg?label=Maven%20Central&color=%230af)](https://search.maven.org/search?q=a:restbed-core)
[![Snyk](https://snyk-widget.herokuapp.com/badge/mvn/com.zuunr.restbed/restbed-core/badge.svg)](https://snyk.io/vuln/maven:com.zuunr.restbed%3Arestbed-core)

This is the core project for Restbed modules. It includes common core functionality used by Restbed tools and services.

## Supported tags

* [`1.0.0.M54-bd5dd10`, (*bd5dd10/pom.xml*)](https://bitbucket.org/zuunr/restbed-core/src/bd5dd10/pom.xml)

## Usage

To use this module, make sure this project's maven artifact is added as a dependency in your module, replacing 1.0.0-abcdefg with a supported tag:

```xml
<dependency>
    <groupId>com.zuunr.restbed</groupId>
    <artifactId>restbed-core</artifactId>
    <version>1.0.0-abcdefg</version>
</dependency>
```

## Configuration

The module requires additional configuration, see src/test/resources/application.yml for a sample.

Remember to use environment variables for security properties instead of the application.yml file in a non-test environment.
